package com.ss8.grpc.skc;

import com.ss8.grpc.skc.config.ServiceTestConfig;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;


@SpringBootTest
@SpringJUnitConfig(classes = { ServiceTestConfig.class })
@DirtiesContext
class GrpcApiApplicationTests{

	@Test
	void contextLoads() {
	}

}
