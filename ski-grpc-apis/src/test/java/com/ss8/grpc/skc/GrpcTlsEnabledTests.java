package com.ss8.grpc.skc;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ss8.grpc.skc.K8SClusterOperatorGrpc.K8SClusterOperatorBlockingStub;
import com.ss8.grpc.skc.config.ServiceTestConfig;

import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import net.devh.boot.grpc.client.inject.GrpcClient;

@SpringBootTest(properties = {
	// "grpc.server.inProcessName=test", // Enable inProcess server
	// "grpc.server.port=-1", // Disable external server
	// "grpc.client.inProcess.address=in-process:test",
	"SKI.INSTALLER_HOME=${user.dir}",
    "SKI.inventory.path=src/test/resources",
	"grpc.server.security.enabled=true",
	"grpc.server.security.certificateChain=file:src/test/resources/certificates/server-cert.pem",
	"grpc.server.security.privateKey=file:src/test/resources/certificates/server-key.pem",
	"grpc.server.security.trustCertCollection=file:src/test/resources/certificates/trusted-clients.crt.collection",
	"grpc.server.security.clientAuth=REQUIRE",
	"grpc.client.test.address=localhost:9091",
	"grpc.client.test.security.authorityOverride=localhost",
	"grpc.client.test.security.trustCertCollection=file:src/test/resources/certificates/trusted-servers.crt.collection",
	"grpc.client.test.security.clientAuthEnabled=true",
	"grpc.client.test.security.certificateChain=file:src/test/resources/certificates/client-cert.pem",
	"grpc.client.test.security.privateKey=file:src/test/resources/certificates/client-key.pem"
	})
@SpringJUnitConfig(classes = { ServiceTestConfig.class })
@DirtiesContext
class GrpcTlsEnabledTests {

	@GrpcClient("test")
    private K8SClusterOperatorBlockingStub myService;

    private static final Logger log = LoggerFactory.getLogger(GrpcTlsEnabledTests.class);

	@Value("${SKI.INSTALLER_HOME}")
	private String SKI_INSTALLER_HOME;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		log.info("tearDownAfterClass");
	}

    @Test
	@DirtiesContext
	public void testConnection(){
        // Check if server-client is built properly
        assertNotNull(this.myService, "grpcClient");
    }

    @Test
	@DirtiesContext
	public void testRunShellCommand(){
		// ClusterDetail request = ClusterDetail.newBuilder().se;
        log.info("Starting runShellCommand Test");
		String cmd = "ls";
		try {
			MyCommand mycmd = MyCommand.newBuilder().setCmd(cmd).build();
			CommandOutput out = myService.runShellCommand(mycmd);
			assertNotNull(out);
			Assert.assertFalse(out.toString().contains("Cannot run"));
			Assert.assertFalse(out.toString().contains("not found"));
		  }
		  catch (Exception e) {
			Assert.fail("Exception " + e);
		  }
	}

	@Test
	@DirtiesContext
	public void testRunShellCommandFails(){
		// ClusterDetail request = ClusterDetail.newBuilder().se;
        log.info("Starting runShellCommandFails Test");
		String cmd = "xyz";
		try {
			MyCommand mycmd = MyCommand.newBuilder().setCmd(cmd).build();
			CommandOutput out = myService.runShellCommand(mycmd);
			assertNotNull(out);
			Assert.assertTrue(out.toString().contains("Cannot run") || out.toString().contains("not found"));
		  }
		  catch (Exception e) {
			Assert.fail("Exception " + e);
		  }
	}

    @Test
    @DirtiesContext
    public void testBuildInventory() {
		log.info("Inventory build is started...");
		// node label with corresponding ip address. We can build this map from csv file in future
		Map<String, String> mAllNodes = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
			
				put("cnode101", "10.0.161.241");	//controller
				/*put("cNode102", "10.0.1.102");	// controller
				put("cwNde103", "10.0.1.103");	// controller and worker
				put("wNode104", "10.0.1.104");	// worker
				put("wNode105", "10.0.1.105");	// worker
				put("wNode106", "10.0.1.106");	// worker
				put("rNode119", "10.0.1.119");	// registry
			*/
	}
		};

		// all nodes start
		List<NodeDetail> allNodeList = new ArrayList<NodeDetail>();
		mAllNodes.forEach((key, val) -> {
			allNodeList.add(NodeDetail.newBuilder().setNodeLabel(key).setNodeIP(val).build());
		});

		Map<String, String> clusterVars = new HashMap<String, String>() {
			/**
			 * Cluster level variables
			 */
			private static final long serialVersionUID = 1L;
			{
				put("cluster_name", "grpc-cluster");
				put("ansible_python_interpreter", "/usr/bin/python3");
				put("ansible_user", "support");
				put("dual_stack_networks", "false");
				put("create_docker_registry", "false");
				put("tmp_dir", "/SS8/tmp");
			}
		};
		// all nodes end

		GroupDetail controllerNodes = GroupDetail.newBuilder().setGroupName(InventoryConstants.KubeControllerNodes)
				.addAllNodeLabel(Arrays.asList("cnode101"))//, "cNode102", "cwNode103"))
				.build();

		GroupDetail etcdNodes = GroupDetail.newBuilder().setGroupName(InventoryConstants.EtcdNodes)
				.addAllNodeLabel(Arrays.asList("cnode101"))//, "cNode102", "cwNode103"))
				.build();

		// worker nodes start
		List <GroupDetail> workerSubGroups = Arrays.asList(
				// DF2 subgroup
				GroupDetail.newBuilder().setGroupName("w1-DF2")
				.addAllNodeLabel(Arrays.asList("cwNode103","wNode104"))
				.putGroupVars("node_labels", "{\"xcipio\":\"DF2\", \"kubernetes.io/role\":\"worker\" }")
				.build(), 
				// ADMF subgroup
				GroupDetail.newBuilder().setGroupName("w2-ADMF")
				.addAllNodeLabel(Arrays.asList("wNode105", "wNode106"))
				.putGroupVars("node_labels", "{\"xcipio\":\"ADMF\", \"kubernetes.io/role\":\"worker\" }")
				.build()
		);

		GroupDetail workerNodes = GroupDetail.newBuilder().setGroupName(InventoryConstants.KubeWorkerNodes)
				//.addChildren(mAllNodes.get("cnode101"))
				.addAllNodeLabel(Arrays.asList("cnode101"))
//				.putGroupVars("node_labels", "{\"xcipio\":\"ADMF\", \"kubernetes.io/role\":\"worker\" }")
				//.addAllSubGroup(workerSubGroups)
				.build();
		// worker nodes end

		// registry nodes start
		Map<String, String> registryVars = new HashMap<String, String>() {
			/**
			 * Registry variables
			 */
			private static final long serialVersionUID = 1L;
			{
				put("registry_vip_enable", "false");
				put("registry_volume_path", "/mnt");
				put("registry_name", "ss8.registry.com");
				put("registry_port", "5000");
				put("registry_interface", "");
				put("registry_vip", "0.0.0.0");
				put("registry_vrr_id","0");
			}
		};

		GroupDetail registryNodes = GroupDetail.newBuilder().setGroupName(InventoryConstants.Registry)
				.addNodeLabel("node119")
				.putAllGroupVars(registryVars)
				.build();
		// registry nodes end

		List<GroupDetail> clusterGroups = 
			(Boolean.parseBoolean(clusterVars.get("create_docker_registry")))
				? Arrays.asList (controllerNodes, etcdNodes, workerNodes, registryNodes)
				: Arrays.asList (controllerNodes, etcdNodes, workerNodes)
			;

		ClusterDetail clusterDetail = ClusterDetail.newBuilder()
				.addAllClusterNode(allNodeList)
				.putAllClusterVars(clusterVars)
				.addAllClusterGroup(clusterGroups)
				.build();


        ExitStatus status =  this.myService.buildInventory(clusterDetail);
		String generatedFilePath = status.getOutFileName();
		String path= SKI_INSTALLER_HOME;
		log.info(status.toString());
		try {
			String generatedFile = new String(Files.readAllBytes(Paths.get(generatedFilePath)), StandardCharsets.UTF_8);
			String sampleFile = new String(Files.readAllBytes(Paths.get(SKI_INSTALLER_HOME+"/src/test/resources/inventory-sample.ini")), StandardCharsets.UTF_8);

			assertEquals(generatedFile, sampleFile);
		  }
		  catch (Exception e) {
			Assert.fail("Exception " + e);
		  }
		  finally{
			if(generatedFilePath!=null){
				File file = new File(generatedFilePath);
				file.delete();
				log.info("File cleaned after test: "+generatedFilePath);
			}
		  }
		
		log.info("Inventory Build Test End !");		
    }

}
