package com.ss8.grpc.skc;

public final class InventoryConstants {
 
	public static final String AllNodes 			= "all";
	public static final String KubeControllerNodes = "kube_controller_nodes";
	public static final String KubeWorkerNodes 	= "kube_worker_nodes";
	public static final String EtcdNodes 			= "etcd";
	public static final String Registry 			= "registry";
	public static final String NOPROMPT				= "noprompt";
	
    private InventoryConstants() {
        
    }
}