// /*
//  * Copyright 2022 The SS8 Authors
//  */
// // https://medium.com/well-red/intro-to-grpc-and-protocol-buffers-c21054ef579c

// package com.ss8.grpc.skc.services.client;

// import com.ss8.grpc.skc.Creds;
// import com.ss8.grpc.skc.ExitStatus;
// import com.ss8.grpc.skc.GroupDetail;
// import com.ss8.grpc.skc.K8SClusterOperatorGrpc;
// import com.ss8.grpc.skc.MyCommand;
// import com.ss8.grpc.skc.CommandOutput;
// import com.ss8.grpc.skc.ClusterDetail;
// import com.ss8.grpc.skc.NodeDetail;
// import com.ss8.grpc.skc.Operation;
// import com.ss8.grpc.skc.OperationDetail;
// import static com.ss8.grpc.skc.InventoryConstants.*;

// import io.grpc.Channel;
// import io.grpc.ManagedChannel;
// import io.grpc.ManagedChannelBuilder;
// import io.grpc.StatusRuntimeException;
// import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
// import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
// import io.grpc.netty.shaded.io.netty.handler.ssl.SslContext;
// import io.grpc.netty.shaded.io.netty.handler.ssl.util.InsecureTrustManagerFactory;
// import javax.net.ssl.SSLException;
// import java.util.List;
// import java.util.ArrayList;
// import java.util.Arrays;
// import java.util.HashMap;
// import java.util.Map;
// import java.util.concurrent.TimeUnit;
// import java.util.logging.Level;
// import java.util.logging.Logger;

// //import org.springframework.beans.factory.annotation.Autowired;
// //import org.springframework.core.env.Environment;

// /**
//  * A simple client that requests K8S cluster operations from the {@link K8SClusterOperatorServer}.
//  */
// public class K8SClusterOperatorClient {
// 	private static final Logger logger = Logger.getLogger(K8SClusterOperatorClient.class.getName());

// 	private final K8SClusterOperatorGrpc.K8SClusterOperatorBlockingStub blockingStub;

// //	@Autowired
// //    private Environment env;

// 	/**
// 	 * Construct client for accessing K8SClusterOperatorServer server using the existing channel.
// 	 */
// 	public K8SClusterOperatorClient(Channel channel) {
// 		// 'channel' here is a Channel, not a ManagedChannel, so it is not this code's
// 		// responsibility to shut it down.

// 		// Passing Channels to code makes code easier to test and makes it easier to
// 		// reuse Channels.
// 		blockingStub = K8SClusterOperatorGrpc.newBlockingStub(channel);
// 	}

// 	/** Send Inventory information to server and receive status */
// 	public String buildInventory() {
// 		logger.info("Inventory build is started...");
// 		// node label with corresponding ip address. We can build this map from csv file in future
// 		Map<String, String> mAllNodes = new HashMap<String, String>() {
// 			private static final long serialVersionUID = 1L;
// 			{
			
// 				put("cnode101", "10.0.161.241");	//controller
// 				/*put("cNode102", "10.0.1.102");	// controller
// 				put("cwNde103", "10.0.1.103");	// controller and worker
// 				put("wNode104", "10.0.1.104");	// worker
// 				put("wNode105", "10.0.1.105");	// worker
// 				put("wNode106", "10.0.1.106");	// worker
// 				put("rNode119", "10.0.1.119");	// registry
// 			*/
// 	}
// 		};

// 		// all nodes start
// 		List<NodeDetail> allNodeList = new ArrayList<NodeDetail>();
// 		mAllNodes.forEach((key, val) -> {
// 			allNodeList.add(NodeDetail.newBuilder().setNodeLabel(key).setNodeIP(val).build());
// 		});

// 		Map<String, String> clusterVars = new HashMap<String, String>() {
// 			/**
// 			 * Cluster level variables
// 			 */
// 			private static final long serialVersionUID = 1L;
// 			{
// 				put("cluster_name", "grpc-cluster");
// 				put("ansible_python_interpreter", "/usr/bin/python3");
// 				put("ansible_user", "support");
// 				put("dual_stack_networks", "false");
// 				put("create_docker_registry", "false");
// 				put("tmp_dir", "/SS8/tmp");
// 			}
// 		};
// 		// all nodes end

// 		GroupDetail controllerNodes = GroupDetail.newBuilder().setGroupName(KubeControllerNodes)
// 				.addAllNodeLabel(Arrays.asList("cnode101"))//, "cNode102", "cwNode103"))
// 				.build();

// 		GroupDetail etcdNodes = GroupDetail.newBuilder().setGroupName(EtcdNodes)
// 				.addAllNodeLabel(Arrays.asList("cnode101"))//, "cNode102", "cwNode103"))
// 				.build();

// 		// worker nodes start
// 		List <GroupDetail> workerSubGroups = Arrays.asList(
// 				// DF2 subgroup
// 				GroupDetail.newBuilder().setGroupName("w1-DF2")
// 				.addAllNodeLabel(Arrays.asList("cwNode103","wNode104"))
// 				.putGroupVars("node_labels", "{\"xcipio\":\"DF2\", \"kubernetes.io/role\":\"worker\" }")
// 				.build(), 
// 				// ADMF subgroup
// 				GroupDetail.newBuilder().setGroupName("w2-ADMF")
// 				.addAllNodeLabel(Arrays.asList("wNode105", "wNode106"))
// 				.putGroupVars("node_labels", "{\"xcipio\":\"ADMF\", \"kubernetes.io/role\":\"worker\" }")
// 				.build()
// 		);

// 		GroupDetail workerNodes = GroupDetail.newBuilder().setGroupName(KubeWorkerNodes)
// 				//.addChildren(mAllNodes.get("cnode101"))
// 				.addAllNodeLabel(Arrays.asList("cnode101"))
// //				.putGroupVars("node_labels", "{\"xcipio\":\"ADMF\", \"kubernetes.io/role\":\"worker\" }")
// 				//.addAllSubGroup(workerSubGroups)
// 				.build();
// 		// worker nodes end

// 		// registry nodes start
// 		Map<String, String> registryVars = new HashMap<String, String>() {
// 			/**
// 			 * Registry variables
// 			 */
// 			private static final long serialVersionUID = 1L;
// 			{
// 				put("registry_vip_enable", "false");
// 				put("registry_volume_path", "/mnt");
// 				put("registry_name", "ss8.registry.com");
// 				put("registry_port", "5000");
// 				put("registry_interface", "");
// 				put("registry_vip", "0.0.0.0");
// 				put("registry_vrr_id","0");
// 			}
// 		};

// 		GroupDetail registryNodes = GroupDetail.newBuilder().setGroupName(Registry)
// 				.addNodeLabel("node119")
// 				.putAllGroupVars(registryVars)
// 				.build();
// 		// registry nodes end

// 		List<GroupDetail> clusterGroups = 
// 			(Boolean.parseBoolean(clusterVars.get("create_docker_registry")))
// 				? Arrays.asList (controllerNodes, etcdNodes, workerNodes, registryNodes)
// 				: Arrays.asList (controllerNodes, etcdNodes, workerNodes)
// 			;

// 		ClusterDetail invetroyRequest = ClusterDetail.newBuilder()
// 				.addAllClusterNode(allNodeList)
// 				.putAllClusterVars(clusterVars)
// 				.addAllClusterGroup(clusterGroups)
// 				.build();

// 		ExitStatus exitStatus = null;

// 		try {
// 			logger.info("before call to buildInventory:");
// 			exitStatus = blockingStub.buildInventory(invetroyRequest);
// 			logger.info("after call to buildInventory: ");// + exitStatus.getMessage());

// 		} catch (StatusRuntimeException e) {
// 			logger.log(Level.WARNING, "RPC failed: {0}", e.getStatus());
// 			return "RPC failed: {0}" +  e.getStatus();
// 		}

// 		logger.info("return status of buildInventory: " +
// 				exitStatus.getStatus() + " outfile:[" + exitStatus.getOutFileName() + "]");
// 		return exitStatus.getOutFileName();
// 	}

// 	public void runClusterOperation (String inventoryfile) {
// 		Operation op = Operation.create;
// 		Creds creds = Creds.newBuilder().setUsername("support").setPassword("ss8inc").build();

// 		OperationDetail installerArgs = OperationDetail.newBuilder()
// 				.setCreds(creds)
// 				.setInventoryPath(inventoryfile)
// 				.setOperation(op)
// 			//	.setArgs("node01,node02") // set only for removenode, empty otherwise
// 				.build();
// 		logger.info("b4 runClusterOperation:" + installerArgs);
// 		 ExitStatus exitStatus = blockingStub.runClusterOperation(installerArgs);
// 		logger.info("return status runClusterOperation ");// + exitStatus.getMessage());

// 	}
// public String runShellCommand(final String cmd) {
// 		MyCommand mycmd = MyCommand.newBuilder().setCmd(cmd).build();
// 		CommandOutput out = blockingStub.runShellCommand(mycmd);
// //System.out.println("cmd=" + cmd + " otput=" + out.getOutput());
// 		return out.getOutput();
// 	}
// 	/**
// 	 * If provided, the first element of {@code args} is the target server:port.
// 	 */
// 	public static void main(String[] args) throws Exception {
// 		String inventoryfile = "inventory.ini"; //env.getProperty("inventoryFile.path"))
// 		// Access a service running on the local machine on port 50051
// 		String h101 = "10.0.157.100", hlc = "localhost";
// //		String target = hlc+":9091";
// 		String target = h101+":9095";
// String cmd = "ls";
// // cmd = "ansible-inventory --list -i inventory/sample/245.ini"; 
// 		// Allow passing in the user and target strings as command line arguments
// 		System.out.println(" args le=" + args.length);
// 		if (args.length > 0) {
// 			if ("--help".equals(args[0])) {
// 				System.err.println("Usage: [ Server:port]");
// 				System.err.println("");
// 				System.err.println("  target  The server to connect to. Defaults to " + target);
// 				System.err.println("  inventoryfile path be picked from pre-generated inventory. Defaults to: " + inventoryfile);
// 				System.exit(1);
// 			}
// 			//target = args[0];
// 			cmd = args[0];
// 			System.out.println("commad is " + cmd);
// 		}


// 		if (args.length > 1) { cmd = args[0]; }

// 		// Create a communication channel to the server, known as a Channel.
// 		// Channels are thread-safe and reusable.
// 		// It is common to create channels at the beginning of your
// 		// application and reuse
// 		// them until the application shuts down.

// 		ManagedChannel mChannel=null;

// 		try {
//             	mChannel = NettyChannelBuilder.forTarget(target)
//                 .sslContext(GrpcSslContexts.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build())
//                 .build();
// 				if(mChannel==null)
// 					throw new IllegalArgumentException("Channel is null");
//         } catch (SSLException e) {
//             // TODO Auto-generated catch block
//             e.printStackTrace();
//         }
		

// 		// mChannel = ManagedChannelBuilder.forTarget(target)
// 		// .usePlaintext()
// 		// .build();
// 				// Channels are secure by default (via SSL/TLS).
// 				// For the example we disable TLS
// 				// to avoid needing certificates.
// 		try {
// 			K8SClusterOperatorClient client = new K8SClusterOperatorClient(mChannel);
// 			// inventoryfile = client.buildInventory();
// 			// client.runClusterOperation(inventoryfile);
// 		String josnInv = client.runShellCommand(cmd);
// 		System.out.println("OUTPUT of " + cmd + ":" +  josnInv + " " + inventoryfile) ;
		
// 		} finally {
// 			// ManagedChannels use resources like threads and TCP connections. To prevent
// 			// leaking these
// 			// resources the channel should be shut down when it will no longer be used. If
// 			// it may be used
// 			// again leave it running.
// 			mChannel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
// 		}
// 	}
// }
