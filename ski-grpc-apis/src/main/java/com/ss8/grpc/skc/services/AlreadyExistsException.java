/**
 * 
 */
package com.ss8.grpc.skc.services;

/**
 * @author khemraj.dhondge
 *
 */
public class AlreadyExistsException extends RuntimeException {

	private static final long serialVersionUID = 1708819959948133029L;

	public AlreadyExistsException(String msg) 
	{
		super(msg);
	}
}
