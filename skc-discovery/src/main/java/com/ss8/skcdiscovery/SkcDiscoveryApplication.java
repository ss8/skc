package com.ss8.skcdiscovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkcDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkcDiscoveryApplication.class, args);
	}

}
