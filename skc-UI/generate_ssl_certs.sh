#!/bin/bash

WORKING_DIR=$(cd $(dirname $0); pwd)

CERTDIR="${WORKING_DIR}/certs"
SERVER_KEY="${CERTDIR}/nginx-key.pem"
SERVER_CSR="${CERTDIR}/nginx-csr.csr"
SERVER_CRT="${CERTDIR}/nginx-cert.pem"
EXTFILE="${WORKING_DIR}/openssl.conf"
OPENSSL_CMD="/usr/bin/openssl"
SED_CMD="/usr/bin/sed"
COMMON_NAME="$1"

function show_usage {
    printf "Usage: $0 [options [parameters]]\n"
    printf "\n"
    printf "Options:\n"
    printf " -cn, Provide Common Name for the certificate\n"
    printf " -h|--help, print help section\n"

    return 0
}

case $1 in
     -cn)
         shift
         COMMON_NAME="$1"
         ;;
     --help|-h)
         show_usage
         exit 0
         ;;
     *)
        ## Use hostname as Common Name
        COMMON_NAME=`/usr/bin/hostname`
        ;;
esac

# generating server key
echo "Generating private key"
mkdir -p ${CERTDIR}
$OPENSSL_CMD genrsa -out $SERVER_KEY  4096 2>/dev/null
if [ $? -ne 0 ] ; then
   echo "ERROR: Failed to generate ${SERVER_KEY}"
   exit 1
fi

## Update Common Name in External File
/bin/echo "commonName				= ${COMMON_NAME}" >> $EXTFILE

# Generating Certificate Signing Request using config file
echo "Generating Certificate Signing Request"
$OPENSSL_CMD req -new -key $SERVER_KEY -out $SERVER_CSR -config $EXTFILE 
if [ $? -ne 0 ] ; then
   echo "ERROR: Failed to generate $SERVER_CSR"
   exit 1
fi


echo "Generating self signed certificate"
$OPENSSL_CMD x509 -req -days 3650 -in $SERVER_CSR -signkey $SERVER_KEY -out $SERVER_CRT 2>/dev/null
if [ $? -ne 0 ] ; then
   echo "ERROR: Failed to generate self-signed certificate file $SERVER_CRT"
fi

$SED_CMD -i "/\bcommonName\b/d" $EXTFILE

# /bin/echo "commonName				= $COMMON_NAME" >> $EXTFILE

# $OPENSSL_CMD req -x509 -nodes -config $EXTFILE -newkey rsa:4096 -keyout $SERVER_KEY -out $SERVER_CRT -days 365
