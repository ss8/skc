import Axios from 'axios';

import AuthToken from './Authorization/AuthToken';

export default Axios.create({
    baseURL: `${process.env.REACT_APP_CLUSTER_MS_URL}`,
    // headers: {
    //     Authorization: `${AuthToken.getToken()}`,
    // },
    proxy: {
        protocol: 'https',
        // host: 'https://localhost:8443',
    },
});