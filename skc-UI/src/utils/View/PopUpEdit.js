import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";
import { Radio, RadioGroup } from "@blueprintjs/core";

import '../../css/AuthenticationPopUp.css';


export default function PopUpEdit(props) {

//   const delay = ms => new Promise(res => setTimeout(res, ms));

  const [modifyNodesAction, setModifyNodesAction] = useState('');

  const handleRadioChange = e => setModifyNodesAction( e.target.value );

  let navigate = useNavigate();

  async function onConfirmation(e) {
    let path = `/edit/${props.clusterToBeDeleted.ClusterName}`; 
    navigate(path, {state: {
      modifyNodesAction: modifyNodesAction,
      clusterToBeDeleted : props.clusterToBeDeleted
    }});
  }

  async function onCancellation(e) {
    props.setPopupEdit(false);
  }

  return (
  <div>
    <Alert
        confirmButtonText="Proceed"
        cancelButtonText="Cancel"
        isOpen={props.popupEdit}
        onCancel={(e)=> onCancellation(e)}
        onConfirm={(e)=> onConfirmation(e)}
        intent={Intent.PRIMARY}
        loading={false}
    >
        <RadioGroup
            label="Please choose one of the following options to proceed with editing of the cluster. You are allowed to perform only one action at a time."
            name="group"
            onChange={handleRadioChange}
            selectedValue={modifyNodesAction}
        >
            <Radio {...modifyNodesAction} label="Add Nodes to the existing cluster" value="addNodes" />
            <Radio {...modifyNodesAction} label="Remove Nodes from the existing cluster" value="removeNodes" />
        </RadioGroup>

    </Alert>
  </div>
  );
}


