import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import AxiosCluster from '../CustomAxios';
import AxiosDashboard from '../CustomDashboardAxios';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import LoadingIcon from '../Create/LoadingIcon';
import { actionCreators } from '../../state/index';


export default function LastConfirmationPopupForRetryRemove(props) {

    let navigate = useNavigate();
    const dispatch = useDispatch();
    const { openSuccessAlert, openErrorAlert, alertMessage } = bindActionCreators(actionCreators, dispatch);

    const sshCredentials = useSelector(state => state.sshAuthenticationCredentials);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const [loading, setLoading] = React.useState(false);
    const [loadingIconState, setLoadingIconState] = React.useState('none');
    const [icon, setIcon] = useState("automatic-updates");

    const [lastConfirmationMessage, setLastConfirmationMessage] = useState(`Are you sure you want to begin the process? Please note that once process has initiated it can not be rolled back.`);

    const [events, setEvents] = useState("");

    const getEvents = () => {
        console.log("inside events ");
        let eventSource = new EventSource(`${process.env.REACT_APP_DASHBOARD_MS_URL}/dashboard/events`);
        console.log(eventSource);
        eventSource.onopen = (event) => console.log("Test events opened");
        eventSource.onmessage = (event) => setEvents(event.data)
        eventSource.onerror = (event) => eventSource.close();
    }

    const firstControllerIp = (props) => {
        let rows = props.node.allLeafChildren;
        for (let i = 0; i < rows.length; i++) {
            if (rows[i].data.Controller)
                return rows[i].data.NodeIP;
        }
        return;
    }


    async function submit(e) {
        e.preventDefault();

        getEvents();

        if (props.action === 'retryCluster') {
            AxiosCluster.post(`/cluster/${parseInt(props.prop.node.allLeafChildren[0].data.ClusterId)}/retry`, {
                "clusterId": props.prop.node.allLeafChildren[0].data.ClusterId,
                "clusterName": props.prop.node.allLeafChildren[0].data.ClusterName
            })
                .then(async res => {
                    var clusterInfoForDashboard;
                    if (res.status === 200) {
                        clusterInfoForDashboard = {
                            "clusterIp": firstControllerIp(props.prop), // Fetching the ip of 1st controller node
                            "clusterName": props.prop.node.allLeafChildren[0].data.ClusterName,
                            "sshUsername": sshCredentials.sshUsername,
                            "sshPassword": sshCredentials.sshPassword
                        }
                        props.setLastPopupForRetryRemove(false);
                        alertMessage("Cluster created and Dashboard installation triggered. Check status on the list for results.")
                        openSuccessAlert(true);
                        await delay(3000);
                        openSuccessAlert(false);
                        return AxiosDashboard.post(`/dashboard/install`, clusterInfoForDashboard)
                    }
                })
                .then(async res => {
                    alertMessage("Dashboard installed. Check status on the list for results.")
                    openSuccessAlert(true);
                    await delay(3000);
                    openSuccessAlert(false);
                })
                .catch(async error => {
                    props.setLastPopupForRetryRemove(false);
                    alertMessage("Error occured. Please try again later")
                    openErrorAlert(true);
                    await delay(3000);
                    openErrorAlert(false);
                })
        }


        else if (props.action === 'retryDashboard') {
            var clusterInfoForDashboard = {
                "clusterIp": firstControllerIp(props.prop), // Fetching the ip of 1st controller node
                "clusterName": props.prop.node.allLeafChildren[0].data.ClusterName,
                "sshUsername": sshCredentials.sshUsername,
                "sshPassword": sshCredentials.sshPassword
            }
            AxiosDashboard.post(`/dashboard/install`, clusterInfoForDashboard)
                .then(async res => {
                    if (res.status == 200) {
                        props.setLastPopupForRetryRemove(false);
                        alertMessage("Dashboard installed. Check status on the list for results.")
                        openSuccessAlert(true);
                        await delay(3000);
                        openSuccessAlert(false);
                    }
                })
                .catch(async error => {
                    props.setLastPopupForRetryRemove(false);
                    alertMessage(error.response.data.error);
                    openErrorAlert(true);
                    await delay(3000);
                    openErrorAlert(false);
                })
        }


        else if (props.action === 'remove') {
            AxiosCluster.delete(`/cluster/${parseInt(props.prop.node.allLeafChildren[0].data.ClusterId)}/delete`)
                .then(async res => {
                    if (res.status == 200) {
                        alertMessage("Cluster successfully deleted");
                        props.setLastPopupForRetryRemove(false);
                        openSuccessAlert(true);
                        await delay(3000);
                        openSuccessAlert(false);
                    }
                })
                .catch(async error => {
                    props.setLastPopupForRetryRemove(false);
                    alertMessage("We are facing some issue at the moment. Please try again later");
                    openErrorAlert(true);
                    await delay(3000);
                    openErrorAlert(false);
                })
        }

    }

    async function onConfirmation(e) {
        submit(e);
        if (props.action === 'retryCluster') {
            setIcon("build");
            setLastConfirmationMessage("Retrying your cluster creation... It might take some time. Hold tight!")
        }
        else if (props.action === 'retryDashboard') {
            setIcon("download");
            setLastConfirmationMessage("Retrying your dashboard installation... It might take some time. Hold tight!")
        }
        else {
            setIcon("trash");
            setLastConfirmationMessage("Removing your cluster... It might take some time. Hold tight!")
        }
        setLoadingIconState('block');
        setLoading(true);
    }

    async function onCancellation(e) {
        props.setLastPopupForRetryRemove(false);
    }

    return (
        <div>
            <Alert
                confirmButtonText={props.action === 'retryCluster' || props.action === 'retryDashboard' ? "Retry" : "Remove"}
                cancelButtonText="Cancel"
                isOpen={props.lastPopupForRetryRemove}
                onCancel={(e) => onCancellation(e)}
                onConfirm={(e) => onConfirmation(e)}
                intent={props.action === 'retryCluster' || props.action === 'retryDashboard' ? Intent.PRIMARY : Intent.DANGER}
                icon={icon}
                loading={loading}
            >
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <p>{lastConfirmationMessage}</p>
                    <div style={{ display: loadingIconState }}>
                        <LoadingIcon />
                    </div>
                </div>
            </Alert>
        </div>
    );
}


