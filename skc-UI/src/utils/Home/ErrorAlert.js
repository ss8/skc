import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';

import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';

import { actionCreators } from '../../state';


export default function ErrorAlert(props) {

    const dispatch = useDispatch();
    const {openSuccessAlert, openErrorAlert, alertMessage} = bindActionCreators(actionCreators, dispatch); 

    const errorAlertState = useSelector(state=> state.errorAlertState);
    const alertMessageContent = useSelector(state=> state.alertMessageContent);

    return (
        // <div style={{position: 'absolute', zIndex: 1, marginTop: -20, marginLeft: '23%', marginRight: '23%'}}> 
        <div style={{position: 'absolute', zIndex: 1, marginTop: -20, display: 'flex', justifyContent: 'center', width: '100vw'}}> 
            <Collapse in={errorAlertState}>
                <Alert
                    variant="filled" severity="error"
                    action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                                openErrorAlert(false);
                            }}
                        >
                            <CloseIcon fontSize="inherit" />
                        </IconButton>
                    }
                    style={{fontFamily: 'Titillium Web'}}
                >
                    {alertMessageContent}
                </Alert>
            </Collapse>
        </div>
    );
}
