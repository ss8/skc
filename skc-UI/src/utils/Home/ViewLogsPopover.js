import * as React from 'react';
import { useSelector } from 'react-redux';

import Popover from '@mui/material/Popover';

import '../../css/ViewLogsPopover.css'


export default function ViewLogsPopover(props) {

  const open = Boolean(props.anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const viewLogsContent = useSelector(state=> state.viewLogsContent);

  return (
      <Popover
        id={id}
        open={open}
        anchorEl={props.anchorEl}
        onClose={props.handleClose}
        anchorOrigin={{
          vertical: 'right',
          horizontal: 'right',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'lest',
          }}
      >
        <div style={{maxHeight: 320}}>
      <div className="viewLogsPopoverContent">
        <p>
          {viewLogsContent}
        </p>
      </div>
      </div>
      </Popover>
  );
}