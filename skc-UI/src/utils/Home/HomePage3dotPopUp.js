import { useState, useRef } from 'react';
import React from 'react';
import Tippy from '@tippyjs/react';

import '../../css/HomePage3dotPopUp.css';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import { useNavigate } from 'react-router';


const HomePage3dotPopUp = (props) => {
    let navigate = useNavigate();
    const tippyRef = useRef(); 
    
    const [visible, setVisible] = useState(false);
    const show = () => setVisible(true);
    const hide = () => setVisible(false);

    const dropDownContent = (
        <div className="menu-container">
            <div onClick={() => onClickHandler('viewCluster')} className="menu-item">
                View Cluster
            </div>
            <div onClick={() => onClickHandler('editCluster')} className="menu-item">
                Edit Cluster
            </div>
            <div onClick={() => onClickHandler('deleteCluster')} className="menu-item">
                Delete Cluster
            </div>
            <div onClick={() => onClickHandler('viewDashboard')} className="menu-item">
                View Dashboard
            </div>
        </div>
    );

    const onClickHandler = (option) => { 
        hide(); 
        let clusterInfo = {
            ClusterId: props.node.allLeafChildren[0].data.ClusterId,
            ClusterName : props.node.allLeafChildren[0].data.ClusterName,
            DashboardId: props.node.allLeafChildren[0].data.DashboardId
        }
        props.setClusterToBeDeleted(clusterInfo);

        if (option === 'viewCluster') {
            console.log(props);
            let path = `/view/${clusterInfo.ClusterName}`;
            navigate(path, {state: {data: clusterInfo}});
        }

        if (option === 'editCluster') {
            props.setPopupEdit(true);
        }

        if (option === 'deleteCluster') {  
            props.setPopupDelete(true);
        }
        
        if (option === 'viewDashboard') {
            let path = `/dashboard`;
            navigate(path, {state: {data: clusterInfo}});
        }
    };
    
    // Conditional cell rendering : Render 3dot icon only at the grouped level, not for each node 
    return (
        props.data == undefined ? 
                <Tippy
                ref={tippyRef}
                content={dropDownContent}
                visible={visible}
                onClickOutside={hide}
                allowHTML={true}
                arrow={false}
                appendTo={document.body}
                interactive={true}
                placement="right"
                > 
                    <MoreHorizIcon className="dots3" fontSize="small" style={{marginTop: '50%'}} onClick={visible ? hide : show}/>
                </Tippy>
        : null
    );
};

export default HomePage3dotPopUp;

