import React, { useState } from 'react'

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import '../../css/AuthenticationPopUp.css';


export default function AuthenticationPopupForRertyRemove(props) {

  const [credentials, setCredentials] = useState({
    userName: "",
    password: ""
  })

  const delay = ms => new Promise(res => setTimeout(res, ms));

  function handleSSHdetailsChange(e) {
    const newData = { ...credentials };
    newData[e.target.id] = e.target.value;
    setCredentials(newData);
  }

  async function onConfirmation(e) {
    if (credentials.userName !== '' && credentials.password !== '') {
      props.setRetryRemovePopup(false);
      props.setLastPopupForRetryRemove(true);
    }
    else if (credentials.password !== "") {
      //   props.setPopup(false);
      //   await delay(500);
      //   props.setWarningMessage("CLUSTER CREATION FAILED! Incorrect SSH credentials");
      //   props.setErrorAlert(true);
    }
    setCredentials({
      userName: "",
      password: ""
    });

  }

  async function onCancellation(e) {
    props.setRetryRemovePopup(false);
    // await delay(500);
    // props.setWarningMessage("CLUSTER CREATION CANCELLED!");
    // props.setErrorAlert(true);
  }

  return (
    <div>
      <Alert
        confirmButtonText="Submit"
        cancelButtonText="Cancel"
        isOpen={props.retyRemovePopup}
        onCancel={(e) => onCancellation(e)}
        onConfirm={(e) => onConfirmation(e)}
        intent={Intent.PRIMARY}
        loading={false}
      >
        <div className="authentication" style={{ display: 'flex', flexDirection: 'column' }}>
          <p>You MUST provide the SSH credentials to initiate the {props.action} process for cluster. Make sure that all IPs provided have the same credentials.</p>
          <label>SSH Username<span style={{ color: '#ff0000' }}> *</span></label>
          <input onChange={(e) => handleSSHdetailsChange(e)} id="userName" value={credentials.userName} name="userName" type="text" />
          <label>SSH Password<span style={{ color: '#ff0000' }}> *</span></label>
          <input onChange={(e) => handleSSHdetailsChange(e)} id="password" value={credentials.password} name="password" type="password" />
        </div>
      </Alert>
    </div>
  );
}


