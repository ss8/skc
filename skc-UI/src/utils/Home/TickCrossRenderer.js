import Checkbox from '@mui/material/Checkbox';
import FavoriteBorder from '@mui/icons-material/FavoriteBorder';
import Favorite from '@mui/icons-material/Favorite';

export default function TickCrossRenderer() {}

    TickCrossRenderer.prototype.init = function(params) {
        this.params = params;

        this.eGui = document.createElement('input');
        this.eGui.type = 'checkbox';
        this.eGui.checked = params.value;       
        if(!this.eGui.checked) {
            this.eGui.style.display = 'none';
        } 
        this.eGui.disabled = true;

        this.checkedHandler = this.checkedHandler.bind(this);
        this.eGui.addEventListener('click', this.checkedHandler);
    }

    TickCrossRenderer.prototype.checkedHandler = function(e) {
        let checked = e.target.checked;
        let colId = this.params.column.colId;
        this.params.node.setDataValue(colId, checked);
    }

    TickCrossRenderer.prototype.getGui = function(params) {
        return this.eGui;   
    }

    TickCrossRenderer.prototype.destroy = function(params) {
        this.eGui.removeEventListener('click', this.checkedHandler);
    }

