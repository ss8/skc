import React, { useEffect, useState } from 'react'

import '../../css/StatusRenderer.css';
import AuthenticationPopupForRertyRemove from './AthenticationPopupForRetryRemove';
import LastConfirmationPopupForRetryRemove from './LastConfirmationPopupForRetryRemove';

const statusTextRenderer = (props)=> {
  let clusterState = props.node.allLeafChildren[0].data.Status;
  // let dashboardState = props.node.allLeafChildren[0].data.DashboardStatus;

  if(clusterState === 'UNKNOWN' || clusterState === 'unknown')  
    return 'Creating Cluster';
  if(clusterState === 'SUCCESS' || clusterState === 'success') {
    // if(dashboardState === 'DEPLOYING')
    //   return 'Deploying Dashboard';
    // else if(dashboardState === 'DASHBOARD_ACTIVE')
    //   return 'Dashboard deployed'
    // else if(dashboardState === 'FAILED')
    //   return 'Dashboard deployment failed';
    return 'Cluster created';
  }
  if(clusterState === 'FAILED' || clusterState === 'failed') {
    return 'Cluster creation failed';
  }
  return '';
}

const StatusRenderer = (props) => {

  const [action, setAction] = useState('');  // Action = retry or remove

  // Authentication popup for Retry-Remove
  const [retyRemovePopup, setRetryRemovePopup] = useState(false);
  const [lastPopupForRetryRemove, setLastPopupForRetryRemove] = useState(false);

  var statusCellText = '';
  var retryDisplayState = 'none', removeDisplayState = 'none', viewLogsDisplayState = 'none';

  // Conditional cell rendering : Render Status text only at the grouped level 0, not for each node
  if(props.data == undefined) statusCellText = statusTextRenderer(props);

  if(statusCellText === 'Creating Cluster' || statusCellText === 'Cluster created') {
    retryDisplayState = removeDisplayState = viewLogsDisplayState = 'none'; //FOR TESTING 'inline-block'
  } 
  else if(statusCellText === 'Dashboard deployment failed') {
    retryDisplayState = viewLogsDisplayState = 'inline-block';
  }
  else if(statusCellText === 'Cluster creation failed') {
    retryDisplayState = removeDisplayState = 'inline-block';
  }
     

  const onClickHandler = (e)=> {
      if(e.target.id === 'retry') {
          if(statusCellText === 'Cluster creation failed')
            setAction('retryCluster');
          else if(statusCellText === 'Dashboard deployment failed')
            setAction('retryDashboard');  
        setRetryRemovePopup(true); //Authentication popup
      }
      else if(e.target.id === 'remove') {
        setAction('remove');
        setRetryRemovePopup(true); //Authentication popup
      }
      else if(e.target.id === 'viewLogs') {
        props.setAnchorElViewLogs(e.target)
      }
  }
  

  // Conditional cell rendering : Render Status and buttons only at the grouped level 0, not for each node
  return (
    props.data == undefined ?
    <div>
      <AuthenticationPopupForRertyRemove
        action={action}
        retyRemovePopup={retyRemovePopup}
        setRetryRemovePopup={setRetryRemovePopup}
        setLastPopupForRetryRemove={setLastPopupForRetryRemove}
      />
      <LastConfirmationPopupForRetryRemove
        action={action}
        setLastPopupForRetryRemove={setLastPopupForRetryRemove}
        lastPopupForRetryRemove={lastPopupForRetryRemove}
        prop={props}
      />
      <span id="homePageStatusCellText">{statusCellText}</span>
      <span className="homePageStatusCellButtons">
          <button onClick={e=> onClickHandler(e)} style={{display: retryDisplayState}} id="retry">retry</button>
          <button onClick={e=> onClickHandler(e)} style={{display: removeDisplayState}} id="remove">remove</button>
          <button onClick={e=> onClickHandler(e)} style={{display: viewLogsDisplayState}} id="viewLogs">view logs</button>
      </span>
    </div>
    : null
  )
}

export default StatusRenderer;
