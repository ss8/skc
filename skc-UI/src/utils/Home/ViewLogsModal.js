import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';

import '../../css/ViewLogsModal.css';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';

import { actionCreators } from '../../state';

// const style = {
//   position: 'absolute',
//   top: '50%',
//   left: '50%',
//   transform: 'translate(-50%, -50%)',
//   width: 750,
//   maxHeight: 400,
// //   bgcolor: 'background.paper',
//   backgroundColor: 'white',
//   border: '1px solid #000',
//   boxShadow: 24,
//   p: 4,
// };

export default function BasicModal() {

    const dispatch = useDispatch();
    const { openViewLogsModal } = bindActionCreators(actionCreators, dispatch); 
    
    const viewLogsContent = useSelector(state=> state.viewLogsContent);

    const viewLogsModalState = useSelector(state=> state.viewLogsModalState);
    const handleClose = () => openViewLogsModal(false);

    return (
        <Modal
            open={viewLogsModalState}
        >
            <div className='viewLogsModal'>
                <div className='viewLogsModalContent'>
                    <h2 style={{marginBottom: 20}}>Dashboard Logs</h2>
                    <p>
                        {viewLogsContent}
                    </p>
                <Button onClick={handleClose} variant="outlined" style={{color: 'white', marginTop: 15}}>Close</Button>
                </div>
            </div>
        </Modal>
    );
}