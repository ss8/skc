import React from 'react';
import { useSelector } from 'react-redux';

import Alert from '@mui/material/Alert';
import Collapse from '@mui/material/Collapse';


export default function SuccessAlert(props) {

  const successAlertState = useSelector(state=> state.successAlertState);
  const alertMessageContent = useSelector(state=> state.alertMessageContent);
  
  return (
    <div style={{position: 'absolute', zIndex: 1, marginTop: -20, display: 'flex', justifyContent: 'center', width: '100vw'}}>    
      <Collapse in={successAlertState}>
        <Alert 
        variant="filled" severity="success"
        style={{fontFamily: 'Titillium Web'}}
        >
          {alertMessageContent}
        </Alert> 
      </Collapse>
    </div>
  );
}
