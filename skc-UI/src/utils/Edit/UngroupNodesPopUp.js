import React from 'react';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";


export default function UngroupNodesPopUp(props) {

    const delay = ms => new Promise(res => setTimeout(res, ms));

    function UnGroupPopupText() {
        let selectedNodes = props.gridApi.getSelectedNodes();
        let nodesToBeUngrouped = "";
        selectedNodes.map((node)=> {    
            nodesToBeUngrouped = nodesToBeUngrouped + " " + node.data.NodeName;
        })
        return nodesToBeUngrouped;
    }

    async function onConfirmation() {
        let selectedNodes = props.gridApi.getSelectedNodes();
        selectedNodes.map((node)=> {
            node.data.NodeGroup = "";
        })
        props.setRowData([...props.rowData]);
        props.setUngroupNodesPopup(false);
        await delay(500);
        props.gridApi.deselectAll();
    }

    function onCancellation() {
        props.setUngroupNodesPopup(false);
    }

  return (
    <div>
        <Alert
            confirmButtonText="Yes, ungroup it"
            cancelButtonText="Cancel"
            isOpen={props.ungroupNodesPopup}
            onCancel={(e)=> onCancellation(e)}
            onConfirm={(e)=> onConfirmation(e)}
            intent={Intent.PRIMARY}
            loading={false}
        >
        <div className="authentication" style={{display: 'flex', flexDirection: 'column'}}>
            <p>Are you sure you want to ungroup selected nodes from group?</p>
        </div>
        </Alert>
    </div>
  );
}
