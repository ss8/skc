export default function ErrorIconRenderer() {}
    
    ErrorIconRenderer.prototype.init = function(params) {
        this.params = params;
        
        this.eGui = document.createElement('div');
        this.eGui.innerHTML = `<span class="my-value"></span>`;
        
        this.eValue = this.eGui.querySelector('.my-value');
        this.cellValue = this.getValueToDisplay(params);
        this.eValue.innerHTML = this.cellValue;
    }
 
    ErrorIconRenderer.prototype.getGui = function(params) {
        return this.eGui;
    }
 
    ErrorIconRenderer.prototype.refresh = function(params) {
        this.cellValue = this.getValueToDisplay(params);

        this.eGui.innerHTML = this.cellValue;
        params.errorNodeIPs.map(node=> {
            if(node == params.data) {
                this.eGui.innerHTML = 
                    `<span style="display:flex">
                            <i class="material-icons" style="font-size:18px;color:red;margin-top:auto;margin-bottom:auto; margin-right:10px">&#xe001;</i>
                            <span class="my-value">${this.cellValue}</span>
                    </span>`;
            }
        })
        return true;
    }
 
    ErrorIconRenderer.prototype.getValueToDisplay = function(params) {
        return params.valueFormatted ? params.valueFormatted : params.value;
    }
 