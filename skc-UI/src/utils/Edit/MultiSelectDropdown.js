import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import Chip from '@mui/material/Chip';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 3 + ITEM_PADDING_TOP,
      width: 301,
    },
  },
};


function getStyles(nodeName, individualNodeName, theme) {
  return {
    fontWeight:
      individualNodeName.indexOf(nodeName) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function MultipleSelectDropdown(props) {
  const theme = useTheme();
  
  const [errorState, setErrorState] = React.useState(false);

  React.useEffect(()=> {
    if(props.individualNodeName.length > 2) {
      props.setWarningMessage("You can not select more than 2 Registry Nodes");
      setErrorState(true);
      props.setErrorAlert(true);
    }
    else {
      setErrorState(false);
      props.registry.nodes = props.individualNodeName;
    }

    if(props.individualNodeName.length > 1) {
      props.setRegistryInnerVipDivDisplayState('block');
    } else {
      props.setRegistryInnerVipDivDisplayState('none');
    }
    // console.log(props.individualNodeName)
  },[props.individualNodeName])

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    props.setIndividualNodeName(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  
  };

  return (
    <div>
      <FormControl style={{marginBottom: 18, width: 301}} size="small" error={errorState}>
        
        <Select
          id="demo-multiple-chip"
          multiple
          value={props.individualNodeName}
          onChange={handleChange}
          renderValue={(selected) => (
            <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.8 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} color="info" size="small"/>
              ))}
            </Box>
          )}
          MenuProps={MenuProps}
        >
          {props.rowData.map((node) => (
            <MenuItem
              key={node.NodeName}
              value={node.NodeName}
              style={getStyles(node.NodeName, props.individualNodeName, theme)}
            >
              {node.NodeName}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
}
