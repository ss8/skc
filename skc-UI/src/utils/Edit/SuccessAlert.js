import React from 'react';

import Alert from '@mui/material/Alert';
import Collapse from '@mui/material/Collapse';


export default function SuccessAlert(props) {
  return (
    <Collapse in={props.successAlert}>
        <Alert 
        variant="filled" severity="success"
        style={{fontFamily: 'Titillium Web', marginLeft: '23%', marginRight: '23%'}}
        >
        Congratulations! Your Kubernetes Cluster <strong>"{props.data.clusterName}"</strong> is successfully edited.</Alert>
    </Collapse>
  );
}
