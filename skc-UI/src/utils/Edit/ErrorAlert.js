import React from 'react';

import Alert from '@mui/material/Alert';
import IconButton from '@mui/material/IconButton';
import Collapse from '@mui/material/Collapse';
import CloseIcon from '@mui/icons-material/Close';


export default function ErrorAlert(props) {
    return (
        <div style={{position:'fixed', zIndex: 1, display: 'flex', width: '95%', justifyContent: 'space-around'}}>
        <Collapse in={props.errorAlert}>
            <Alert
                variant="filled" severity="error"
                action={
                    <IconButton
                        aria-label="close"
                        color="inherit"
                        size="small"
                        onClick={() => {
                            props.setErrorAlert(false);
                        }}
                    >
                        <CloseIcon fontSize="inherit" />
                    </IconButton>
                }
                style={{fontFamily: 'Titillium Web', paddingTop: 0, paddingBottom: 0, fontSize: 14}}
            >
                {props.warningMessage}
            </Alert>
        </Collapse>
        </div>
    );
}
