import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import  Axios  from '../CustomAxios';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import LoadingIcon from './LoadingIcon';
import { actionCreators } from '../../state/index';

export default function LastConfirmationPopUpForEdit(props) {

    let navigate = useNavigate();
    const dispatch = useDispatch();
    const {openSuccessAlert, openErrorAlert, alertMessage} = bindActionCreators(actionCreators, dispatch);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const [loading, setLoading] = React.useState(false);
    const [loadingIconState, setLoadingIconState] = React.useState('none');
    const [icon, setIcon] = useState("automatic-updates");
    
    var firstControllerIp; 

    const [lastConfirmationMessage, setLastConfirmationMessage] = useState("Are you sure you want to continue with the updation of cluster? Please note that once cluster updation is initiated it can not be rolled back.");

    var jsonData = {
        "clusterId": props.data.clusterId,
        "clusterName": props.data.clusterName,
        "nodes": [], 
        "groups": [
            {
                "name": "kube_controller_nodes",
                "nodes": [],
                "groupVars": {}
            },
            {
                "name": "kube_worker_nodes",
                "nodes": [],
                "groupVars": {
                    "node_labels": `{\"kubernetes.io/role\":\"${props.details.workerNodeLabel}\" }`
                },
                "subGroups": []
            },
            {
                "name": "etcd",
                "nodes": [],
                "groupVars": {}
            }

        ],
        "tmpStorageLocation": props.details.tmpStorageLocation,
        "isDualStackEnabled": props.details.isDualStackEnabled,
        "kubeAPIServerPortRange": props.details.kube_apiserver_node_port_range,
        "kubeletStoragePath": props.details.kubelet_storage,
        "dockerStoragePath": props.details.dockerStoragePath,
        "isRegistryEnabled": props.details.isRegistryEnabled,
        "registry": {
            "registryName": "registry_nodes",
            "nodes": props.registry.nodes,
            "port": parseInt(props.registry.port),
            "volumePath": props.registry.volumePath,
            "containerName": props.registry.containerName,
            "isRegistryVIPEnabled": props.registry.isRegistryVIPEnabled,
            "vip": {
                "interface": props.vip.interface,
                "ip": props.vip.ip,
                "vrrId": props.vip.vrrId
            }
        },
        "inventoryPath": "",
        "dashboardId": "",
    }

    var jsonDataInitialState = {
        "clusterId": props.data.clusterId,
        "clusterName": props.data.clusterName,
        "nodes": [], 
        "groups": [
            {
                "name": "kube_controller_nodes",
                "nodes": [],
                "groupVars": {}
            },
            {
                "name": "kube_worker_nodes",
                "nodes": [],
                "groupVars": {
                    "node_labels": `{\"kubernetes.io/role\":\"${props.details.workerNodeLabel}\" }`
                },
                "subGroups": []
            },
            {
                "name": "etcd",
                "nodes": [],
                "groupVars": {}
            }

        ],
        "tmpStorageLocation": props.details.tmpStorageLocation,
        "isDualStackEnabled": props.details.isDualStackEnabled,
        "kubeAPIServerPortRange": props.details.kube_apiserver_node_port_range,
        "kubeletStoragePath": props.details.kubelet_storage,
        "dockerStoragePath": props.details.dockerStoragePath,
        "isRegistryEnabled": props.details.isRegistryEnabled,
        "registry": {
            "registryName": "registry_nodes",
            "nodes": props.registry.nodes,
            "port": parseInt(props.registry.port),
            "volumePath": props.registry.volumePath,
            "containerName": props.registry.containerName,
            "isRegistryVIPEnabled": props.registry.isRegistryVIPEnabled,
            "vip": {
                "interface": props.vip.interface,
                "ip": props.vip.ip,
                "vrrId": props.vip.vrrId
            }
        },
        "inventoryPath": "",
        "dashboardId": "",
    };

    var modifiedNodesToBeSent = [];

    async function formationOfModifiedNodesToBeSent() {
        props.modifiedNodes.map(nodeDetails=> {
            let roles = [];
            if(nodeDetails.Controller) roles.push("CONTROLLER")
            if(nodeDetails.Etcd) roles.push("ETCD")
            if(nodeDetails.Worker) roles.push("WORKER")
            let tempModifiedNode = {
                "name": nodeDetails.NodeName,
                "ip": nodeDetails.NodeIP,
                "roles": roles
            }
            modifiedNodesToBeSent.push(tempModifiedNode);
        })
        // console.log(modifiedNodesToBeSent);
    }


    async function formationOfjsonDataToBeSent() {

        for(let i=0; i<props.rowData.length; i++) {

            var roles = [];

            if(props.rowData[i].Controller) {
                roles.push("CONTROLLER");
                if(jsonData.groups[0].nodes.length == 0) firstControllerIp = props.rowData[i].NodeIP;
                jsonData.groups[0].nodes.push(props.rowData[i].NodeName);
            }
            if(props.rowData[i].Etcd) {
                roles.push("ETCD");
                jsonData.groups[2].nodes.push(props.rowData[i].NodeName);
            }
            if(props.rowData[i].Worker) {
                roles.push("WORKER");
                if(props.rowData[i].NodeGroup == "" || props.rowData[i].NodeGroup == undefined) {
                    jsonData.groups[1].nodes.push(props.rowData[i].NodeName);
                }
            }


            if(props.rowData[i].NodeGroup !== "" && props.rowData[i].NodeGroup !== undefined) {
                let groupName = props.rowData[i].NodeGroup;
                props.workerSubGroups.set(groupName, {
                    name: props.rowData[i].NodeGroup,
                    nodes : [...props.workerSubGroups.get(props.rowData[i].NodeGroup).nodes, props.rowData[i].NodeName],
                    groupVars : {
                        node_labels : `{\"kubernetes.io/role\":\"${props.details.workerNodeLabel}\" }`
                    }
                });

            }

            var info = {
                "name": props.rowData[i].NodeName,
                "ip": props.rowData[i].NodeIP,
                "roles": roles
            };
            jsonData.nodes.push(info);          
        }

        [...props.workerSubGroups.keys()].map(key=> {
            if(props.workerSubGroups.get(key).nodes.length > 0) {
                jsonData.groups[1].subGroups.push(props.workerSubGroups.get(key));
            }
        })
        return;
    }

    async function submit(e) {
        e.preventDefault();

        if( props.addNodeState ) {
            // Send data to the backend via POST 
            Axios.post(`/cluster/${props.data.clusterId}/node/add`, {"jsonData":jsonData, "modifiedNodes": modifiedNodesToBeSent})
            .then(async res=> {
                // console.log({"jsonData":jsonData, "modifiedNodes": modifiedNodesToBeSent});
                props.setLastPopup(false);
                if(res.status === 200) { 
                    props.setDisabled(true);
                    navigate(`/`);
                    alertMessage("Cluster has successfully has been edited");
                    openSuccessAlert(true);
                    await delay(3000);
                    openSuccessAlert(false);
                } 
                else {
                    props.setWarningMessage("We are facing some issue at the moment. Please try again later");
                    props.setErrorAlert(true);
                }
            })
            .catch(async error=> {
                navigate(`/`);
                alertMessage("Error occured in editing cluster");
                openErrorAlert(true);
                await delay(3000);
                openErrorAlert(false);
            })
        } else {
            // Send data to the backend via DELETE 
            Axios.delete(`/cluster/${props.data.clusterId}/node/delete`, {data:{"jsonData":jsonData, "modifiedNodes": modifiedNodesToBeSent}})
            .then(async res=> {
                // console.log({"jsonData":jsonData, "modifiedNodes": modifiedNodesToBeSent});
                props.setLastPopup(false);
                if(res.status === 200) { 
                    props.setDisabled(true);
                    navigate(`/`);
                    alertMessage("Cluster has successfully has been edited");
                    openSuccessAlert(true);
                    await delay(3000);
                    openSuccessAlert(false);
                } 
                else {
                    props.setWarningMessage("We are facing some issue at the moment. Please try again later");
                    props.setErrorAlert(true);
                }
            })
            .catch(async error=> {
                navigate(`/`);
                alertMessage("Error occured in editing cluster");
                openErrorAlert(true);
                await delay(3000);
                openErrorAlert(false);
            })
        }
    }

    async function onConfirmation(e) {
        await formationOfjsonDataToBeSent();
        await formationOfModifiedNodesToBeSent();
        submit(e);
        setIcon("build");
        setLastConfirmationMessage("Your cluster updation is in progress... It might take some time. Hold tight!")
        setLoadingIconState('block');
        setLoading(true);
    }

    async function onCancellation(e) {
        props.setLastPopup(false);
        jsonData = jsonDataInitialState;
    }

    return (
    <div>
        <Alert
            confirmButtonText="Update"
            cancelButtonText="Cancel"
            isOpen={props.lastPopup}
            onCancel={(e)=> onCancellation(e)}
            onConfirm={(e)=> onConfirmation(e)}
            intent={Intent.PRIMARY}
            icon={icon}
            loading={loading}
        >
        <div style={{display: 'flex', flexDirection: 'column'}}>
            <p>{lastConfirmationMessage}</p>
            <div style={{display: loadingIconState}}>
                <LoadingIcon/>
            </div>
        </div>
        </Alert>
    </div>
    );
}


