import React from 'react';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

export default function RemoveWorkerRolePopUp(props) {

    function onConfirmation() {

    }
    function onCancellation() {
        props.setRemoveWorkerRolePopup(false);
    }
    return (
        <div>
            <Alert
                confirmButtonText="Yes, change it"
                cancelButtonText="Cancel"
                isOpen={props.removeWorkerRolePopup}
                onCancel={(e)=> onCancellation(e)}
                onConfirm={(e)=> onConfirmation(e)}
                intent={Intent.PRIMARY}
                loading={false}
            >
            <div className="authentication" style={{display: 'flex', flexDirection: 'column'}}>
                <p>Changing this node to a NON-WORKER node will remove it automatically from group.</p>
                <p>Are you sure you want to change it?</p>
            </div>
            </Alert>
        </div>
      );
}
