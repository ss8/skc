import React, { useEffect, useState } from 'react';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";
import GroupSelectDropdown from './GroupSelectDropdown';


export default function GroupNodesPopUp(props) {

  const [availableSubGroups, setAvailableSubGroups] = useState([...props.workerSubGroups.keys()]);

  const [value, setValue] = React.useState('');

  const delay = ms => new Promise(res => setTimeout(res, ms));

  async function onConfirmation() {
    let selectedNodes = props.gridApi.getSelectedNodes();
    selectedNodes.map((node)=> {
        node.data.NodeGroup = value;
        props.workerSubGroups.set(value , {
          name: value,
          nodes: [],
          groupVars: {
              node_labels: `{\"kubernetes.io/role\":\"${props.details.workerNodeLabel}\" }`
          }
      });
    })
    const isExisting = availableSubGroups.some((option) => value === option);
    if(!isExisting) {
      setAvailableSubGroups([...availableSubGroups, value]);
    }
    props.setRowData([...props.rowData]);
    props.setGroupNodesPopup(false);
    await delay(500);
    props.gridApi.deselectAll();
  }

  function onCancellation() {
    console.log(value)
    props.setGroupNodesPopup(false);
  }

  return (
      <div>
        <Alert
            confirmButtonText="Group"
            cancelButtonText="Cancel"
            isOpen={props.groupNodesPopup}
            onCancel={onCancellation}
            onConfirm={onConfirmation}
            intent={Intent.PRIMARY}
        >
            <div>
                <GroupSelectDropdown
                  workerSubGroups={props.workerSubGroups}
                  availableSubGroups={availableSubGroups}
                  setAvailableSubGroups={setAvailableSubGroups}
                  value={value}
                  setValue={setValue}
                />
            </div>
            <p style={{marginTop: 8}}></p>
            <p>Group Name max length is 63 characters with no spaces and only lower case alphanumeric, hyphen and period characters are allowed.</p>
        </Alert>
      </div>
  );
}
