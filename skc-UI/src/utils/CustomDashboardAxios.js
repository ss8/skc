import Axios from 'axios';

import AuthToken from './Authorization/AuthToken';

export default Axios.create({
    baseURL: `${process.env.REACT_APP_DASHBOARD_MS_URL}`,
    headers: {
        Authorization: `${AuthToken.getToken()}`,
    },
    proxy: {
        protocol: 'https',
        // host: 'https://10.0.161.241',
    },
}) 