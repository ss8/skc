import * as React from 'react';
import { useNavigate } from 'react-router';

import Popover from '@mui/material/Popover';

import '../../css/HeaderAvatarPopover.css';


const HeaderAvatarPopover = (props)=> {

    let navigate = useNavigate();

    const open = Boolean(props.anchorEl);
    const id = open ? 'simple-popover' : undefined;

    const dropDownContent = (
        <div className='popover-container'>
            <div onClick={() => onClickHandler('changePassword')} className="popover-item">
                Change Password
            </div>
        </div>
    );

    const onClickHandler = (option) => { 
        props.handleClose();
        
        if (option === 'changePassword') {
            let path = `/changePassword`;
            navigate(path);
        }
    };

  return (
      <Popover
        id={id}
        open={open}
        anchorEl={props.anchorEl}
        onClose={props.handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        style={{marginTop: 9}}
      >
        {dropDownContent}
      </Popover>
  );
}

export default HeaderAvatarPopover;