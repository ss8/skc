import * as React from 'react';
import Popover from '@mui/material/Popover';

export default function InformationPopover(props) {

  const open = Boolean(props.anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
      <Popover
        id={id}
        open={open}
        anchorEl={props.anchorEl}
        onClose={props.handleClose}
        anchorOrigin={{
          vertical: 'right',
          horizontal: 'right',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'lest',
        }}
      >
      <div className="informationPopoverContent" style={{maxWidth:300, padding: 10, backgroundColor: '#DBDCDC'}}>
          <p>{props.informationContent}</p>
      </div>
      </Popover>
  );
}