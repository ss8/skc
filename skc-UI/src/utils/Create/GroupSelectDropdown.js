import * as React from 'react';
import TextField from '@mui/material/TextField';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';

const filter = createFilterOptions();

export default function FreeSoloCreateOption(props) {

  return (
    <Autocomplete
      value={props.value}
      onChange={(event, newValue) => {
        if (typeof newValue === 'string') {
          props.setValue(newValue);
        } else if (newValue && newValue.inputValue) {
          // Create a new value from the user input
          props.setValue(newValue.inputValue);
        } else {
          props.setValue(newValue);
        }
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params);

        const { inputValue } = params;
        // Suggest the creation of a new value
        const isExisting = options.some((option) => inputValue === option);
        if (inputValue !== '' && !isExisting) {
          filtered.push(inputValue);
        }

        return filtered;
      }}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      id="subGroup-dropdown"
      options={props.availableSubGroups}
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option;
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue;
        }
        // Regular option
        return option;
      }}
      renderOption={(props, option) => <li {...props}>{option}</li>}
      freeSolo
      size="small"
      renderInput={(params) => (
        <TextField {...params} label="Select or create a new group" />
      )}
    />
  );
}