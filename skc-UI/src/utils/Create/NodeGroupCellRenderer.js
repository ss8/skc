import { useState, useRef } from 'react';
import React from 'react';
import Tippy from '@tippyjs/react';


const NodeGroupCellRenderer = (props) => {
    const tippyRef = useRef(); 

    const show = () => props.setVisible(true);
    const hide = () => props.setVisible(false);

    const dropDownContent = (
        <div className='group-list'>
            <p>1</p>
            <p>1</p>
            <p>1</p>
        </div>
    );
  return (
    <Tippy
        ref={tippyRef}
        content={dropDownContent}
        visible={props.visible}
        onClickOutside={hide}
        allowHTML={true}
        arrow={false}
        appendTo={document.body}
        interactive={true}
        placement="bottom"
    >
        <div></div>
        </Tippy> 
  )
}

export default NodeGroupCellRenderer;