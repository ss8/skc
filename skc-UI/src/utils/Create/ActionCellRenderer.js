import { useRef } from 'react';
import React from 'react';
import Tippy from '@tippyjs/react';

import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';

const ActionCellRenderer = () => {
  const tippyRef = useRef();

  return (
    <Tippy
      ref={tippyRef}
      allowHTML={true}
      arrow={false}
      interactive={true}
      placement="right"
    >
      <button style={{background: 'none', border: 'none', paddingTop: 4}}>
      <RemoveCircleOutlineIcon fontSize="small" color="warning"/>
      </button>
    </Tippy>
  );
};

export default ActionCellRenderer;