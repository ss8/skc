import React from 'react';
import { Outlet, Navigate, useLocation } from 'react-router-dom';

import AuthToken from './AuthToken';


export const ProtectedRoute = () => {
    const location = useLocation();
    
    return AuthToken.isTokenValid() ? <Outlet/> : 
        <Navigate to="/signIn" replace state={{from: location}}/>
    
}
