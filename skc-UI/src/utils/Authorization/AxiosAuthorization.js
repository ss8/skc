import Axios from 'axios';

import AuthToken from './AuthToken';


const AxiosAuthorization = Axios.create({
	baseURL: `${process.env.REACT_APP_DASHBOARD_MS_URL}`,
	proxy: {
		protocol: 'https',
		host: 'localhost',
	},

})

AxiosAuthorization.interceptors.request.use((config) => {
	if (AuthToken.getToken() != null) {
		config.headers.Authorization = AuthToken.getToken();
	}

	return config;
});

Object.freeze(AxiosAuthorization)

export default AxiosAuthorization;