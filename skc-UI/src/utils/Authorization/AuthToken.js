import jwt_decode from 'jwt-decode';

class AuthToken {
    tokenName = "SKC-Authorization";

    isTokenValid() {
        var token = localStorage.getItem(this.tokenName);

        if (token != null && ((jwt_decode(token).exp * 1000) > new Date().getTime())) {
            return true;
        }
        else {
            this.removeToken()
            return false;
        }
    }

    getToken(){
        return localStorage.getItem(this.tokenName);
    }

    setToken(token){
        localStorage.setItem(this.tokenName, token);
    }

    removeToken(){
        localStorage.removeItem(this.tokenName);
    }

}

export default new AuthToken();