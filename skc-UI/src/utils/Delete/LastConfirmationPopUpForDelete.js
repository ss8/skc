import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import Axios  from '../CustomAxios';
import AxiosDashboard from '../CustomDashboardAxios';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import LoadingIcon from '../Create/LoadingIcon';
import { actionCreators } from '../../state/index';

export default function LastConfirmationPopUpForDelete(props) {

    let navigate = useNavigate();
    const dispatch = useDispatch();
    const {openSuccessAlert, openErrorAlert, alertMessage} = bindActionCreators(actionCreators, dispatch);

    const sshCredentials = useSelector(state=> state.sshAuthenticationCredentials);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const [loading, setLoading] = React.useState(false);
    const [loadingIconState, setLoadingIconState] = React.useState('none');
    const [icon, setIcon] = useState("delete");
    
    const [lastConfirmationMessage, setLastConfirmationMessage] = useState("Are you sure you want to continue with the deletion of cluster? You will lose all your data.");
       
    async function submit(e) {
        e.preventDefault();

        AxiosDashboard.get(`/dashboard/list?search=${props.clusterToBeDeleted.ClusterName}`)
        .then(res=> {
            if(res.data.list_data.dashboard_list.length > 0) {
                let dashboardId = res.data.list_data.dashboard_list[0].id;
                let dashboardUrl = res.data.list_data.dashboard_list[0].url;

                return AxiosDashboard.delete(`/dashboard/delete/${parseInt(dashboardId)}`, {data: {
                        "clusterIp": dashboardUrl.match(/[0-9.]+(?=:)/)[0],   //EXTRACTING CONTROLLER IP FROM DASHBOARD URL USING REGEX
                        "clusterName": res.data.list_data.dashboard_list[0].name,
                        "sshUsername": sshCredentials.sshUsername, 
                        "sshPassword": sshCredentials.sshPassword
                    }
                });
            }
            else {
                return new Promise(res=> res("Delete cluster if Dashboard not present"))
            }
        })
        .then(()=> {
            return Axios.delete(`/cluster/${parseInt(props.clusterToBeDeleted.ClusterId)}/delete`)
        })
        .then(async res=> {
            props.setLastPopupDelete(false);
            if(res.status === 200) { 
                alertMessage("Cluster deletion is successfull");
                openSuccessAlert(true);
                await delay(3000);
                openSuccessAlert(false);
            } 
            else {
                alertMessage("We are facing some issue at the moment. Please try again later");
                openErrorAlert(true);
                await delay(3000);
                openErrorAlert(false);
            }
        })
        .catch(async error=> {
            props.setLastPopupDelete(false);
            alertMessage("Error occured. Could not delete your cluster");
            openErrorAlert(true);
            await delay(3000);
            openErrorAlert(false);
        })
    }

    async function onConfirmation(e) {
        submit(e);
        setIcon("trash");
        setLastConfirmationMessage("Your cluster deletion is in progress... It might take some time.")
        setLoadingIconState('block');
        setLoading(true);
    }

    async function onCancellation(e) {
        props.setLastPopupDelete(false);
    }

    return (
    <div>
        <Alert
            confirmButtonText="Delete"
            cancelButtonText="Cancel"
            isOpen={props.lastPopupDelete}
            onCancel={(e)=> onCancellation(e)}
            onConfirm={(e)=> onConfirmation(e)}
            intent={Intent.DANGER}
            icon={icon}
            loading={loading}
        >
        <div style={{display: 'flex', flexDirection: 'column'}}>
            <p>{lastConfirmationMessage}</p>
            <div style={{display: loadingIconState}}>
                <LoadingIcon/>
            </div>
        </div>
        </Alert>
    </div>
    );
}
