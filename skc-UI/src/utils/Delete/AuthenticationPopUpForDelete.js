import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import '../../css/AuthenticationPopUp.css';
import { actionCreators } from '../../state';


export default function AuthenticationPopUpForDelete(props) {

  const dispatch = useDispatch();
  const { openErrorAlert, alertMessage, sshAuthentication } = bindActionCreators(actionCreators, dispatch);


  const delay = ms => new Promise(res => setTimeout(res, ms));

  const [credentials, setCredentials] = useState({
    userName: "",
    password: ""
  })

  function handleSSHdetailsChange(e) {
    const newData = { ...credentials };
    newData[e.target.id] = e.target.value;
    setCredentials(newData);
  }

  async function onConfirmation(e) {
    if (credentials.userName !== '' && credentials.password !== '') {
      sshAuthentication({
        "sshUsername": credentials.userName,
        "sshPassword": credentials.password
      });
      props.setPopupDelete(false);
      props.setLastPopupDelete(true);
    }
    else if (credentials.password !== "") {
      props.setPopupDelete(false);
      alertMessage("Incorrect SSH Credentails");
      openErrorAlert(true);
      await delay(3000);
      openErrorAlert(false);
    }
    setCredentials({
      userName: "",
      password: ""
    });

  }

  async function onCancellation(e) {
    props.setPopupDelete(false);
  }

  return (
    <div>
      <Alert
        confirmButtonText="Submit"
        cancelButtonText="Cancel"
        isOpen={props.popupDelete}
        onCancel={(e) => onCancellation(e)}
        onConfirm={(e) => onConfirmation(e)}
        intent={Intent.PRIMARY}
        loading={false}
      >
        <div className="authentication" style={{ display: 'flex', flexDirection: 'column' }}>
          <p>You MUST provide the SSH credentials to initiate the deletion process for cluster ”{props.clusterToBeDeleted.ClusterName}".</p>
          <label>SSH Username<span style={{ color: '#ff0000' }}> *</span></label>
          <input onChange={(e) => handleSSHdetailsChange(e)} id="userName" value={credentials.userName} name="userName" type="text" />
          <label>SSH Password<span style={{ color: '#ff0000' }}> *</span></label>
          <input onChange={(e) => handleSSHdetailsChange(e)} id="password" value={credentials.password} name="password" type="password" />
        </div>
      </Alert>
    </div>
  );
}


