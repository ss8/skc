import * as React from 'react';
import Box from '@mui/material/Box';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import Button from '@mui/material/Button';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import DeviceHubIcon from '@mui/icons-material/DeviceHub';
import { useNavigate } from 'react-router-dom';

export default function ClusterDrawer(props) {
  var navigate = useNavigate();

  const [state, setState] = React.useState({right: false});

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    setState({ ...state, [anchor]: open });
  };

  const switchDashboard = (text)=> {
    let clusterInfo = {
      ClusterId: undefined,
      ClusterName : text,
      DashboardId: undefined
    };
    let path = `/dashboard`;
    navigate(path, {state: {data: clusterInfo}});
  }

  const list = (anchor) => (
    <Box
      sx={{ width: 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
      {
        props.clustersList.map((text, index) => (
          <ListItem button key={text} onClick={()=> switchDashboard(text)}>
            <ListItemIcon>
              <DeviceHubIcon/>
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        )) 
      }
      </List>
      <Divider />
    </Box>
  );

  return (
    <div style={{position: 'absolute', zIndex: 1, marginLeft: '82%', marginTop: 8}}>
      {['right'].map((anchor) => (
        <React.Fragment key={anchor}>
          <Button style={{fontSize: 25, display: props.buttonDisplay}} onClick={toggleDrawer(anchor, true)}>&#10247;</Button>
          <SwipeableDrawer
            anchor={anchor}
            open={state[anchor]}
            onClose={toggleDrawer(anchor, false)}
            onOpen={toggleDrawer(anchor, true)}
          >
            {list(anchor)}
          </SwipeableDrawer>
        </React.Fragment>
      ))}
    </div>
  );
}