import React, { useEffect, useState } from 'react'

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import '../../css/ExistingClusterDetailsPopup.css';
import ClusterSelectDropdown from './ClusterSelectDropdown';
import AxiosDashboard from '../CustomDashboardAxios';

const ExistingClusterDetailsPopup = (props) => {

    const [clusterList, setClusterList] = useState([]);
    const [clusterName, setClusterName] = React.useState([]);

    useEffect(async ()=>{
        await AxiosDashboard.get(`dashboard/clusterList`)
        .then(res=> {
            let allClusters = [];
            res.data.cluster_data.clusterList.map(cluster=> {
                let name = cluster.match(/[a-z0-9.-]+(?=:)/);
                allClusters.push(name);
            })
            setClusterList(allClusters);
        })
        .catch((error)=> console.log(error.response.error))
    },[])

    const onConfirmation = ()=> {
        let newData = {...props.clusterDetailsForExistingCluster};
        newData.clusterName = clusterName[0];
        props.setClusterDetailsForExistingCluster(newData);

        if(props.clusterDetailsForExistingCluster.clusterName !== "") {
            props.setExistingClusterPopup(false);
            props.setAuthPopupExistingCluster(true);
            setClusterName([]);
        }
    }

    const onCancellation = ()=> {
        props.setExistingClusterPopup(false);
        setClusterName([]);
    }

    return (
    <div>
        <Alert
            confirmButtonText="Proceed"
            cancelButtonText="Cancel"
            isOpen={props.existingClusterPopup}
            onCancel={(e)=> onCancellation(e)}
            onConfirm={(e)=> onConfirmation(e)}
            intent={Intent.PRIMARY}
            loading={false}
        >
        <div className="existingClusterDetails" style={{display: 'flex', flexDirection: 'column'}}>
            <p>Select existing cluster name to proceed with the installation of Dashboard.</p>
            <p></p>
            <label style={{marginBottom: 2}}>Cluster Name<span style={{color:'#ff0000'}}> *</span></label>
            <ClusterSelectDropdown
                clusterList={clusterList}
                clusterName={clusterName}
                setClusterName={setClusterName}
            />
        </div>
        </Alert>
    </div>
    );
}

export default ExistingClusterDetailsPopup;
