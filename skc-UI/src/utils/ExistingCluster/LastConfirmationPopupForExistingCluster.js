import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import AxiosCluster from '../CustomAxios';
import AxiosDashboard from '../CustomDashboardAxios';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import LoadingIcon from './LoadingIcon';
import { actionCreators } from '../../state/index';

export default function LastConfirmationPopUp(props) {

    let navigate = useNavigate();
    const dispatch = useDispatch();
    const { openSuccessAlert, openErrorAlert, alertMessage, openViewLogsModal, viewLogs, dashboardStatus, sshAuthentication } = bindActionCreators(actionCreators, dispatch);

    const sshCredentials = useSelector(state => state.sshAuthenticationCredentials);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const [loading, setLoading] = React.useState(false);
    const [loadingIconState, setLoadingIconState] = React.useState('none');
    const [icon, setIcon] = useState("new-layers");

    const [lastConfirmationMessage, setLastConfirmationMessage] = useState("Are you sure you want to continue with the installation of dashboard on your cluster? Please note that once installation is initiated it can not be rolled back.");

    const [events, setEvents] = useState("");

    const getEvents = () => {
        let eventSource = new EventSource(`${process.env.REACT_APP_DASHBOARD_MS_URL}/dashboard/events`);
        eventSource.onopen = (event) => console.log("Test events opened");
        eventSource.onmessage = (event) => setEvents(event.data)
        eventSource.onerror = (event) => eventSource.close();
    }

    const submit = (e) => {
        e.preventDefault();

        getEvents();
        var clusterInfoForDashboard;

        AxiosCluster.post(`/cluster/controller/details`, {
            "clusterName": props.clusterDetailsForExistingCluster.clusterName,
        })
            .then(res => {
                clusterInfoForDashboard = {
                    "clusterIp": res.data.controllerIP,
                    "clusterName": res.data.clusterName,
                    "sshUsername": sshCredentials.sshUsername,
                    "sshPassword": sshCredentials.sshPassword
                };
                // dashboardStatus('DEPLOYING');
                return AxiosDashboard.post(`/dashboard/install`, clusterInfoForDashboard)
            })
            .then(async res => {
                console.log(res);
                props.setLastPopupExistingCluster(false);
                alertMessage("Dashboard installation is successfull");
                openSuccessAlert(true);
                await delay(3000);
                openSuccessAlert(false);
            })
            .catch(async error => {
                // dashboardStatus('FAILED');
                viewLogs(error.response.data.logs != undefined ? error.response.data.logs : "");
                openViewLogsModal(true);
                props.setLastPopupExistingCluster(false);
                alertMessage(typeof error.response.data.error == "string" ? error.response.data.error : "Cluster unreachable");
                openErrorAlert(true);
                await delay(3000);
                openErrorAlert(false);
            })
    }

    const onConfirmation = (e) => {
        submit(e);
        setIcon("download");
        setLastConfirmationMessage("Your dashboard installation is in progress... It might take few minutes!")
        setLoadingIconState('block');
        setLoading(true);
    }

    const onCancellation = () => {
        props.setLastPopupExistingCluster(false);
    }

    return (
        <div>
            <Alert
                confirmButtonText="Install"
                cancelButtonText="Cancel"
                isOpen={props.lastPopupExistingCluster}
                onCancel={(e) => onCancellation(e)}
                onConfirm={(e) => onConfirmation(e)}
                intent={Intent.PRIMARY}
                icon={icon}
                loading={loading}
            >
                <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <p>{lastConfirmationMessage}</p>
                    <div style={{ display: loadingIconState }}>
                        <LoadingIcon />
                    </div>
                </div>
            </Alert>
        </div>
    );
}


