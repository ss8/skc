import * as React from 'react';
import Popover from '@mui/material/Popover';
import Typography from '@mui/material/Typography';
import FileUploadRoundedIcon from '@mui/icons-material/FileUploadRounded';

import '../../css/ExistingClusterPopover.css';


export default function ExistingClusterPopover(props) {

  const open = Boolean(props.anchorEl);
  const id = open ? 'simple-popover' : undefined;

  return (
      <Popover
        id={id}
        open={open}
        anchorEl={props.anchorEl}
        onClose={props.handleClose}
        style={{marginTop: 8}}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
      >
        <div className="popoverContent" style={{height: 220, width: 450, padding: 32}}>
          <p>Do you want to create cluster from an already existing Inventory file? You can import your Inventory file here.</p>
          <p>Upload the Inventory file with extention ".ini" only</p>
          <input type="file" accept='.ini'/>
          <button><FileUploadRoundedIcon/> Upload</button>
        </div>
      </Popover>
  );
}