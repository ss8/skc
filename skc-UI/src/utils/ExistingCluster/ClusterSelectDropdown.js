import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 3 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(cluster, clusterName, theme) {
  return {
    fontWeight:
      clusterName.indexOf(cluster) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

export default function ClusterSelectDropdown(props) {
  const theme = useTheme();

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    props.setClusterName(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    );
  };

  return (
    <div>
      <FormControl fullWidth>
        <Select
          value={props.clusterName}
          onChange={handleChange}
          MenuProps={MenuProps}
          size="small"
        >
          {
            props.clusterList.map((cluster) => (
              <MenuItem
                key={cluster}
                value={cluster}
                style={getStyles(cluster, props.clusterName, theme)}
              >
                {cluster}
              </MenuItem>
            ))
          }
        </Select>
      </FormControl>
    </div>
  );
}