import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';

import '@blueprintjs/core/lib/css/blueprint.css';
import { Alert, Intent } from "@blueprintjs/core";

import '../../css/AuthenticationPopUp.css';
import { actionCreators } from '../../state';

export default function AuthenticationPopupForExistingCluster(props) {

  const dispatch = useDispatch();
  const { openErrorAlert, alertMessage } = bindActionCreators(actionCreators, dispatch);

  const [credentials, setCredentials] = useState({
    userName: "",
    password: ""
  })

  const delay = ms => new Promise(res => setTimeout(res, ms));

  function handleSSHdetailsChange(e) {
    const newData = { ...credentials };
    newData[e.target.id] = e.target.value;
    setCredentials(newData);
  }

  async function onConfirmation(e) {
    if (credentials.userName !== '' && credentials.password !== '') {
      props.setAuthPopupExistingCluster(false);
      props.setLastPopupExistingCluster(true);
    }
    else if (credentials.password !== "") {
      props.setAuthPopupExistingCluster(false);
      alertMessage("Incorrect SSH Credentails");
      openErrorAlert(true);
      await delay(3000);
      openErrorAlert(false);
    }
    setCredentials({
      userName: "",
      password: ""
    });

  }

  async function onCancellation(e) {
    props.setAuthPopupExistingCluster(false);
  }

  return (
    <div>
      <Alert
        confirmButtonText="Submit"
        cancelButtonText="Cancel"
        isOpen={props.popupExistingCluster}
        onCancel={(e) => onCancellation(e)}
        onConfirm={(e) => onConfirmation(e)}
        intent={Intent.PRIMARY}
        loading={false}
      >
        <div className="authentication" style={{ display: 'flex', flexDirection: 'column' }}>
          <p>You MUST provide the SSH credentials to initiate the installation process of Dashboard on cluster "{props.clusterDetailsForExistingCluster.clusterName}". Make sure that IPs provided have the same credentials.</p>
          <label>SSH Username<span style={{ color: '#ff0000' }}> *</span></label>
          <input onChange={(e) => handleSSHdetailsChange(e)} id="userName" value={credentials.userName} name="userName" type="text" />
          <label>SSH Password<span style={{ color: '#ff0000' }}> *</span></label>
          <input onChange={(e) => handleSSHdetailsChange(e)} id="password" value={credentials.password} name="password" type="password" />
        </div>
      </Alert>
    </div>
  );
}


