import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link,
    Navigate,
  } from 'react-router-dom';
 
import Header from './Header';
import SignIn from './SignIn';
import SignUp from './SignUp';
import Home from './Home';
import Create from './Create';
import View from './View';
import Edit from './Edit';
import Dashboard from './Dashboard';
import PageNotFound from './PageNotFound';
import ChangePassword from './ChangePassword';

import AuthToken from '../utils/Authorization/AuthToken';
import { ProtectedRoute } from '../utils/Authorization/ProtectedRoute';



class Main extends Component {

    constructor(props) {
        super(props);

        this.state = {
            login: "NOT_LOGGED_IN"
        };

        this.setLoginState.bind(this);
        this.handleLogout.bind(this);
    }

    componentDidMount() {
        if (AuthToken.getToken()) {
            this.setState({
                login: "LOGGED_IN"
            })
            return <Navigate to="/" />
        }
    }

    setLoginState = (loginStatus) => {
        this.setState({
            login: loginStatus
        });
    }
    
    handleLogout() {
        console.log("logout called");
        AuthToken.removeToken();
        this.setState({
            login: "NOT_LOGGED_IN"
        })
        
    }

    render() {
        return (
            <div className="wrapper">
                <Header {...this.state} avatar="" title="Me" logout={() => this.handleLogout()} />
                <Routes >
                    <Route exact path="/signIn" element={<SignIn setLoginState={this.setLoginState} loggedIn={this.state.login}/>}></Route>
                    <Route path='/signUp' element={ <SignUp/> }></Route>
                    <Route path='/' element={<ProtectedRoute/>} >
                        <Route path='/' element={ <Home/> }></Route>
                        <Route path='/create' element={ <Create/> }></Route>
                        <Route path='/edit/:clusterName' element={ <Edit/> }></Route>
                        <Route path='/dashboard' element={ <Dashboard/> }></Route>
                        <Route path='/view/:clusterName' element={ <View/> }></Route>
                        <Route path='/changePassword' element={ <ChangePassword/> }></Route>
                        <Route path='*' element={ <PageNotFound/> }></Route>
                    </Route>
                </Routes>
            </div>
        );
    }
}

export default Main;