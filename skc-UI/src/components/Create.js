import React, { useEffect, useState } from 'react';
import { useNavigate, useLocation } from "react-router-dom";

import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

import '../css/Create.css';
import CheckboxRenderer from '../utils/Create/CheckboxRenderer';
import GroupNodesPopUp from '../utils/Create/GroupNodesPopUp';
import UngroupNodesPopUp from '../utils/Create/UngroupNodesPopUp';
import RemoveWorkerRolePopUp from '../utils/Create/RemoveWorkerRolePopUp';
import ErrorIconRenderer from '../utils/Create/ErrorIconRenderer'
import MultiSelectDropdown from '../utils/Create/MultiSelectDropdown';
import AuthenticationPopup from '../utils/Create/AuthenticationPopUp';
import LastConfirmationPopUp from '../utils/Create/LastConfirmationPopUp';
import ErrorAlert from '../utils/Create/ErrorAlert';
import SuccessAlert from '../utils/Create/SuccessAlert';
import ActionCellRenderer from '../utils/Create/ActionCellRenderer';
import NodeGroupCellRenderer from '../utils/Create/NodeGroupCellRenderer';
import InformationPopover from '../utils/Create/InformationPopover';
import Footer from './Footer';



export default function Create() {

    const delay = ms => new Promise(res => setTimeout(res, ms));

    // Warning Alert Message - Different alert message for each validation error
    const [warningMessage, setWarningMessage] = React.useState(null);
    const [errorAlert, setErrorAlert] = React.useState(false);

    const [successAlert, setSuccessAlert] = React.useState(false); 

    const [disabled, setDisabled] = React.useState(false);

    const [groupNodesPopup, setGroupNodesPopup] = React.useState(false);

    const [ungroupNodesPopup, setUngroupNodesPopup] = React.useState(false);
    const [removeWorkerRolePopup, setRemoveWorkerRolePopup] = React.useState(false);

    // Authentication Popup state 
    const [popup, setPopup] = React.useState(false); 

    // Last Confirmation Popup state
    const [lastPopup, setLastPopup] = React.useState(false); 

    const [anchorEl, setAnchorEl] = React.useState(null);

    const [arrowIcon, setArrowIcon] = React.useState(<ArrowDropDownIcon/>);

    const [errorIconRendererState, setErrorIconRendererState] = React.useState('');
    const [errorNodeIPs, setErrorNodeIPs] = useState([]); 

    const [advancedSettings, setAdvancedSettings] = React.useState('none');
    const [registryOuterDivDisplayState, setRegistryOuterDivDisplayState] = React.useState('none');
    const [registryInnerVipDivDisplayState, setRegistryInnerVipDivDisplayState] = React.useState('none');

    const [informationContent, setInformationContent] = React.useState("");
    
    const [data, setData] = useState({
        clusterId : "",
        clusterName : "",
    });

    const [details, setDetails] = useState({
        workerNodeLabel : "worker",
        tmpStorageLocation : "/SS8/tmp",
        isDualStackEnabled : true,
        kube_apiserver_node_port_range: "\"30000-32767\"",
        kubelet_storage: "/var/lib/kubelet",
        dockerStoragePath: "/var/lib/docker",
        isRegistryEnabled : false,
        inventoryPath : "",
        dashboardId : ""
    });

    const [workerSubGroups, setWorkerSubGroups] = React.useState(new Map(
        [
        ["ADMF", {
            name: "ADMF",
            nodes: [],
            groupVars: {
                node_labels: `{\"kubernetes.io/role\":\"${details.workerNodeLabel}\" }`
            }
        }],
        ["DF2", {
            name: "DF2",
            nodes: [],
            groupVars: {
                node_labels: `{\"kubernetes.io/role\":\"${details.workerNodeLabel}\" }`
            }
        }]
    ]));


    const [registry, setRegistry] = useState({
        nodes: [],
        name: "registry_nodes",
        port: "5000",
        volumePath: "",
        containerName: "ss8-docker-registry",
        isRegistryVIPEnabled: false 
    });

    const [vip, setVip] = useState({
        interface: "",
        ip: "",
        vrrId: ""
    });

    const [rowData, setRowData] = useState([
        {"NodeName": "node-01", "NodeIP": "0.0.0.0", "Controller": true, "Etcd": true, "Worker": true, "NodeGroup": ""}
    ]);

    function test() {
        for(let i=0; i<=100; i++) {
            gridApi.applyTransaction({ add : [{NodeName: `node-${i}`, NodeIP: `10.0.157.${i}`, Controller: false , Etcd: false, Worker: true, NodeGroup: ""}]});
        }
        let newRowData = [];
        gridApi.forEachNode(node=> newRowData.push(node.data));
        setRowData(newRowData);
    }

    const [gridApi, setGridApi] = useState(null);
    const [gridColumnApi, setGridColumApi] = useState(null);

    const isRowSelectable = (node)=> {
        return node.data.Worker;
    }

    const workerValueChanged = (e)=>{
        if(e.node.data.NodeGroup !== "") {
            console.log(gridApi.getEditingCells());
            setRemoveWorkerRolePopup(true);
        }
    }
 
    const columnDefs = [
        {field: "", checkboxSelection: isRowSelectable, flex: 0.18, floatingFilter: false},
        {headerName: "Node Name", field: "NodeName", cellRenderer: errorIconRendererState, onCellValueChanged:(e)=> nodeIPOrNameChanged(e), flex: 1},
        {headerName: "Node IP", field: "NodeIP", cellRenderer: 'errorIconRenderer', cellRendererParams: {errorNodeIPs:errorNodeIPs,setErrorNodeIPs:setErrorNodeIPs}, onCellValueChanged:(e)=> nodeIPOrNameChanged(e), cellStyle: params=> !params.value.match(/(\d[.])+/) ? params.value != "" ? {color: 'red'} : null : null, flex: 0.75},
        {headerName: "Controller", field: "Controller", cellRenderer: 'checkboxRenderer', flex: 0.5},
        {headerName: "Etcd", field: "Etcd", cellRenderer: 'checkboxRenderer', flex: 0.5},
        {headerName: "Worker", field: "Worker", cellRenderer: 'checkboxRenderer', onCellValueChanged:(e)=> workerValueChanged(e), flex: 0.5},
        {headerName: "Node Group", field: "NodeGroup", flex: 1.2},
        {headerName: "Action", colId: "Action", editable: false, flex: 0.6, floatingFilter: false, cellRenderer: ActionCellRenderer, onCellClicked:(e)=> deleteRow(e)},
    ];

    const defaultColDef = {
        sortable: true,
        editable: !disabled,
        filter: true,
        floatingFilter: true,
    };

    const components = {
        checkboxRenderer: CheckboxRenderer,
        errorIconRenderer: ErrorIconRenderer
    }

    const onGridReady = (params)=> {
        setGridApi(params.api);
        setGridColumApi(params.columnApi);
    };

    useEffect(()=> {
        const newDetailsData = {...details, isRegistryEnabled: false};
        setDetails(newDetailsData);
        setRegistryOuterDivDisplayState('none');
    },[rowData]);

    function handleClusterNameChange(e) {
        const newData = {...data};
        newData[e.target.id] = e.target.value;
        setData(newData);
        e.target.style.borderColor = !e.target.value.match(/(^[a-z][a-z0-9.-]{1,245})([a-z0-9])$/) ? 'red' : '#DBDCDC';
    }

    function handleAdditionalSettingsChange(e) {
        const newDetailsData = {...details};
        newDetailsData[e.target.id] = e.target.type === 'checkbox' ? e.target.checked ? true : false : e.target.value;
        setDetails(newDetailsData);
        if(e.target.id === 'kube_apiserver_node_port_range') {
            e.target.style.borderColor = !e.target.value.match(/^\".+\"$/) ? 'red' : '#DBDCDC';
        }
        if(e.target.id === 'isRegistryEnabled') {
            setRegistryOuterDivDisplayState(e.target.checked ? 'block' : 'none');
        }
    }

    function handleRegistryChange(e) {
        const newRegistryData = {...registry};
        newRegistryData[e.target.id] = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
        setRegistry(newRegistryData);
    }

    function handleRegistryVIPChange(e) {
        const newVipData = {...vip};
        newVipData[e.target.id] = e.target.value;
        setVip(newVipData);
    }

    async function isNodeIPOrNameDuplicated(rowIndex, nodeName, ip) {
        var isValid = true, nodeNameMatch = true;
        gridApi.forEachNode(node => {
            if(node.rowIndex !== rowIndex) {
                if((nodeName !== "" && (nodeName === node.data.NodeName)) ||(ip !== "" && (ip === node.data.NodeIP))) isValid = false;
                if(!nodeName.match(/(^[a-z][a-z0-9.-]{1,60})([a-z0-9])$/)) nodeNameMatch = false;
            }
        })
        if(!nodeNameMatch) {
            setWarningMessage("Node Name is not appropriate. Please review entries.");
            setErrorAlert(true);
            await delay(4000);
            setErrorAlert(false);
        }
        else if(!isValid) {
            setWarningMessage("Node IP and Node Name both must be unique in the list. Please review entries.");
            setErrorAlert(true);
            await delay(4000);
            setErrorAlert(false);
        }
        return;
    }

    async function nodeIPOrNameChanged(e) {
            await isNodeIPOrNameDuplicated(e.node.rowIndex, e.data.NodeName, e.data.NodeIP);      
    }

    async function nodeIPValidation() {
        let nodeIPValid = true; var tempNode = [];

        for(let i=0; i<rowData.length; i++) {
            if(
                !rowData[i].NodeIP.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)
                &&
                !rowData[i].NodeIP.match(/^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){5}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)
            ) {
                nodeIPValid = false;
                tempNode.push(rowData[i]);
            }
        }
        setErrorNodeIPs(tempNode);
        if(!nodeIPValid) {
            setWarningMessage(`Node IPs might not be valid. Please review entries.`);
            setErrorAlert(true);
            await delay(4000);
            setErrorAlert(false);
        }
        return nodeIPValid;
    }

    async function nodeNameValidation() {
        let nodeNameValid = true;

        for(let i=0; i<rowData.length; i++) {
            if(!rowData[i].NodeName.match(/(^[a-z][a-z0-9.-]{1,60})([a-z0-9])$/)) {
                nodeNameValid = false;
                break;
            }
        }
        if(!nodeNameValid) {
            setWarningMessage("Node Names might not be valid. Please review entries.");
            setErrorAlert(true);
            await delay(4000);
            setErrorAlert(false);
        }
        return nodeNameValid;
    }

    const addNewRow = ()=> {
        gridApi.applyTransaction({ add : [{NodeName: "", NodeIP: "", Controller: false , Etcd: false, Worker: true, NodeGroup: ""}]});
        let newRowData = [];
        gridApi.forEachNode(node=> newRowData.push(node.data));
        setRowData(newRowData);
    }

    const deleteRow = (e)=> {
        gridApi.applyTransaction({ remove : [e.data] });
        let newRowData = [];
        gridApi.forEachNode(node=> newRowData.push(node.data));
        setRowData(newRowData);
    }

    const selectedRowsForGrouping = async()=> {
        if(gridApi.getSelectedNodes().length > 0)
            setGroupNodesPopup(true);
        return;
    }

    const selectedRowsForUngrouping = ()=> {
        if(gridApi.getSelectedNodes().length > 0)
            setUngroupNodesPopup(true);     
        return;
    }

    async function eachNodeMustHaveAtleastOneRoleValidation(node) {
        if(node.Controller || node.Etcd || node.Worker || (registry.nodes.indexOf(node.NodeName) > -1)) {
            return true;
        }
        else {
            setWarningMessage("Each node must have atleast one role ie Controller, Etcd, Worker or Registry");
            setErrorAlert(true);
            await delay(4000);
            setErrorAlert(false);
        }
        return false;
    }

    async function formationOfNodeRolesDataFromRowDataState() {

        var result = true;

        for(let i=0; i<rowData.length; i++) {

            result = eachNodeMustHaveAtleastOneRoleValidation(rowData[i]);
            if(!result) break;

            if(rowData[i].Controller) {
                groups[0].nodes.push(rowData[i].NodeName);
            }
            if(rowData[i].Etcd) {
                groups[2].nodes.push(rowData[i].NodeName);
            }
            if(rowData[i].Worker) {
                groups[1].nodes.push(rowData[i].NodeName);
            }
        }
        return result;
    } 

    async function isAnyNodeRolesColumnEmpty() {

        if(groups[0].nodes.length > 0 && groups[1].nodes.length > 0 && groups[2].nodes.length > 0) {
            if(groups[2].nodes.length % 2 !== 0) {
                return true;
            }
            else {
                setWarningMessage("For Etcd nodes selection keep odd numbers");
                setErrorAlert(true);
                await delay(4000);
                setErrorAlert(false);
            }
        }
        else {
            setWarningMessage("Any role column must not be empty.");
            setErrorAlert(true);
            await delay(4000);
            setErrorAlert(false);
        }

        return false;
    }

    async function clusterNameValidation() {

        if(!data.clusterName.match(/(^[a-z][a-z0-9.-]{1,245})([a-z0-9])$/)) {
            setWarningMessage("Cluster Name is either empty or inappropriate. Please review entry.");
            setErrorAlert(true);
            return false;
        }
        return true;
    }

    function toggleAdvancedSettingsDisplay() {
        if(advancedSettings === 'none') {
            setArrowIcon(<ArrowDropUpIcon/>)
            setAdvancedSettings('block');
        }
        else {
            setArrowIcon(<ArrowDropDownIcon/>);
            setAdvancedSettings('none');
        }
    }

    var groups = [
            {
                "name": "kube_controller_nodes",
                "nodes": [],
                "labels": {}
            },
            {
                "name": "kube_worker_nodes",
                "nodes": [],
                "labels": {
                    "node_labels": `{\"kubernetes.io/role\":\"${details.workerNodeLabel}\" }`
                },
                "subGroups": workerSubGroups
            },
            {
                "name": "etcd",
                "nodes": [],
                "labels": {}
            }
    ];

    var initialGroupsState = [
        {
            "name": "kube_controller_nodes",
            "nodes": [],
            "labels": {}
        },
        {
            "name": "kube_worker_nodes",
            "nodes": [],
            "labels": {
                "node_labels": `{\"kubernetes.io/role\":\"${details.workerNodeLabel}\" }`
            }
        },
        {
            "name": "etcd",
            "nodes": [],
            "labels": {}
        }
];

    async function clickedOnCreateClusterButton(e) {
        // gridApi.refreshCells();
        const  resultOfclusterNameValidation = await clusterNameValidation();
        
        if(resultOfclusterNameValidation) {
            const resultOfeachNodeMustHaveAtleastOneRole = await formationOfNodeRolesDataFromRowDataState();
            
            if(resultOfeachNodeMustHaveAtleastOneRole) {
                const resultOfisAnyNodeRolesColumnEmpty = await isAnyNodeRolesColumnEmpty();
                
                if(resultOfisAnyNodeRolesColumnEmpty) {
                    const resultOfIPValidation = await nodeIPValidation();
                    
                    if(resultOfIPValidation) {
                        const resultOfNodeNameValidation = await nodeNameValidation();
                        
                        if(resultOfNodeNameValidation) {
                            
                            if((!details.isRegistryEnabled) || ((0 < registry.nodes.length) && (registry.nodes.length <= 2))) {
                                setPopup(true);
                            }
                            else {
                                setWarningMessage("Registry nodes can not be empty if Registry is enabled.");
                                setErrorAlert(true);
                                await delay(4000);
                                setErrorAlert(false);
                            }

                        }
                    }
                }
            }
            groups = initialGroupsState;
        }
    }

    const handleOpen = (event)=> {
        if(event.currentTarget.id === 'tmpStorageLocationHeading') setInformationContent("This is the parent directory under which all the offline packages/rpms will be copied. Must have at least 1.5 GB of space");
        if(event.currentTarget.id === 'workerNodeLabelHeading') setInformationContent("Label to group the worker nodes.");
        if(event.currentTarget.id === 'kube_apiserver_node_port_range') setInformationContent("A port range to reserve for services with NodePort visibility. Default kube api-server nodeport range is \"30000-32767\"");
        setAnchorEl(event.currentTarget)
    }
 
    const handleClose = () => {
        setAnchorEl(null);
      };

    let navigate = useNavigate();
    function clickedOnCancelNewClusterButton(e) {
        let path = `/`; 
        navigate(path);
    }

    return (
        <div className="main-body">
            <SuccessAlert
                successAlert={successAlert}
                data={data}
            />
            <div className="top-most">
                <p>New Cluster</p>
                <ErrorAlert
                    errorAlert={errorAlert}
                    setErrorAlert={setErrorAlert}
                    warningMessage={warningMessage}
                />
                <div className="buttons">
                    {/* <button onClick={test}>Testing</button> */}
                    <button type="reset" className="btn-cancelCluster" onClick={(e)=> clickedOnCancelNewClusterButton(e)}>Cancel</button>
                    <button type="button" className="btn-create" onClick={(e)=> clickedOnCreateClusterButton(e)}>Create Cluster</button>
                </div>
            </div>

            <div className="input">
                <p style={{marginBottom: 1}}>Cluster Name<span style={{color:'#ff0000'}}> *</span></p>
                <input disabled={disabled} onChange={e=> handleClusterNameChange(e)} id="clusterName" value={data.clusterName} type="text" name="clusterName"/>
                <p style={{color: '#5C7080', fontSize: 12}}>Max 250 characters with no spaces. Allowed only lower case alphanumeric characters, hyphen and period is allowed but shouldn't start or end with it</p>
                <div className="information">
                    <p style={{color: '#182026', fontWeight: 600, marginTop: 12, marginBottom: 8}}>Nodes</p>
                    <p style={{marginBottom: 0}}>Add at least one node to the cluster.</p>
                    <p>For Etcd nodes selection keep odd numbers </p>
                </div>
            </div>

            <div>
                <div className="addRow-groupUngroup-buttons">
                    <button className="add-row-button" type="button" onClick={()=> addNewRow()}><AddCircleOutlineIcon style={{fontSize: 15, marginTop: 3, marginRight: 4, color: '#5C7080'}}/>Add Node</button>
                    <div className="groupUngroup-button">
                        <button type="button" onClick={()=> selectedRowsForUngrouping()}>Ungroup</button>
                        <button type="button" onClick={()=> selectedRowsForGrouping()}>Group node</button>
                    </div>
                </div>
                <div>
                    <GroupNodesPopUp
                        groupNodesPopup={groupNodesPopup}
                        setGroupNodesPopup={setGroupNodesPopup}
                        workerSubGroups={workerSubGroups}
                        gridApi={gridApi}
                        rowData={rowData}
                        details={details}
                        setRowData={setRowData}
                    />
                    <UngroupNodesPopUp
                        ungroupNodesPopup={ungroupNodesPopup}
                        setUngroupNodesPopup={setUngroupNodesPopup}
                        workerSubGroups={workerSubGroups}
                        gridApi={gridApi}
                        rowData={rowData}
                        setRowData={setRowData}
                    />
                    <RemoveWorkerRolePopUp
                        removeWorkerRolePopup={removeWorkerRolePopup}
                        setRemoveWorkerRolePopup={setRemoveWorkerRolePopup}
                    />
                    <AuthenticationPopup 
                        popup={popup} 
                        setPopup={setPopup} 
                        lastPopup={lastPopup}
                        setLastPopup={setLastPopup}
                        setErrorAlert={setErrorAlert} 
                        setWarningMessage={setWarningMessage} 
                        setSuccessAlert={setSuccessAlert}
                        data={data}
                    />
                    <LastConfirmationPopUp
                        popup={popup} 
                        setPopup={setPopup} 
                        lastPopup={lastPopup}
                        setLastPopup={setLastPopup}
                        setErrorAlert={setErrorAlert} 
                        setWarningMessage={setWarningMessage} 
                        setSuccessAlert={setSuccessAlert}
                        setDisabled={setDisabled}
                        rowData={rowData}
                        data={data}
                        workerSubGroups={workerSubGroups}
                        details={details} 
                        registry={registry} 
                        vip={vip} 
                        
                    />
                </div>
                <div className="ag-theme-balham" style={{height: 190, marginTop: 10}}>
                    <AgGridReact
                        rowData={rowData}
                        columnDefs={columnDefs}
                        defaultColDef={defaultColDef}
                        components={components}
                        onGridReady={onGridReady}
                        rowSelection= 'multiple'
                        suppressRowClickSelection= {true}
                    />
                </div> 
                <div className="div-above-advanced">
                <p style={{marginBottom: 0}}><b>NOTE :</b> For <b>Node Name</b> max length is 63 characters with no spaces and only lower case alphanumeric, hyphen and period characters are allowed.</p> 
                <p>Click on a row for inline editing.</p>
                </div>
            </div>
                        
            {/* ///////////////// */}
            {/* Advanced Settings */}
            {/* ///////////////// */}

            <button type="button" id="advancedSettingsButton" onClick={()=> toggleAdvancedSettingsDisplay()}>Advanced Settings {arrowIcon}</button>
            <div style={{display: advancedSettings}}>
            <InformationPopover anchorEl={anchorEl} handleClose={handleClose} informationContent={informationContent}/>
                <div style={{marginTop: 22}} className="advancedSettingCommonStyleDiv">
                    <div>
                        <p>Worker Node Label <button id="workerNodeLabelHeading" style={{background: 'none', border: 'none'}} onClick={(event)=> handleOpen(event)}><InfoOutlinedIcon fontSize='inherit' color='info'/></button></p>
                        <input disabled={disabled} type="text" onChange={e=> handleAdditionalSettingsChange(e)} id="workerNodeLabel" value={details.workerNodeLabel}/>
                    </div>
                    <div>
                        <p>Temporary Storage Location <button id="tmpStorageLocationHeading" style={{background: 'none', border: 'none'}} onClick={(event)=> handleOpen(event)}><InfoOutlinedIcon fontSize='inherit' color='info'/></button></p>
                        <input disabled={disabled} type="text" onChange={e=> handleAdditionalSettingsChange(e)} id="tmpStorageLocation" value={details.tmpStorageLocation}/>
                    </div>
                    <div>
                        <p style={{marginBottom: 25}}></p>
                        <input disabled={disabled}  type="checkbox" onChange={e=> handleAdditionalSettingsChange(e)} checked={details.isDualStackEnabled} value={details.isDualStackEnabled} id="isDualStackEnabled"/>
                        <label>Enable Dual Stack IPv4/IPv6 addressing</label>
                    </div>
                </div>
                <div className="advancedSettingCommonStyleDiv">
                    <div>
                        <p>Kube API Server Node Port Range <button id="kube_apiserver_node_port_range" style={{background: 'none', border: 'none'}} onClick={(event)=> handleOpen(event)}><InfoOutlinedIcon fontSize='inherit' color='info'/></button></p>
                        <input disabled={disabled} type="text" onChange={e=> handleAdditionalSettingsChange(e)} id="kube_apiserver_node_port_range" value={details.kube_apiserver_node_port_range}/>
                    </div>
                    <div>
                        <p>Kubelet Storage Directory Path</p>
                        <input disabled={disabled} type="text" onChange={e=> handleAdditionalSettingsChange(e)} id="kubelet_storage" value={details.kubelet_storage}/>
                    </div>
                    <div>
                        <p>Docker Storage Directory Path</p>
                        <input disabled={disabled} type="text" onChange={e=> handleAdditionalSettingsChange(e)} id="dockerStoragePath" value={details.dockerStoragePath}/>
                    </div>
                </div>

                <div className="advancedSettingCheckbox">
                    <input disabled={disabled} type="checkbox" onChange={e=> handleAdditionalSettingsChange(e)} checked={details.isRegistryEnabled} value={details.isRegistryEnabled} className="checkbox"  id="isRegistryEnabled" />
                    <label>Enable Registry</label>
                </div>
                
                <div style={{display: registryOuterDivDisplayState}}>
                    <div className="advancedSettingCommonStyleDiv">
                        <div>
                            <p>Nodes<span style={{color:'#ff0000'}}> *</span></p>
                            <MultiSelectDropdown 
                                rowData={rowData}
                                registry={registry}
                                setRegistry={setRegistry}
                                setErrorAlert={setErrorAlert}
                                setWarningMessage={setWarningMessage}
                                setRegistryInnerVipDivDisplayState={setRegistryInnerVipDivDisplayState}
                            />
                        </div>
                    </div>
                    <div className="advancedSettingCommonStyleDiv">
                        <div>
                            <p>Docker Registry Container Name</p>
                            <input disabled={disabled} type="text" onChange={e=> handleRegistryChange(e)} id="containerName" value={registry.containerName}/>
                        </div>
                        <div>
                            <p>Registry Port</p>
                            <input disabled={disabled} type="text" onChange={e=> handleRegistryChange(e)} id="port" value={registry.port}/>
                        </div>
                        <div>
                            <p>Registry Volume Path</p>
                            <input disabled={disabled} type="text" onChange={e=> handleRegistryChange(e)} id="volumePath" value={registry.volumePath}/>
                        </div>
                    </div>
                    <div style={{display: registryInnerVipDivDisplayState}}>
                        <div className="advancedSettingCommonStyleDiv">
                            <div>
                                <p>Registry Interface</p>
                                <input disabled={disabled} type="text" onChange={e=> handleRegistryVIPChange(e)} id="interface" value={vip.interface}/>
                            </div>
                            <div>
                                <p>Registry VIP</p>
                                <input disabled={disabled} type="text" onChange={e=> handleRegistryVIPChange(e)} id="ip" value={vip.ip}/>
                            </div>
                            <div>
                                <p>Registry VRR ID</p>
                                <input disabled={disabled} type="text" onChange={e=> handleRegistryVIPChange(e)} id="vrrId" value={vip.vrrId}/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>  
            </div>               
        </div>
    );
}

 
