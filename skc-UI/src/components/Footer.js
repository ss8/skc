import React from 'react'

const Footer = () => {
  return (
    <div style={{display: 'flex', flexDirection: 'column'}}>
        <hr style={{height: 1, backgroundColor: '#DBDCDC', marginTop: 20, marginBottom: 10}}/>
        <p style={{display: 'flex', justifyContent: 'center'}}>SS8 Networks ©</p>
    </div>
  )
}

export default Footer