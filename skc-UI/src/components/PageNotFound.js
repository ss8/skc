import React from 'react'
import { useLocation, useNavigate } from 'react-router-dom'


const PageNotFound = ()=> {
  var navigate = useNavigate();
  let location = useLocation();

  let buttonStyle = {
      position: 'fixed', zIndex: 1, 
      marginTop: '71vh', marginLeft: '47%',
      backgroundColor: 'white',
      borderColor: '#137CBD', borderWidth: '2px', borderRadius: '15px',
      fontSize: '18px', fontFamily: 'Titillium Web',
      paddingLeft: 10, paddingRight: 10, 
  }

  return (
    <div style={{display: 'flex', flexDirection: 'column'}}>
        <img style={{width: '100vw', height: '80vh', objectFit: 'contain'}} src="/404-pages.jpg" alt='404 - Page Node Found'/>
        <button style={buttonStyle} onClick={()=> navigate(`/`)}>Home Page</button>
    </div>
  )
}

export default PageNotFound;