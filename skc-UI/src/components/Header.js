import React, { useEffect, useState } from 'react'
import { useNavigate } from "react-router-dom";

import { Avatar } from '@mui/material';

import '../css/Header.css';
import HeaderAvatarPopover from '../utils/Header/HeaderAvatarPopover';

export default function Header(props) {
    let navigate = useNavigate();  

    const [anchorEl, setAnchorEl] = React.useState(null);

    var signInSignOutButtonText = props.login === 'LOGGED_IN' ? 'Sign out' : 'Sign in';

    var isUserLoggedIn = props.login === 'LOGGED_IN' ? true : false;

    function handleSignInSignOutButton() {
        if(props.login === 'LOGGED_IN')
            props.logout();    
        navigate(`/signIn`);
    }

    function clickedOnSS8Icon() {
        navigate(`/`);
    }

    const handleClose = () => {
        setAnchorEl(null);
      };

    return (
        <div className="header">
            <div className="header_left" onClick={clickedOnSS8Icon}>
                <img src="/logo-ss8.svg" alt="SS8 Icon"/>
                <p style={{marginBottom: 1, fontWeight: 100}}>Kubernetes Cluster</p>
            </div>

            <div className="header_right">
                <button onClick={handleSignInSignOutButton}>{signInSignOutButtonText}</button>
                <Avatar className="avatar" sx={{ width: 25, height: 25 }} onClick={(e)=>setAnchorEl(e.target)}>
                    {localStorage.hasOwnProperty("username") ? isUserLoggedIn ? localStorage.getItem("username")[0] : null : null}
                </Avatar>
                <HeaderAvatarPopover
                    anchorEl={anchorEl} 
                    handleClose={handleClose}
                />
            </div>
        </div>
    )
}
