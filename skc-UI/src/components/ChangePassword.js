import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';

import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

import AxiosDashbaord from '../utils/CustomDashboardAxios';
import SuccessAlert from '../utils/Home/SuccessAlert';
import ErrorAlert from '../utils/Home/ErrorAlert';
import { actionCreators } from '../state/index';


const theme = createTheme();

export default function ChangePassword() {

  let navigate = useNavigate();
  const dispatch = useDispatch();
  const {openSuccessAlert, openErrorAlert, alertMessage} = bindActionCreators(actionCreators, dispatch); 

  const delay = ms=> new Promise(res=> setTimeout(res,ms));

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = new FormData(event.currentTarget);

    if(data.get('newPasswordAgain') === data.get('newPassword')) {
    
      const infoForChangePassword = {
        username: data.get('username'),
        oldPassword: data.get('oldPassword'),
        newPassword: data.get('newPassword'),
      };

      if(infoForChangePassword.username != '' && infoForChangePassword.oldPassword != '' & infoForChangePassword.newPassword != '') {
        AxiosDashbaord.post('/user/changePassword', infoForChangePassword)
        .then(async res=> {
          if(res.data == true) {
            alertMessage("Your password has successfully been changed");
            openSuccessAlert(true);
            await delay(3000);
            openSuccessAlert(false);
            navigate(`/`);
          }
          else console.log("inside else")
        })
        .catch(async error=> {
          alertMessage("Your username or old password was incorrect");
          openErrorAlert(true);
          await delay(3000);
          openErrorAlert(false);
        })
      }
    }
    else {
      alertMessage("New Password and Re-entered Password do not match")
      openErrorAlert(true);
      await delay(3000);
      openErrorAlert(false);
    }
  };

  return (
    <div>
      <SuccessAlert/>
      <ErrorAlert/>
      <div>
      <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: '' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Change Password
            </Typography>
            <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <TextField
                    autoComplete="username"
                    required
                    name="username"
                    required
                    fullWidth
                    id="username"
                    label="Username"
                    autoFocus
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <TextField
                    required
                    fullWidth
                    id="oldPassword"
                    name="oldPassword"
                    label="Old Password"
                    type="password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="newPassword"
                    name="newPassword"
                    label="New Password"
                    type="password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <TextField
                    required
                    fullWidth
                    id="newPasswordAgain"
                    name="newPasswordAgain"
                    label="Re-enter New Password"
                    type="password"
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={4}>
                  <Button
                    type="reset"
                    fullWidth
                    variant="contained"
                    style={{backgroundColor: 'white', color: '#454B4F'}}
                    sx={{ mt: 3, mb: 2 }}
                    onClick={()=>navigate(`/`)}
                  >
                    Cancel
                  </Button>
                </Grid>
                <Grid item xs={12} sm={8}>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Update Password
                  </Button>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Container>
      </ThemeProvider>
      </div>
    </div>
  );
}