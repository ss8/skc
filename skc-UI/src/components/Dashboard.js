import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';

import ClusterDrawer from '../utils/Dashboard/ClusterDrawer'

import AxiosDashboard from '../utils/CustomDashboardAxios';
import AxiosCluster from '../utils/CustomAxios';
import PageNotFound from './PageNotFound';


export default function Dashboard() {
  var navigate = useNavigate()
  let { state } = useLocation(); // coming from utils/home/homePage3dotsPop
  const [url, setUrl] = React.useState('');
  const [buttonDisplay, setButtonDisplay] = useState('none');

  const [allControllerIPs, setAllControllerIPs] = useState([]);
  var clustersList = [];

  const delay = ms=> new Promise(res=> setTimeout(res, ms)); 

  AxiosCluster.get(`/cluster/list`)
  .then(res=> {
    res.data.map(clusterInfo=> clustersList.push(clusterInfo.clusterName))
  })
  .catch(err=> console.error(err))

  useEffect(()=>{
 
  AxiosCluster.get(`/cluster/${state.data.ClusterId}`)
  .then(async res=> {
    if(res.status == 200) {
      var tempAllControllerIPs = [];

      res.data.nodes.map(node=> {
        if(node.roles.indexOf("CONTROLLER") >= 0) tempAllControllerIPs.push(node.ip);
      })
      setAllControllerIPs(tempAllControllerIPs);

      return AxiosDashboard.post(`/dashboard/refresh/${state.data.ClusterName}`, allControllerIPs)
    }
  })
  .then(res=> {
    if(res.status == 200) 
      return AxiosDashboard.get(`/dashboard/list?search=${state.data.ClusterName}`)
  }) 
  .then(async res=> {
    if(res.status === 200) {
      let dashboardUrl = res.data.list_data.dashboard_list.length !=0 ? res.data.list_data.dashboard_list[0].url : undefined;     
      setUrl(dashboardUrl);
      await delay(3000);
      setButtonDisplay('inline-block');
    }
  })
  .catch(error=> {
    if(error.response.status == 404) {
      setUrl(undefined);
    }
  })
  },[])


  return (
    url == undefined ?
      <PageNotFound/>
    :
      <div>
        <ClusterDrawer buttonDisplay={buttonDisplay} clustersList={clustersList}/>
        <div style={{ height: "92vh", width: "100vw", marginTop: -10 }}>
          <iframe title="dashboard" src={url} width="100%" height="100%" />
        </div>
      </div>      
  );
}
