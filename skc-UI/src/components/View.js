import React, { useEffect, useState } from 'react';
import { useNavigate, useLocation } from "react-router";

import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AgGridColumn, AgGridReact } from 'ag-grid-react';
import ArrowDropUpIcon from '@mui/icons-material/ArrowDropUp';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';

import '../css/View.css';
import Footer from './Footer';
import TickCrossRenderer from '../utils/Home/TickCrossRenderer';
import MultiSelectDropdown from '../utils/View/MultiSelectDropdown';
import ErrorAlert from '../utils/Create/ErrorAlert';
import SuccessAlert from '../utils/Create/SuccessAlert';
import AuthenticationPopupForDelete from '../utils/Delete/AuthenticationPopUpForDelete';
import LastConfirmationPopUpForDelete from '../utils/Delete/LastConfirmationPopUpForDelete';
import PopUpEdit from '../utils/View/PopUpEdit';
import Axios from '../utils/CustomAxios';


export default function View() {

    let navigate = useNavigate();
    let {state} = useLocation();

    const disabled = true;

    // Warning Alert Message - Different alert message for each validation error
    const [warningMessage, setWarningMessage] = React.useState(null);
    const [errorAlert, setErrorAlert] = React.useState(false);

    const [successAlert, setSuccessAlert] = React.useState(false);

    // Authentication Popup for Delete state
    const [popupDelete, setPopupDelete] = React.useState(false); 
    
    const [popupEdit, setPopupEdit] = React.useState(false); 

    // Last Confirmation Popup state
    const [lastPopupDelete, setLastPopupDelete] = React.useState(false); 
    

    const [arrowIcon, setArrowIcon] = React.useState(<ArrowDropDownIcon/>);

    const [advancedSettings, setAdvancedSettings] = React.useState('none');
    const [registryOuterDivDisplayState, setRegistryOuterDivDisplayState] = React.useState('none');
    const [registryInnerVipDivDisplayState, setRegistryInnerVipDivDisplayState] = React.useState('none');
    
    const [data, setData] = useState({
        clusterId : "",
        clusterName : "",
    });

    const [details, setDetails] = useState({
        workerNodeLabel : "worker",
        tmpStorageLocation : "",
        isDualStackEnabled : false,
        kube_apiserver_node_port_range: "",
        kubelet_storage: "",
        dockerStoragePath: "",
        isRegistryEnabled : false,
        inventoryPath : "",
        dashboardId : ""
    });

    const [registry, setRegistry] = useState({
        nodes: [],
        name: "registry_nodes",
        port: "",
        volumePath: "",
        containerName: "",
        isRegistryVIPEnabled: false // ==============
    });

    const [vip, setVip] = useState({
        interface: "",
        ip: "",
        vrrId: ""
    });

    const [clusterToBeDeleted, setClusterToBeDeleted] = React.useState({
        ClusterId : "",
        ClusterName : "",
        DashboardId: ""
    });

    const [rowData, setRowData] = useState([]);

    const [gridApi, setGridApi] = useState(null);
    const [gridColumnApi, setGridColumApi] = useState(null);
 
    const columnDefs = [
        {field: "", flex: 0.15, floatingFilter: false},
        {headerName: "Node Name", field: "NodeName", flex: 1},
        {headerName: "Node IP", field: "NodeIP", flex: 0.75},
        {headerName: "Controller", field: "Controller", cellRenderer: 'tickCrossRenderer', flex: 0.5},
        {headerName: "Etcd", field: "Etcd", cellRenderer: 'tickCrossRenderer', flex: 0.5},
        {headerName: "Worker", field: "Worker", cellRenderer: 'tickCrossRenderer', flex: 0.5},
        {headerName: "Node Group", field: "NodeGroup", flex: 1.2},
        // {headerName: "", colId: "", flex: 0.6, floatingFilter: false},
    ];

    const defaultColDef = {
        sortable: true,
        editable: false,
        filter: true,
        floatingFilter: true,
    };

    const components = {
        tickCrossRenderer: TickCrossRenderer,
    }

    const onGridReady = (params)=> {
        setGridApi(params.api);
        setGridColumApi(params.columnApi);
    };

    useEffect(()=> {
        Axios.get(`/cluster/${state.data.ClusterId}`)
        .then(res=> {
            console.log(res.data);
            setData({
                clusterId: parseInt(res.data.clusterId),
                clusterName: res.data.clusterName
            });
            setClusterToBeDeleted({
                ClusterId: parseInt(res.data.clusterId),
                ClusterName: res.data.clusterName
            });
            setDetails({
                workerNodeLabel : "worker",
                tmpStorageLocation : res.data.tmpStorageLocation,
                isDualStackEnabled : res.data.isDualStackEnabled,
                kube_apiserver_node_port_range: res.data.kubeAPIServerPortRange,
                kubelet_storage: res.data.kubeletStoragePath,
                dockerStoragePath: res.data.dockerStoragePath,
                isRegistryEnabled : res.data.isRegistryEnabled,
                inventoryPath : res.data.inventoryPath,
                dashboardId : res.data.dashboardId
            });
            setRegistry({
                nodes: res.data.registry.nodes,
                name: "registry_nodes",
                port: res.data.registry.port,
                volumePath: res.data.registry.volumePath,
                containerName: res.data.registry.containerName,
                isRegistryVIPEnabled: res.data.registry.isRegistryVIPEnabled,
            });
            setVip({
                interface: "",
                ip: "",
                vrrId: ""
            });
            let newRowsData = [];
                res.data.nodes.map(node=> {
                    let controller = false, worker = false, etcd = false, registry = false;
                    node.roles.map(role=> {
                        if(role == 'CONTROLLER') controller =  true;
                        if(role == 'ETCD') etcd =  true;
                        if(role == 'WORKER') worker =  true;
                        if(role == 'Registry') registry =  true;
                    })
                    let rowData = {NodeName: node.name, NodeIP: node.nodeIp, Controller: controller, Etcd: etcd, Worker: worker, Registry: registry}
                    newRowsData.push(rowData);
                }); 
            setRowData(newRowsData);
        })
    },[]);

    function toggleAdvancedSettingsDisplay() {
        if(advancedSettings === 'none') {
            setArrowIcon(<ArrowDropUpIcon/>)
            setAdvancedSettings('block');
        }
        else {
            setArrowIcon(<ArrowDropDownIcon/>);
            setAdvancedSettings('none');
        }
        if(details.isRegistryEnabled) setRegistryOuterDivDisplayState('block');
        if(registry.isRegistryVIPEnabled) setRegistryInnerVipDivDisplayState('block');
    }

    function clickedOnEditClusterButton(e) {
    setPopupEdit(true);
    }

    function clickedOnDeleteClusterButton(e) {
      setPopupDelete(true);
    }

    function clickedOnBackButton(e) {
        let path = `/`; 
        navigate(path);
    }

    return (
        <div className="main-body">
            <div className="top-most">
                <p>View Cluster</p>
                <div className="buttons">
                    <button type="button" className="btn-modify" onClick={(e)=> clickedOnDeleteClusterButton(e)}>Delete</button>
                    <button type="button" className="btn-modify" onClick={(e)=> clickedOnEditClusterButton(e)}>Edit</button>
                    <button type="reset" className="btn-back" onClick={(e)=> clickedOnBackButton(e)}>Back</button>
                </div>
            </div>

            <div className="input">
                <p style={{marginBottom: 1}}>Cluster Name<span style={{color:'#ff0000'}}> *</span></p>
                <input disabled={true} id="clusterName" value={data.clusterName} type="text" name="clusterName"/>
                <div className="information">
                    <p style={{color: '#182026', fontWeight: 600, marginTop: 40, marginBottom: 8}}>Nodes</p>
                </div> 
            </div>
            <div> 
                <AuthenticationPopupForDelete 
                    popupDelete={popupDelete} 
                    setPopupDelete={setPopupDelete} 
                    lastPopupDelete={lastPopupDelete}
                    setLastPopupDelete={setLastPopupDelete}
                    setErrorAlert={setErrorAlert} 
                    setWarningMessage={setWarningMessage} 
                    setSuccessAlert={setSuccessAlert}
                    clusterToBeDeleted={clusterToBeDeleted}
                />
                <LastConfirmationPopUpForDelete
                    popupDelete={popupDelete} 
                    setPopupDelete={setPopupDelete} 
                    lastPopupDelete={lastPopupDelete}
                    setLastPopupDelete={setLastPopupDelete}
                    setErrorAlert={setErrorAlert} 
                    setWarningMessage={setWarningMessage} 
                    setSuccessAlert={setSuccessAlert}
                    clusterToBeDeleted={clusterToBeDeleted}
                />
                <PopUpEdit 
                    popupEdit={popupEdit}
                    setPopupEdit={setPopupEdit}
                    clusterToBeDeleted={clusterToBeDeleted}
                />
            </div>
            <div className="ag-theme-balham" style={{height: 190, marginTop: 10}}>
                <AgGridReact
                    rowData={rowData}
                    columnDefs={columnDefs}
                    defaultColDef={defaultColDef}
                    components={components}
                    onGridReady={onGridReady}
                    suppressRowClickSelection= {true}
                />
            </div> 
                        
            {/* ///////////////// */}
            {/* Advanced Settings */}
            {/* ///////////////// */}

            <button type="button" id="advancedSettingsButton" onClick={()=> toggleAdvancedSettingsDisplay()}>Advanced Settings {arrowIcon}</button>
            <div style={{display: advancedSettings}}>

                <div style={{marginTop: 22}} className="advancedSettingCommonStyleDiv">
                    <div>
                        <p>Worker Node Label</p>
                        <input disabled={disabled} type="text" id="workerNodeLabel" value={details.workerNodeLabel}/>
                    </div>
                    <div>
                        <p>Temporary Storage Location</p>
                        <input disabled={disabled} type="text" id="tmpStorageLocation" value={details.tmpStorageLocation}/>
                    </div>
                    <div>
                        <p style={{marginBottom: 25}}></p>
                        <input disabled={disabled}  type="checkbox" checked={details.isDualStackEnabled} value={details.isDualStackEnabled} id="isDualStackEnabled"/>
                        <label>Enable Dual Stack IPv4/IPv6 addressing</label>
                    </div>
                </div>
                <div className="advancedSettingCommonStyleDiv">
                    <div>
                        <p>Kube API Server Node Port Range</p>
                        <input disabled={disabled} type="text" id="kube_apiserver_node_port_range" value={details.kube_apiserver_node_port_range}/>
                    </div>
                    <div>
                        <p>Kubelet Storage Directory Path</p>
                        <input disabled={disabled} type="text" id="kubelet_storage" value={details.kubelet_storage}/>
                    </div>
                    <div>
                        <p>Docker Storage Directory Path</p>
                        <input disabled={disabled} type="text" id="dockerStoragePath" value={details.dockerStoragePath}/>
                    </div>
                </div>

                <div className="advancedSettingCheckbox">
                    <input disabled={disabled} type="checkbox" checked={details.isRegistryEnabled} value={details.isRegistryEnabled} className="checkbox"  id="isRegistryEnabled" />
                    <label>Enable Registry</label>
                </div>
                
                <div style={{display: registryOuterDivDisplayState}}>
                    <div className="advancedSettingCommonStyleDiv">
                        <div>
                            <p>Nodes<span style={{color:'#ff0000'}}> *</span></p>
                            <MultiSelectDropdown 
                                rowData={rowData}
                                registry={registry}
                                setRegistry={setRegistry}
                                // setErrorAlert={setErrorAlert}
                                // setWarningMessage={setWarningMessage}
                            />
                        </div>
                    </div>
                    <div className="advancedSettingCommonStyleDiv">
                        <div>
                            <p>Docker Registry Container Name</p>
                            <input disabled={disabled} type="text" id="containerName" value={registry.containerName}/>
                        </div>
                        <div>
                            <p>Registry Port</p>
                            <input disabled={disabled} type="text" id="port" value={registry.port}/>
                        </div>
                        <div>
                            <p>Registry Volume Path</p>
                            <input disabled={disabled} type="text" id="volumePath" value={registry.volumePath}/>
                        </div>
                    </div>
                    <div style={{display: registryInnerVipDivDisplayState}}>
                        <div className="advancedSettingCommonStyleDiv">
                            <div>
                                <p>Registry Interface</p>
                                <input disabled={disabled} type="text" id="interface" value={vip.interface}/>
                            </div>
                            <div>
                                <p>Registry VIP</p>
                                <input disabled={disabled} type="text" id="ip" value={vip.ip}/>
                            </div>
                            <div>
                                <p>Registry VRR ID</p>
                                <input disabled={disabled} type="text" id="vrrId" value={vip.vrrId}/>
                            </div>
                        </div>
                    </div>
                    <Footer/>
                </div>  
            </div>               
        </div>
    );
}

 