import React, { useEffect, useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import  Axios  from '../utils/CustomAxios';
import AxiosDashboard from '../utils/CustomDashboardAxios';
 
import 'ag-grid-enterprise';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-balham.css';
import { AgGridReact } from 'ag-grid-react';
import TextField from '@mui/material/TextField';

import '../css/Home.css';
import { actionCreators } from '../state/index';
import TickCrossRenderer from '../utils/Home/TickCrossRenderer';
import HomePage3dotPopUp from '../utils/Home/HomePage3dotPopUp';
import StatusRenderer from '../utils/Home/StatusRenderer';
import AuthenticationPopupForDelete from '../utils/Delete/AuthenticationPopUpForDelete';
import LastConfirmationPopUpForDelete from '../utils/Delete/LastConfirmationPopUpForDelete';
import SuccessAlert from '../utils/Home/SuccessAlert';
import ErrorAlert from '../utils/Home/ErrorAlert';
import PopUpEdit from '../utils/View/PopUpEdit';
import ExistingClusterPopover from '../utils/ExistingCluster/ExistingClusterPopover';
import ExistingClusterDetailsPopup from '../utils/ExistingCluster/ExistingClusterDetailsPopup';
import AuthenticationPopupForExistingCluster from '../utils/ExistingCluster/AuthenticationPopupForExistingCluster';
import LastConfirmationPopUpForExestingCluster from '../utils/ExistingCluster/LastConfirmationPopupForExistingCluster';
import ViewLogsPopover from '../utils/Home/ViewLogsPopover';

import ViewLogsModal from '../utils/Home/ViewLogsModal';



export default function Home() {  

    // Warning Alert Message - Different alert message for each validation error
    const [warningMessage, setWarningMessage] = React.useState("");

    const [successMessage, setSuccessMessage] = React.useState("");

    // Authentication Popup state
    const [popupDelete, setPopupDelete] = React.useState(false); 
    // Last Confirmation Popup state
    const [lastPopupDelete, setLastPopupDelete] = React.useState(false); 
    const [clusterToBeDeleted, setClusterToBeDeleted] = React.useState({});

    const [popupEdit, setPopupEdit] = React.useState(false); 

    const [existingClusterPopup, setExistingClusterPopup] = React.useState(false);
    const [popupExistingCluster, setAuthPopupExistingCluster] = React.useState(false);
    const [lastPopupExistingCluster, setLastPopupExistingCluster] = React.useState(false); 

    const [clusterDetailsForExistingCluster, setClusterDetailsForExistingCluster] = useState({
        clusterIP: "",
        clusterName: ""
    });

    var [homePageRowData, setHomePageRowData] = useState([]);

    const [anchorEl, setAnchorEl] = React.useState(null);
    const [anchorElViewLogs, setAnchorElViewLogs] = React.useState(null);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const dispatch = useDispatch();
    const { dashboardStatus } = bindActionCreators(actionCreators, dispatch); 

    const dashboardState = useSelector(state=> state.dashboardState);

    useEffect(async ()=> {
        await Axios.get(`/cluster/list`)
        .then(res=> {
            let newRowsData = [];
            res.data.map(row=> {

                // AxiosDashboard.get(`/dashboard/list?search=${row.clusterName}`)
                // .then(res=> {
                //     if(res.status == 200) dashboardStatus(res.data.list_data.dashboard_list.length !=0 ? res.data.list_data.dashboard_list[0].dashboardStatus : dashboardState)
                // })
                // .catch(error=> dashboardStatus('UNKNOWN'))

                row.nodes.map(node=> {
                    let controller = false, worker = false, etcd = false, registry = false;
                    node.roles.map(role=> {
                        if(role == 'CONTROLLER') controller =  true;
                        if(role == 'ETCD') etcd =  true;
                        if(role == 'WORKER') worker =  true;
                    })
                    if(row.registry.nodes.indexOf(node.name) >= 0) registry = true; 

                    let rowData = {ClusterId:parseInt(row.clusterId), ClusterName: row.clusterName, DashboardId: row.dashboardId , Status: row.status, DashboardStatus: '', NodeName: node.name, NodeIP: node.nodeIp, Controller: controller, Etcd: etcd, Worker: worker, Registry: registry}
                    newRowsData.push(rowData);
                }); 
            })
            setHomePageRowData(newRowsData);
        })
        .catch(error=> console.log(error))
        .finally(async()=> await delay(5000))       
    });

    const [gridApi, setGridApi] = useState(null);
    const [gridColumApi, setGridColumnApi] = useState(null);

    const onGridReady = (params)=> {
        setGridApi(params.api);
        setGridColumnApi(params.columnApi);
    }; 

    const columnDefs= [
        {pinned: 'left',width:30, filter: false, lockPosition: true, sortable: false, cellRenderer: HomePage3dotPopUp, cellRendererParams: {
            setPopupDelete: setPopupDelete, 
            popupDelete: popupDelete,
            setClusterToBeDeleted: setClusterToBeDeleted,
            setPopupEdit: setPopupEdit,
        }},
        {headerName: "Cluster Name", field: "ClusterName", flex: 0.17, rowGroup: true, hide: true},
        {headerName: "Cluster Id", field: "ClusterId", hide: true},
        {headerName: "Status", field: "Status", flex: 0.25, cellRenderer: StatusRenderer, cellRendererParams: {
            setAnchorElViewLogs: setAnchorElViewLogs,
        }}, 
        {headerName: "Node Name", field: "NodeName", flex: 0.125},
        {headerName: "Node IP", field: "NodeIP", flex: 0.1},
        {headerName: "Controller", field: "Controller", cellRenderer: 'tickCrossRenderer', flex: 0.09},
        {headerName: "Etcd", field: "Etcd", cellRenderer: 'tickCrossRenderer', flex: 0.075},
        {headerName: "Worker", field: "Worker", cellRenderer: 'tickCrossRenderer', flex: 0.075},
        {headerName: "Registry", field: "Registry", cellRenderer: 'tickCrossRenderer', flex: 0.08},
    ];

    const defaultColDef= {
        sortable: true,
        filter: true,
        floatingFilter: true,
        suppressMovable: true,
    };

    const components = {
        tickCrossRenderer: TickCrossRenderer,
    }

    const autoGroupColumnDef= { 
        headerName: "Cluster Name",  
    };

    var [toggleGroupsState, setToggleGroupsState] = useState('expand');
    const toggleGroups = async ()=> {
        if(toggleGroupsState == 'expand') {
            gridApi.expandAll();
            setToggleGroupsState('collapse');
        } else {
            gridApi.collapseAll();
            setToggleGroupsState('expand');
        }
    }

    const clickedOnExistingClusterButton = () => {
        setExistingClusterPopup(true);
      };

    const clickedOnUploadInventoryButton = (event) => {
        setAnchorEl(event.currentTarget);
      };
    
      const handleClose = () => {
        setAnchorEl(null);
        setAnchorElViewLogs(null);
      };

    // function deleteAllClusters() {
    //     Axios.delete('http://localhost:8083/cluster/delete/all')
    //     .then(res=> console.log(res));
    // }

    let navigate = useNavigate(); 
    const createClusterRoute = () =>{ 
      let path = `/create`; 
      navigate(path);
    }

  return (
    <div className="body">
        <SuccessAlert/>
        <ErrorAlert/>
        <div className="topmost">
            <div>
                {/* <button onClick={deleteAllClusters}>Delete All</button> */}
                <button onClick={toggleGroups}>Expand all/Close all</button>
            </div>
            
            <div>
                <button onClick={createClusterRoute}>New Cluster</button>
                <button onClick={clickedOnExistingClusterButton}>Install Dashboard</button>
                {/* <button onClick={clickedOnUploadInventoryButton}>Upload Inventory</button> */}
                <ExistingClusterPopover anchorEl={anchorEl} handleClose={handleClose}/>
                <TextField
                    id="search-bar"
                    placeholder="Search"
                    size="small"
                    // value={}
                    // onChange={handleChange}
                />
            </div>
        </div>
        <div>
            <AuthenticationPopupForDelete 
                popupDelete={popupDelete} 
                setPopupDelete={setPopupDelete} 
                lastPopupDelete={lastPopupDelete}
                setLastPopupDelete={setLastPopupDelete} 
                setWarningMessage={setWarningMessage} 
                clusterToBeDeleted={clusterToBeDeleted}
            />
            <LastConfirmationPopUpForDelete
                popupDelete={popupDelete} 
                setPopupDelete={setPopupDelete} 
                lastPopupDelete={lastPopupDelete}
                setLastPopupDelete={setLastPopupDelete} 
                setWarningMessage={setWarningMessage} 
                clusterToBeDeleted={clusterToBeDeleted}
            />
            <PopUpEdit 
                popupEdit={popupEdit}
                setPopupEdit={setPopupEdit}
                clusterToBeDeleted={clusterToBeDeleted}
            />
            <ExistingClusterDetailsPopup
                existingClusterPopup={existingClusterPopup}
                setExistingClusterPopup={setExistingClusterPopup}
                setAuthPopupExistingCluster={setAuthPopupExistingCluster}
                clusterDetailsForExistingCluster={clusterDetailsForExistingCluster}
                setClusterDetailsForExistingCluster={setClusterDetailsForExistingCluster}
            />
            <AuthenticationPopupForExistingCluster
                popupExistingCluster={popupExistingCluster}
                setAuthPopupExistingCluster={setAuthPopupExistingCluster}
                clusterDetailsForExistingCluster={clusterDetailsForExistingCluster}
                setLastPopupExistingCluster={setLastPopupExistingCluster}
            />
            <LastConfirmationPopUpForExestingCluster
                clusterDetailsForExistingCluster={clusterDetailsForExistingCluster}
                lastPopupExistingCluster={lastPopupExistingCluster}
                setLastPopupExistingCluster={setLastPopupExistingCluster}
            />
            <ViewLogsModal/>
        </div>
        <ViewLogsPopover anchorEl={anchorElViewLogs} handleClose={handleClose}/>
        <div className="ag-theme-balham">
            <AgGridReact
                rowData={homePageRowData}
                onGridReady={onGridReady}
                columnDefs={columnDefs}
                autoGroupColumnDef={autoGroupColumnDef}
                defaultColDef={defaultColDef}
                components={components}
                rowSelection="multiple"
                groupSelectsChildren={true}
                suppressRowClickSelection= {true}
                animateRows={true}             
            />
        </div>
    </div>
  );
}
