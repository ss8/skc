const reducer = (state=false, action)=> {
    if(action.type === 'isSuccessAlertOpen') {
        return action.payload;
    }

    else return state;
}

export default reducer;