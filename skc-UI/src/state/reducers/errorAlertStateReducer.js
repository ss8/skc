const reducer = (state=false, action)=> {
    if(action.type === 'isErrorAlertOpen') {
        return action.payload;
    }

    else return state;
}

export default reducer;