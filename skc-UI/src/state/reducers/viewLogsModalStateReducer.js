const reducer = (state=false, action)=> {
    if(action.type === 'isViewLogsModalOpen') {
        return action.payload;
    }

    else return state;
}

export default reducer;