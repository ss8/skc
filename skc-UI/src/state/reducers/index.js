import { combineReducers } from "redux";
import successAlertStateReducer from './successAlertStateReducer';
import errorAlertStateReducer from './errorAlertStateReducer';
import alertMessageStateReducer from './alertMessageStateReducer';
import viewLogsStateReducer from './viewLogsStateRenderer';
import viewLogsModalStateReducer from './viewLogsModalStateReducer';
import sshAuthenticationStateReducer from './sshAuthenticationStateReducer';
import dashboardStateReducer from './dashboardStateReducer';

const reducers = combineReducers({
    successAlertState: successAlertStateReducer,
    errorAlertState: errorAlertStateReducer,
    alertMessageContent : alertMessageStateReducer,
    viewLogsContent: viewLogsStateReducer,
    viewLogsModalState: viewLogsModalStateReducer,
    sshAuthenticationCredentials: sshAuthenticationStateReducer,
    dashboardState: dashboardStateReducer,
})

export default reducers;