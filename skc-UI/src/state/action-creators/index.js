export const openSuccessAlert = (successAlertState)=> {
    return (dispatch)=> {
        dispatch({
            type: 'isSuccessAlertOpen',
            payload: successAlertState
        })
    }
}

export const openErrorAlert = (errorAlertState)=> {
    return (dispatch)=> {
        dispatch({
            type: 'isErrorAlertOpen',
            payload: errorAlertState
        })
    }
}

export const alertMessage = (alertMessageContent)=> {
    return (dispatch)=> {
        dispatch({
            type: 'alertMessage',
            payload: alertMessageContent
        })
    }
}

export const openViewLogsModal = (viewLogsModalState)=> {
    return (dispatch)=> {
        dispatch({
            type: 'isViewLogsModalOpen',
            payload: viewLogsModalState
        })
    }
}

export const viewLogs = (viewLogsContent)=> {
    return (dispatch)=> {
        dispatch({
            type: 'viewLogs',
            payload: viewLogsContent
        })
    }
}

export const sshAuthentication = (sshCredentials)=> {
    return (dispatch)=> {
        dispatch({
            type: 'sshCredentialsAuthentication',
            payload: sshCredentials
        })
    }
} 

export const dashboardStatus = (dashboardState)=> {
    return (dispatch)=> {
        dispatch({
            type: 'dashboardStatus',
            payload: dashboardState
        })
    }
}
