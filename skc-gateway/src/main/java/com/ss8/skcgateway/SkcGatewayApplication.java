package com.ss8.skcgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SkcGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkcGatewayApplication.class, args);
	}

}
