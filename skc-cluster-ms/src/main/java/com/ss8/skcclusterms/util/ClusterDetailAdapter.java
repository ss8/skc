package com.ss8.skcclusterms.util;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.ss8.skcclusterms.dao.Cluster;
import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.dto.RegistryDetailsDTO;
import com.ss8.grpc.skc.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClusterDetailAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(ClusterDetailAdapter.class);

    /**
	 * Converts ClusterDetailDTO object to JSON string
	 * @param clusterDetailDTO object
	 * @return JSON string of ClusterDetailDTO object
	 */
	public static String convertClusterDetailDTOtoJSON(final ClusterDetailDTO clusterDetailDTO) {
		String clusterDetailDTOString = new Gson().toJson(clusterDetailDTO);
		
//		JsonElement je =  new Gson().toJsonTree(clusterDetailDTOString);
//		System.out.println("JsonElement=" + je + " type=" + je.getClass().getSimpleName());

//		JsonObject jo = new JsonObject(clusterDetailDTO);
//		new JsonParser().parse(clusterDetailDTOString).getAsJsonObject();
		LOG.trace("From ClusterDetailDTO to String: "+ clusterDetailDTOString + " type=" + (new Gson().toJson(clusterDetailDTO)).getClass().getSimpleName());
		return clusterDetailDTOString;
	}

	public static String extractInventoryName(final String inv_path)
	{
		//String inv_path = "/home/support/kkraj/ski/ski-grpc-apis/inventory/sample/inventory-5153779798045840850.ini";
		// int loc = inv_path.lastIndexOf('/');
    	// String invExtract = inv_path.substring(loc+1);
        // System.out.println(invExtract);
		// return(invExtract);

		return inv_path;
	}

	public static boolean isInvFile(final String inv_path)
	{
		//String inv_path = "/home/support/kkraj/ski/ski-grpc-apis/inventory/sample/inventory-5153779798045840850.in";
		int loc = inv_path.lastIndexOf('.');
    	String fileFormat = inv_path.substring(loc+1);
    	System.out.println(fileFormat.equals("ini"));
		return fileFormat.equals("ini");
	}

	public static String convertClusterDetailDTOtoJSON_addStatus(final ClusterDetailDTO clusterDetailDTO,String status) {
		//String clusterDetailDTOString = new Gson().toJson(clusterDetailDTO);
		// Add Cluster Status
		Gson gson = new Gson();
		JsonElement jsonElement = gson.toJsonTree(clusterDetailDTO);
		jsonElement.getAsJsonObject().addProperty("status",status);
		
		// Todo Inventory-name(future Phase)
		return gson.toJson(jsonElement);
	}
	
	/**
	 * Converts JSON string to ClusterDetailDTO object
	 * @param JSON form of clusterDetailDTO
	 * @return ClusterDetailDTO object
	 */
	public static ClusterDetailDTO convertJSONtoClusterDetailDTO(String clusterDetailDTOString) {
		ClusterDetailDTO clusterDetailDTO = new Gson().fromJson(clusterDetailDTOString, ClusterDetailDTO.class);
		LOG.trace("From JSON to ClusterDetailDTO: "+ clusterDetailDTO);
		return clusterDetailDTO;
	}
    
	public static ClusterDetail convertClusterDetailGrpcToClusterDetailDTO(ClusterDetailDTO clusterDetailDTO,String ansibleUser){
		Map<String, String> clusterVars = clusterDetailDTO.getClusterVarMap();

        //TODO collect from UI
        clusterVars.put("ansible_user", ansibleUser);

        //TODO Don't remove this, needed by SKI
        clusterVars.put("ansible_python_interpreter", "/usr/bin/python3");

        ArrayList<NodeDetail> allNodesInsideCluster = new ArrayList<>();
        clusterDetailDTO.getClusterNodes().forEach(nodeDTO -> {
            allNodesInsideCluster.add(
                NodeDetail.newBuilder()
                    .setNodeLabel(nodeDTO.getNodeName())
                    .setNodeIP(nodeDTO.getNodeIp())
                    .build()
            );
        });

        List<GroupDetail> clusteGroups = new ArrayList<>();

        clusterDetailDTO.getClusterGroups().forEach(groupDTO -> {
            // handle subgrouping
            List<GroupDetail> subGroups = new ArrayList<>();
            // check whether groupDTO has subgroups or not
            if( null != groupDTO.getSubGroups()) {
                groupDTO.getSubGroups().forEach(subGroupDTO -> {
                    subGroups.add(
                        GroupDetail.newBuilder()
                        .setGroupName(subGroupDTO.getGroupName())
                        .addAllNodeLabel(subGroupDTO.getGroupNodes())
                        .putAllGroupVars(subGroupDTO.getGroupVars())
                        .build()
                    );
                });
            }

            // Build cluster groups
            clusteGroups.add(
                GroupDetail.newBuilder()
                .setGroupName(groupDTO.getGroupName())
                .addAllNodeLabel(groupDTO.getGroupNodes())
                .putAllGroupVars(groupDTO.getGroupVars())
                .addAllSubGroup(subGroups)
                .build() 
            );
        });

   
        if(clusterDetailDTO.getIsRegistryEnabled()) {
            RegistryDetailsDTO regDTO = clusterDetailDTO.getRegistryDetailsDTO();
            GroupDetail registryGroupDetail = GroupDetail.newBuilder()
                .setGroupName(regDTO.getRegistryName())
                .addAllNodeLabel(regDTO.getRegNodes())
                .putAllGroupVars(regDTO.getRegistryVarMap())
                .build()
            ;
            clusteGroups.add(registryGroupDetail);
        }

        ClusterDetail clusterDetail = ClusterDetail.newBuilder()
                .addAllClusterNode(allNodesInsideCluster)
                .putAllClusterVars(clusterVars)
                .addAllClusterGroup(clusteGroups)
                .build();

		return clusterDetail;
	}
}
