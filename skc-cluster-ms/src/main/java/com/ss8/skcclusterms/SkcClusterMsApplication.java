package com.ss8.skcclusterms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkcClusterMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkcClusterMsApplication.class, args);
	}

}
