package com.ss8.skcclusterms.enums;

public enum ClusterStatus {
    SUCCESS("SUCCESS"),
    FAILED("FAILED"),
    UNKNOWN("UNKNOWN");

    private final String status;

    private ClusterStatus(String status)
    {
        this.status = status;
    }

    //this class returns datatype of ClusterStatus
    //used to convert it into string 
    @Override
    public String toString()
    {
        return this.status;
    }
}
