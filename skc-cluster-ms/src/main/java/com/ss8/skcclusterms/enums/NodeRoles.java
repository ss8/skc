package com.ss8.skcclusterms.enums;

public enum NodeRoles {

  CONTROLLER("controller"),
  WORKER("worker"),
  ETCD("etcd"),
  REGISTRY("registry");

  private final String role;

  private NodeRoles(String role) {
    this.role = role;
  }

  public String getRole() {
    return role;
  }

  public String toString() {
    return this.role;
  }
}
