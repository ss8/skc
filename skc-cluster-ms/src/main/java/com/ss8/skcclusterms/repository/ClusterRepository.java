package com.ss8.skcclusterms.repository;

import com.ss8.skcclusterms.dao.Cluster;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
//public interface ClusterRepository extends JpaRepository<Cluster,Long>{
public interface ClusterRepository extends JpaRepository<Cluster,Long>{

    Cluster findByClusterName(String clusterName);
    Boolean existsByClusterName(String clusterName);
    Boolean deleteByClusterName(String clusterName);
 
    // @Query("SELECT u FROM Cluster u WHERE u.cluster_name = :cluster_name")
    // Cluster findByName(@Param("cluster_name") String clusterName);

    // @Query("SELECT u FROM Cluster u WHERE u.cluster_id = :clusterId")
    // Optional<?> findInventoryById(@Param("clusterId") Long clusterId);

}
