package com.ss8.skcclusterms.service;

import java.util.List;

import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.dto.NodeDetailDTO;
import com.ss8.skcclusterms.dto.ResponseDTO;

public interface ClusterService {

  public ResponseDTO createCluster(ClusterDetailDTO clusterDetailDTO);

  public ResponseDTO retryCluster(Long clusterId);

  public List<ClusterDetailDTO> getClusterList();

  public ClusterDetailDTO getClusterDetail(Long clusterId);

  public ResponseDTO deleteCluster(Long clusterId);

  public ResponseDTO deleteClusterNode(ClusterDetailDTO clusterDetailDTO, List<NodeDetailDTO> nodesRemovedFromCluster);

  public ResponseDTO addClusterNode(ClusterDetailDTO clusterDetailDTO, List<NodeDetailDTO> nodesAddedToCluster);

  public String fetchControllerIp(String clusterName);

  //public String test(String clusterName);
}
