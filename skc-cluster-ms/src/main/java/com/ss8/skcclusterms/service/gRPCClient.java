/*
 * Copyright 2015 The gRPC Authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ss8.skcclusterms.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.ss8.grpc.skc.*;
import com.ss8.skcclusterms.dto.ClusterConstants;
import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.dto.NodeDetailDTO;
import com.ss8.skcclusterms.dto.NodeGroupDetailDTO;
import com.ss8.skcclusterms.dto.RegistryDetailsDTO;
import com.ss8.skcclusterms.dto.RegistryVIPDetailsDTO;
import com.ss8.skcclusterms.enums.NodeRoles;
import com.ss8.skcclusterms.util.ClusterDetailAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Value;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.netty.shaded.io.grpc.netty.GrpcSslContexts;
import io.grpc.netty.shaded.io.grpc.netty.NettyChannelBuilder;
import io.grpc.netty.shaded.io.netty.handler.ssl.SslContext;
import io.grpc.netty.shaded.io.netty.handler.ssl.util.InsecureTrustManagerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import javax.net.ssl.SSLException;
/**
 * K8SClusterOperator client code that makes gRPC calls to the server.
 */
public class gRPCClient {

    private String SKI_INSTALLER_HOME;
    private String ansibleUser;

    private static final Logger LOG = LoggerFactory.getLogger(gRPCClient.class);

    private K8SClusterOperatorGrpc.K8SClusterOperatorBlockingStub blockingStub ;
    private ManagedChannel mChannel = null;

   
    /** 
     * Construct client for accessing K8SClusterOperator server using the existing channel. 
     * @param target gRPC server url with port included
     * @param serverTrustedCollection collection file containing certificates of trusted servers
     * @param clientCertFile
     * @param clientCertKeyFile
     * 
     * */
    public gRPCClient(final String target, final Resource serverTrustedCollection, final Resource clientCertFile, final Resource clientCertKeyFile, String installerHome,String sshUserName) {
        this.SKI_INSTALLER_HOME=installerHome;
        this.ansibleUser = sshUserName;
        try {
            InputStream a = serverTrustedCollection.getInputStream();
            InputStream b = clientCertFile.getInputStream();
            InputStream c = clientCertKeyFile.getInputStream();
            
            //TLS Configuration
            SslContext sslContext = GrpcSslContexts.forClient()
             // if server's cert doesn't chain to a standard root, provide trusted server certificate collection file
                .trustManager(a)
                .keyManager(b, c) // client cert
                .build();

            this.mChannel = NettyChannelBuilder.forTarget(target)
                .sslContext(sslContext)
                // .sslContext(GrpcSslContexts.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build())
                .build();
            if(this.mChannel==null)
                throw new IllegalArgumentException("Problem Building gRPC client Channel, check arguments !");
        } catch (SSLException e) {
            LOG.error(e.getMessage());
            e.printStackTrace();
        } catch(IllegalArgumentException e){
            LOG.error(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            LOG.error("Error reading certificate Files\n"+e.getMessage());
            e.printStackTrace();
        }

        //For plaintext communication
            // this.mChannel = ManagedChannelBuilder.forTarget(target)
            // .usePlaintext()
            // .build()
            // ;
            this.blockingStub = K8SClusterOperatorGrpc.newBlockingStub(mChannel);
    }

    public ClusterDetailDTO buildClusterDTO(final String inventoryPath){
        
        LOG.debug("Received inventory path = "+ inventoryPath);
      
        String cmd = "ansible-inventory --list -i "+inventoryPath;
        MyCommand fetchClusterInfoCmd = MyCommand.newBuilder().setCmd(cmd).build();
		CommandOutput out = blockingStub.runShellCommand(fetchClusterInfoCmd);
        String jsonInv = out.getOutput();
        
        // String checkDir = "bash -c \"if [[ -d " + ansiblePkgDir +" ]]; then echo 'Exists'; else echo 'Not found'; fi\"";


        if(out.getRetVal()!=0){
            if(jsonInv.contains("ansible-inventory: command not found") || jsonInv.contains("Cannot run program \"ansible-inventory\"")){
                LOG.warn("Ansible-inventory not found !!");
                LOG.info("Starting ansible-inventory installation before importing cluster");
                String ansiblePkgDir = SKI_INSTALLER_HOME+"/offline_files/local/ansible_kube_pippkgs";
                // String checkDir = "if [[ -d " + ansiblePkgDir +" ]]; then echo 'Exists'; else echo 'Not found'; fi";
                // MyCommand cmdDir = MyCommand.newBuilder().setCmd(checkDir).build();
                // out = blockingStub.runShellCommand(cmdDir);
                // String dirCheck = out.getOutput();
                // LOG.debug("Checking for prerequisites before starting installtion: "+dirCheck);
                // if(dirCheck.equals("Not found")) {
                //     throw new IllegalArgumentException("Error! Can't install dashboard, check whether SKI is installed correctly or "+SKI_INSTALLER_HOME+" is mounted correctly");
                // }
                
                // LOG.debug("Requirements met, proceeding to install ansible-inventory...");
    
                String installScript ="pip3 install --user --no-index --find-links=" + ansiblePkgDir + " -r "+ ansiblePkgDir + "/requirements.txt";
                MyCommand ansibleInstallCmd = MyCommand.newBuilder().setCmd(installScript).build();
                out = blockingStub.runShellCommand(ansibleInstallCmd);
                String installResult = out.getOutput();
    
                LOG.info("Installation finished, Result: \n"+installResult);

                if(out.getRetVal()!=0){
                        throw new IllegalArgumentException("Error! Ansible install failed, Please Check if "+ ansiblePkgDir +" exists, Exiting...");
                }   
                LOG.info("Ansible installation finished, trying to fetch inventory again...");
                out = blockingStub.runShellCommand(fetchClusterInfoCmd);
                jsonInv = out.getOutput();
            } else{
                throw new IllegalArgumentException("Error! cannot import inventory: "+jsonInv);
            }

        }

        if(jsonInv.contains("No inventory was parsed")){
            throw new IllegalArgumentException("Error! Inventory file not found for the given path: "+inventoryPath);
        }

        LOG.trace("JSON Inventory fetch command output: "+jsonInv);
        
        Gson gson = new Gson();
        JsonElement json = gson.fromJson(jsonInv, JsonElement.class);
        JsonObject jsonObj = json.getAsJsonObject();
        Set<Entry<String, JsonElement>> groupSet= jsonObj.entrySet();

        LOG.debug("Parsing Inventory files: ");
        for(Entry<String,JsonElement> a : groupSet){
            LOG.debug(a.getKey()+" : "+a.getValue());
        }
        Set<String> nodesList= new HashSet<>();
        List<String> controllerList=gson.fromJson(jsonObj.get(ClusterConstants.Controller).getAsJsonObject().get("hosts"), List.class); 
        List<String> workerList=gson.fromJson(jsonObj.get(ClusterConstants.Worker).getAsJsonObject().get("hosts"), List.class); 
        List<String> etcdList=gson.fromJson(jsonObj.get(ClusterConstants.ETCD).getAsJsonObject().get("hosts"), List.class);
        JsonElement reg = jsonObj.get(ClusterConstants.Registry);
        List<String> registryList= new ArrayList<>();
        if(reg!=null){
            
            registryList=gson.fromJson(jsonObj.get(ClusterConstants.Registry).getAsJsonObject().get("hosts"), List.class);
        } else
            LOG.debug("Registry is null !!");
            
        
        
        if(controllerList!=null)
            nodesList.addAll(controllerList);
        if(workerList!=null){
            nodesList.addAll(workerList);
        }     
        if(etcdList!=null)    
            nodesList.addAll(etcdList);

        String clName=null;
        String nodeIp=null;
        Boolean isRegistryEnabled=null;
        List<NodeDetailDTO> clusterNodes = new ArrayList<>();

        //Building NodeDetailDTO
        for(String node: nodesList){
            nodeIp= jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(node).getAsJsonObject().get(ClusterConstants.AnsibleHost).getAsString();
            clName= jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(node).getAsJsonObject().get(ClusterConstants.ClusterName).getAsString();
            isRegistryEnabled=gson.fromJson(jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(node).getAsJsonObject().get(ClusterConstants.IsRegistryEnabled), Boolean.class); 
            
            List<NodeRoles> roles=new ArrayList<>();
            if(controllerList!=null && controllerList.contains(node))
                roles.add(NodeRoles.CONTROLLER);
            if(etcdList!=null && etcdList.contains(node))
                roles.add(NodeRoles.ETCD);
            if(workerList!=null && workerList.contains(node))
                roles.add(NodeRoles.WORKER);

            NodeDetailDTO nodeDto= new NodeDetailDTO();
            nodeDto.setNodeIp(nodeIp);
            nodeDto.setNodeName(node);
            nodeDto.setRoles(roles);
         
            LOG.debug("Node dto built: "+nodeDto.toString());
            clusterNodes.add(nodeDto);
        }
        //Building NodeGroupDetailDTO
        NodeGroupDetailDTO etcdGrpDetailDTO = buildNodeGroupDetailspDTO(ClusterConstants.ETCD, etcdList, jsonObj,gson);
        NodeGroupDetailDTO controllerDetailDto = buildNodeGroupDetailspDTO(ClusterConstants.Controller, controllerList, jsonObj,gson);
        NodeGroupDetailDTO workerDetailDto = buildNodeGroupDetailspDTO(ClusterConstants.Worker, workerList, jsonObj,gson);


        List<NodeGroupDetailDTO> clusterGroups = new ArrayList<>();
        if(etcdGrpDetailDTO != null) clusterGroups.add(etcdGrpDetailDTO);
        if(controllerDetailDto != null) clusterGroups.add(controllerDetailDto);
        if(workerDetailDto != null) clusterGroups.add(workerDetailDto);

        //Building RegistryDetailsDTO
        RegistryDetailsDTO regDetailsDto=null;
        RegistryVIPDetailsDTO regVipDto=null;
        if(reg!=null){
            if(registryList.size() > 0){
                String regName = jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(registryList.get(0)).getAsJsonObject().get(ClusterConstants.RegistryName).getAsString();
                String regPort = jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(registryList.get(0)).getAsJsonObject().get(ClusterConstants.RegistryPort).getAsString();
                String regVolPath = jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(registryList.get(0)).getAsJsonObject().get(ClusterConstants.RegistryVolumePath).getAsString();
                Boolean isVIPEnabled = jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(registryList.get(0)).getAsJsonObject().get(ClusterConstants.RegistryVIPEnable).getAsBoolean();
                String containerName = "ss8-docker-registry";
                
                String regIntf = jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(registryList.get(0)).getAsJsonObject().get(ClusterConstants.RegistryInterface).getAsString();
                String vrrId = jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(registryList.get(0)).getAsJsonObject().get(ClusterConstants.RegistryVrrId).getAsString();
                String ip = jsonObj.get("_meta").getAsJsonObject().get("hostvars").getAsJsonObject().get(registryList.get(0)).getAsJsonObject().get(ClusterConstants.RegistryVIP).getAsString();
                regVipDto = new RegistryVIPDetailsDTO();
                regVipDto.setIntf(regIntf);
                regVipDto.setIp(ip);
                regVipDto.setVrrId(vrrId);

                regDetailsDto = new RegistryDetailsDTO();

                regDetailsDto.setRegistryName(regName);
                regDetailsDto.setRegistryPort(regPort);
                regDetailsDto.setVolumePath(regVolPath);
                regDetailsDto.setIsVIPEnabled(isVIPEnabled);
                regDetailsDto.setRegNodes(registryList);
                regDetailsDto.setRegistryVIPDetails(regVipDto);
                regDetailsDto.setContainerName(containerName);
                                
                LOG.debug("Registry DTO built: "+ regDetailsDto);
            }
        } else {
            regVipDto = new RegistryVIPDetailsDTO();
            regVipDto.setIntf("");
            regVipDto.setIp("");
            regVipDto.setVrrId("");
         
            regDetailsDto = new RegistryDetailsDTO();
            regDetailsDto.setIsVIPEnabled(false);
            regDetailsDto.setRegNodes(registryList);
            regDetailsDto.setRegistryVIPDetails(regVipDto);

            LOG.debug("Registry DTO built: "+ regDetailsDto);
        }
        ClusterDetailDTO clusterDto = new ClusterDetailDTO();
        clusterDto.setClusterName(clName);
        clusterDto.setClusterNodes(clusterNodes);
        clusterDto.setClusterGroups(clusterGroups);
        clusterDto.setIsRegistryEnabled(isRegistryEnabled);
        clusterDto.setRegistryDetailsDTO(regDetailsDto);
                                        // .build();

        LOG.debug("-----------------------------");
        LOG.debug("Cluster DTO built: " + clusterDto);
        LOG.debug("------------------------------");

        return clusterDto;
    }

    private NodeGroupDetailDTO buildNodeGroupDetailspDTO(String groupName, List<String> groupNodes, JsonObject jsonObj,Gson gson){
        NodeGroupDetailDTO grpDto= new NodeGroupDetailDTO();
        grpDto.setGroupName(groupName);
        grpDto.setGroupNodes(groupNodes);
                                    
        //Building subgroup
        List<NodeGroupDetailDTO> subGrpList = new ArrayList<>();
        if(groupName.equals(ClusterConstants.Worker)) {
            LinkedList<String> workerChildren=gson.fromJson(jsonObj.get("kube_worker_nodes").getAsJsonObject().get("children"), LinkedList.class); 
            if(workerChildren == null) return null;
            LinkedList<String> tempList=new LinkedList<>(workerChildren);
            while(tempList.size()>0){
                LOG.debug("Found children nodes for worker node:"+ workerChildren);
                // groupNodes.addAll(workerChildren);
                NodeGroupDetailDTO childNode = new NodeGroupDetailDTO();
                childNode.setGroupName(tempList.poll());
                subGrpList.add(childNode);
                
            }
        }
        grpDto.setSubGroups(subGrpList);
        LOG.debug("NodeGroupDetailDTO for "+groupName+" built: "+ grpDto.toString());
        return grpDto;
    }

    /**
     * Blocking unary call. Calls buildInventory and receives the response.
     */
    public String buildInventory(ClusterDetailDTO clusterDetailDTO) {

        LOG.debug("Cluster created from gRPC");
        LOG.debug("ClusterDetailDTO: "+clusterDetailDTO);

        LOG.debug("*** Inventory Build Started... ***");

        ClusterDetail clusterDetail = ClusterDetailAdapter.convertClusterDetailGrpcToClusterDetailDTO(clusterDetailDTO,ansibleUser);

        LOG.debug("ClusterDetail: "+clusterDetail);

        ExitStatus exitStatus;
        //TODO Write unit test case for RPC-failed
        try {
            exitStatus = blockingStub.buildInventory(clusterDetail);
        } catch(StatusRuntimeException e) {
            LOG.warn("RPC failed: {0}", e.getStatus());
            return "Exception in buildInventory RPC failed:"+ e.getStatus();
        }
 
        LOG.debug("Return Status of buildInventory: "+ exitStatus);
        return  exitStatus.getOutFileName();
    }

    public ExitStatus runClusterOperation(final OperationDetail action)
    {
        // add one more catch block 
        ExitStatus exitStatus=ExitStatus.getDefaultInstance();
        try {
            exitStatus = blockingStub.runClusterOperation(action);
            LOG.debug("exitStatus = "+exitStatus.getAllFields()); // newly added
        } catch(StatusRuntimeException e) {
            //gRPC connection failed
            LOG.error("RPC failed message: {0}", e.getMessage()); //changed
            LOG.error("Exit Status: "+exitStatus);
            return exitStatus;
        }

        LOG.debug("Return Status of runClusterOperation: "+ exitStatus);
        // return "Return Status of runClusterOperation: "+ exitStatus.getStatus();
        return exitStatus; 
    }
}

