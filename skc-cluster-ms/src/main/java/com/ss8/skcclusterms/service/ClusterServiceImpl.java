package com.ss8.skcclusterms.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;

import com.ss8.grpc.skc.*;
import com.ss8.skcclusterms.dao.Cluster;
import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.dto.NodeDetailDTO;
import com.ss8.skcclusterms.dto.ResponseDTO;
import com.ss8.skcclusterms.enums.ClusterStatus;
import com.ss8.skcclusterms.enums.NodeRoles;
import com.ss8.skcclusterms.repository.ClusterRepository;
import com.ss8.skcclusterms.util.ClusterDetailAdapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class ClusterServiceImpl implements ClusterService {

  @Autowired
  ClusterRepository clusterRepository;

  @Value("${grpc.server.target}")
  private String target;

  @Value("${cert.server.trustedCertFile}")
  private Resource serverTrustedCollection;

  @Value("${cert.client.certFile}")
  private Resource clientCertFile;

  @Value("${cert.client.keyFile}")
  private Resource clientCertKeyFile;

  @Value("${SKI.INSTALLER_HOME}")
  private String SKI_INSTALLER_HOME;

  @Value("${SKI.INSTALLER_LOGS}")
  private String SKI_INSTALLER_LOGS;

  @Value("${ssh.username}")
  private String sshUsername;

  @Value("${ssh.password}")
  private String sshPassword;

  private gRPCClient skiClient = null;
  private static final Logger LOG = LoggerFactory.getLogger(ClusterService.class);

  @PostConstruct
  private void init() {
    LOG.info("Connecting to gRPC server:" + this.target);

    this.skiClient = new gRPCClient(target, serverTrustedCollection, clientCertFile, clientCertKeyFile, SKI_INSTALLER_HOME,sshUsername);
  }

  @Override
  public ResponseDTO createCluster(ClusterDetailDTO clusterDetailDTO) {
    LOG.info("createCluster from ServiceImpl");
    LOG.info("ClusterDetailDTO: " + clusterDetailDTO.toString());

    // if error occured throw it accordingly
    String inventoryPath = "";
    int status;
    ResponseDTO responseDTO = new ResponseDTO();
    try {
      // write Inventory file and pass it to create cluster
      // System.out.println("build Inventory Yet to start");
      inventoryPath = skiClient.buildInventory(clusterDetailDTO);
      LOG.info("Creating Inventory : " + inventoryPath);

      // checking inventoryPath is having path or error
      if (!ClusterDetailAdapter.isInvFile(inventoryPath)) {
        responseDTO.setStatus(3);
        responseDTO.setError("gRPC throws build inventory failed");
        responseDTO.setMessage("gRPC throws build inventory failed : " + inventoryPath);
        LOG.warn("build inv failed : creation of cluster stopped and end.");
        return responseDTO;
      }

      Cluster cluster = new Cluster(clusterDetailDTO, "inventory-empty", ClusterStatus.UNKNOWN.toString());

      // Exract inventory name
      String inventoryName = ClusterDetailAdapter.extractInventoryName(inventoryPath);

      // update inventory to clusterDetailDTO
      clusterDetailDTO.setInventoryPath(inventoryName);
      cluster = new Cluster(cluster, clusterDetailDTO);
      cluster.setInventoryName(inventoryName);

      // TODO: collect creds from UI (https header)
      // Creds creds =
      // Creds.newBuilder().setUsername("root").setPassword("ss8inc").build();
      // System.out.println("Running Creds and OperationDetail");
      Creds creds = Creds.newBuilder().setUsername(sshUsername).setPassword(sshPassword).build();

      OperationDetail action = OperationDetail.newBuilder()
          .setOperation(Operation.create)
          .setCreds(creds)
          .setInventoryPath(inventoryName)
          .build();

      LOG.info("Call gRPC server");
      ExitStatus exitStatus = skiClient.runClusterOperation(action); // almost 1hr
      status =  exitStatus.getCmdOut().getRetVal();
      // update cluster DB after cluster creation depending on status
      // if (status.equals(ClusterStatus.SUCCESS.toString().toUpperCase())) {
      if (status==0) {
        cluster.setClusterStatus(ClusterStatus.SUCCESS.toString());
        LOG.info("clusterCreation : Success => Saved to Database");
        responseDTO.setStatus(status);
        responseDTO.setMessage("Cluster creation : success");
      } 
      // else if (status.equals(ClusterStatus.FAILED.toString().toUpperCase())) {
        else if (status==-1){
          // for testing purpose only
          LOG.warn("gRPC error = " + exitStatus.toString());
          cluster.setClusterStatus("gRPC or Comment");
          responseDTO.setStatus(status);
          responseDTO.setMessage("gRPC throws error : " + status);
        } else {
        cluster.setClusterStatus(ClusterStatus.FAILED.toString());
        LOG.warn("clusterCreation : Failed = "+ exitStatus.toString());
        // TODO throw exception
        // TODO counter implement(next phase)
        responseDTO.setStatus(status);
        responseDTO.setMessage("Cluster creation : failed = "+exitStatus.getCmdOut().getOutput());
      } 
      clusterDetailDTO.setStatus(cluster.getClusterStatus());
      cluster = new Cluster(cluster, clusterDetailDTO);
      clusterRepository.saveAndFlush(cluster);

      // TODO Call dashboard ms to install dashboard
      // dashboard install endpoint >>independent

      // System.out.println("Commit or Rollback");

    } catch (Exception e) {
      LOG.error(e.toString());
      responseDTO.setError(e.toString());
    }
    // TODO must update
    return responseDTO;
  }

  public ResponseDTO retryCluster(Long clusterId) {
    LOG.debug("retryCreateCluster from ServiceImpl");
    LOG.debug("ClusterDetailDTO: " + clusterId);

    LOG.debug("retryCreateCluster from ServiceImpl");
    LOG.debug("ClusterDetailDTO: " + clusterId);

    Cluster cluster;
    ResponseDTO responseDTO = new ResponseDTO();

    // fetch data from DataBase
    if (clusterRepository.existsById(clusterId) && clusterRepository.getById(clusterId).getClusterStatus()
        .equals(ClusterStatus.FAILED.toString().toUpperCase())) {
      cluster = clusterRepository.getById(clusterId);
      // System.out.println("Cluster Status value is equals to FAILED");
      // System.out.println("Cluster retry is processing");
    } else {
      LOG.debug((clusterRepository.existsById(clusterId)) ? ("Cluster Status value is SUCCESS")
          : ("ClusterId does not exist"));
      responseDTO.setStatus(2);
      responseDTO
          .setMessage((clusterRepository.existsById(clusterId)) ? ("Cluster Status value is SUCCESS")
              : ("ClusterId does not exist"));
      return responseDTO;
    }

    int status;

    LOG.debug("Inventory Name = " + cluster.getInventoryName());

    try {

      // System.out.println(cluster);

      ClusterDetailDTO clusterDetailDTO = ClusterDetailAdapter
          .convertJSONtoClusterDetailDTO(cluster.getClusterDetailDTO());

      // // write Inventory file and pass it to create cluster
      // System.out.println("build Inventory Yet to start");
      // inventoryPath = skiClient.buildInventory(clusterDetailDTO);
      // System.out.println("Creating Inventory : " + inventoryPath);
      // // Exract inventory name
      // String inventoryName =
      // ClusterDetailAdapter.extractInventoryName(inventoryPath);

      // update inventory to clusterDetailDTO
      // clusterDetailDTO.setInventoryPath(inventoryName);
      // cluster = new Cluster(cluster, clusterDetailDTO);
      // cluster.setInventoryName(inventoryName);

      // TODO: collect creds from UI (https header)
      // Creds creds =
      // Creds.newBuilder().setUsername("root").setPassword("ss8inc").build();

      Creds creds = Creds.newBuilder().setUsername(sshUsername).setPassword(sshPassword).build();

      // cleanup
      // LOG.debug("Running Cleaning Part");
      // OperationDetail cleanup = OperationDetail.newBuilder()
      // .setOperation(Operation.reset)
      // .setCreds(creds)
      // .setInventoryPath(cluster.getInventoryName())
      // .build();

      // String cleanupStatus = skiClient.runClusterOperation(cleanup);
      // LOG.debug("CleanUp Status = " + cleanupStatus);
      // System.out.println("CleanUp Status = " + cleanupStatus);

      // if (!cleanupStatus.equals(ClusterStatus.SUCCESS.toString().toUpperCase())) {
      // LOG.debug("CleanUp Operation FAILED");
      // responseDTO.setStatus(3);
      // responseDTO.setMessage("CleanUp Operation FAILED");
      // return responseDTO;
      // }

      System.out.println("Running Creds and OperationDetail");

      LOG.debug("Running Creds and OperationDetail");

      OperationDetail action = OperationDetail.newBuilder()
          .setOperation(Operation.create)
          .setCreds(creds)
          .setInventoryPath(cluster.getInventoryName())
          .build();

      LOG.debug("Call gRPC server");
      ExitStatus exitStatus = skiClient.runClusterOperation(action); // almost 1hr
      status = exitStatus.getCmdOut().getRetVal();
      // update cluster DB after cluster creation depending on status
      // if (status.equals(ClusterStatus.SUCCESS.toString().toUpperCase())) {
      if (status==0) {
        cluster.setClusterStatus(ClusterStatus.SUCCESS.toString());
        LOG.info("clusterCreation : Success => Saved to Database");
        responseDTO.setStatus(status);
        responseDTO.setMessage("clusterCreation : Success => Saved to Database");
      } 
      else if(status==-1){
        // for testing purpose only
        LOG.warn("gRPC error = " + exitStatus.toString());
        cluster.setClusterStatus("gRPC or Comment");
        responseDTO.setStatus(status);
        responseDTO.setMessage("gRPC error");
      }
      else {
        cluster.setClusterStatus(ClusterStatus.FAILED.toString());
        LOG.warn("clusterCreation : Failed = " + exitStatus.toString());
        // TODO throw exception
        // TODO counter implement(next phase)
        responseDTO.setStatus(status);
        responseDTO.setMessage("clusterCreation : Failed: "+exitStatus.getCmdOut().getOutput());
      }

      clusterDetailDTO.setStatus(cluster.getClusterStatus());
      cluster = new Cluster(cluster, clusterDetailDTO);
      clusterRepository.saveAndFlush(cluster);

      // TODO Call dashboard ms to install dashboard
      // dashboard install endpoint >>independent

      // System.out.println("Commit or Rollback");
      // System.out.println(4);
      // System.out.println(cluster);

    } catch (Exception e) {
      LOG.error(e.toString());
    }

    return responseDTO;
  }

  @Override
  public List<ClusterDetailDTO> getClusterList() {
    // TODO Auto-generated method stub
    // clusters' details
    List<Cluster> list_clusterDAO = clusterRepository.findAll();
    /*
     * ########################
     * need beautificatiion
     * ########################
     */
    List<ClusterDetailDTO> list_clusterDTO = new ArrayList<ClusterDetailDTO>(list_clusterDAO.size());

    list_clusterDAO.forEach(cdao -> {
      // cdao
      ClusterDetailDTO temp_cDTO = ClusterDetailAdapter.convertJSONtoClusterDetailDTO(cdao.getClusterDetailDTO());
      temp_cDTO.setClusterId(cdao.getClusterId());
      list_clusterDTO.add(temp_cDTO);
    });

    /*
     * for(int i = 0; i < list_clusterDAO.size(); i++)
     * {
     * String temp_cDTO_string = list_clusterDAO.get(i).getClusterDetailDTO();
     * ClusterDetailDTO temp_cDTO =
     * ClusterDetailAdapter.convertJSONtoClusterDetailDTO(temp_cDTO_string);
     * temp_cDTO.setClusterId(list_clusterDAO.get(i).getClusterId().toString());
     * list_clusterDTO.add(temp_cDTO);
     * }
     */

    return list_clusterDTO;
  }

  @Override
  public ClusterDetailDTO getClusterDetail(Long clusterId) {
    // TODO Auto-generated method stub

    // getting the reponse from the Data Base
    Cluster c_tmp = clusterRepository.findById(clusterId).orElse(null);
    // c_tmp can be null => need some exception handling
    ClusterDetailDTO cDTO = ClusterDetailAdapter.convertJSONtoClusterDetailDTO(c_tmp.getClusterDetailDTO());
    cDTO.setClusterId(c_tmp.getClusterId());
    return cDTO;
  }

  @Override
  public ResponseDTO deleteCluster(Long clusterId) {
    int flag = 0;
    // TODO Call dashboard ms to delete dashboard
    LOG.debug("Cluster Deletion Start !!");
    // TODO: getInventory(clusterId);
    Cluster cluster = clusterRepository.getById(clusterId);
    String inventoryName = cluster.getInventoryName();
    LOG.debug("Got Inventory: " + inventoryName);
    // TODO: collect creds from UI
    Creds creds = Creds.newBuilder().setUsername(sshUsername).setPassword(sshPassword).build();

    ResponseDTO responseDTO = new ResponseDTO();

    OperationDetail action = OperationDetail.newBuilder()
        .setOperation(Operation.reset)
        .setCreds(creds)
        .setInventoryPath(inventoryName)
        .build();

    ExitStatus exitStatus = skiClient.runClusterOperation(action); // almost 1hr
    LOG.info(exitStatus.toString());
    // System.out.println(status);
    // delete cluster DB after cluster deletion depending on status
    int status = exitStatus.getCmdOut().getRetVal();
    // if (status.equals(ClusterStatus.SUCCESS.toString().toUpperCase())) {
    if(status==0){
      // flag++;
      clusterRepository.deleteById(clusterId);
      // System.out.println("Deleted from DB!!");
      responseDTO.setStatus(status);
      responseDTO.setMessage("Success : Cluster Deleted = "+exitStatus.getCmdOut().getOutput());
      LOG.info("Deleted");
    } // else if (status.equals(ClusterStatus.FAILED.toString().toUpperCase())) {
      else if(status>0){
      cluster.setClusterStatus(ClusterStatus.FAILED.toString());
      responseDTO.setStatus(exitStatus.getCmdOut().getRetVal());
      responseDTO.setMessage("Cluster Deletion : Failed = "+exitStatus.getCmdOut().getOutput());
      // TODO throw exception
      // TODO counter implement(next phase)
      // System.out.println("Failed");
      LOG.warn("Cluster Deletion : Failed = " + exitStatus);
    } else {
      // for testing purpose only
      responseDTO.setStatus(exitStatus.getCmdOut().getRetVal());
      responseDTO.setMessage("gRPC error : "+exitStatus.getCmdOut().getOutput());
      LOG.warn("gRPC error : " + exitStatus);
    }
    // System.out.println("End of delete cluster");
    // Post processing like delete entry from the database
    // clusterRepository.deleteById(clusterId); // DO NOT DELETE FROM DATABASE...
    // FOR TESTING
    return responseDTO;

    // return (skiClient.runClusterOperation(action));
  }

  @Override
  public ResponseDTO deleteClusterNode(ClusterDetailDTO clusterDetailDTO, List<NodeDetailDTO> nodesRemovedFromCluster) {
    ResponseDTO responseDTO = new ResponseDTO();
    // TODO Auto-generated method stub

    Cluster cluster = clusterRepository.getById(clusterDetailDTO.getClusterId());
    String inventoryName = cluster.getInventoryName();

    // list to string
    String str_node_list = nodesRemovedFromCluster.get(0).getNodeName();
    for (int i = 1; i < nodesRemovedFromCluster.size(); ++i) {
      str_node_list += "," + nodesRemovedFromCluster.get(i).getNodeName();
    }
    // TODO: Operation delops = (isNodePrimaryController(nodeName))
    // ? Operation.removeprimarycontroller : Operation.removenode;

    // TODO Implement this function & call deleteClusterNode from service layer to
    // delete a cluster node
    // TODO collect the inventory-name form database

    // TODO perform delete node operation

    // fetch the existing cDTO (get node list , update the list by removing the list

    // of nodes to be deleted)
    // TODO call the build inventory with new cluster detail dto
    Creds creds = Creds.newBuilder().setUsername(sshUsername).setPassword(sshPassword).build();
    OperationDetail action = OperationDetail.newBuilder()
        .setOperation(Operation.removenode)
        .setCreds(creds)
        .setInventoryPath(inventoryName)
        .setArgs(str_node_list)
        .build();

    // create new inv with new clusterDetailDTO
    String newInventoryPath = skiClient.buildInventory(clusterDetailDTO);

    //checking for inventory file
    if (!ClusterDetailAdapter.isInvFile(newInventoryPath)) {
      responseDTO.setStatus(3);
      responseDTO.setError("gRPC throws build inventory failed");
      responseDTO.setMessage("gRPC throws build inventory failed : " + newInventoryPath);
      return responseDTO;
      // return ("New-Inv build failed: Error = "+newInventoryPath);
    }

    String newInventoryName = ClusterDetailAdapter.extractInventoryName(newInventoryPath);
    LOG.info("newInvName = " + newInventoryName);

    // update the cluster row with new inventory

    String operationOutput = "";
    ExitStatus exitStatus = skiClient.runClusterOperation(action);
    int status = exitStatus.getCmdOut().getRetVal();
    // update cluster DB after cluster creation depending on status
    // if (status.equals(ClusterStatus.SUCCESS.toString().toUpperCase())) {
    if(status==0){
      clusterDetailDTO.setInventoryPath(newInventoryName);
      cluster.setInventoryName(newInventoryName);
      clusterDetailDTO.setStatus(ClusterStatus.SUCCESS.toString());
      cluster.setClusterDetailDTO(ClusterDetailAdapter.convertClusterDetailDTOtoJSON(clusterDetailDTO));
      //operationOutput = "addClusterOpertion Success : Node is added & Updated in DB";
      responseDTO.setStatus(status);
      responseDTO.setMessage("addClusterOpertion Success : Node is added & Updated in DB");
      LOG.info("addClusterOpertion Success : Node is added & Updated in DB");
    } 
    // else if (status.equals(ClusterStatus.FAILED.toString().toUpperCase())) {
      else if(status==-1){
        // for testing purpose only
        // operationOutput = "gRPC Connection Error";
        responseDTO.setStatus(status);
        responseDTO.setMessage("gRPC Connection Error : "+exitStatus.getCmdOut().getOutput());
        LOG.warn("gRPC Connection Error : "+exitStatus);
      } else {
        // dont save to DB
        //operationOutput = "addClusterOpertion FAILED";
        responseDTO.setStatus(status);
        responseDTO.setMessage("addClusterOpertion FAILED: "+exitStatus.getCmdOut().getOutput());
        LOG.warn("addClusterOpertion FAILED: "+exitStatus);
      }

    clusterRepository.saveAndFlush(cluster);

    //return target + ":" + newInventoryPath + " status: " + status + " operation output " + operationOutput;
    return responseDTO;
  }

  @Override
  public ResponseDTO addClusterNode(ClusterDetailDTO clusterDetailDTO, List<NodeDetailDTO> nodesAddedToCluster) {

    ResponseDTO responseDTO = new ResponseDTO();
    // TODO fetch the existing cDTO & get node list append new node
    Cluster cluster = clusterRepository.getById(clusterDetailDTO.getClusterId());

    // TODO call the build inventory with new cluster detail dto
    String newInventoryPath = skiClient.buildInventory(clusterDetailDTO);

    //checking for inventory file
    if (!ClusterDetailAdapter.isInvFile(newInventoryPath)) {
      responseDTO.setStatus(3);
      responseDTO.setError("gRPC throws build inventory failed");
      responseDTO.setMessage("gRPC throws build inventory failed : " + newInventoryPath);
      return responseDTO;
      //return ("New-Inv build failed: Error = "+newInventoryPath);
    }

    // Exract inventory name
    String newInventoryName = ClusterDetailAdapter.extractInventoryName(newInventoryPath);

    Operation addops = Operation.addnode;
    // set Operation to addcontroller node if any newly added node is a controller
    for (int i = 0; i < nodesAddedToCluster.size(); ++i) {
      if (nodesAddedToCluster.get(i).getRoles().contains(NodeRoles.CONTROLLER)) {
        addops = Operation.addcontroller;
        break;
      }
    }

    LOG.debug("cluster details =  " + clusterDetailDTO + "   nodes added =  " + nodesAddedToCluster);

    Creds creds = Creds.newBuilder().setUsername(sshUsername).setPassword(sshPassword).build();
    OperationDetail action = OperationDetail.newBuilder()
        .setOperation(addops)
        .setCreds(creds)
        .setInventoryPath(newInventoryName)
        .build();

    String operationOutput = "";
    ExitStatus exitStatus = skiClient.runClusterOperation(action);
    int status = exitStatus.getCmdOut().getRetVal();
    // update cluster DB after cluster creation depending on status
    // if (status.equals(ClusterStatus.SUCCESS.toString().toUpperCase())) {
    if(status==0){
      clusterDetailDTO.setInventoryPath(newInventoryName);
      clusterDetailDTO.setStatus(ClusterStatus.SUCCESS.toString());
      cluster.setInventoryName(newInventoryName);
      cluster.setClusterDetailDTO(ClusterDetailAdapter.convertClusterDetailDTOtoJSON(clusterDetailDTO));
      //operationOutput = "addClusterOpertion Success : Node is added & Updated in DB";
      responseDTO.setStatus(status);
      responseDTO.setMessage("addClusterOpertion Success : Node is added & Updated in DB");
      LOG.info("addClusterOpertion Success : Node is added & Updated in DB");
    } 
    else if(status==-1){
      // for testing purpose only
      //operationOutput = "gRPC Connection Error";
      
      responseDTO.setStatus(status);
      responseDTO.setMessage("gRPC Connection Error : "+exitStatus.getCmdOut().getOutput());
      LOG.warn("gRPC Connection Error : "+exitStatus);
    }
    // else if (status.equals(ClusterStatus.FAILED.toString().toUpperCase())) {
    else{
      // dont save to DB
      //operationOutput = "addClusterOpertion FAILED";
      responseDTO.setStatus(status);
      responseDTO.setMessage("addClusterOpertion FAILED: "+exitStatus.getCmdOut().getOutput());
      LOG.warn("addClusterOpertion FAILED: "+exitStatus);
    } 
    
    LOG.info("Status: "+exitStatus);
    // LOG.info(status); // SUCCESS
    // LOG.info(status.getClass().getSimpleName()); // String
    // LOG.info(ClusterStatus.SUCCESS.toString().toUpperCase()); // old-success //should be now SUCCESS
    // LOG.info(ClusterStatus.SUCCESS.toString().getClass().getSimpleName()); // String

    // LOG.info(status.equals(ClusterStatus.SUCCESS.toString().toUpperCase()) ? "YES" : "NO"); // NO

    clusterRepository.saveAndFlush(cluster);

    LOG.info("status on adding nodes:=  " + status);

    //return target + ":" + newInventoryPath + " status: " + status + " operation output " + operationOutput;
    return responseDTO;

  }

  @Override
  public String fetchControllerIp(final String clusterName) {

    LOG.debug("Fetching controller node ip for the cluster: "+clusterName);

    ClusterDetailDTO clusterDetailDto = null;
    String inventoryPath =  SKI_INSTALLER_LOGS + "/" + clusterName + "/" + clusterName + ".ini";
    String controllerNodeIp = null;
    try {
      LOG.debug("Building Cluster DTO for the inventory file: "+inventoryPath);
      clusterDetailDto = this.skiClient.buildClusterDTO(inventoryPath);
    } catch (IllegalArgumentException e) {
      LOG.error(e.getMessage());
      LOG.trace(e.getStackTrace().toString());
      return e.getMessage();
    }

    if (clusterDetailDto == null)
      return null;

    List<NodeDetailDTO> nodeList = clusterDetailDto.getClusterNodes();
    for (NodeDetailDTO node : nodeList) {
      if (node.getRoles().contains(NodeRoles.CONTROLLER)) {
        controllerNodeIp = node.getNodeIp();
        LOG.debug("Found controller node ip for dashboard installation: ", controllerNodeIp);
        break;
      }
    }

    if (controllerNodeIp.length() < 1)
      LOG.error("Controller node not found for the cluster: " + clusterName);

    // Saving the generated dto to DB
    Cluster cluster = new Cluster(clusterDetailDto,inventoryPath,ClusterStatus.SUCCESS.toString());
    clusterRepository.saveAndFlush(cluster);

    return controllerNodeIp;
  }

  // public String test(String clusterName) {

  // ClusterDetailDTO clusterDetailDTO = new ClusterDetailDTO("cluster-1");
  // Cluster expectedResult = new Cluster(1L, clusterDetailDTO, "inv", "SUCCESS");
  // System.out.println(expectedResult);
  // // System.out.println(clusterRepository.save(expectedResult));
  // clusterRepository.save(expectedResult);
  // Cluster actualResult = clusterRepository.findByClusterName("cluster-1");
  // System.out.println(clusterRepository.findById(1L));

  // return actualResult.getClusterName();
  // }

}
