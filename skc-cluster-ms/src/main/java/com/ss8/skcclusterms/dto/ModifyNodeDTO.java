package com.ss8.skcclusterms.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ss8.skcclusterms.enums.NodeRoles;

public class ModifyNodeDTO {
    @JsonProperty("jsonData")
    private ClusterDetailDTO clusterDetailDTO;

    @JsonProperty("modifiedNodes")
    private List<NodeDetailDTO> modifiedNodes;

    //constructor

    public ModifyNodeDTO(ClusterDetailDTO clusterDetailDTO, List<NodeDetailDTO> modifiedNodes) {
        this.clusterDetailDTO = clusterDetailDTO;
        this.modifiedNodes = modifiedNodes;
    }
    public ModifyNodeDTO() {
    }

    //getter and setters

    @Override
    public String toString() {
        return "ModifyNodeDTO [clusterDetailDTO=" + clusterDetailDTO + ", modifiedNodes=" + modifiedNodes + "]";
    }
    public ClusterDetailDTO getClusterDetailDTO() {
        return clusterDetailDTO;
    }
    public void setClusterDetailDTO(ClusterDetailDTO clusterDetailDTO) {
        this.clusterDetailDTO = clusterDetailDTO;
    }
    public List<NodeDetailDTO> getModifiedNodes() {
        return modifiedNodes;
    }
    public void setModifiedNodes(List<NodeDetailDTO> modifiedNodes) {
        this.modifiedNodes = modifiedNodes;
    }
}
