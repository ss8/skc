package com.ss8.skcclusterms.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ss8.skcclusterms.enums.NodeRoles;

import lombok.Builder;
import lombok.Data;

@Data
// @Builder
public class NodeDetailDTO {

  @JsonProperty("name")
  private String nodeName;

  @JsonProperty("ip")
  private String nodeIP;

  @JsonProperty("roles")
  private List<NodeRoles> roles;


  public NodeDetailDTO(){
    
  }

  public NodeDetailDTO(String nodeName, String nodeIP, List<NodeRoles> roles) {
    this.nodeName = nodeName;
    this.nodeIP = nodeIP;
    this.roles = roles;
  }

  // Getters and Setters

 
  public String getNodeName() {
    return nodeName;
  }

  public void setNodeName(String name) {
    this.nodeName = name;
  }

  public String getNodeIp() {
    return nodeIP;
  }

  public void setNodeIp(String ip) {
    this.nodeIP = ip;
  }

  public List<NodeRoles> getRoles() {
    return roles;
  }

  public void setRoles(List<NodeRoles> roles) {
    this.roles = roles;
  }

  @Override
  public String toString() {
    return "NodeDetailDTO [nodeName=" + nodeName + ", nodeIP=" + nodeIP + ", nodeRoles=" + roles + "]";
  }

}
