package com.ss8.skcclusterms.dto;

// class can't be extended to avoid members modification
public final class ClusterConstants {
	// package level privates

	// ####### Cluster Section ####### \\
	public static final String ETCD = "etcd";
	public static final String Worker = "kube_worker_nodes";
	public static final String Controller = "kube_controller_nodes";
	public static final String Registry = "registry";
	public static final String ClusterName = "cluster_name";
	public static final String AnsibleUser= "ansible_user";
	public static final String AnsibleHost= "ansible_host";
	public static final String DualStackEnabled = "dual_stack_networks";
	public static final String NodeLabels = "node_labels";
	public static final String TmpDir = "tmp_dir";
	public static final String DockerStoragePath = "docker_daemon_graph";
	public static final String KubeletStoragePath = "kubelet_storage";
	public static final String KubeAPIServerPortRange = "kube_apiserver_node_port_range";
	public static final String IsRegistryEnabled = "registry_enabled";
	// ####### Registry Section ####### \\
	public static final String RegistryVIPEnable = "registry_vip_enable";
	public static final String RegistryName = "registry_name";
	public static final String RegistryPort = "registry_port"; 
	public static final String CreateRegistry = "create_docker_registry";
	public static final String RegistryVIP= "registry_vip";
	public static final String RegistryVolumePath= "registry_volume_path"; 
	public static final String RegistryInterface = "registry_interface";
	public static final String RegistryVrrId= "registry_vrr_id";
	public static String RegistryInstanceName = "registry_instance_name";
	
	// no instance creation allowed
    private ClusterConstants() {}
}