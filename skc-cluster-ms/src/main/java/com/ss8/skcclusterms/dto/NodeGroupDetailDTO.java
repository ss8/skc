package com.ss8.skcclusterms.dto;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
// @Builder
public class NodeGroupDetailDTO {

	@JsonProperty("name")
	private String groupName;

	@JsonProperty("nodes")
	private List<String> groupNodes;

	@JsonProperty("groupVars") 
	private HashMap<String, String> groupVars;
	
	@JsonProperty("subGroups") 
	private List<NodeGroupDetailDTO> subGroups;

	//add group nodes
	public void addGroupNodes(String node)
	{
		this.groupNodes.add(node);
	}

	// Getters and Setters
	public List<NodeGroupDetailDTO> getSubGroups() {
		return subGroups;
	}

	public void setSubGroups(List<NodeGroupDetailDTO> subGroups) {
		this.subGroups = subGroups;
	}

	public HashMap<String, String> getGroupVars() {
		return groupVars;
	}

	public void setGroupVars(final HashMap<String, String> groupVars) {
		this.groupVars = groupVars;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(final String name) {
		this.groupName = name;
	}

	public List<String> getGroupNodes() {
		return groupNodes;
	}

	public void setGroupNodes(final List<String> nodes) {
		this.groupNodes = nodes;
	}

	@Override
	public String toString() {
		return "NodeGroupDetailDTO [groupName=" + groupName + ", groupNodes=" + groupNodes + ", groupVars=" + groupVars
				+ ", subGroups=" + subGroups + "]";
	}

}
