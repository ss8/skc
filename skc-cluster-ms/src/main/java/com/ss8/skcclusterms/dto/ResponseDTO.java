package com.ss8.skcclusterms.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseDTO {

  private ClusterDetailDTO clusterDetailDTO = null;
  private List<ClusterDetailDTO> clusterDetailDTOlist = null;
  private int status = -2;
  private String message = null;
  private String error = null;
  private String logs = null;
  private Exception exception = null;

  @Override
  public String toString() {
    return "ResponseDTO [clusterDetailDTO=" + clusterDetailDTO + ", clusterDetailDTOlist=" + clusterDetailDTOlist
        + ", error=" + error + ", exception=" + exception + ", logs=" + logs + ", message=" + message + ", status=" + status + "]";
  }

  public ResponseDTO() {
  }

  public ClusterDetailDTO getClusterDetailDTO() {
    return clusterDetailDTO;
  }

  public void setClusterDetailDTO(ClusterDetailDTO clusterDetailDTO) {
    this.clusterDetailDTO = clusterDetailDTO;
  }

  public List<ClusterDetailDTO> getClusterDetailDTOlist() {
    return clusterDetailDTOlist;
  }

  public void setClusterDetailDTOlist(List<ClusterDetailDTO> clusterDetailDTOlist) {
    this.clusterDetailDTOlist = clusterDetailDTOlist;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getError() {
    return error;
  }

  public void setError(String error) {
    this.error = error;
  }

  public String getLogs() {
    return logs;
  }

  public void setLogs(String logs) {
    this.logs = logs;
  }

  public Exception getException() {
    return exception;
  }

  public void setException(Exception exception) {
    this.exception = exception;
  }

}
