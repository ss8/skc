package com.ss8.skcclusterms.dto;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ss8.skcclusterms.enums.NodeRoles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.Builder;
import lombok.Data;

@Data
// @Builder
public class ClusterDetailDTO {
	// @Builder.Default
	private HashMap<String, String> clusterVars = new HashMap<>();

	@JsonProperty("clusterId")
	private Long clusterId = -1L;

	@JsonProperty("clusterName")
	private String clusterName;

	@JsonProperty("nodes")
	private List<NodeDetailDTO> clusterNodes;

	@JsonProperty("groups")
	private List<NodeGroupDetailDTO> clusterGroups;

	@JsonProperty("workerNodeLabel")
	private String workerNodeLabel;

	@JsonProperty("tmpStorageLocation")
	private String tmpStorageLocation;

	@JsonProperty("isDualStackEnabled")
	private Boolean dualStackEnabled;

	@JsonProperty("isRegistryEnabled")
	private Boolean isRegistryEnabled;

	@JsonProperty("registry")
	private RegistryDetailsDTO registryDetailsDTO;

	@JsonProperty("inventoryPath")
	private String inventoryPath;

	@JsonProperty("dashboardId")
	private String dashboardId;

	@JsonProperty("dockerStoragePath")
	private String dockerStoragePath;

	@JsonProperty("kubeletStoragePath")
	private String kubeletStoragePath;

	@JsonProperty("kubeAPIServerPortRange")
	private String kubeAPIServerPortRange;

	@JsonProperty("status")
	private String status;

	//Construtor
	public ClusterDetailDTO(String clusterName)
	{
		this.clusterName = clusterName;
	}
	
	public ClusterDetailDTO()
	{
		
	}

	
	// Setters and Getters

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getClusterId() {
		return clusterId;
	}

	public void setClusterId(Long id) {
		this.clusterId = id;
	}

	public String getClusterName() {
		return clusterName;
	}

	public void setClusterName(String clusterName) {
		clusterVars.put(ClusterConstants.ClusterName, clusterName);
		this.clusterName = clusterName;
	}

	public String getKubeletStoragePath() {
		return kubeletStoragePath;
	}

	public void setKubeletStoragePath(String kubeletStoragePath) {
		clusterVars.put(ClusterConstants.KubeletStoragePath, kubeletStoragePath);
		this.kubeletStoragePath = kubeletStoragePath;
	}

	public String getKubeAPIServerPortRange() {
		return kubeAPIServerPortRange;
	}

	public void setKubeAPIServerPortRange(String kubeAPIServerPortRange) {
		clusterVars.put(ClusterConstants.KubeAPIServerPortRange, kubeAPIServerPortRange);
		this.kubeAPIServerPortRange = kubeAPIServerPortRange;
	}

	public String getDockerStoragePath() {
		return dockerStoragePath;
	}

	public void setDockerStoragePath(String dockerStoragePath) {
		clusterVars.put(ClusterConstants.DockerStoragePath, dockerStoragePath);
		this.dockerStoragePath = dockerStoragePath;
	}

	public List<NodeDetailDTO> getClusterNodes() {
		return clusterNodes;
	}

	public void setClusterNodes(List<NodeDetailDTO> clusterNodes) {
		this.clusterNodes = clusterNodes;
	}

	public List<NodeGroupDetailDTO> getClusterGroups() {
		return clusterGroups;
	}

	public void setClusterGroups(List<NodeGroupDetailDTO> groups) {
		this.clusterGroups = groups;
	}

	public String getWorkerNodeLabel() {
		return workerNodeLabel;
	}

	public void setWorkerNodeLabel(String workerNodeLabel) {
		clusterVars.put(ClusterConstants.NodeLabels, workerNodeLabel);
		this.workerNodeLabel = workerNodeLabel;
	}

	public String getTmpStorageLocation() {
		return tmpStorageLocation;
	}

	public void setTmpStorageLocation(String tmpStorageLocation) {
		clusterVars.put(ClusterConstants.TmpDir, tmpStorageLocation);
		this.tmpStorageLocation = tmpStorageLocation;
	}

	public Boolean getDualStackEnabled() {
		return dualStackEnabled;
	}

	public void setDualStackEnabled(Boolean dualStackEnabled) {
		clusterVars.put(ClusterConstants.DualStackEnabled, dualStackEnabled.toString());
		this.dualStackEnabled = dualStackEnabled;
	}

	public Boolean getIsRegistryEnabled() {
		return this.isRegistryEnabled;
	}

	public void setIsRegistryEnabled(Boolean isRegistryEnabled) {
		if(isRegistryEnabled == null) isRegistryEnabled = false;
		clusterVars.put(ClusterConstants.CreateRegistry, isRegistryEnabled.toString());
		this.isRegistryEnabled = isRegistryEnabled;
	}

	public RegistryDetailsDTO getRegistryDetailsDTO() {
		return this.registryDetailsDTO;
	}

	public void setRegistryDetailsDTO(RegistryDetailsDTO registryDetailsDTO) {
		this.registryDetailsDTO = registryDetailsDTO;
	}

	public String getInventoryPath() {
		return this.inventoryPath;
	}

	public void setInventoryPath(String inventoryPath) {
		this.inventoryPath = inventoryPath;
	}

	public String getDashboardId() {
		return this.dashboardId;
	}

	public void setDashboardId(String dashboardId) {
		this.dashboardId = dashboardId;
	}

	public HashMap<String, String> getClusterVarMap() {
		return this.clusterVars;
	}

	public void setClusterVarMap(final HashMap<String, String> clusterVars) {
		this.clusterVars = clusterVars;
	}

	@Override
	public String toString() {
		return "ClusterDetailDTO [clusterId=" + clusterId + ", clusterName=" + clusterName + ", clusterNodes="
				+ clusterNodes + ", clusterGroups=" +
				clusterGroups + ", workerNodeLabel=" + workerNodeLabel + ", tmpStorageLocation=" +
				tmpStorageLocation + ", dualStackEnabled=" + dualStackEnabled + ", registryEnabled=" +
				isRegistryEnabled + ", registryDetailsDTO=" + registryDetailsDTO + ", inventoryPath=" +
				inventoryPath + ", dashboardId=" + dashboardId + ", dockerStoragePath=" + dockerStoragePath
				+ ",kubeletStoragePath="
				+ kubeletStoragePath + ", kubeAPIServerPortRange=" + kubeAPIServerPortRange +
				"]";
	}
}
