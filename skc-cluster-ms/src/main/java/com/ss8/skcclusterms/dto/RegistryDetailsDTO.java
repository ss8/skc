package com.ss8.skcclusterms.dto;

import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
// @Builder
@ToString
public class RegistryDetailsDTO {

	@JsonProperty("registryName")
	private String registryName;

	@JsonProperty("port")
	private String registryPort;

	@JsonProperty("volumePath")
	private String volumePath;

	@JsonProperty("containerName")
	private String containerName;

	@JsonProperty("isRegistryVIPEnabled")
	private Boolean isVIPEnabled;

	@JsonProperty("vip")
	private RegistryVIPDetailsDTO registryVIPDetails;

	@JsonProperty("nodes")
	private List<String> regNodes;

	// @Builder.Default
	private HashMap<String, String> registryVars= new HashMap<>();

	// Getters and Setters

	public String getRegistryName() {
		return registryName;
	}

	public void setRegistryName(String name) {
		this.registryName = name;
		registryVars.put(ClusterConstants.RegistryName, this.registryName);
	}

	public String getRegistryPort() {
		return registryPort;
	}

	public void setRegistryPort(String port) {
		this.registryPort = port;
		registryVars.put(ClusterConstants.RegistryPort, this.registryPort);
	}

	public String getVolumePath() {
		return volumePath;
	}

	public void setVolumePath(String volumePath) {
		this.volumePath = volumePath;
		registryVars.put(ClusterConstants.RegistryVolumePath, this.volumePath);

	}


	public String getContainerName() {
		return containerName;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
		registryVars.put(ClusterConstants.RegistryInstanceName, this.containerName);
	}

	public Boolean getIsVIPEnabled() {
		return isVIPEnabled;
	}

	public void setIsVIPEnabled(Boolean isVIPEnabled) {
		this.isVIPEnabled = isVIPEnabled;
		registryVars.put(ClusterConstants.RegistryVIPEnable, this.isVIPEnabled.toString());
	}

	public RegistryVIPDetailsDTO getRegistryVIPDetails() {
		return registryVIPDetails;
	}

	public void setRegistryVIPDetails(RegistryVIPDetailsDTO registryVIPDetails) {
		this.registryVIPDetails = registryVIPDetails;
		registryVars.put(ClusterConstants.RegistryVrrId, registryVIPDetails.getVrrId());
		registryVars.put(ClusterConstants.RegistryInterface, registryVIPDetails.getIntf());
		registryVars.put(ClusterConstants.RegistryVIP, registryVIPDetails.getIp());
	}

	public HashMap<String, String> getRegistryVarMap() {
		return registryVars;
	}

	public void setRegistryVarMap(final HashMap<String, String> map) {
		this.registryVars = map;
	}

	public List<String> getRegNodes() {
		return regNodes;
	}

	public void setRegNodes(List<String> nodes) {
		this.regNodes = nodes;
	}

}
