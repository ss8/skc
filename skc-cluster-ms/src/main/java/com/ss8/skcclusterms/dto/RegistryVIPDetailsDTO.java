package com.ss8.skcclusterms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
// @Builder
public class RegistryVIPDetailsDTO {

  @JsonProperty("interface")
  private String intf;

  @JsonProperty("ip")
  private String ip;

  @JsonProperty("vrrId")
  private String vrrId;

  // Getters and Setters

  public String getIntf() {
    return intf;
  }

  public void setIntf(String intf) {
    this.intf = intf;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getVrrId() {
    return vrrId;
  }

  public void setVrrId(String vrrId) {
    this.vrrId = vrrId;
  }

  @Override
  public String toString() {
    return "RegistryVIPDetailsDTO [intf=" + intf + ", ip=" + ip + ", vrrId=" + vrrId + "]";
  }

}
