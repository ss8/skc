package com.ss8.skcclusterms.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.dto.ModifyNodeDTO;
import com.ss8.skcclusterms.dto.ResponseDTO;
import com.ss8.skcclusterms.service.ClusterService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// @CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("cluster")
public class ClusterController {

  @Autowired
  ClusterService clusterService;

  private static final Logger LOG = LoggerFactory.getLogger(ClusterController.class);

  /**
   * Responds with cluster details if cluster is created successfully.
   * Cluster details are passed in request body as ClusterDetailDTO.
   * 
   * @param ClusterDetailDTO
   * @return ResponseDTO<ClusterDetailDTO>
   */

  // Only for testing the MS is working or not
  @GetMapping(value = "")
  public String homepage() {
    return "this is the home page : SKC Cluster Microservice hello";
  }

  @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> createCluster(@Validated @RequestBody ClusterDetailDTO clusterDetailDTO) {

    LOG.debug("Create Cluster from Controller");
    LOG.debug("clusterDetailDTO: " + clusterDetailDTO.toString());
    ResponseDTO responseDTO = clusterService.createCluster(clusterDetailDTO);
    if (responseDTO.getStatus() == 0) {
      return ResponseEntity.ok(responseDTO.getMessage());
    } else {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseDTO.getMessage());
    }
  }

  @PostMapping(value = "/{clusterId}/retry")
  public ResponseEntity<?> retryClusterCreate(@PathVariable(name = "clusterId", required = true) Long clusterId) {

    LOG.debug("Create Cluster from Controller");
    LOG.debug("clusterDetailDTO: " + clusterId);

    // return ResponseEntity.ok("This is output :" +
    // clusterService.retryCluster(clusterId));;
    // return ResponseEntity.ok(clusterService.retryCluster(clusterId));
    ResponseDTO responseDTO = clusterService.retryCluster(clusterId);

    if (responseDTO.getStatus() == 0) {
      return ResponseEntity.ok(responseDTO.getMessage());
    } else {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseDTO.getMessage());
    }
  }

  // testing purpose
  // @PostMapping(value = "/test/{clusterName}", produces =
  // MediaType.APPLICATION_JSON_VALUE)
  // public ResponseEntity<?> test(@PathVariable(name = "clusterName", required =
  // true) String clusterName) {

  // return ResponseEntity.ok(clusterService.test(clusterName));
  // }

  //////////////////////////////////////////////////////////////////////////////////

  /**
   * Returns a list of all clusters created.
   * 
   * @return ResponseDTO<List<ClusterDetailDTO>>
   */
  @GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<ClusterDetailDTO>> getAllClusters() {
    // TODO Implement this function & call getAllClusters function to list all

    return ResponseEntity.ok(clusterService.getClusterList());
  }

  /**
   * Returns details of given clusterId
   * 
   * @param clusterId int
   * @return ResponseDTO<ClusterDetailDTO>
   */
  @GetMapping(value = "/{clusterId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> getClusterDetail(
      @PathVariable(name = "clusterId", required = true) Long clusterId) {
    // TODO implement this function & call getClusterDetail from service layer to
    // get details
    return ResponseEntity.ok(clusterService.getClusterDetail(clusterId));
  }

  /**
   * Returns the ID of the cluster deleted
   * 
   * @param clusterId string
   * @return ResponseDTO<String>
   */
  @DeleteMapping(value = "/{clusterId}/delete", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> deleteCluster(
      @PathVariable(name = "clusterId", required = true) Long clusterId) {
    // TODO Implement this function & call deleteCluster from service layer to
    // delete a cluster
    // return ResponseEntity.ok(clusterService.deleteCluster(clusterId));
    ResponseDTO responseDTO = clusterService.deleteCluster(clusterId);
    if (responseDTO.getStatus() == 0) {
      return ResponseEntity.ok(responseDTO.getMessage());
    } else {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseDTO.getMessage());
    }
  }

  /**
   * Deletes a given node from a given cluster
   * 
   * @param clusterId string
   * @param nodeName  string
   * @return ResponseDTO<ClusterDetailDTO>
   */
  @DeleteMapping(value = "/{clusterId}/node/delete", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> deleteClusterNode(@Validated @RequestBody ModifyNodeDTO mNodeDTO) {
    LOG.debug("Deleting node...");
    LOG.debug(mNodeDTO.getClusterDetailDTO().toString());
    LOG.debug(mNodeDTO.getModifiedNodes().toString());
    if (mNodeDTO.getModifiedNodes().isEmpty()) {
      // return ResponseEntity.ok("No list in listNodes");
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No list in listNodes");
    } else {
      // return
      // ResponseEntity.ok(clusterService.deleteClusterNode(mNodeDTO.getClusterDetailDTO(),
      // mNodeDTO.getModifiedNodes()));
      ResponseDTO responseDTO = clusterService.deleteClusterNode(mNodeDTO.getClusterDetailDTO(),
          mNodeDTO.getModifiedNodes());

      if (responseDTO.getStatus() == 0) {
        return ResponseEntity.ok(responseDTO.getMessage());
      } else {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseDTO.getMessage());
      }
    }
  }

  /**
   * Adds a node in a given cluster.
   * Node can be a controller or a worker node.
   * 
   * @return ResponseDTO<ClusterDetailDTO>
   */
  @PostMapping(value = "/{clusterId}/node/add", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> addClusterNode(@Validated @RequestBody ModifyNodeDTO mNodeDTO) {
    LOG.debug("Adding node...");
    LOG.debug(mNodeDTO.getClusterDetailDTO().toString());
    LOG.debug(mNodeDTO.getModifiedNodes().toString());

    if (mNodeDTO.getModifiedNodes().isEmpty()) {
      // return ResponseEntity.ok("No list in listNodes");
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("No list in listNodes");
    } else {
      // return
      // ResponseEntity.ok(clusterService.addClusterNode(mNodeDTO.getClusterDetailDTO(),
      // mNodeDTO.getModifiedNodes()));
      ResponseDTO responseDTO = clusterService.addClusterNode(mNodeDTO.getClusterDetailDTO(),
          mNodeDTO.getModifiedNodes());

      if (responseDTO.getStatus() == 0) {
        return ResponseEntity.ok(responseDTO.getMessage());
      } else {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(responseDTO.getMessage());
      }
    }
  }

  /**
   * Returns controller node Ip after parsing the inventory file.
   * Accpts cluster name to identify the path to inventory file.
   * 
   * @return ResponseDTO<Map>
   */
  @PostMapping(value = "/controller/details", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> importClusterInv(@Validated @RequestBody Map<String, String> cluster) {
    String clusterName = cluster.get("clusterName");
    String ip = clusterService.fetchControllerIp(clusterName);

    if (ip == null) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND)
          .body("Controller IP not found for the given cluster, check request body !");
    } else if (ip.contains("Error"))
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ip);


    Map<String, String> clusterDetails = new HashMap<>();
    clusterDetails.put("clusterName", clusterName);
    clusterDetails.put("controllerIP", ip);

    return ResponseEntity.ok(clusterDetails);
  }

}
