package com.ss8.skcclusterms.dao;

import java.util.HashMap;
import java.util.List;

import com.ss8.grpc.skc.GroupDetail;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NodeGroupDetail {
  private String groupName;
  private List<String> groupNodes;
  private HashMap<String, String> groupVars;
  private List<NodeGroupDetail> subGroups;
}
