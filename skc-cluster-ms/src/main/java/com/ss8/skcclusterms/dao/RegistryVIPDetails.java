package com.ss8.skcclusterms.dao;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegistryVIPDetails {
  private String intf;
  private String ip;
  private String vrrId;
}
