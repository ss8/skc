package com.ss8.skcclusterms.dao;

import java.util.List;

import com.ss8.skcclusterms.enums.NodeRoles;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NodeDetail {
  private String nodeName;
  private String nodeIP;
  private List<NodeRoles> roles;
}
