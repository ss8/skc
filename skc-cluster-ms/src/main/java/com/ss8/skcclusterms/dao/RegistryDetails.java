package com.ss8.skcclusterms.dao;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RegistryDetails {

  private String registryName;
  private String registryPort;
  private String volumePath;
  private String containerName;
  private Boolean isVIPEnabled;
  
  private List<String> regNodes;

  private RegistryVIPDetails registryVIPDetails;
  
}
