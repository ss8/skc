package com.ss8.skcclusterms.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.util.ClusterDetailAdapter;
import com.vladmihalcea.hibernate.type.json.JsonType;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

@Entity
@Table(name = "cluster_details")
@TypeDef(name = "json", typeClass = JsonType.class)
public class Cluster {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "cluster_id", updatable = false, nullable = false)
    private Long clusterId;

    @Column(name = "cluster_name", unique = true)
    private String clusterName;

    @Type(type = "json")
    @Column(name = "cluster_detail_dto", columnDefinition = "json")
    private String clusterDetailDTO;

    @Column(name = "inventory_name")
    private String inventoryName;

    @Column(name = "cluster_status", length = 10)
    private String clusterStatus;

    // Constructor
    public Cluster() {
        super();
    }

    public Cluster(Long Id, ClusterDetailDTO clusterDetailDTO, String inventoryName, String clusterStatus) {
        clusterDetailDTO.setInventoryPath(inventoryName);
        clusterDetailDTO.setStatus(clusterStatus);
        clusterDetailDTO.setClusterId(Id);
        this.clusterId = Id;
        this.clusterName = clusterDetailDTO.getClusterName();
        this.clusterDetailDTO = ClusterDetailAdapter.convertClusterDetailDTOtoJSON(clusterDetailDTO);
        this.inventoryName = inventoryName;
        this.clusterStatus = clusterStatus;
    }

    public Cluster(ClusterDetailDTO clusterDetailDTO, String inventoryName, String clusterStatus) {
        super();
        this.clusterName = clusterDetailDTO.getClusterName();
        clusterDetailDTO.setInventoryPath(inventoryName);
        clusterDetailDTO.setStatus(clusterStatus);
        // add cluster id to json
        // JsonElement jsonElement = new Gson().toJsonTree(clusterDetailDTO);
        // jsonElement.getAsJsonObject().addProperty("clusterId", getClusterId());
        // System.out.println(jsonElement);
        // this.clusterDetailDTO =
        // ClusterDetailAdapter.convertClusterDetailDTOtoJSON(clusterDetailDTO);
        this.clusterDetailDTO = ClusterDetailAdapter.convertClusterDetailDTOtoJSON(clusterDetailDTO);
        this.inventoryName = inventoryName;
        this.clusterStatus = clusterStatus;
    }

    public Cluster(Cluster cluster, ClusterDetailDTO clusterDetailDTO) {
        this.clusterId = cluster.getClusterId();
        this.clusterName = cluster.getClusterName();
        this.clusterDetailDTO = ClusterDetailAdapter.convertClusterDetailDTOtoJSON(clusterDetailDTO);
        this.clusterStatus = cluster.getClusterStatus();
        this.inventoryName = cluster.getInventoryName();
    }

    // Setters and Getters

    public Long getClusterId() {
        return clusterId;
    }

    public void setClusterId(Long clusterId) {
        // this.clusterId = UUID.randomUUID().toString();
        this.clusterId = clusterId;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getClusterDetailDTO() {
        return clusterDetailDTO;
    }

    public void setClusterDetailDTO(String clusterDetailDTO) {
        this.clusterDetailDTO = clusterDetailDTO;
    }

    public String getClusterStatus() {
        return clusterStatus;
    }

    public void setClusterStatus(String clusterStatus) {
        this.clusterStatus = clusterStatus;
    }

    public String getInventoryName() {
        return inventoryName;
    }

    public void setInventoryName(String inventoryName) {
        this.inventoryName = inventoryName;
    }

    @Override
    public String toString() {
        return "Cluster [clusterDetailDTO=" + clusterDetailDTO + ", clusterId=" + clusterId + ", clusterName="
                + clusterName + ", clusterStatus=" + clusterStatus + ", inventoryName=" + inventoryName + "]";
    }

    public String toStringWithoutId() {
        return "Cluster [clusterDetailDTO=" + clusterDetailDTO + ", clusterName="
                + clusterName + ", clusterStatus=" + clusterStatus + ", inventoryName=" + inventoryName + "]";
    }

}