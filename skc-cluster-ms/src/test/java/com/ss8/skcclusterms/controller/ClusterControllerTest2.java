package com.ss8.skcclusterms.controller;

import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.ss8.skcclusterms.dao.Cluster;
import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.dto.ModifyNodeDTO;
import com.ss8.skcclusterms.dto.NodeDetailDTO;
import com.ss8.skcclusterms.dto.ResponseDTO;
import com.ss8.skcclusterms.enums.NodeRoles;
import com.ss8.skcclusterms.repository.ClusterRepository;
import com.ss8.skcclusterms.service.ClusterService;
import com.ss8.skcclusterms.service.ClusterServiceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Value;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureMockMvc
public class ClusterControllerTest2 {

        private MockMvc mockMvc;
        private ObjectMapper mapper;

        @Mock
        private ClusterRepository clusterRepository;

        @Mock
        private ClusterDetailDTO clusterDetailDTO;

        private ClusterDetailDTO c1;

        @Mock
        private Cluster cluster;

        @Mock
        private ClusterServiceImpl clusterServiceImpl;

        @Mock
        private ClusterService clusterService;

        private ResponseDTO responseDTO;

        @InjectMocks
        private ClusterController clusterController;

        @Before
        public void setUp() {
                mockMvc = MockMvcBuilders.standaloneSetup(clusterController).build();
                mapper = new ObjectMapper();
        }

        @Test
        public void homepage() throws Exception {

                MvcResult mvcResult = this.mockMvc
                                .perform(get("/cluster")
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andReturn();
                System.out.println(mvcResult.toString());
                System.out.println(mvcResult.getResponse().getContentAsString());
                // clusterController.createCluster(clusterDetailDTO);
                assertEquals(true, (mvcResult.getResponse() != null));
                assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        }

        @Test
        public void createCluster() throws Exception {
                c1 = clusterDetailDTOpopulate();
                responseDTO = new ResponseDTO();
                responseDTO.setStatus(0);
                when(clusterService.createCluster(Mockito.any(ClusterDetailDTO.class))).thenReturn(responseDTO);
                MvcResult mvcResult = this.mockMvc
                                .perform(post("/cluster/create")
                                                .content(mapper.writeValueAsString(c1))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andReturn();

                // clusterController.createCluster(clusterDetailDTO);
                assertEquals(true, (mvcResult.getResponse() != null));
                assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
                System.out.println(mvcResult.getResponse().getContentAsString());
        }

        @Test
        public void listCluster() throws Exception {
                List<ClusterDetailDTO> clusterlist = new ArrayList<>();
                clusterlist.add(clusterDetailDTOpopulate());
                clusterlist.add(clusterDetailDTOpopulate());
                when(clusterService.getClusterList()).thenReturn(clusterlist);
                MvcResult mvcResult = this.mockMvc
                                .perform(get("/cluster/list")
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andReturn();
                System.out.println(mvcResult.getResponse().getContentAsString());
                assertEquals(true, (mvcResult.getResponse() != null));
                assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        }

        @Test
        public void deleteCluster() throws Exception {
                responseDTO = new ResponseDTO();
                responseDTO.setStatus(0);
                when(clusterService.deleteCluster(131L)).thenReturn(responseDTO);
                MvcResult mvcResult = this.mockMvc
                                .perform(delete("/cluster/131/delete")
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andReturn();
                assertEquals(true, (mvcResult.getResponse() != null));
                assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        }

        @Test
        public void getCluster() throws Exception {
                c1 = clusterDetailDTOpopulate();
                when(clusterService.getClusterDetail(131L)).thenReturn(c1);
                MvcResult mvcResult = this.mockMvc
                                .perform(get("/cluster/131")
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andReturn();
                assertEquals(true, (mvcResult.getResponse() != null));
                assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        }

        @Test
        public void addClusterNode() throws Exception {
                c1 = clusterDetailDTOpopulate();
                // when(clusterService.getClusterDetail(131L)).thenReturn(c1);
                List<NodeDetailDTO> nodesAddedToCluster = new ArrayList<>();
                List<NodeRoles> nodeRoles = new ArrayList<>();
                nodeRoles.add(NodeRoles.CONTROLLER);
                NodeDetailDTO node = new NodeDetailDTO("node-1", "12.0.10.155", nodeRoles);
                nodesAddedToCluster.add(node);
                responseDTO = new ResponseDTO();
                responseDTO.setStatus(0);
                ModifyNodeDTO modifyNodeDTO = new ModifyNodeDTO(c1, nodesAddedToCluster);
                // when(clusterService.addClusterNode(c1,
                // nodesAddedToCluster)).thenReturn(responseDTO);
                when(clusterService.addClusterNode(Mockito.any(ClusterDetailDTO.class), Mockito.anyList()))
                                .thenReturn(responseDTO);
                MvcResult mvcResult = this.mockMvc
                                .perform(post("/cluster/131/node/add")
                                                .content(mapper.writeValueAsString(modifyNodeDTO))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andReturn();
                System.out.println("Add Cluster Node" + '\n');
                System.out.println(mvcResult.getResponse().getContentAsString());
                assertEquals(true, (mvcResult.getResponse() != null));
                assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        }

        @Test
        public void deleteClusterNode() throws Exception {
                c1 = clusterDetailDTOpopulate();
                // when(clusterService.getClusterDetail(131L)).thenReturn(c1);
                List<NodeDetailDTO> nodesAddedToCluster = new ArrayList<>();
                List<NodeRoles> nodeRoles = new ArrayList<>();
                nodeRoles.add(NodeRoles.CONTROLLER);
                NodeDetailDTO node = new NodeDetailDTO("node-1", "12.0.10.155", nodeRoles);
                nodesAddedToCluster.add(node);
                responseDTO = new ResponseDTO();
                responseDTO.setStatus(0);
                ModifyNodeDTO modifyNodeDTO = new ModifyNodeDTO(c1, nodesAddedToCluster);
                // when(clusterService.addClusterNode(c1,
                // nodesAddedToCluster)).thenReturn(responseDTO);
                when(clusterService.deleteClusterNode(Mockito.any(ClusterDetailDTO.class), Mockito.anyList()))
                                .thenReturn(responseDTO);
                MvcResult mvcResult = this.mockMvc
                                .perform(delete("/cluster/131/node/delete")
                                                .content(mapper.writeValueAsString(modifyNodeDTO))
                                                .contentType(MediaType.APPLICATION_JSON)
                                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isOk()).andReturn();
                System.out.println(mvcResult.getResponse().getContentAsString());
                assertEquals(true, (mvcResult.getResponse() != null));
                assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        }

        public ClusterDetailDTO clusterDetailDTOpopulate() throws IOException {
                c1 = mapper.readValue(DTOtext(),
                                new TypeReference<ClusterDetailDTO>() {
                                });

                return c1;
        }

        public String DTOtext() throws IOException {
                System.out.println("DTOtext:start");

                // ObjectMapper mapper = new ObjectMapper(); // can reuse, share globally
                mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
                JsonNode node;

                String wd = System.getProperty("user.dir");
                System.out.println("Working Directory = " + wd);

                BufferedReader defValuesReader = new BufferedReader(
                                // new FileReader("/SS8/skc/skc-cluster-ms/src/test/resources/Cluster_creation_testJSON.json"));
                                new FileReader(wd + "/src/test/resources/Cluster_creation_testJSON.json"));
                node = mapper.readTree(defValuesReader);
                System.out.println("DTOtext:end");
                return node.toString();

        }

}
