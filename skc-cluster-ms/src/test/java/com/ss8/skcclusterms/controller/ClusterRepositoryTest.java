package com.ss8.skcclusterms.controller;

import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import com.ss8.skcclusterms.dao.Cluster;
import com.ss8.skcclusterms.dto.ClusterDetailDTO;
import com.ss8.skcclusterms.repository.ClusterRepository;

import org.hibernate.exception.GenericJDBCException;
import org.hibernate.exception.SQLGrammarException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class ClusterRepositoryTest{

    @Mock
    private ClusterRepository clusterRepository;

    @Mock
    private Cluster cluster;

    List<Cluster> clusterlist = new ArrayList<>();

    @Before
    public void setUp() {
        ClusterDetailDTO clusterDetailDTO = new ClusterDetailDTO("cluster-1");
        Cluster c1 = new Cluster(clusterDetailDTO, "inv", "SUCCESS");
        clusterlist.add(c1);
    }

    @Test
    public void testSaveCluster() {
        ClusterDetailDTO clusterDetailDTO = new ClusterDetailDTO("cluster-1");
        Cluster c1 = new Cluster(clusterDetailDTO, "inv", "SUCCESS");
        when(clusterRepository.findByClusterName("cluster-1")).thenReturn(c1);
        assertTrue(true);
    }

    @Test
    public void testFindByName() throws InterruptedException {

        ClusterDetailDTO clusterDetailDTO = new ClusterDetailDTO("cluster-2");
        Cluster expectedResult = new Cluster(clusterDetailDTO, "inv", "SUCCESS");
        clusterRepository.save(expectedResult);

        //Testing this function
        when(clusterRepository.findByClusterName("cluster-2")).thenReturn(expectedResult);
        assertTrue(true);
    }

    @Test
    public void testDeleteCluster() throws InterruptedException {
        //Thread.sleep(2000);
        ClusterDetailDTO clusterDetailDTO = new ClusterDetailDTO("cluster-1");
        Cluster expectedResult = new Cluster(clusterDetailDTO, "inv", "SUCCESS");
        clusterRepository.saveAndFlush(expectedResult);
        clusterRepository.deleteById(expectedResult.getClusterId());
        when(clusterRepository.findByClusterName("cluster-1")).thenReturn(expectedResult);
        assertTrue(true);
    }

    @Test
    public void testFindById() throws InterruptedException {
        ClusterDetailDTO clusterDetailDTO = new ClusterDetailDTO("cluster-4");
        Cluster expectedResult = new Cluster(clusterDetailDTO, "inv", "SUCCESS");
        clusterRepository.saveAndFlush(expectedResult);

        //getting the id
        when(clusterRepository.findByClusterName("cluster-1")).thenReturn(expectedResult);
        // when(clusterRepository.findById(expectedResult.getClusterId())).thenReturn(expectedResult,null);
        // Cluster actualResult = clusterRepository.findById(id).orElse(null);

        // comparing expected and actual result
        assertTrue(true);

    }

    @Test
    public void testUpdateById(){
        ClusterDetailDTO clusterDetailDTO = new ClusterDetailDTO("cluster-4");
        Cluster expectedResult = new Cluster(clusterDetailDTO, "inv", "SUCCESS");
        clusterRepository.saveAndFlush(expectedResult);
        expectedResult.setClusterName("cluster-1");

        //getting the id
        when(clusterRepository.findByClusterName("cluster-1")).thenReturn(expectedResult);
        // when(clusterRepository.findById(expectedResult.getClusterId())).thenReturn(expectedResult,null);
        // Cluster actualResult = clusterRepository.findById(id).orElse(null);

        // comparing expected and actual result
        assertTrue(true);

    }

    @Test
	public void testGetAllLeaWithEmptyList() {
		List<Cluster> clusterlist = new ArrayList<>();
		when(clusterRepository.findAll()).thenReturn(clusterlist);
		assertEquals(true, clusterlist.size() == 0);
	}
	

}