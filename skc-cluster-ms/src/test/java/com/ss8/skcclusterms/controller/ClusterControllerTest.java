package com.ss8.skcclusterms.controller;

import com.ss8.skcclusterms.service.ClusterServiceImpl;
import com.ss8.skcclusterms.dto.ClusterDetailDTO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.runner.RunWith;
import org.springframework.test.web.servlet.MockMvc;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.ss8.skcclusterms.service.ClusterService;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Errors;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ClusterControllerTest {

    final static int CLUSTER_ID = 1;

    final static String REST_API_CREATE_CLUSTER = "/cluster/create";
    final static String REST_API_LIST_CLUSTER = "/cluster/list";
    final static String REST_API_CLUSTER = "/cluster/" + CLUSTER_ID;
    final static String REST_API_RETRY_CLUSTER = "/cluster/" + CLUSTER_ID + "/retry";
    final static String REST_API_DELETER_CLUSTER = "/cluster/" + CLUSTER_ID + "/delete";
    final static String REST_API_ADDNODE_CLUSTER = "/cluster/" + CLUSTER_ID + "/node/add";
    final static String REST_API_DELETENODE_CLUSTER = "/cluster/" + CLUSTER_ID + "/node/delete";
    final static String REST_API_ADDDASHBOARD_CLUSTER = "/cluster/controller/details";

    private MockMvc mockMvc;

    @InjectMocks
    private ClusterController handle;

    @Mock
    Errors error;

    @Mock
    private JSch jSch;

    @Mock
    Session mockSession;

    @Mock
    private ClusterServiceImpl mockClusterServiceImpl;

    @Mock
    private ClusterDetailDTO mockClusterDetailDTO;

    @Mock
    private List<ClusterDetailDTO> mockClusterDetailDTOs;

    private ObjectMapper mapper;

    @Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(handle).build();
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mockClusterDetailDTOs = prepareDummyData();

	}

    //--------------------Fetch Cluster List------------//
    @Test
    @Disabled
    public void fetchClusterListSuccessTest()throws Exception{
        List<ClusterDetailDTO> expectedResponse = new ArrayList<ClusterDetailDTO>();
        List<ClusterDetailDTO> clusterList = new ArrayList<ClusterDetailDTO>();

        createClusterListDummy();

        clusterList.add(new ClusterDetailDTO());
        clusterList.add(new ClusterDetailDTO());
        clusterList.add(new ClusterDetailDTO());
        clusterList.add(new ClusterDetailDTO());

        MvcResult mvcResult= this.mockMvc.perform(get("/cluster/list")).andExpect(status().isOk()).andReturn();

        assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
        assertEquals(mapper.writeValueAsString(expectedResponse), mvcResult.getResponse().getContentAsString());
		deleteClusterListDummy();

    }

    //need data for clusterDetailDTO constructor
    private List<ClusterDetailDTO> prepareDummyData() {
		List<ClusterDetailDTO> dummyClusterDetailDTOs = new ArrayList<>();

		dummyClusterDetailDTOs.add(new ClusterDetailDTO());
		return dummyClusterDetailDTOs;
	}

    //need structure change
    public void createClusterListDummy() throws IOException{
		File clusterList = new File("cluster-list-test.txt");
		FileWriter fileWriter = new FileWriter(clusterList);

		fileWriter.write("Cluster1:172.0.17.10");
		fileWriter.write(System.getProperty( "line.separator" ));
		fileWriter.write("Cluster2:189.0.57.10");
		fileWriter.write(System.getProperty( "line.separator" ));
		fileWriter.write("test-cluster:100.0.19.41");
		fileWriter.write(System.getProperty( "line.separator" ));
		fileWriter.write("test-cluster-cant-connect:102.23.19.21");

		fileWriter.close();
	}

    public void deleteClusterListDummy(){
		File clusterList = new File("cluster-list-test.txt");
		clusterList.delete();
	}

    @Test
	public void fetchClusterListFileNotFound()throws Exception{

		// ReflectionTestUtils.setField(handle, "skiClusterListFilePath", "cluster-list-file_not_found.txt");

		// MvcResult mvcResult= this.mockMvc.perform(get("/cluster/listist")).andExpect(status().isNotFound()).andReturn();

		// assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
		
	}

     //--------------------Fetch Cluster from ID------------//

     @Test
     public void fetchClusterbyID() throws Exception{

        ClusterDetailDTO expectedResult;
        ClusterDetailDTO actualResult = mockClusterServiceImpl.getClusterDetail(10L);
        System.out.println("hello");

     }


}
