# Refer: https://dev.to/techschoolguru/how-to-secure-grpc-connection-with-ssl-tls-in-go-4ph

# Current client’s and server’s private key that we are using are not encrypted. It’s because we use the -nodes option when generating them
# 1. Generate CA's private key and self-signed certificate

openssl req -x509 -newkey rsa:4096 -days 3650 -nodes -keyout ca-key.pem -out ca-cert.pem -subj "/C=US/ST=California/L=Milipitas/O=SS8/OU=Engineering/CN=*.ss8.com/emailAddress=support@ss8.com"

echo "CA's self-signed certificate"
openssl x509 -in ca-cert.pem -noout -text

# 2. Generate web server's private key and certificate signing request (CSR)
openssl req -newkey rsa:4096 -nodes -keyout server-key.pem -out server-req.pem -subj "/C=US/ST=California/L=Milipitas/O=SS8/OU=Engineering/CN=*.ss8.com/emailAddress=support@ss8.com"

# 3. Use CA's private key to sign web server's CSR and get back the signed certificate
openssl x509 -req -in server-req.pem -days 3650 -CA ca-cert.pem -CAkey ca-key.pem -CAcreateserial -out server-cert.pem -extfile server-ext.cnf

echo "Server's signed certificate"
openssl x509 -in server-cert.pem -noout -text


# 4. Generate client's private key and certificate signing request (CSR)
openssl req -newkey rsa:4096 -nodes -keyout client-key.pem -out client-req.pem -subj "/C=US/ST=California/L=Milipitas/O=SS8/OU=Engineering/CN=*.ss8.com/emailAddress=support@ss8.com"

# 5. Use CA's private key to sign client's CSR and get back the signed certificate
openssl x509 -req -in client-req.pem -days 3650 -CA ca-cert.pem -CAkey ca-key.pem -CAcreateserial -out client-cert.pem -extfile client-ext.cnf

echo "Client's signed certificate"
openssl x509 -in client-cert.pem -noout -text

# Add generated client certificate to trusted-clients collection
cat client-cert.pem > trusted-clients.crt.collection

# Add generated server certificate to trusted-server collection
cat server-cert.pem > trusted-servers.crt.collection