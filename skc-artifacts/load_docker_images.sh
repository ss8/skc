#!/bin/bash

WORKING_DIR=$(cd $(dirname $0); pwd)
DIMGDIR="${WORKING_DIR}/skc-docker-images"

# This function loads all docker images from tar files in a folder.
# It takes 1 argument
# $1: Path to docker images folder
load_docker_images()
{
    local imagedir=${1}

    echo "Loading docker images related to SKC"
    echo "${PASSWORD}" \
         | sudo -S tar -xzvf "${imagedir}.tgz" --directory ${WORKING_DIR}
    
    for file in ${imagedir}/*.tar; do
        docker image load < ${file}
    done

    echo "${PASSWORD}" \
        | sudo -S rm -rf ${imagedir}

    echo "Done: Loading docker images related to SKC"
}

# This function removes docker images listed in given folder.
unload_docker_images()
{
    local imagedir=${1}

    echo "Unloading docker images related to SKC"
    echo "${PASSWORD}" \
        | sudo -S tar -xzvf "${imagedir}.tgz" --directory ${WORKING_DIR}

    for image in ${imagedir}/*.tar; do 
        echo ${image} | sed -r "s#(.*)_#\1:#g" |sed "s#_#\/#g" \
    	    | sed "s#.tar##"| sed "s#${imagedir}##"| xargs docker rmi;
    done

    echo "${PASSWORD}" \
        | sudo -S rm -rf ${imagedir}

    echo "Done: Unloading docker images related to SKC"
}


if [[ $# -ne 2 ]]; then
	echo "Some or all arguments are missing.."
	exit 1
fi

if [[ "$1" == "load" ]]; then
    PASSWORD="$2"
	load_docker_images ${DIMGDIR}
fi

if [[ "$1" == "unload" ]]; then
    PASSWORD="$2"
	#unload_docker_images ${DIMGDIR}
fi
