#!/bin/bash

set -o pipefail -e

CURRENT_DIR=$(cd $(dirname $0); pwd)
WORKING_DIR=${CURRENT_DIR}/..

DASHBOARD_DIR=${WORKING_DIR}/skc-dashboard-ms
SKC_UI_DIR=${WORKING_DIR}/skc-UI
SKC_CLUSTER_DIR=${WORKING_DIR}/skc-cluster-ms
SKI_GRPC_DIR=${WORKING_DIR}/ski-grpc-apis
DOCKERFILES_DIR=${CURRENT_DIR}/dockerfiles
SKC_INSTALL_DIR=/opt/ss8-skc-installer
SKC_IMAGE_DIR=skc-docker-images
SKC_GIT_URL="https://bitbucket.org/ss8/skc.git"

DASHBOARD_IMAGE="ss8/skc-dashboard"
CLUSTER_IMAGE="ss8/skc-cluster"
GRPC_IMAGE="ss8/skc-grpc"
MARIADB_IMAGE="mariadb"
TAG="latest"
COMMON_NAME="skc.ss8.com"

helpfunction() {
	echo ""
	echo "Usage: $0 -t cluster_type -cn common_name"
	echo "Get Help: $0 -h|--help"
	echo -e "\t -n|--commonname \t Common Name to be given to SKC UI SSL certificate"
	echo -e "\t -t|--tag \t SKC RPM Tag"
	echo -e "\t -h|--help \t Help or Usage"
}

generate_skc_ui_certs() {
	mkdir -p ${SKC_UI_DIR}/certs
	sh ${SKC_UI_DIR}/generate_ssl_certs.sh -cn ${1}
}

generate_dashboard_resources_tar() {
	cp ${DASHBOARD_DIR}/images_list.txt .
	sh ${DASHBOARD_DIR}/scripts/generate_dashboard_resources_tar.sh
	rm images_list.txt
}

git_clone_repos() {
	echo -e "\nCloning SKC Repository..."
	git -C ${WORKING_DIR} clone ${SKC_GIT_URL}
}

genarate_mariadb_docker_image(){
	# Pull mariadb:latest image
	tag=${1}
	mariadbimg="ss8/${MARIADB_IMAGE}:${tag}"
	echo -e "\nPulling ${MARIADB_IMAGE} docker image..."
	docker pull ${MARIADB_IMAGE}
	docker image tag ${MARIADB_IMAGE} ${mariadbimg}	
	local mariadb_tar=$(gen_filename  ${mariadbimg})
	docker save -o ${SKC_IMAGE_DIR}/${mariadb_tar} ${mariadbimg}
}

generate_skc_image(){
	local tag=${1}
	local ms=$(echo ${2,,})	## ms->microservice
	local msimg=$(echo "${2}"_IMAGE)
	msimgtag="${!msimg}:${tag}"

	echo -e "\nBuilding ${msimgtag} docker image..."
	docker build -t ${msimgtag} -f "${DOCKERFILES_DIR}/${ms}.gradle.Dockerfile" ${WORKING_DIR}
	echo -e "Saving ${msimgtag} docker image..."
	local ms_tar=$(gen_filename ${msimgtag})
	docker save -o ${SKC_IMAGE_DIR}/${ms_tar} ${msimgtag}

}

build_skc_docker_images()
{
        echo "Build and package skc microservices docker images"
	tag="v${1}"
	mkdir -p ${SKC_IMAGE_DIR}

	for ms in DASHBOARD CLUSTER GRPC; do
	     generate_skc_image ${tag} ${ms}
	     echo ""
	done

	## Pull latest MARIDB image and save it as tar file
	genarate_mariadb_docker_image ${tag}

	echo -e "\nGenerating ${SKC_IMAGE_DIR}.tgz..."
	ls ${SKC_IMAGE_DIR}
	tar -cvzf ${SKC_IMAGE_DIR}.tgz ${SKC_IMAGE_DIR}/
	rm -rf ${SKC_IMAGE_DIR}
}

gen_filename() {
	### replace slash(/) and colon(:) with under_score and append .tar
	echo ${1} | sed "s#/#_#g" | sed "s#:#_#g" | sed 's/$/.tar/'
}

update_tag() {
	echo -e "\nAdding the user given TAG to ${2} file..."
	sed -i "s/\bv1.0.0\b/v${1}/g" ${2}
}

generate_final_tar() 
{
	local tag=${1}
	local ss8_skc_installer="ss8-skc-installer"
	local final_dir="${WORKING_DIR}/${ss8_skc_installer}-${tag}"
	
	mkdir -p ${final_dir}
	
	for file in load_docker_images.sh skc-installer.sh docker-compose.prod.yml; do
		cp ${CURRENT_DIR}/${file} ${final_dir}
	done
	mv ${WORKING_DIR}/${SKC_IMAGE_DIR}.tgz ${final_dir}
	
	update_tag ${tag} ${final_dir}/docker-compose.prod.yml

	echo -e "\nGenerating final ${ss8_skc_installer}.tgz..."
	tar -C ${WORKING_DIR} -cvzf ${ss8_skc_installer}.tgz ${final_dir}/
	rm -rf ${final_dir}
}

main() {

#	git_clone_repos
	generate_skc_ui_certs ${COMMON_NAME}
	generate_dashboard_resources_tar
        build_skc_docker_images ${TAG}
	generate_final_tar ${TAG}
}

ARGS=$(getopt -a --options n:t:h --long "commonname:,tag:,help" -- "$@")
 
eval set -- "$ARGS"
 
while true; do
  case "$1" in
    -n|--commonname)
      COMMON_NAME="$2"
      echo "Common Name for SKC UI certs: $COMMON_NAME"
      shift 2;;
	-t|--tag)
	  TAG="$2"
	  echo "SKC RPM TAG: $TAG"
	  shift 2;;
    -h|--help)
      helpfunction
	  exit
      break;;
    --)
	#   exit
      break;;
  esac
done

if [[ ${TAG} == "latest" ]]; then
	echo "Please provide a tag for SKC RPM"
	helpfunction
	exit 1
fi

if [[ ${COMMON_NAME} == "skc.ss8.com" ]]; then
	echo "Using default common name: ${COMMON_NAME} for SKC UI SSL certificates"
fi

main

set +o pipefail +e
