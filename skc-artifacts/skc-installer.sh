#!/bin/bash

# Installation script to run after untar

# Global Variables
WORKING_DIR=$(cd $(dirname $0); pwd)

CLUSTERLIST_FILE_PATH="/opt/ss8-ski-installer"

CLUSTERLIST_FILE_NAME="cluster-list.txt"
SKI_CLUSTER_LIST_EXIST_BIND_MOUNT="$CLUSTERLIST_FILE_PATH/$CLUSTERLIST_FILE_NAME:$CLUSTERLIST_FILE_PATH/$CLUSTERLIST_FILE_NAME:ro"
SKI_CLUSTER_LIST_NOT_EXIST_BIND_MOUNT="$CLUSTERLIST_FILE_PATH"
DELETE_VOLUMES=0

helpfunction() {
	echo ""
	echo "Usage: sh $0 <action> -p|--password <password>"
	echo "Get Help: sh $0 -h|--help"
	echo -e "\nActions:"
	echo -e "\tinstall \t Installs SKC"
	echo -e "\t\t\t (Loads SKC related images & deploys them using docker-compose)"
	echo -e "\tuninstall \t Uninstalls SKC"
	echo -e "\t\t\t (Unloads SKC related images & delete the deployment created by docker-compose)"
	echo -e "\nOptions:"
	echo -e "\t -p|--password \t User Password"
	echo -e "\t\t\t (Provide the current user account password)"
	echo -e "\t --volume \t Delete volumes"
	echo -e "\t\t\t Please note that this option is available only with uninstall action and will be ignored by install"
	echo -e "\t\t\t This will delete all the volumes created by SKC"
	echo -e "\t -h|--help \t Help or Usage"
	# exit 1
}

_brk_line() {
	echo "--------------------------------------------------------------------------------"
}

get_skc_ip() {
	ip -4 addr show eth0 |grep -oP "(?<=inet ).*(?=/)"|head -n 1
}

load_docker_images() {
	sh ${WORKING_DIR}/load_docker_images.sh load "${PASSWORD}"
}

unload_docker_images() {
	sh ${WORKING_DIR}/load_docker_images.sh unload "${PASSWORD}"
}


pre_install() {
	_brk_line
	echo "Performing Pre-install Tasks"
	_brk_line

	# Fetching Host Machine IP
	echo "Fetching Host Machine IP"
	export SKC_IP=$(get_skc_ip)
	if [[ -z "$SKC_IP" ]]; then
		echo "Could not fetch Host Machine IP... Aborting.."
		exit 1
	fi
	echo "Host IP ($SKC_IP) fetched..!!!"

	# Check if ski cluster-list exists...
	if [ -f "$CLUSTERLIST_FILE_PATH/$CLUSTERLIST_FILE_NAME" ]; then
		# echo "${PASSWORD}" \
		# 	| sudo -S mkdir -p $CLUSTERLIST_FILE_PATH
		# echo "${PASSWORD}" \
		# 	| sudo -S echo > "$CLUSTERLIST_FILE_PATH/$CLUSTERLIST_FILE_NAME"
		export SKI_CLUSTER_LIST_BIND_MOUNT=${SKI_CLUSTER_LIST_EXIST_BIND_MOUNT}
	else
		export SKI_CLUSTER_LIST_BIND_MOUNT=${SKI_CLUSTER_LIST_NOT_EXIST_BIND_MOUNT}
	fi

	# Load Docker Images realted to SKC
	load_docker_images
	
	_brk_line
	echo "Done: Pre-install Tasks"
	_brk_line
}

post_install() {
	_brk_line
	echo "Performing Post-install Tasks"
	_brk_line

	_brk_line
	echo "Done: Performing Post-install Tasks"
	_brk_line
}


up_docker_compose() {
	# Deploy SKC
	echo "Deploying SKC using Docker-Compose.."
	docker-compose -f docker-compose.prod.yml up -d
	local ret=$?
	if [[ "$?" -ne 0 ]]; then
		echo "An error occured while deploying SKC"
		exit 1
	fi
	echo "SKC deployed successfully..."
}

down_docker_compose() {
	# Removing SKC Deployment
	echo "Removing SKC Deployment.."
	if [[ ${DELETE_VOLUMES} -eq 0 ]]; then
		docker-compose --file docker-compose.prod.yml down
		echo "---Volumes are preserved---"
	else
		docker-compose --file docker-compose.prod.yml down -v
		echo "---Volumes are deleted---"
	fi
	echo "SKC Deployment removed.."
}


install_docker_compose() {
	_brk_line
	echo "Installing SKC App using docker-compose"
	_brk_line
	# Perform Pre-Install Tasks\
	pre_install

	# Checking if docker-compose file is valid
	echo "Checking Docker-Compose YAML validity.."
	docker-compose -f docker-compose.prod.yml config
	local ret=$?
	if [[ "$ret" -ne 0 ]]; then
		echo "There is an error in Docker-Compose YAML.. Aborting.."
		exit 1
	fi
	echo "Docker-compose YAML valid!!"

	# Deploy SKC
	up_docker_compose

	# Adding skc-dashboard container to default bridge network
	docker network connect bridge skc-dashboard

	# Perform Post-Install Tasks
	post_install

	_brk_line
	echo "Done: Installing SKC using docker-compose"
	_brk_line
}


uninstall_docker_compose() {
	_brk_line
	echo "Deleting SKC deployment created by docker-compose"
	_brk_line

	export SKC_IP=$(get_skc_ip)

	# Check if ski cluster-list exists...
	if [ -f "$CLUSTERLIST_FILE_PATH/$CLUSTERLIST_FILE_NAME" ]; then
		# echo "${PASSWORD}" \
		# 	| sudo -S mkdir -p $CLUSTERLIST_FILE_PATH
		# echo "${PASSWORD}" \
		# 	| sudo -S echo > "$CLUSTERLIST_FILE_PATH/$CLUSTERLIST_FILE_NAME"
		export SKI_CLUSTER_LIST_BIND_MOUNT=${SKI_CLUSTER_LIST_EXIST_BIND_MOUNT}
	else
		export SKI_CLUSTER_LIST_BIND_MOUNT=${SKI_CLUSTER_LIST_NOT_EXIST_BIND_MOUNT}
	fi

	# Remove SKC Deployment
	down_docker_compose

	# Unload Docker Images
	#unload_docker_images

	_brk_line
	echo "Done: Deleting SKC deployment created by docker-compose"
	_brk_line
}

show_volume_prompt() {
	echo -e "\n --volume option is selected..."
	echo -e "\n\n\t Please note that this will delete all the volumes created by SKC which includes logs and database data"
	echo -e "\n\t This action is irreversible and data will be lost forever.."
	read -p " Are you sure, you want to procede? [Y/n]  " response

	case $response in
		[yY][eE][sS]|[yY]|[jJ])
			echo -e "\n\n\t Volumes will now be deleted"
			break;;
		*)
			echo -e "\n\n\t Exiting..."
			echo -e "\t No changes made"
			exit 1
	esac
}


ACTION=$1
shift 1

if [[ "$ACTION" != "install" ]] && [[ "$ACTION" != "uninstall" ]]; then
	echo "Please provide a valid action to perform (install/uninstall)"
	helpfunction
	exit 1
fi

ARGS=$(getopt -a --options p:h --long "password:,volume,help" -- "$@")
 
eval set -- "$ARGS"
 
while true; do
  case "$1" in
    -p|--password)
      PASSWORD="$2"
      shift 2;;
    -h|--help)
      helpfunction
	  exit
      break;;
	--volume)
	  DELETE_VOLUMES=1
	  shift 1;;
    --)
	#   exit
      break;;
  esac
done

if [ -z "$PASSWORD" ]; then
	echo "Please provide user password!" >&2
	helpfunction
	exit 1
fi

if [ "$ACTION" == "install" ]; then
	install_docker_compose
elif [ "$ACTION" == "uninstall" ]; then
	if [[ ${DELETE_VOLUMES} -ne 0 ]]; then
		show_volume_prompt
	fi
	uninstall_docker_compose
fi

