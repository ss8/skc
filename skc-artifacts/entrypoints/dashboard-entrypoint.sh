#!/bin/sh

DB_TIMEOUT_SEC=60

if [[ -n "$OVERRIDE_FILES" ]] && [[ ! "$OVERRIDE_FILES" = ,* ]]; then
	OVERRIDE_FILES=",$OVERRIDE_FILES"
fi

	echo "Starting SKC Frontend and Backend..."
	
	echo "Java Options:- $JAVA_OPTS"
	echo "Override Files:- $OVERRIDE_FILES"
	echo "Command Line Arguments:- $COMMAND_LINE_ARGUMENTS"

if [[ "$1" == "container" ]]; then
	echo "Docker container selected..."
	
	### Running UI with nginx + skc-dashboard-ms.jar + skc-cluster-ms.jar	
	(trap 'kill 0' SIGINT; nginx && java ${JAVA_OPTS} -jar /opt/SKC/skc-dashboard-ms/skc-dashboard.jar --spring.config.location=/opt/SKC/skc-dashboard-ms/config/application.yml${OVERRIDE_FILES} ${COMMAND_LINE_ARGUMENTS})

elif [[ "$1" == "compose" ]]; then
	echo "Docker compose selected..."
	
	echo "Waiting for skc-maria-db service to start..."
	echo "SKC Backend will fail to start if Database is not ready within $DB_TIMEOUT_SEC sec"
	sh /wait-for skc-maria-db:3306 -t ${DB_TIMEOUT_SEC} -- nginx && java -jar ${JAVA_OPTS} /opt/SKC/skc-dashboard-ms/skc-dashboard.jar --spring.config.location=/opt/SKC/skc-dashboard-ms/config/application.yml${OVERRIDE_FILES} ${COMMAND_LINE_ARGUMENTS}
fi

