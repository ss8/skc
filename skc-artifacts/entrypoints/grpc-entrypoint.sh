#!/bin/bash

DB_TIMEOUT_SEC=60

if [[ -n "$OVERRIDE_FILES" ]] && [[ ! "$OVERRIDE_FILES" = ,* ]]; then
	OVERRIDE_FILES=",$OVERRIDE_FILES"
fi

	echo "Starting SKC gRPC microservice..."
	
	echo "Java Options:- $JAVA_OPTS"
	echo "Override Files:- $OVERRIDE_FILES"
	echo "Command Line Arguments:- $COMMAND_LINE_ARGUMENTS"

if [[ "$1" == "container" ]]; then
	echo "Docker container selected..."
	
	### Running  skc-grpc.jar
	(trap 'kill 0' SIGINT; java ${JAVA_OPTS} -jar /opt/SKC/skc-grpc-ms/skc-grpc.jar --spring.config.location=/opt/SKC/skc-grpc-ms/config/application.yml${OVERRIDE_FILES} ${COMMAND_LINE_ARGUMENTS})

elif [[ "$1" == "compose" ]]; then
	echo "Docker compose selected..."
	
	java -jar ${JAVA_OPTS} /opt/SKC/skc-grpc-ms/skc-grpc.jar --spring.config.location=/opt/SKC/skc-grpc-ms/config/application.yml${OVERRIDE_FILES} ${COMMAND_LINE_ARGUMENTS}
fi

