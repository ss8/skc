### Stage 1: Build SKC dashboard microservice
FROM gradle:6.3.0-jdk8 AS skc-dashboard-builder
WORKDIR /skc/skc-dashboard-ms
RUN apt-get update
## unit test cases need maria/mysql database
## So we are below databse stuff during compilation only 
# RUN apt-get install mysql-server -y
# RUN apt-get install mariadb-server -y
# ARG db_name=skc
# ARG db_user=skc
# ARG db_password=8uoNoxoS+SlWPmv8EMT+RA==
# ENV DEBIAN_FRONTEND=noninteractive
# RUN     service mysql start && \
#         mysql -e "CREATE DATABASE ${db_name} /*\!40100 DEFAULT CHARACTER SET utf8 */;" && \
#         mysql -e "CREATE USER ${db_user}@localhost IDENTIFIED BY '${db_password}';" && \
#         mysql -e "GRANT ALL PRIVILEGES ON ${db_name}.* TO '${db_user}'@'localhost';" && \
#         mysql -e "FLUSH PRIVILEGES;"
COPY ./skc-dashboard-ms .
#RUN service mysql start && \
RUN gradle build --no-daemon

### Stage 2: Build UI 
FROM node:16.14.0-alpine AS frontend-builder
ENV NODE_ENV production
# Add a work directory
WORKDIR /skc-ui
# Cache and Install dependencies
COPY ./skc-UI/package.json .
# COPY yarn.lock .
RUN yarn install --production
# Copy app files
COPY ./skc-UI .
# Build the app
RUN yarn build

# -- RELEASE --
### Stage 3: Build combined (UI+Dashboard) image
FROM nginx:1.21.0-alpine AS release
ENV NODE_ENV production
RUN apk update && \
	apk --no-cache add \
	openjdk8 \
	expect \
	openssh

ENV JAVA_HOME=/usr/lib/jvm/java-8-openjdk
ENV PATH=$JAVA_HOME:$PATH

# Copying SKC Frontend files
WORKDIR /opt/SKC/skc-ui
COPY --from=frontend-builder /skc-ui/build /usr/share/nginx/html
# copy .env.example as .env to the release build
COPY ./skc-UI/.env /usr/share/nginx/html/.env
# Add your nginx.conf
COPY ./skc-artifacts/nginx.conf /etc/nginx/nginx.conf

# Add nginx certificate & key
COPY ./skc-UI/certs/ /etc/ssl/nginx

# Copying SKC Backend files
WORKDIR /opt/SKC/skc-dashboard-ms/dashboard-resources
COPY ./skc-dashboard-ms/skc-resources/ .
COPY ./skc-dashboard-ms/scripts/scp_expect.sh .
WORKDIR /opt/SKC/skc-dashboard-ms
COPY --from=skc-dashboard-builder /skc/skc-dashboard-ms/build/libs/*.jar ./skc-dashboard.jar
COPY ./skc-dashboard-ms/config ./config
ENV JAVA_OPTS=""
ENV OVERRIDE_FILES=""
ENV COMMAND_LINE_ARGUMENTS=""
# Add docker-entry script
COPY ./skc-artifacts/wait-for /
COPY ./skc-artifacts/entrypoints/dashboard-entrypoint.sh /

RUN ["chmod", "+x", "/wait-for"]
RUN ["chmod", "+x", "/dashboard-entrypoint.sh"]
# Expose port
EXPOSE 80 443 8081 8443
# Set docker entrypoint
ENTRYPOINT ["/dashboard-entrypoint.sh"]
CMD ["container"]
VOLUME ["/var/log/skc", "/var/log/nginx", "/opt/SKC/skc-dashboard-ms/dashboard-resources", "/opt/ss8-ski-installer"]
