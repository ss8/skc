### Stage 1: Build SKC cluster microservice
FROM gradle:7.3.2-jdk8 AS grpc-ms-builder
WORKDIR /skc-grpc-ms
COPY ./ski-grpc-apis .
RUN apt-get update  \
    && gradle build --no-daemon  \
    && apt-get clean

### We need pyhton3 for k8s and java for gRPC APIs
FROM python:3.6-slim AS final
RUN apt update \
	&& apt install -y --no-install-recommends sshpass  \
	&& apt install -y --no-install-recommends openssh-client  \
	&& apt install -y --no-install-recommends sudo  \
	&& apt install -y --no-install-recommends default-jre  \
	&& ln -s /usr/local/bin/python3 /usr/bin/python3 \
	&& java --version  \
	&& apt clean all

## Copy cluster-ms files
WORKDIR /opt/SKC/skc-grpc-ms
COPY ./ski-grpc-apis/config ./config

# Add docker-entry script
COPY ./skc-artifacts/entrypoints/grpc-entrypoint.sh /grpc-entrypoint.sh

COPY --from=grpc-ms-builder /skc-grpc-ms/build/libs/*.jar ./skc-grpc.jar

## pip installs packages on /root/.local/bin
ENV PATH="/root/.local/bin:${PATH}"


RUN ["chmod", "+x", "/grpc-entrypoint.sh"]

# Expose port
EXPOSE 9091

# Set docker entrypoint
ENTRYPOINT ["/grpc-entrypoint.sh"]
CMD ["container"]

