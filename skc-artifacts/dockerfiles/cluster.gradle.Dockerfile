### Stage 1: Build SKC cluster microservice
FROM gradle:7.3.2-jdk8 AS cluster-ms-builder
WORKDIR /skc-cluster-ms
RUN apt-get update
COPY ./skc-cluster-ms .
RUN gradle build --no-daemon
RUN apt-get clean

FROM adoptopenjdk/openjdk8:alpine-jre AS final
## Copy cluster-ms files
WORKDIR /opt/SKC/skc-cluster-ms
COPY --from=cluster-ms-builder /skc-cluster-ms/build/libs/*.jar ./skc-cluster.jar
COPY ./skc-cluster-ms/config ./config

# Add docker-entry script
COPY ./skc-artifacts/wait-for /
COPY ./skc-artifacts/entrypoints/cluster-entrypoint.sh /

RUN ["chmod", "+x", "/wait-for"]
RUN ["chmod", "+x", "/cluster-entrypoint.sh"]

# Expose port
EXPOSE 447 8447

# Set docker entrypoint
ENTRYPOINT ["/cluster-entrypoint.sh"]
CMD ["container"]

