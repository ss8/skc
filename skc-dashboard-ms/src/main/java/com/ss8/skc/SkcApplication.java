package com.ss8.skc;

import java.util.Timer;

import com.jcraft.jsch.JSch;
import com.ss8.skc.dto.ResponseDTO;
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;


@SpringBootApplication
@Component("com.ss8.skc")
@PropertySource("classpath:messages.properties")
public class SkcApplication {

	@Autowired
	PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SkcApplication.class, args);
    }

    @Bean
    public Timer getTimer(){
        return new Timer();
    }

    @Bean
    public JSch getJSch(){
        return new JSch();
    }

    @Bean
    public ProcessBuilder getProcessBuilder(){
        return new ProcessBuilder();
    }

    @Bean
    public ResponseDTO getResponseDTO() {
        return new ResponseDTO();
    }
    

    @Bean
    public ServletWebServerFactory servletContainer() {

        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {

            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint securityConstraint = new SecurityConstraint();
                securityConstraint.setUserConstraint("CONFIDENTIAL");

                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/dashboard/*");
                collection.addPattern("/user/*");

                securityConstraint.addCollection(collection);
                context.addConstraint(securityConstraint);
            }
        };

        tomcat.addAdditionalTomcatConnectors(httpToHttpsRedirectConnector());

        return tomcat;
    }

    //Redirect Http traffic to Https
    private Connector httpToHttpsRedirectConnector(){
        Connector connector = new Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
        connector.setScheme("http");
        connector.setPort(8091);
        connector.setSecure(false);
        connector.setRedirectPort(8443);

        return connector;
    }

}
