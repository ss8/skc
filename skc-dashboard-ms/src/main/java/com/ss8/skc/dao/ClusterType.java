package com.ss8.skc.dao;

public enum ClusterType {
	AWS("aws"),
	GCP("gcp"),
	ON_PREMISE("on_prem"),
	AZURE("azu");
	
	private final String type;

	private ClusterType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return this.type;
	}
}
