package com.ss8.skc.dao;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DashboardStatusConverter implements AttributeConverter<DashboardStatus, String> {
	
	public String convertToDatabaseColumn(DashboardStatus status) {
		switch(status) {
		case DASHBOARD_ACTIVE:
			return "A";
		
		case DASHBOARD_INACTIVE:
			return "I";
		
		default:
			throw new IllegalArgumentException("DashboardStatus ["+status+"] not supported");
		}
	}
	
	public DashboardStatus convertToEntityAttribute(String data) {
		switch(data) {
		case "A":
			return DashboardStatus.DASHBOARD_ACTIVE;
		case "I":
			return DashboardStatus.DASHBOARD_INACTIVE;
		default:
			throw new IllegalArgumentException("DashboardStatus ["+data+"] not supported");
		}
	}
}
