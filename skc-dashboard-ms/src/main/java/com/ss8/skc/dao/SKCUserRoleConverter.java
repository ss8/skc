package com.ss8.skc.dao;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.ss8.skc.security.SKCUserRole;

import org.springframework.stereotype.Component;

@Converter(autoApply = true)
public class SKCUserRoleConverter implements AttributeConverter<SKCUserRole, String> {

    public String convertToDatabaseColumn(SKCUserRole type) {
		switch(type) {
		case ADMIN:
			System.out.println("Converting ADMIN to admin");
			return "admin";
		
		case SUPPORT:
			System.out.println("Converting SUPPORT to support");
			return "support";
			
		default:
			throw new IllegalArgumentException("SKCUserRole ["+type+"] not supported");
		}
	}
	
	public SKCUserRole convertToEntityAttribute(String data) {
		switch(data) {
		case "admin":
			return SKCUserRole.ADMIN;
		case "support":
			return SKCUserRole.SUPPORT;
		
		default:
			throw new IllegalArgumentException("SKCUserRole ["+data+"] not supported");
		}
	}
}
