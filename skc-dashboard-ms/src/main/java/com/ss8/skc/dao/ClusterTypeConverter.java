package com.ss8.skc.dao;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ClusterTypeConverter implements AttributeConverter<ClusterType, String> {
	
	public String convertToDatabaseColumn(ClusterType type) {
		switch(type) {
		case AWS:
			return "aws";
		
		case AZURE:
			return "azu";
			
		case GCP:
			return "gcp";
			
		case ON_PREMISE:
			return "on_prem";
		
		default:
			throw new IllegalArgumentException("ClusterType ["+type+"] not supported");
		}
	}
	
	public ClusterType convertToEntityAttribute(String data) {
		switch(data) {
		case "aws":
			return ClusterType.AWS;
		case "gcp":
			return ClusterType.GCP;
		case "azu":
			return ClusterType.AZURE;
		case "on_prem":
			return ClusterType.ON_PREMISE;
		default:
			throw new IllegalArgumentException("ClusterType ["+data+"] not supported");
		}
	}

}
