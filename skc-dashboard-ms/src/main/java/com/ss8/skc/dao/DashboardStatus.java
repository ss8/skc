package com.ss8.skc.dao;

public enum DashboardStatus {
	DASHBOARD_ACTIVE("A"),
	DASHBOARD_INACTIVE("I");
	
	private final String status;

	private DashboardStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return this.status;
	}
}
