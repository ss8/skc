package com.ss8.skc.dao;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "Dashboard_Details")
public class DashboardDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Integer id;
	
	private String name;
	
	private String url;
	
	@Column(name = "created_at")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private LocalDateTime updatedAt;
	
    @Column(name = "dashboard_status")
	private DashboardStatus dashboardStatus;
	
    @Column(name = "cluster_type")
	private ClusterType clusterType;

	
	/**
	 * 
	 */
	public DashboardDetails() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param url
	 * @param createdAt
	 * @param updatedAt
	 * @param dashboardStatus
	 * @param clusterType
	 */
	public DashboardDetails(Integer id, String name, String url, LocalDateTime createdAt, LocalDateTime updatedAt,
			DashboardStatus dashboardStatus, ClusterType clusterType) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.dashboardStatus = dashboardStatus;
		this.clusterType = clusterType;
	}

	/**
	 * @return the id
	 */
	public Integer getid() {
		return id;
	}

	/**
	 * @param did the did to set
	 */
	public void setid(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	public DashboardStatus getDashboardStatus() {
		return dashboardStatus;
	}

	public void setDashboardStatus(DashboardStatus dashboardStatus) {
		this.dashboardStatus = dashboardStatus;
	}

	public ClusterType getClusterType() {
		return clusterType;
	}

	public void setClusterType(ClusterType clusterType) {
		this.clusterType = clusterType;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}
	
}
