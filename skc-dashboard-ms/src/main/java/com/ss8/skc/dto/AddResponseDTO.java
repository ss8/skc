package com.ss8.skc.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddResponseDTO {

	@JsonProperty("dashboardDetails")
	private DashboardDetailsDTO dashboardDetailsDTO;

	public DashboardDetailsDTO getDashboardDetailsDTO() {
		return dashboardDetailsDTO;
	}

	public void setDashboardDetailsDTO(DashboardDetailsDTO dashboardDetailsDTO) {
		this.dashboardDetailsDTO = dashboardDetailsDTO;
	}

	public AddResponseDTO() {
		super();
	}

	public AddResponseDTO(DashboardDetailsDTO dashboardDetailsDTO) {
		super();
		this.dashboardDetailsDTO = dashboardDetailsDTO;
	}

}
