package com.ss8.skc.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ss8.skc.dao.ClusterType;
import com.ss8.skc.dao.DashboardDetails;
import com.ss8.skc.dao.DashboardStatus;


public class DashboardDetailsDTO {

	private Integer id;

	@Pattern(regexp = "^[a-zA-Z0-9\\.-]{3,252}$", message = "invalid.dashboard_name")
	private String name;

	private String url;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createdAt;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime updatedAt;

	private DashboardStatus dashboardStatus;

	private ClusterType clusterType;

	/**
	 * 
	 */
	public DashboardDetailsDTO() {
		super();
	}

	/**
	 * @param id
	 * @param name
	 * @param url
	 * @param createdAt
	 * @param updatedAt
	 * @param dashboardStatus
	 * @param clusterType
	 */
	public DashboardDetailsDTO(Integer id, String name, String url, LocalDateTime createdAt, LocalDateTime updatedAt,
			DashboardStatus dashboardStatus, ClusterType clusterType) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.createdAt = createdAt;
		this.updatedAt = updatedAt;
		this.dashboardStatus = dashboardStatus;
		this.clusterType = clusterType;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the createdAt
	 */
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}

	/**
	 * @return the updatedAt
	 */
	public LocalDateTime getUpdatedAt() {
		return updatedAt;
	}

	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(LocalDateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	/**
	 * @return the dashboardStatus
	 */
	public DashboardStatus getDashboardStatus() {
		return dashboardStatus;
	}

	/**
	 * @param dashboardStatus the dashboardStatus to set
	 */
	public void setDashboardStatus(DashboardStatus dashboardStatus) {
		this.dashboardStatus = dashboardStatus;
	}

	/**
	 * @return the clusterType
	 */
	public ClusterType getClusterType() {
		return clusterType;
	}

	/**
	 * @param clusterType the clusterType to set
	 */
	public void setClusterType(ClusterType clusterType) {
		this.clusterType = clusterType;
	}

	public static DashboardDetailsDTO valueOf(DashboardDetails dashboardDetails) {
		DashboardDetailsDTO dashboardDetailsDTO = new DashboardDetailsDTO();
		dashboardDetailsDTO.setId(dashboardDetails.getid());
		dashboardDetailsDTO.setName(dashboardDetails.getName());
		dashboardDetailsDTO.setUrl(dashboardDetails.getUrl());
		dashboardDetailsDTO.setDashboardStatus(dashboardDetails.getDashboardStatus());
		dashboardDetailsDTO.setClusterType(dashboardDetails.getClusterType());
		dashboardDetailsDTO.setCreatedAt(dashboardDetails.getCreatedAt());
		dashboardDetailsDTO.setUpdatedAt(dashboardDetails.getUpdatedAt());

		return dashboardDetailsDTO;
	}

	public static DashboardDetails createEntity(DashboardDetailsDTO dashboardDetailsDTO) {
		DashboardDetails dashboardDetails = new DashboardDetails();

		dashboardDetails.setName(dashboardDetailsDTO.getName());
		dashboardDetails.setUrl(dashboardDetailsDTO.getUrl());
		dashboardDetails.setClusterType(dashboardDetailsDTO.getClusterType());
		dashboardDetails.setDashboardStatus(dashboardDetailsDTO.getDashboardStatus());

		return dashboardDetails;
	}
}
