package com.ss8.skc.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

public class ChangePasswordDTO {

    @Length(max = 20, min = 1, message = "invalid.username")
    private String username;

    @NotNull
    @Length(min = 5, max = 20, message = "invalid.password")
    private String oldPassword;

    @NotNull
    @Length(min = 5, max = 20, message = "invalid.password")
    private String newPassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}
