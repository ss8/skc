package com.ss8.skc.dto;

import java.util.List;

public class NodeDetailDTO {
  private String nodeId;

  private String nodeLabel;

  private String nodeURL;

  private List<String> nodeRoles;
  
}
