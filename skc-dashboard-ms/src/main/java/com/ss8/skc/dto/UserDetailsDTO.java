package com.ss8.skc.dto;

import javax.validation.constraints.NotNull;

import com.ss8.skc.dao.UserDetails;

import org.apache.catalina.User;
import org.hibernate.validator.constraints.Length;

public class UserDetailsDTO {
    private Integer id;

    @Length(max = 20, min = 1, message = "invalid.username")
    private String username;

    @NotNull
    private Integer dashboardId;

    @Length(min = 1, message = "invalid.token")
    private String token;

    public static UserDetailsDTO valueOf(UserDetails userDetails){
        UserDetailsDTO user = new UserDetailsDTO();
        user.setDashboardId(userDetails.getDashboardId());
        user.setId(userDetails.getId());
        user.setUsername(userDetails.getUsername());
        user.setToken(userDetails.getToken());

        return user;
    }

    public static UserDetails createEntity(UserDetailsDTO userDetailsDTO){
        UserDetails user = new UserDetails();

        user.setUsername(userDetailsDTO.getUsername());
        user.setDashboardId(userDetailsDTO.getDashboardId());
        user.setToken(userDetailsDTO.getToken());

        return user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(Integer dashboardId) {
        this.dashboardId = dashboardId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    
}
