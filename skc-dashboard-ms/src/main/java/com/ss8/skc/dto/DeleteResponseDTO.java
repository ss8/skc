package com.ss8.skc.dto;

public class DeleteResponseDTO {

	private int id;

	public DeleteResponseDTO() {
		super();
	}

	public DeleteResponseDTO(int id) {
		super();
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
