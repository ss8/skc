package com.ss8.skc.dto;

public class SshDetailsDTO {

    private String clusterIp;
    private String clusterName;
    private String sshUsername;
    private String sshPassword;

    public SshDetailsDTO() {
        super();
    }

    public SshDetailsDTO(String clusterIp, String clusterName, String sshUsername, String sshPassword) {
        this.clusterIp = clusterIp;
        this.clusterName=clusterName;
        this.sshUsername = sshUsername;
        this.sshPassword = sshPassword;
    }

    public String getClusterIp() {
        return clusterIp;
    }

    public void setClusterIp(String dashboardIp) {
        this.clusterIp = dashboardIp;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getSshUsername() {
        return sshUsername;
    }

    public void setSshUsername(String sshUsername) {
        this.sshUsername = sshUsername;
    }

    public String getSshPassword() {
        return sshPassword;
    }

    public void setSshPassword(String sshPassword) {
        this.sshPassword = sshPassword;
    }

}
