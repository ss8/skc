package com.ss8.skc.dto;

public class UpdateResponseDTO {

	private int dashboardId;

	public UpdateResponseDTO() {
		super();
	}

	public UpdateResponseDTO(int dashboardId) {
		super();
		this.dashboardId = dashboardId;
	}

	public int getdashboardId() {
		return dashboardId;
	}

	public void setdashboardId(int dashboardId) {
		this.dashboardId = dashboardId;
	}

}
