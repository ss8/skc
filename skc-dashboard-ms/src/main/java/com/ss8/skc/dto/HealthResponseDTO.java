package com.ss8.skc.dto;

public class HealthResponseDTO {

	private Boolean success;

	public HealthResponseDTO() {
		super();
	}

	public HealthResponseDTO(Boolean flag) {
		super();
		this.success = flag;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

}
