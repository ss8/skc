package com.ss8.skc.dto;

import java.util.List;

public class ClusterDetailDTO {
  private String clusterId;

  private String clusterName;

  private List<NodeDetailDTO> nodeList;

  private String inventoryPath;

  private Boolean isRegistryEnabled;

  /**
   * @return String return the clusterId
   */
  public String getClusterId() {
      return clusterId;
  }

  /**
   * @param clusterId the clusterId to set
   */
  public void setClusterId(String clusterId) {
      this.clusterId = clusterId;
  }

  /**
   * @return String return the clusterName
   */
  public String getClusterName() {
      return clusterName;
  }

  /**
   * @param clusterName the clusterName to set
   */
  public void setClusterName(String clusterName) {
      this.clusterName = clusterName;
  }

  /**
   * @return List<NodeDetailDTO> return the nodeList
   */
  public List<NodeDetailDTO> getNodeList() {
      return nodeList;
  }

  /**
   * @param nodeList the nodeList to set
   */
  public void setNodeList(List<NodeDetailDTO> nodeList) {
      this.nodeList = nodeList;
  }

  /**
   * @return String return the inventoryPath
   */
  public String getInventoryPath() {
      return inventoryPath;
  }

  /**
   * @param inventoryPath the inventoryPath to set
   */
  public void setInventoryPath(String inventoryPath) {
      this.inventoryPath = inventoryPath;
  }

  /**
   * @return Boolean return the isRegistryEnabled
   */
  public Boolean isIsRegistryEnabled() {
      return isRegistryEnabled;
  }

  /**
   * @param isRegistryEnabled the isRegistryEnabled to set
   */
  public void setIsRegistryEnabled(Boolean isRegistryEnabled) {
      this.isRegistryEnabled = isRegistryEnabled;
  }

}
