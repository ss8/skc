package com.ss8.skc.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClusterListDTO {
    
    @JsonProperty("clusterList")
	private List clusterList;


	public List getClusterList() {
        return clusterList;
    }

    public void setClusterList(List clusterList) {
        this.clusterList = clusterList;
    }

    public ClusterListDTO() {
		super();
	}

    public ClusterListDTO(List clusterList) {
        this.clusterList = clusterList;
    }

}
