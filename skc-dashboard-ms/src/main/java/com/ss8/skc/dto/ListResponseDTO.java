package com.ss8.skc.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ListResponseDTO {

	@JsonProperty("dashboard_list")
	private List<DashboardDetailsDTO> dashboardDetailsDTO;

	public ListResponseDTO() {
		super();
	}

	public ListResponseDTO(List<DashboardDetailsDTO> dashboardDetailsDTOs) {
		super();
		this.dashboardDetailsDTO = dashboardDetailsDTOs;
	}

	public List<DashboardDetailsDTO> getDashboardDetailsDTO() {
		return dashboardDetailsDTO;
	}

	public void setDashboardDetailsDTO(List<DashboardDetailsDTO> dashboardDetailsDTO) {
		this.dashboardDetailsDTO = dashboardDetailsDTO;
	}
}
