package com.ss8.skc.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ResponseDTO {

	@JsonProperty("add_data")
	private AddResponseDTO addResponseDTO = null;

	@JsonProperty("update_data")
	private UpdateResponseDTO updateResponseDTO = null;

	@JsonProperty("list_data")
	private ListResponseDTO listResponseDTO = null;

	@JsonProperty("delete_data")
	private DeleteResponseDTO deleteResponseDTO = null;

	@JsonProperty("health_data")
	private HealthResponseDTO healthResponseDTO = null;

	@JsonProperty("cluster_data")
	private ClusterListDTO clusterListDTO = null;

	private String error = null;
	
	private String logs=null;

	/**
	 * 
	 */
	public ResponseDTO() {
		super();
	}

	/**
	 * @param data
	 * @param error
	 */
	public ResponseDTO(String error) {
		super();
		this.error = error;
	}

	public void successClusterDetailsDTO(List clusterList) {
		this.clusterListDTO = new ClusterListDTO(clusterList);
		this.addResponseDTO = null;
		this.deleteResponseDTO = null;
		this.updateResponseDTO = null;
		this.listResponseDTO = null;
		this.error = null;
	}

	public void successAddDashboardDetailsDTO(DashboardDetailsDTO dashboardDetailsDTO) {
		this.addResponseDTO = new AddResponseDTO(dashboardDetailsDTO);


		this.deleteResponseDTO = null;
		this.updateResponseDTO = null;
		this.listResponseDTO = null;
		this.error = null;
		this.clusterListDTO = null;
	}

	public void successListDashboardDetailsDTOs(List<DashboardDetailsDTO> dashboardDetailsDTOs) {
		this.listResponseDTO = new ListResponseDTO(dashboardDetailsDTOs);

		this.addResponseDTO = null;
		this.updateResponseDTO = null;
		this.deleteResponseDTO = null;
		this.error = null;
		this.clusterListDTO = null;
	}

	public void successDeleteDashboardDetailsDTO(int id) {
		this.deleteResponseDTO = new DeleteResponseDTO(id);

		this.addResponseDTO = null;
		this.listResponseDTO = null;
		this.updateResponseDTO = null;
		this.error = null;
		this.clusterListDTO = null;
	}

	public void successUpdateDashboardDetailsDTO(int dashboardId) {
		this.updateResponseDTO = new UpdateResponseDTO(dashboardId);

		this.addResponseDTO = null;
		this.deleteResponseDTO = null;
		this.listResponseDTO = null;
		this.error = null;
		this.clusterListDTO = null;
	}

	public void successHealthResponseDTO(Boolean flag) {
		this.healthResponseDTO = new HealthResponseDTO(flag);

		this.addResponseDTO = null;
		this.listResponseDTO = null;
		this.deleteResponseDTO = null;
		this.updateResponseDTO = null;
		this.error = null;
		this.clusterListDTO = null;
	}

	
	public String getLogs() {
		return logs;
	}

	public void setLogs(String logs) {
		this.logs = logs;
	}

	public String getError() {
		return error;
	}

	
	public void setError(String error) {
		this.error = error;
	}

	public AddResponseDTO getAddResponseDTO() {
		return addResponseDTO;
	}

	public void setAddResponseDTO(AddResponseDTO addResponseDTO) {
		this.addResponseDTO = addResponseDTO;
	}

	public DeleteResponseDTO getDeleteResponseDTO() {
		return deleteResponseDTO;
	}

	public void setDeleteResponseDTO(DeleteResponseDTO deleteResponseDTO) {
		this.deleteResponseDTO = deleteResponseDTO;
	}

	public ListResponseDTO getListResponseDTO() {
		return listResponseDTO;
	}

	public void setListResponseDTO(ListResponseDTO listResponseDTO) {
		this.listResponseDTO = listResponseDTO;
	}

	public UpdateResponseDTO getUpdateResponseDTO() {
		return updateResponseDTO;
	}

	public void setUpdateResponseDTO(UpdateResponseDTO updateResponseDTO) {
		this.updateResponseDTO = updateResponseDTO;
	}

	public ClusterListDTO getClusterListDTO() {
		return clusterListDTO;
	}

	public void setClusterListDTO(ClusterListDTO clusterListDTO) {
		this.clusterListDTO = clusterListDTO;
	}

}
