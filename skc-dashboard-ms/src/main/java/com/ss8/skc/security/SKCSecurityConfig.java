package com.ss8.skc.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import static com.ss8.skc.security.SKCUserPermission.*;

import javax.crypto.SecretKey;

import com.ss8.skc.auth.SKCUserService;
import com.ss8.skc.dao.SKCUserRoleConverter;
import com.ss8.skc.dao.UserCredentials;
import com.ss8.skc.jwt.JwtConfig;
import com.ss8.skc.jwt.JwtTokenVerifier;
import com.ss8.skc.jwt.JwtUsernameAndPasswordAuthenticationFilter;
import com.ss8.skc.repository.SKCUsersRepository;

@Configuration
@EnableWebSecurity
public class SKCSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;
    private final SKCUserService skcUserService;
    private final SecretKey secretKey;
    private final JwtConfig jwtConfig;

    @Autowired
    SKCUsersRepository usersRepository;

    @Autowired
    public SKCSecurityConfig(PasswordEncoder passwordEncoder, SKCUserService skcUserService, SecretKey secretKey,
            JwtConfig jwtConfig) {
        this.passwordEncoder = passwordEncoder;
        this.skcUserService = skcUserService;
        this.secretKey = secretKey;
        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable() // Enable while deploying
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .addFilter(
                        new JwtUsernameAndPasswordAuthenticationFilter(authenticationManager(), jwtConfig, secretKey))
                .addFilterAfter(new JwtTokenVerifier(secretKey, jwtConfig),
                        JwtUsernameAndPasswordAuthenticationFilter.class)
                .authorizeRequests()
                // .antMatchers("/dashboard/add",
                // "/dashboard/update/*",
                // "/dashboard/delete/*").hasRole(ADMIN.name())
                .antMatchers(HttpMethod.GET,"/").permitAll()
                .antMatchers(HttpMethod.GET,"/static/**").permitAll()
                .antMatchers(HttpMethod.GET,"/add").permitAll()
                .antMatchers(HttpMethod.GET,"/list").permitAll()
                .antMatchers(HttpMethod.GET,"/dashboard").permitAll()
                .antMatchers(HttpMethod.GET,"/changePassword").permitAll()
                .antMatchers(HttpMethod.POST,"/dashboard/health").permitAll()
                .antMatchers(HttpMethod.GET,"/dashboard/events").permitAll()
                .antMatchers(HttpMethod.POST,"/user/changePassword").permitAll()
                .antMatchers(HttpMethod.POST,"/dashboard/install").hasAuthority(DASHBOARD_INSTALL.getPermission())
                .antMatchers(HttpMethod.POST, "/dashboard/add").hasAuthority(DASHBOARD_CREATE.getPermission())
                .antMatchers(HttpMethod.PUT, "/dashboard/update/*").hasAuthority(DASHBOARD_UPDATE.getPermission())
                .antMatchers(HttpMethod.DELETE, "/dashboard/delete/*").hasAuthority(DASHBOARD_DELETE.getPermission())
                .anyRequest().authenticated();
        // .and()
        // .httpBasic();

        UserCredentials defaultUser = new UserCredentials();
        defaultUser.setUsername("admin");
        defaultUser.setPassword(passwordEncoder.encode("admin"));
        defaultUser.setUserRole(SKCUserRole.ADMIN);
        defaultUser.setAccountNonExpired(true);
        defaultUser.setAccountNonLocked(true);
        defaultUser.setCredentialsNonExpired(true);
        defaultUser.setEnabled(true);

        if (!usersRepository.findByUsername("admin").isPresent()) {
            usersRepository.saveAndFlush(defaultUser);
        }


        UserCredentials supportUser = new UserCredentials();
        supportUser.setUsername("support");
        supportUser.setPassword(passwordEncoder.encode("support"));
        supportUser.setUserRole(SKCUserRole.SUPPORT);
        supportUser.setAccountNonExpired(true);
        supportUser.setAccountNonLocked(true);
        supportUser.setCredentialsNonExpired(true);
        supportUser.setEnabled(true);

        if (!usersRepository.findByUsername("support").isPresent()) {
            usersRepository.saveAndFlush(supportUser);
        }

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // TODO Auto-generated method stub
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        
        

        provider.setPasswordEncoder(passwordEncoder);
        provider.setUserDetailsService(skcUserService);

        return provider;
    }

    // @Override
    // @Bean
    // protected UserDetailsService userDetailsService() {

    // UserDetails adminUser = User.builder()
    // .username("admin")
    // .password(passwordEncoder.encode("admin"))
    // // .roles(ADMIN.name())
    // .authorities(ADMIN.getgrantedAuthorities())
    // .build();

    // UserDetails supportUser = User.builder()
    // .username("support")
    // .password(passwordEncoder.encode("support"))
    // // .roles(SUPPORT.name())
    // .authorities(SUPPORT.getgrantedAuthorities())
    // .build();

    // return new InMemoryUserDetailsManager(
    // adminUser,
    // supportUser
    // );
    // }
}
