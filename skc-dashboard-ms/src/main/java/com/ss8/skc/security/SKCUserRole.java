package com.ss8.skc.security;

import com.google.common.collect.Sets;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static com.ss8.skc.security.SKCUserPermission.*;

import java.util.Set;
import java.util.stream.Collectors;

public enum SKCUserRole {
    SUPPORT(Sets.newHashSet(DASHBOARD_CREATE, DASHBOARD_INSTALL, DASHBOARD_DELETE,
    DASHBOARD_READ, DASHBOARD_UPDATE)), 
    ADMIN(Sets.newHashSet(DASHBOARD_CREATE, DASHBOARD_INSTALL, DASHBOARD_DELETE,
            DASHBOARD_READ, DASHBOARD_UPDATE, MONITOR_EVENTS, CHANGE_PASSWORD));

    private final Set<SKCUserPermission> permissions;

    SKCUserRole(Set<SKCUserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<SKCUserPermission> getPermissions() {
        System.out.println(permissions);
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getgrantedAuthorities() {

        Set<SimpleGrantedAuthority> permissions = getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission())).collect(Collectors.toSet());

        permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));

        return permissions;
    }


}
