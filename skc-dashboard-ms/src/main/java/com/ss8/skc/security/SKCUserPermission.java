package com.ss8.skc.security;

public enum SKCUserPermission {

    DASHBOARD_CREATE("dashboard.create"),
    DASHBOARD_INSTALL("dashboard.install"),
    DASHBOARD_READ("dashboard.read"),
    DASHBOARD_UPDATE("dashboard.update"),
    DASHBOARD_DELETE("dashboard.delete"),
    CHANGE_PASSWORD("change.password"),
    MONITOR_EVENTS("monitor.events");
    

    private final String permission;

    SKCUserPermission(String permission){
        this.permission=permission;
    }

    public String getPermission() {
        return permission;
    }
}
