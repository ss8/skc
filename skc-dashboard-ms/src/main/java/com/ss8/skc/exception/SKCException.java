package com.ss8.skc.exception;

import java.util.HashMap;

import org.springframework.http.HttpStatus;

public class SKCException extends Exception {
	private static final long serialVersionUID = 1L;
	private HttpStatus errorCode;
	private String errorMessage;
	
	public SKCException(HttpStatus errorCode,String errorMessage) {
		this.errorCode=errorCode;
		this.errorMessage=errorMessage;		
	}

	public HttpStatus getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	@Override
	public String toString() {
		
		return "SKCException : "+errorCode.toString() + " - "+errorMessage;
	}


	

}
