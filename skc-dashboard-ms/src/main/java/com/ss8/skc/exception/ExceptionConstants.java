package com.ss8.skc.exception;

public enum ExceptionConstants {

	SERVER_ERROR("server.error"),
	DASHBOARD_NOT_FOUND("dashboard.not_found"),
	DASHBOARD_ALREADY_EXISTS_NAME("dashboard.already_exists.name"),
	DASHBOARD_ALREADY_EXISTS_URL("dashboard.already_exists.url"),
	NULL_INPUT("null.input"),
	EVENT_INVALID("event.invalid"),
	EVENT_NULL("event.null"),
	INPUT_ID_NULL("input.id.null");

	private final String type;

	private ExceptionConstants(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return this.type;
	}

}
