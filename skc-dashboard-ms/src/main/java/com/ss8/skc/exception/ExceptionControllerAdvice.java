package com.ss8.skc.exception;

import com.ss8.skc.dto.ErrorMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionControllerAdvice {

	@Autowired
	private Environment environment;

	@ExceptionHandler(Exception.class)
	public String exceptionHandler(Exception ex) {
		return ex.getMessage();
	}

	@ExceptionHandler(SKCException.class)
	public ResponseEntity<ErrorMessage> exceptionHandler(SKCException se) {
		ErrorMessage errorMessage = new ErrorMessage();

		if (environment.containsProperty(se.getErrorMessage()))
			errorMessage.setMessage(environment.getProperty(se.getErrorMessage()));
		else
			errorMessage.setMessage(se.getErrorMessage());

		
		return new ResponseEntity<>(errorMessage, se.getErrorCode());
	}

}
