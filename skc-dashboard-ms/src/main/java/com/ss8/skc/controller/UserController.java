package com.ss8.skc.controller;

import java.util.stream.Collectors;

import javax.validation.Valid;

import com.ss8.skc.dto.ChangePasswordDTO;
import com.ss8.skc.dto.UserDetailsDTO;
import com.ss8.skc.exception.ExceptionConstants;
import com.ss8.skc.exception.SKCException;
import com.ss8.skc.service.UserService;

import org.apache.coyote.http11.Http11AprProtocol;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDetailsDTO> addUser(@RequestBody @Valid UserDetailsDTO userDetailsDTO, Errors error)
            throws SKCException {

        String response;

        if (error.hasErrors()) {
            response = error.getAllErrors().stream().map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(","));
            logger.error(response);
            throw new SKCException(HttpStatus.NOT_ACCEPTABLE,response);
        }

        if (userDetailsDTO == null) {
            throw new SKCException(HttpStatus.NOT_ACCEPTABLE,ExceptionConstants.NULL_INPUT.toString());
        }

        UserDetailsDTO user = userService.addUser(userDetailsDTO);

        return ResponseEntity.ok(user);

    }

    @GetMapping(value = "/token", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> fetchToken(@RequestBody @Valid UserDetailsDTO userDetailsDTO, Errors error)
            throws SKCException {
        String response;

        if (error.hasErrors()) {
            response = error.getAllErrors().stream().map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(","));
            logger.error(response);
            throw new SKCException(HttpStatus.NOT_ACCEPTABLE,response);
        }

        if (userDetailsDTO == null) {
            throw new SKCException(HttpStatus.NOT_ACCEPTABLE,ExceptionConstants.NULL_INPUT.toString());
        }

        String userToken = userService.fetchToken(userDetailsDTO);

        return ResponseEntity.ok(userToken);

    }

    @DeleteMapping(value = "/delete/{userId}")
    public ResponseEntity<Boolean> deleteUserByUserId(@PathVariable("userId") Integer userId) throws SKCException {
        // TODO: process POST request

        return ResponseEntity.ok(userService.deleteUserByUserId(userId));

    }

    @DeleteMapping(value = "/delete/dashboard/{dashboardId}")
    public ResponseEntity<Boolean> deleteUsersByDashboardId(@PathVariable("dashboardId") Integer dashboardId)
            throws SKCException {
        // TODO: process POST request

        return ResponseEntity.ok(userService.deleteUsersByDashboardId(dashboardId));
    }

    @PostMapping(value = "/changePassword")
    public ResponseEntity<Boolean> changePassword(@RequestBody @Valid ChangePasswordDTO changePasswordDTO, Errors error)
            throws SKCException {

        String response;

        if (error.hasErrors()) {
            response = error.getAllErrors().stream().map(ObjectError::getDefaultMessage)
                    .collect(Collectors.joining(","));
            logger.error(response);
            throw new SKCException(HttpStatus.NOT_ACCEPTABLE,response);
        }

        if (changePasswordDTO == null) {
            throw new SKCException(HttpStatus.NOT_ACCEPTABLE,ExceptionConstants.NULL_INPUT.toString());
        }

        return ResponseEntity.ok(userService.changePassword(changePasswordDTO));
    }
}
