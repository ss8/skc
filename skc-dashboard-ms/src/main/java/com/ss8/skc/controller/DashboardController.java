package com.ss8.skc.controller;

import static com.ss8.skc.controller.DashboardEvents.BASHRC_DELETE;
import static com.ss8.skc.controller.DashboardEvents.CHECK_CONTROLLERS_REACHABLE;
import static com.ss8.skc.controller.DashboardEvents.CHECK_STORAGE_AVAILABLE;
import static com.ss8.skc.controller.DashboardEvents.CRONJOB_DELETE;
import static com.ss8.skc.controller.DashboardEvents.DASHBOARD_INSTALL_COMPLETE;
import static com.ss8.skc.controller.DashboardEvents.DASHBOARD_INSTALL_STARTED;
import static com.ss8.skc.controller.DashboardEvents.DASHBOARD_UNINSTALL_COMPLETE;
import static com.ss8.skc.controller.DashboardEvents.DASHBOARD_UNINSTALL_STARTED;
import static com.ss8.skc.controller.DashboardEvents.DOCKER_IMAGES_LOADED;
import static com.ss8.skc.controller.DashboardEvents.DOCKER_IMAGES_LOADING;
import static com.ss8.skc.controller.DashboardEvents.DOCKER_IMAGES_UNLOADED;
import static com.ss8.skc.controller.DashboardEvents.DOCKER_IMAGES_UNLOADING;
import static com.ss8.skc.controller.DashboardEvents.END;
import static com.ss8.skc.controller.DashboardEvents.NONE;
import static com.ss8.skc.controller.DashboardEvents.POST_INSTALL;
import static com.ss8.skc.controller.DashboardEvents.RESOURCE_DELETE_COMPLETE;
import static com.ss8.skc.controller.DashboardEvents.RESOURCE_DELETE_STARTED;
import static com.ss8.skc.controller.DashboardEvents.RESOURCE_TRANSFER_COMPLETE;
import static com.ss8.skc.controller.DashboardEvents.RESOURCE_TRANSFER_STARTED;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.ss8.skc.dto.DashboardDetailsDTO;
import com.ss8.skc.dto.ResponseDTO;
import com.ss8.skc.dto.SshDetailsDTO;
import com.ss8.skc.exception.ExceptionConstants;
import com.ss8.skc.exception.SKCException;
import com.ss8.skc.service.DashboardService;
import com.ss8.skc.service.UserService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
@CrossOrigin
@RequestMapping("/dashboard")
public class DashboardController {

	private final static String WEBHOOK_EVENT_HEADER = "X-Event";

	@Autowired
	private DashboardService dashboardService;

	@Autowired
	private UserService userService;

	@Autowired
	JSch jSch;

	@Value("${ski.clusterListFilePath}")
	private String skiClusterListFilePath;

	private final ExecutorService executor = Executors.newSingleThreadExecutor();

	private DashboardEvents dashboardEvents = NONE;
	private DashboardEvents previousEvents = null;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostConstruct
	public void init() {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			executor.shutdown();
			try {
				executor.awaitTermination(1, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				logger.error(e.toString());
			}
		}));
	}

	// @Autowired
	// private ResponseDTO responseDTO;
	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> addDashboard(@Valid @RequestBody DashboardDetailsDTO dashboardDetailsDTO,
			Errors error) throws SKCException {
		logger.trace("Inside addDashboard() method of {}", this.getClass());

		String response;

		if (error.hasErrors()) {
			response = error.getAllErrors().stream().map(ObjectError::getDefaultMessage)
					.collect(Collectors.joining(","));
			logger.error(response);
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, response);
		}

		if (dashboardDetailsDTO == null)
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, ExceptionConstants.NULL_INPUT.toString());

		DashboardDetailsDTO responseDashboardDetailsDTO = dashboardService.addDashboard(dashboardDetailsDTO);
		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.successAddDashboardDetailsDTO(responseDashboardDetailsDTO);

		logger.debug("Dashboard Details added: {}", responseDashboardDetailsDTO);
		logger.trace("Exiting addDashboard() method of {}", this.getClass());
		logger.info(ResponseEntity.ok(responseDTO).toString());
		return ResponseEntity.ok(responseDTO);
	}

	// Endpoint to handle update requests
	@PutMapping(value = "/update/{dashboardId}")
	public ResponseEntity<ResponseDTO> updateDashboard(@PathVariable("dashboardId") int dashboardId,
			@Valid @RequestBody DashboardDetailsDTO dashboardDetailsDTO, Errors error) throws SKCException {

		logger.trace("Inside updateDashboard() method of {}", this.getClass());

		String response;

		if (error.hasErrors()) {
			response = error.getAllErrors().stream().map(ObjectError::getDefaultMessage)
					.collect(Collectors.joining(","));
			logger.error(response);
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, response);
		}

		if (dashboardDetailsDTO == null)
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, ExceptionConstants.NULL_INPUT.toString());

		int updatedDashboardId = dashboardService.updateDashboard(dashboardId, dashboardDetailsDTO);

		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.successUpdateDashboardDetailsDTO(updatedDashboardId);

		logger.debug("Dashboard Details updated for dashboardId: {}",
				responseDTO.getUpdateResponseDTO().getdashboardId());
		logger.trace("Exiting updateDashboard() method of {}", this.getClass());
		return ResponseEntity.ok(responseDTO);
	}

	@GetMapping(value = "/list", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> listDashboards(@RequestParam(value = "search", defaultValue = "") String search)
			throws SKCException, UnknownHostException {
		logger.trace("Inside listDashboards() method of {}", this.getClass());

		List<DashboardDetailsDTO> dashboardDetailsDTOs = dashboardService.listDashboards(search);

		// logger.warn(InetAddress.);

		logger.debug("No. of Dashboard DTOs received: {}", dashboardDetailsDTOs.size());

		// this.responseDTO.successListDashboardDetailsDTOs(dashboardDetailsDTOs);
		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.successListDashboardDetailsDTOs(dashboardDetailsDTOs);
		return ResponseEntity.ok(responseDTO);
	}

	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<ResponseDTO> deleteDashboard(@PathVariable int id, @RequestBody SshDetailsDTO sshDetailsDTO)
			throws SKCException {
		logger.trace("Inside deleteDashboard() of {}", this.getClass());

		ResponseDTO responseDTO = new ResponseDTO();
		StringBuilder logs = new StringBuilder();
		Session session = null;
		Boolean deleteDashboard = false;
		dashboardEvents = NONE;
		HttpStatus errorResponseStatus = null;
		String errorResponseMessage = "";
		Boolean controllerNodesReachable = false;
		Boolean controllerNodesExist = false;
		Boolean isStorageAvailable = false;

		try {
			dashboardEvents = NONE;
			if (!InetAddress.getByName(sshDetailsDTO.getClusterIp()).isReachable(2000)) {
				throw new SKCException(HttpStatus.NOT_FOUND, "Cluster unreachable");
			}
			session = jSch.getSession(sshDetailsDTO.getSshUsername(), sshDetailsDTO.getClusterIp(), 22);
			session.setPassword(sshDetailsDTO.getSshPassword());
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();

			// Fetch List of Nodes on the Cluster
			logger.debug("Calling fetchClusterList in Uninstall");
			String[] nodesList = dashboardService.fetchClusterNodesList(session, logs);

			if (nodesList.length == 1 && nodesList[0].equals("")) {
				throw new SKCException(HttpStatus.PRECONDITION_FAILED, "No Controller Nodes found. Cannot proceed");
			}

			controllerNodesExist = true;

			session.disconnect();

			// Check if All Controller Nodes are reachable
			dashboardEvents = CHECK_CONTROLLERS_REACHABLE;
			controllerNodesReachable = dashboardService.controllerNodesReachable(nodesList, logs);

			// Check if Enough Storage is Available on each Controller Node
			dashboardEvents = CHECK_STORAGE_AVAILABLE;
			isStorageAvailable = dashboardService.isStorageAvailable(nodesList, 50,sshDetailsDTO.getSshUsername(),
					sshDetailsDTO.getSshPassword(), logs);

			// Transfering Resources on Nodes
			logger.debug("Calling transferAndExtractResources in Install");
			dashboardEvents = RESOURCE_TRANSFER_STARTED;
			dashboardService.transferAndExtractDeleteResources(nodesList, sshDetailsDTO.getSshUsername(),
					sshDetailsDTO.getSshPassword(), logs);
			dashboardEvents = RESOURCE_TRANSFER_COMPLETE;

			logger.debug("Triggering delete Script from Uninstall");
			dashboardEvents = DASHBOARD_UNINSTALL_STARTED;
			deleteDashboard = dashboardService.triggerDeleteDashboardScript(Arrays.copyOfRange(nodesList, 0, 1),
					sshDetailsDTO.getSshUsername(), sshDetailsDTO.getSshPassword(), logs);

			if (deleteDashboard) {

				dashboardEvents = DASHBOARD_UNINSTALL_COMPLETE;
				logger.debug("Calling dockerImagesTask in Uninstall");
				dashboardEvents = DOCKER_IMAGES_UNLOADING;

				dashboardService.dockerImagesTask(Arrays.copyOfRange(nodesList, 1, nodesList.length),
						sshDetailsDTO.getSshUsername(), sshDetailsDTO.getSshPassword(), "unload", logs);
				dashboardEvents = DOCKER_IMAGES_UNLOADED;

				// logger.info("Calling bashrcTask in Uninstall");
				// dashboardEvents = BASHRC_DELETE;

				// dashboardService.bashrcTask(Arrays.copyOfRange(nodesList, 1, nodesList.length),
				// 		sshDetailsDTO.getSshUsername(), sshDetailsDTO.getSshPassword(), "remove", logs);

				logger.debug("Calling cronJobTask in Uninstall");
				dashboardEvents = CRONJOB_DELETE;
				dashboardService.cronJobTask(Arrays.copyOfRange(nodesList, 1, nodesList.length),
						sshDetailsDTO.getSshUsername(), sshDetailsDTO.getSshPassword(), "remove", logs);
			} else {
				// Cleanup Script
				deleteDashboard = dashboardService.triggerCleanUpScript(nodesList, sshDetailsDTO.getSshUsername(),
						sshDetailsDTO.getSshPassword(), logs);
			}

			// deletes resources from all nodes 0-n
			logger.debug("Calling deleteResources in Uninstall");
			dashboardEvents = RESOURCE_DELETE_STARTED;
			logs.append("Deleting Resources");
			dashboardService.deleteResources(Arrays.copyOfRange(nodesList, 0, nodesList.length),
					sshDetailsDTO.getSshUsername(), sshDetailsDTO.getSshPassword(), logs);
			dashboardEvents = RESOURCE_DELETE_COMPLETE;

		} catch (SKCException skcEx) {
			logger.error(skcEx.toString());
			logs.append(skcEx.getErrorMessage() + "\n");

			dashboardEvents = END;
			deleteDashboard = false;
			errorResponseStatus = skcEx.getErrorCode();
			errorResponseMessage = skcEx.getErrorMessage();
		} catch (JSchException jSchException) {
			dashboardEvents = END;
			deleteDashboard = false;

			if (jSchException.getMessage().equals("Auth fail")) {
				errorResponseStatus = HttpStatus.UNAUTHORIZED;
				errorResponseMessage = "Authentication failed!! Please provide Correct credentials";
			}
			logger.error(jSchException.toString() + " for SSH user - " + sshDetailsDTO.getSshUsername());
			logs.append(jSchException.toString() + "\n");

		} catch (Exception e) {
			dashboardEvents = END;
			errorResponseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			deleteDashboard = false;

			logger.error(e.toString());
			logs.append(e.toString() + "\n");
			errorResponseMessage = e.getMessage();
		}

		dashboardEvents = END;

		if (deleteDashboard || (controllerNodesExist && !controllerNodesReachable)) {

			userService.deleteUsersByDashboardId(id);
			int return_id = dashboardService.deleteDashboard(id);

			logger.debug("Dashboard with id {} deleted", return_id);

			responseDTO.successDeleteDashboardDetailsDTO(id);
			responseDTO.setLogs(logs.toString());
			return ResponseEntity.ok(responseDTO);
		}

		responseDTO.setError(errorResponseMessage);
		responseDTO.setLogs(logs.toString());
		return ResponseEntity.status(errorResponseStatus).body(responseDTO);

	}

	@PostMapping(value = "/health", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> healthCheck(
			@RequestHeader(value = WEBHOOK_EVENT_HEADER, defaultValue = "") String event,
			@Valid @RequestBody DashboardDetailsDTO payload, Errors error) throws SKCException {

		logger.trace("Inside healthCheck() method of {}", this.getClass());

		String response;

		if (error.hasErrors()) {
			response = error.getAllErrors().stream().map(ObjectError::getDefaultMessage)
					.collect(Collectors.joining(","));
			logger.error(response);
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, response);
		}

		if (event.isEmpty()) {
			logger.error("Bad Request as event is missing in header");
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, ExceptionConstants.EVENT_NULL.toString());
		}

		Boolean isDone = dashboardService.healthCheck(event, payload);
		logger.debug("Event registered? {}", isDone);

		// this.responseDTO.successHealthResponseDTO(isDone);

		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.successHealthResponseDTO(isDone);

		return ResponseEntity.ok(responseDTO);
	}

	// Get a list of Clusters Deployed by SKI
	// returns Cluster Name and VIP/Controller Node IP
	@GetMapping(value = "/clusterList")
	public ResponseEntity<ResponseDTO> clusterList() throws IOException {

		List<String> clusterList = new ArrayList<String>();
		Boolean fileNotFound = false;

		BufferedReader br = null;
		String logs = "";

		try {
			br = new BufferedReader(new FileReader(skiClusterListFilePath));
		} catch (Exception e) {
			logger.error(e.toString());
			fileNotFound = true;
		}

		if (!fileNotFound) {
			try {

				String line = br.readLine();

				while (line != null) {
					clusterList.add(line);
					logs += line + "\n";
					line = br.readLine();
				}
			} catch (Exception e) {
				logger.error(e.toString());
			} finally {
				br.close();
			}
		}

		ResponseDTO responseDTO = new ResponseDTO();

		if (clusterList.size() > 0) {

			logger.debug("Clusters Found - " + clusterList.toString());

			responseDTO.setLogs(logs);
			responseDTO.successClusterDetailsDTO(clusterList);

			return ResponseEntity.ok(responseDTO);
		}

		logger.debug("No Clusters Found - " + clusterList.toString());

		responseDTO.setError("No Clusters found");
		responseDTO.setLogs(logs.toString());

		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(responseDTO);
	}

	@PostMapping(value = "/install", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseDTO> installDashboard(@Valid @RequestBody SshDetailsDTO sshDetailsDTO, Errors error)
			throws SKCException, InterruptedException {
		String response;

		if (error.hasErrors()) {
			response = error.getAllErrors().stream().map(ObjectError::getDefaultMessage)
					.collect(Collectors.joining(","));
			logger.error(response);
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, response);
		}

		StringBuilder logs = new StringBuilder(); // Stores Installation Logs and forwards to Frontend

		ResponseDTO responseDTO = new ResponseDTO();
		Session session = null;
		Integer installDashboard = 0;
		String[] nodesList = {};
		dashboardEvents = NONE;
		HttpStatus errorResponseStatus = null;
		String errorResponseMessage = null;

		Boolean controllerNodesReachable = false;
		Boolean isStorageAvailable = false;

		try {
			dashboardEvents = NONE;

			if (!dashboardService.pingHost(sshDetailsDTO.getClusterIp(), 22, 2000)) {
				throw new SKCException(HttpStatus.NOT_FOUND, "Cluster unreachable");
			}

			session = jSch.getSession(sshDetailsDTO.getSshUsername(), sshDetailsDTO.getClusterIp(), 22);
			session.setPassword(sshDetailsDTO.getSshPassword());
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();

			// Fetch List of Nodes on the Cluster
			logger.debug("Calling fetchClusterNodesList in Install");
			nodesList = dashboardService.fetchClusterNodesList(session, logs);

			if (nodesList.length == 1 && nodesList[0].equals("")) {
				throw new SKCException(HttpStatus.PRECONDITION_FAILED, "No Controller Nodes found. Cannot proceed");
			}

			session.disconnect();

			// Check if All Controller Nodes are reachable
			dashboardEvents = CHECK_CONTROLLERS_REACHABLE;
			controllerNodesReachable = dashboardService.controllerNodesReachable(nodesList, logs);

			// Check if Enough Storage is Available on each Controller Node
			dashboardEvents = CHECK_STORAGE_AVAILABLE;
			isStorageAvailable = dashboardService.isStorageAvailable(nodesList, 1024,sshDetailsDTO.getSshUsername(),
					sshDetailsDTO.getSshPassword(), logs);

			// Transfering Resources on Nodes
			logger.debug("Calling transferAndExtractResources in Install");
			dashboardEvents = RESOURCE_TRANSFER_STARTED;
			dashboardService.transferAndExtractResources(nodesList, sshDetailsDTO.getSshUsername(),
					sshDetailsDTO.getSshPassword(), logs);
			dashboardEvents = RESOURCE_TRANSFER_COMPLETE;

			// Loading docker images on all controller nodes
			logger.debug("Calling loadDockerImages in Install");
			dashboardEvents = DOCKER_IMAGES_LOADING;
			dashboardService.dockerImagesTask(nodesList, sshDetailsDTO.getSshUsername(), sshDetailsDTO.getSshPassword(),
					"load", logs);
			dashboardEvents = DOCKER_IMAGES_LOADED;

			// Trigger Install Script
			logger.debug("Calling triggerAddDashboardScript in Install");
			dashboardEvents = DASHBOARD_INSTALL_STARTED;
			// 0-Successful Deployment, 1-Error, 2-Dashboard Already Exists
			installDashboard = dashboardService.triggerAddDashboardScript(nodesList, sshDetailsDTO.getSshUsername(),
					sshDetailsDTO.getSshPassword(), sshDetailsDTO.getClusterName(), logs);

			if (installDashboard == 0) {
				dashboardEvents = DASHBOARD_INSTALL_COMPLETE;

				// logger.info("Calling postInstallScript in Install");
				// dashboardEvents = POST_INSTALL;
				// dashboardService.postInstallScript(nodesList, sshDetailsDTO.getSshUsername(),
				// 		sshDetailsDTO.getSshPassword(), logs);
			}

		} catch (SKCException skcEx) {
			logger.error(skcEx.toString());
			logs.append(skcEx.getErrorMessage() + "\n");

			dashboardEvents = END;
			installDashboard = 1;
			errorResponseStatus = skcEx.getErrorCode();
			errorResponseMessage = skcEx.getErrorMessage();
		} catch (JSchException jSchException) {
			dashboardEvents = END;
			installDashboard = 1;

			if (jSchException.getMessage().equals("Auth fail")) {
				errorResponseStatus = HttpStatus.UNAUTHORIZED;
				errorResponseMessage = "Authentication failed!! Please provide Correct credentials";
			}
			logger.error(jSchException.toString() + " for SSH user - " + sshDetailsDTO.getSshUsername());
			logs.append(errorResponseMessage + "\n");

		} catch (Exception e) {
			dashboardEvents = END;
			errorResponseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			installDashboard = 1;
			logger.error(e.toString());
			logs.append(e.toString() + "\n");
			errorResponseMessage = e.getMessage();
		}

		// deletes resources from all nodes 0-n
		logger.debug("Calling deleteResources in Uninstall");
		dashboardEvents = RESOURCE_DELETE_STARTED;
		logs.append("Deleting Resources");
		dashboardService.deleteResources(Arrays.copyOfRange(nodesList, 0, nodesList.length),
				sshDetailsDTO.getSshUsername(), sshDetailsDTO.getSshPassword(), logs);
		dashboardEvents = RESOURCE_DELETE_COMPLETE;

		dashboardEvents = END;
		if (installDashboard == 0) {
			DashboardDetailsDTO dashboardDetailsDTO = new DashboardDetailsDTO();
			dashboardDetailsDTO.setUrl(sshDetailsDTO.getClusterIp());
			responseDTO.successAddDashboardDetailsDTO(dashboardDetailsDTO);
			responseDTO.setLogs(logs.toString());

			return ResponseEntity.ok(responseDTO);
		} else if (installDashboard == 1) {
			if (errorResponseMessage == null && errorResponseStatus == null) {
				errorResponseStatus = HttpStatus.INTERNAL_SERVER_ERROR;
				errorResponseMessage = "Dashboard install script failed. Check logs";
			}
		} else if (installDashboard == 2) {
			errorResponseStatus = HttpStatus.PRECONDITION_FAILED;
			errorResponseMessage = "Dashboard Already Exists";
		}

		responseDTO.setError(errorResponseMessage);
		responseDTO.setLogs(logs.toString());

		return ResponseEntity.status(errorResponseStatus).body(responseDTO);

	}

	@GetMapping(value = "/events")
	public SseEmitter eventsEmitter() {

		logger.info("Inside eventsEmitter()");
		// dashboardEvents = NONE;

		// try {
		// Thread.sleep(200);
		// } catch (InterruptedException e2) {
		// e2.printStackTrace();
		// }

		SseEmitter sseEmitter = new SseEmitter(Long.MAX_VALUE);

		sseEmitter.onCompletion(() -> logger.info("SseEmitter in Completed"));
		sseEmitter.onTimeout(() -> logger.info("SseEmitter is Timed Out"));
		sseEmitter.onError((e) -> logger.info("SseEmitter got Error:", e));

		executor.execute(() -> {

			logger.debug("Starting Event emitter for - Dashboard Events");

			int waitCount = 0;

			while (true) {

				if (dashboardEvents == NONE) {
					if (waitCount == 80) {
						break;
					}
					waitCount++;
				}

				if (dashboardEvents == END) {
					dashboardEvents = NONE;
					previousEvents = null;
					break;
				}

				try {

					if (previousEvents != dashboardEvents) {
						previousEvents = dashboardEvents;
						sseEmitter.send(dashboardEvents.toString());
						logger.debug("Dashboard Event - " + dashboardEvents);
					}

					Thread.sleep(200);
				} catch (IOException e) {
					e.printStackTrace();
					sseEmitter.completeWithError(e);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
					sseEmitter.completeWithError(e1);
				}
			}
			sseEmitter.complete();
			logger.debug("Event emmitter completed for - Dashboard Events");
		});

		logger.debug("eventsEmitter() complete");
		return sseEmitter;
	}

	@PostMapping(value = "/refresh/{clusterName}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> refresh(@PathVariable(name = "clusterName", required = true) String clusterName,
			@Valid @RequestBody List<String> ipList, Errors error) {
		return ResponseEntity.ok(dashboardService.refresh(ipList, clusterName));
	}

}
