package com.ss8.skc.controller;

public enum DashboardEvents {
    NONE("Waiting for Update"),
    START("start"),
    END("end"),
    RESOURCE_TRANSFER_STARTED("Transfering resources to Controller Nodes"),
    RESOURCE_TRANSFER_COMPLETE("Transfer of resources complete"),
    RESOURCE_DELETE_STARTED("Deleting resources from Controller Nodes"),
    RESOURCE_DELETE_COMPLETE("Deletion of resources complete"),
    DOCKER_IMAGES_LOADING("Loading docker images"),
    DOCKER_IMAGES_LOADED("Docker Images successfully Loaded"),
    DOCKER_IMAGES_UNLOADING("Unloading docker Images"),
    DOCKER_IMAGES_UNLOADED("Docker Images successfully Unloaded"),
    DASHBOARD_INSTALL_STARTED("Installing Dashboard GUI. This could take few minutes"),
    DASHBOARD_INSTALL_COMPLETE("Dashboard GUI Installed successfully"),
    DASHBOARD_UNINSTALL_STARTED("Uninstalling Dashboard GUI. This could take few minutes"),
    DASHBOARD_UNINSTALL_COMPLETE("Dashboard GUI Uninstalled successfully"),
    POST_INSTALL("Updating .bashrc and creating CronJob"),
    BASHRC_SAVE("Added SKC IP to .bashrc"),
    BASHRC_DELETE("Removed SKC IP from .bashrc"),
    CRONJOB_CREATE("Adding Cronjob"),
    CRONJOB_DELETE("Cronjob Deleted"),
    CHECK_CONTROLLERS_REACHABLE("Checking if all Controller are reachable"),
    CHECK_STORAGE_AVAILABLE("Verifying storage requirements");


    private final String type;

	private DashboardEvents(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return this.type;
	}
}
