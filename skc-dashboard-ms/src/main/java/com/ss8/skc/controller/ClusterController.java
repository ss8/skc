package com.ss8.skc.controller;

import javax.validation.Valid;

import com.ss8.skc.dto.ClusterDetailDTO;
import com.ss8.skc.dto.ResponseDTO;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/cluster")
public class ClusterController {

  /**
   * Responds with cluster details if cluster is created successfully.
   * Cluster details are passed in request body as ClusterDetailDTO.
   * 
   * @param ClusterDetailDTO
   * @return  ResponseDTO<ClusterDetailDTO>
   */
  @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResponseDTO> createCluster(@Valid @RequestBody ClusterDetailDTO clusterDetailDTO, Errors errors) {
    // TODO Implement this function & call createCluster function to create cluster
    return null;
  }

  /**
   * Returns a list of all clusters created.
   * 
   * @return ResponseDTO<List<ClusterDetailDTO>>
   */
  @GetMapping(value="/list", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResponseDTO> getAllClusters() {
    // TODO Implement this function & call getAllClusters function to list all clusters' details
      return null;
  }

  /**
   * Returns details of given clusterId
   * 
   * @param clusterId string
   * @return ResponseDTO<ClusterDetailDTO>
   */
  @GetMapping(value="/{clusterId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResponseDTO> getClusterDetail(@PathVariable(name = "clusterId", required = true) String clusterId) {
    // TODO implement this function & call getClusterDetail from service layer to get details
      return null;
  }

  /**
   * Returns the ID of the cluster deleted
   * 
   * @param clusterId string
   * @return ResponseDTO<String>
   */
  @DeleteMapping(value = "/{clusterId}/delete", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResponseDTO> deleteCluster(@PathVariable(name = "clusterId", required = true) String clusterId) {
    // TODO Implement this function & call deleteCluster from service layer to delete a cluster
    return null;
  }
  
  /**
   * Deletes a given node from a given cluster
   * 
   * @param clusterId string
   * @param nodeId  string
   * @return ResponseDTO<ClusterDetailDTO>
   */
  @DeleteMapping(value = "/{clusterId}/node/{nodeId}/delete", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResponseDTO> deleteClusterNode(@PathVariable(name = "clusterId", required = true) String clusterId, @PathVariable(name = "nodeId", required = true) String nodeId) {
    // TODO Implement this function & call deleteClusterNode from service layer to delete a cluster node
    return null;
  }

  /**
   * Adds a node in a given cluster.
   * Node can be a controller or a worker node.
   * 
   * @return ResponseDTO<ClusterDetailDTO>
   */
  @PostMapping(value = "/{clusterId}/node/add", consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ResponseDTO> addClusterNode() {
    // TODO Implement this function & call addClusterNode from service layer to add a cluster node
    return null;
  }
  
}
