package com.ss8.skc.service;

import java.util.List;

import com.ss8.skc.dto.ClusterDetailDTO;

public interface ClusterService {
  public ClusterDetailDTO createCluster(ClusterDetailDTO clusterDetailDTO);

  public List<ClusterDetailDTO> getAllClusters();

  public ClusterDetailDTO getClusterDetail(String clusterId);

  public String deleteCluster(String clusterId);

  public String deleteClusterNode(String clusterId, String nodeId);

  public ClusterDetailDTO addClusterNode(String clusterId);
  
}
