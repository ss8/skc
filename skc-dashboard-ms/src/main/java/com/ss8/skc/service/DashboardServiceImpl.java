package com.ss8.skc.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.ss8.skc.dao.DashboardDetails;
import com.ss8.skc.dao.DashboardStatus;
import com.ss8.skc.dto.DashboardDetailsDTO;
import com.ss8.skc.exception.ExceptionConstants;
import com.ss8.skc.exception.SKCException;
import com.ss8.skc.repository.DashboardRepository;
import com.ss8.skc.utils.WebhookEventConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class DashboardServiceImpl implements DashboardService {

	// @Value("${ski.path}")
	// private String skiPath;

	@Value("${resources.srcPath}")
	private String resSrcPath;

	@Value("${resources.destPath}")
	private String resDestPath;

	@Autowired
	private DashboardRepository dashboardRepository;

	@Autowired
	JSch jSch;

	@Autowired
	ProcessBuilder processBuilder;

	@Autowired
	Timer timer;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	static HashMap<Integer, DashboardStatusDetails> hashMap = new HashMap<>();

	@Override
	public List<DashboardDetailsDTO> listDashboards(String search) throws SKCException {
		logger.trace("Inside listDashboards() method of {}", this.getClass());

		List<DashboardDetails> dashboardDetails;

		if (search.isEmpty())
			dashboardDetails = dashboardRepository.findAll();
		else
			dashboardDetails = dashboardRepository.findByNameContaining(search);

		logger.debug("No. of Dashboards retrieved: {}", dashboardDetails.size());

		return dashboardDetails.stream().map(DashboardDetailsDTO::valueOf).collect(Collectors.toList());
	}

	@Override
	public DashboardDetailsDTO addDashboard(DashboardDetailsDTO dashboardDetailsDTO) throws SKCException {
		logger.trace("Inside addDashboard() method of {}", this.getClass());

		List<DashboardDetails> dashboards = dashboardRepository.findByName(dashboardDetailsDTO.getName());
		if (!dashboards.isEmpty())
			throw new SKCException(HttpStatus.CONFLICT, ExceptionConstants.DASHBOARD_ALREADY_EXISTS_NAME.toString());

		dashboards = dashboardRepository.findByUrl(dashboardDetailsDTO.getUrl());

		if (!dashboards.isEmpty())
			throw new SKCException(HttpStatus.CONFLICT, ExceptionConstants.DASHBOARD_ALREADY_EXISTS_URL.toString());

		DashboardDetails dashboardDetails = DashboardDetailsDTO.createEntity(dashboardDetailsDTO);
		dashboardDetails = dashboardRepository.saveAndFlush(dashboardDetails);
		logger.debug(DashboardDetailsDTO.valueOf(dashboardDetails).toString());
		logger.trace("Exiting addDashboard() method of {}", this.getClass());
		return DashboardDetailsDTO.valueOf(dashboardDetails);
	}

	@Override
	public Integer updateDashboard(Integer dashboardId, DashboardDetailsDTO dashboardDetailsDTO) throws SKCException {

		logger.trace("Inside updateDashboard() method of {}", this.getClass());
		Optional<DashboardDetails> dashboardOpt = dashboardRepository.findById(dashboardId);

		if (dashboardOpt.isPresent()) {

			DashboardDetails dashboardEntity = dashboardOpt.get();

			if (dashboardDetailsDTO.getName() != null)
				dashboardEntity.setName(dashboardDetailsDTO.getName());

			if (dashboardDetailsDTO.getClusterType() != null)
				dashboardEntity.setClusterType(dashboardDetailsDTO.getClusterType());

			if (dashboardDetailsDTO.getDashboardStatus() != null)
				dashboardEntity.setDashboardStatus(dashboardDetailsDTO.getDashboardStatus());

			if (dashboardDetailsDTO.getUrl() != null)
				dashboardEntity.setUrl(dashboardDetailsDTO.getUrl());

			int response = dashboardRepository.saveAndFlush(dashboardEntity).getid();

			logger.debug("Dashboard Details updated for dashboardId: {}", response);

			logger.trace("Exiting updateDashboard() method of {}", this.getClass());
			return response;

		} else {
			logger.trace("Exiting updateDashboard() method of {}", this.getClass());
			throw new SKCException(HttpStatus.NOT_FOUND, ExceptionConstants.DASHBOARD_NOT_FOUND.toString());
		}

	}

	@Override
	public int deleteDashboard(int id) throws SKCException {
		logger.trace("Inside deleteDashboard() method of {}", this.getClass());

		Optional<DashboardDetails> dashboardDetails = dashboardRepository.findById(id);

		if (!dashboardDetails.isPresent()) {
			logger.error("{} with id {}", ExceptionConstants.DASHBOARD_NOT_FOUND, id);
			throw new SKCException(HttpStatus.NOT_FOUND, ExceptionConstants.DASHBOARD_NOT_FOUND.toString());
		}

		dashboardRepository.deleteById(id);
		removeHashMap(id);
		logger.debug("Dashboard with {} deleted", id);

		return id;
	}

	// Provides Host public IP when SKC is deployed in a container
	@Override
	public String getSkcHostIp() throws SKCException {

		String skcIp = "";

		try {
			System.out.println("inside getSKCIP - try");
			skcIp = System.getenv("SKC_IP") != null ? System.getenv("SKC_IP").toString()
					: System.getenv().get("SKC_IP");
			System.out.println(skcIp);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		logger.info("SKC Ip fetched from env - " + skcIp);

		if (skcIp.equals("")) {
			throw new SKCException(HttpStatus.NOT_FOUND, "SKC IP not set in env variable. Cannot proceed");
		}

		return skcIp;
	}

	@Override
	public boolean pingHost(String host, int port, int timeout) {
		try (Socket socket = new Socket()) {
			socket.connect(new InetSocketAddress(host, port), timeout);
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	// Fetches a list of Nodes Present in a Cluster
	@Override
	public String[] fetchClusterNodesList(Session session, StringBuilder logs)
			throws SKCException, JSchException, IOException {

		ChannelExec channel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;
		StringBuilder nodesListResponse = new StringBuilder();

		try {
			logger.info("------------------------------------------------------------------------------------------");
			logger.info("Fetching List of Controller Nodes on the Cluster");
			logger.info("------------------------------------------------------------------------------------------");

			logs.append("------------------------------------------------------------------------------------------\n");
			logs.append("Fetching List of Controller Nodes on the Cluster\n");
			logs.append("------------------------------------------------------------------------------------------\n");
			channel = (ChannelExec) session.openChannel("exec");
			channel.setCommand("kubectl get nodes -o wide |grep control-plane |awk '{print $2,$6}'");

			in = channel.getInputStream();

			channel.connect();

			tmp = new byte[1024];

			while (true) {
				Thread.sleep(1000);

				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;

					String logStream = new String(tmp, 0, i);

					for (String s : logStream.split("\n")) {
						logger.info(s);
					}
					nodesListResponse.append(logStream);

				}

				if (channel.isClosed()) {
					int exitCode = channel.getExitStatus();
					logger.info("exit-status: " + channel.getExitStatus());
					logs.append("exit-status: " + channel.getExitStatus() + "\n");
					if (exitCode == 0) {
						logger.info("Controller Nodes Fetched - " + nodesListResponse.toString());
						logs.append("Controller Nodes Fetched - " + nodesListResponse.toString() + "\n");
					} else {
						throw new SKCException(HttpStatus.NOT_FOUND, "Could not Fetch Controller Nodes");
					}
					break;
				}

			}
			channel.disconnect();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		String[] nodesListWithStatus = nodesListResponse.toString().trim().split("\n");

		if (nodesListWithStatus.length == 1 && nodesListWithStatus[0].equals("")) {
			throw new SKCException(HttpStatus.PRECONDITION_FAILED, "No Controller Nodes found. Cannot proceed");
		}

		StringBuilder notReady = new StringBuilder();
		String[] statusAndIp = {};
		ArrayList<String> nodesList = new ArrayList<>();

		for (String node : nodesListWithStatus) {
			statusAndIp = node.split(" ");

			if (!statusAndIp[0].equals("Ready")) {
				notReady.append(node + "\n");
			}
			nodesList.add(statusAndIp[1]);
		}

		System.out.println(notReady.toString());

		if (notReady.length() > 0) {
			String errorMessage = "All controller nodes not active - \n";
			throw new SKCException(HttpStatus.NOT_FOUND, errorMessage + notReady.toString());
		}

		return nodesList.toArray(new String[0]);
	}

	@Override
	public Boolean controllerNodesReachable(String[] nodesList, StringBuilder logs) throws SKCException {

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Checking if Controller Nodes are Reachable");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Checking if Controller Nodes are Reachable\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		StringBuilder notReachable = new StringBuilder();
		String errorMessage = "Following Nodes are not reachable - \n";

		for (String node : nodesList) {

			if (!pingHost(node, 22, 2000)) {
				notReachable.append(node + " ");
				logger.info("Pinged node - " + node + " - Unreachable");
				logs.append("Pinged node - " + node + " - Unreachable\n");
			} else {
				logger.info("Pinged node - " + node + " - Reachable");
				logs.append("Pinged node - " + node + " - Reachable\n");
			}
		}

		if (notReachable.length() > 0) {
			throw new SKCException(HttpStatus.REQUEST_TIMEOUT, errorMessage + notReachable.toString());
		}

		return true;
	}

	@Override
	public Boolean isStorageAvailable(String[] nodesList, Integer expectedStorageInMB, String sshUsername,
			String sshPassword, StringBuilder logs) throws SKCException {
		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Checking Storage Requirements");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Checking Storage Requirements\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;
		StringBuilder availableStorage;
		StringBuilder insufficientStorage = new StringBuilder();
		Integer availableStorageValue;
		String errorMessage = null;

		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;
			availableStorage = new StringBuilder();

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());
				// Creating Destination Directory
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");
				nodeChannel.setCommand("df -Ph -BM . | awk 'NR==2 {print $4}'");
				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {

					Thread.sleep(1000);

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;

						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}

						logs.append(logStream + "\n");
						availableStorage.append(logStream);

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							String storageString = availableStorage.toString().trim();

							availableStorageValue = Integer
									.parseInt(storageString.substring(0, storageString.length() - 1));

							if (availableStorageValue >= expectedStorageInMB) {
								logger.info("Node " + node + " - " + availableStorageValue + "MB found is sufficient");
								logs.append(
										"Node " + node + " - " + availableStorageValue + "MB found is sufficient\n");
							} else {
								logger.info(
										"Node " + node + " - " + availableStorageValue + "MB found is insufficient");
								logs.append(
										"Node " + node + " - " + availableStorageValue + "MB found is insufficient\n");

								insufficientStorage.append("Node " + node + " - " + availableStorageValue + "MB\n");
							}

						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not fetch Storage from  - " + node);
						}
						break;
					}

				}
			} catch (JSchException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if (insufficientStorage.length() > 0) {
			errorMessage = "Insufficient Storage. Expected - " + expectedStorageInMB + "MB, found:\n";
			throw new SKCException(HttpStatus.INSUFFICIENT_STORAGE, errorMessage + insufficientStorage.toString());
		}

		return true;
	}

	/*
	 * Prepares the cluster nodes for Installation of Dashboard by : Transferring
	 * Dashboard resources such as scripts and Docker images
	 */
	@Override
	public void transferAndExtractResources(String[] nodesList, String sshUsername, String sshPassword,
			StringBuilder logs) throws SKCException {

		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;

		final String addResourceTar= "dashboard-resources.tgz";
		final String untar ="tar -xvzf ";

		// ProcessBuilder processBuilder = new ProcessBuilder();

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Transferring resources to Controller Nodes");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Transferring resources to Controller Nodes\n");
		logs.append("------------------------------------------------------------------------------------------\n");
		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());
				// Creating Destination Directory
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");
				nodeChannel.setCommand("mkdir -p " + resDestPath);
				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;

						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}

						logs.append(logStream + "\n");

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("Directory Created - " + resDestPath);
							logs.append("Directory Created - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not create Directory - " + resDestPath);
						}
						break;
					}

				}

				// SCP resources to remote machine
				logger.info("Transfering resources to - " + node);
				logs.append("Transfering resources to - " + node + "\n");

				// expect -f scp_expect.sh dash-res user ip dest pass

				List<String> cmd = new ArrayList();
				cmd.add("expect");
				cmd.add("-f");
				cmd.add(resSrcPath + "scp_expect.sh");
				cmd.add(resSrcPath + addResourceTar);
				// cmd.add(resSrcPath + "tar_extract.sh");
				cmd.add(sshUsername);
				cmd.add(node);
				cmd.add(resDestPath);
				cmd.add(sshPassword);

				logger.info(cmd.toString());
				processBuilder.command(cmd);

				Process process = processBuilder.start();

				int exitVal = process.waitFor();

				if (exitVal == 0) {
					logger.info("Transfer of Resources Complete");
					logs.append("Transfer of Resources Complete" + "\n");
				} else {
					logger.info(process.getErrorStream().toString());
					logs.append(process.getErrorStream().toString() + "\n");
					throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
							"Could not Transfer Resources to - " + resDestPath);
				}
				process.destroy();

				// Extraction of resources
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");
				logger.info("Extracting resources at - " + nodeSession.getHost());
				logger.info(untar + resDestPath + addResourceTar + " -C " + resDestPath);
				nodeChannel.setCommand(untar + resDestPath + addResourceTar+ " -C " + resDestPath);

				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;

						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}

						logs.append(logStream);
					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("Extraction of resources completed at - " + resDestPath);
							logs.append("Extraction of resources completed at - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not extract resources at - " + resDestPath);
						}
						break;
					}

				}

				logger.info("Disconnecting from - " + nodeSession.getHost());
				nodeChannel.disconnect();
				nodeSession.disconnect();
			} catch (JSchException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			logger.info(node);
		}

	}

	@Override
	public void transferAndExtractDeleteResources(String[] nodesList, String sshUsername, String sshPassword,
			StringBuilder logs) throws SKCException {

		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;

		final String deleteResourceTar = "delete-dashboard-resources.tgz";
		final String untar ="tar -xvzf ";

		// ProcessBuilder processBuilder = new ProcessBuilder();

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Transferring resources to Controller Nodes");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Transferring resources to Controller Nodes\n");
		logs.append("------------------------------------------------------------------------------------------\n");
		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());
				// Creating Destination Directory
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");
				nodeChannel.setCommand("mkdir -p " + resDestPath);
				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;

						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}

						logs.append(logStream + "\n");

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("Directory Created - " + resDestPath);
							logs.append("Directory Created - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not create Directory - " + resDestPath);
						}
						break;
					}

				}

				// SCP resources to remote machine
				logger.info("Transfering resources to - " + node);
				logs.append("Transfering resources to - " + node + "\n");

				// expect -f scp_expect.sh dash-res user ip dest pass

				List<String> cmd = new ArrayList();
				cmd.add("expect");
				cmd.add("-f");
				cmd.add(resSrcPath + "scp_expect.sh");
				cmd.add(resSrcPath +  deleteResourceTar);
				// cmd.add(resSrcPath + "tar_extract.sh");
				cmd.add(sshUsername);
				cmd.add(node);
				cmd.add(resDestPath);
				cmd.add(sshPassword);

				logger.info(cmd.toString());
				processBuilder.command(cmd);

				Process process = processBuilder.start();

				int exitVal = process.waitFor();

				if (exitVal == 0) {
					logger.info("Transfer of Resources Complete");
					logs.append("Transfer of Resources Complete" + "\n");
				} else {
					logger.info(process.getErrorStream().toString());
					logs.append(process.getErrorStream().toString() + "\n");
					throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
							"Could not Transfer Resources to - " + resDestPath);
				}
				process.destroy();

				// Extraction of resources
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");
				logger.info("Extracting resources at - " + nodeSession.getHost());
				logger.info(untar + resDestPath + deleteResourceTar + " -C " + resDestPath);
				nodeChannel
						.setCommand(untar + resDestPath + deleteResourceTar + " -C " + resDestPath);

				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {
					try {
						Thread.sleep(1000);
					} catch (Exception e) {
						e.printStackTrace();
					}

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;

						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}

						logs.append(logStream);
					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("Extraction of resources completed at - " + resDestPath);
							logs.append("Extraction of resources completed at - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not extract resources at - " + resDestPath);
						}
						break;
					}

				}

				logger.info("Disconnecting from - " + nodeSession.getHost());
				nodeChannel.disconnect();
				nodeSession.disconnect();
			} catch (JSchException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			logger.info(node);
		}

	}

	/*
	 * Deletes the following resources from the cluster nodes which were transfered
	 * for Installation of Dashboard by : Scripts and Docker images
	 */
	@Override
	public void deleteResources(String[] nodesList, String sshUsername, String sshPassword, StringBuilder logs)
			throws SKCException {

		System.out.println("Inside Delete resources \nNodes");
		for (String node : nodesList) {
			System.out.println(node);
		}

		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Deleting resources from Controller Nodes");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Deleting resources from Controller Nodes\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());
				// Creating Destination Directory
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");

				nodeChannel.setCommand("rm -rf " + resDestPath);

				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {

					Thread.sleep(1000);

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;

						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}

						logs.append(logStream + "\n");

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("Resources Deleted - " + resDestPath);
							logs.append("Resources Deleted - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not delete Resources - " + resDestPath);
						}
						break;
					}

				}

				logger.info("Disconnecting from - " + nodeSession.getHost());
				nodeChannel.disconnect();
				nodeSession.disconnect();
			} catch (JSchException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			logger.info(node);
		}



	}

	// Returns 0-Successful Deployment, 1-Error, 2-Dashboard Already Exists
	@Override
	public Integer triggerAddDashboardScript(String[] nodesList, String sshUsername, String sshPassword,
			String clusterName, StringBuilder logs) throws SKCException {

		System.out.println("Inside triggerAddDashboardScript  \nNodes");
		for (String node : nodesList) {
			System.out.println(node);
		}

		Session session = null;
		ChannelExec channel;
		byte tmp[] = new byte[1024];
		InputStream in = null;
		Integer successFlag = 0; // 0-Successful Deployment, 1-Error, 2-Dashboard Already Exists

		String node = nodesList[0];

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Triggering Install Script");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Triggering Install Script\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		try {
			session = jSch.getSession(sshUsername, node, 22);
			session.setPassword(sshPassword);
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();

			logger.info("Connecting to - " + node);
			logs.append("Connecting to - " + node);
			logger.info("\n=== Installing K8s Community Dashboard ===");
			logs.append("\n=== Installing K8s Community Dashboard ===\n");

			String skcIp = getSkcHostIp();
			logger.info("SKC IP - " + skcIp);
			logs.append("SKC IP - " + skcIp + "\n");

			channel = (ChannelExec) session.openChannel("exec");
			// channel.setCommand("ls "+resDestPath);
			logger.info(resDestPath + "dashboard-resources/scripts/add_dbrd.sh -n " + clusterName + " -t "
					+ "ON_PREMISE -i https://" + skcIp + ":8443");
			logs.append(resDestPath + "dashboard-resources/scripts/add_dbrd.sh -n " + clusterName + " -t "
					+ "ON_PREMISE -i https://" + skcIp + ":8443" + "\n");

			channel.setCommand(resDestPath + "dashboard-resources/scripts/add_dbrd.sh -n " + clusterName + " -t "
					+ "ON_PREMISE -i https://" + skcIp + ":8443");

			ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
			channel.setOutputStream(responseStream);

			in = channel.getInputStream();

			channel.connect();

			tmp = new byte[1024];

			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0)
						break;
					String logStream = new String(tmp, 0, i);

					for (String s : logStream.split("\n")) {
						logger.info(s);
					}
					logs.append(logStream);
				}

				if (channel.isClosed()) {
					int exitCode = channel.getExitStatus();
					logger.info("exit-status: " + channel.getExitStatus());
					logs.append("exit-status: " + channel.getExitStatus() + "\n");
					if (exitCode == 0) {
						logger.info("----------------SKC Successfully Installed !!--------------------");
						logs.append("----------------SKC Successfully Installed !!--------------------\n");
					} else if (exitCode == 2) {
						logger.info("SKC already Exists.");
						logs.append("SKC already Exists.\n");
					} else {
						logger.info("SKC install Failed");
						logs.append("SKC install Failed\n");

						logger.info("Cleaning up Clusters.");
						logs.append("Cleaning up Clusters.\n");
						deleteResources(nodesList, sshUsername, sshPassword, logs);
					}

					successFlag = exitCode;
					channel.disconnect();
					session.disconnect();
					break;
				}

				Thread.sleep(1000);
			}
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (JSchException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException ee) {
			ee.printStackTrace();
		}
		// catch (Exception ee) {
		// ee.printStackTrace();
		// }

		return successFlag;
	}

	@Override
	public void postInstallScript(String[] nodesList, String sshUsername, String sshPassword, StringBuilder logs)
			throws SKCException {
		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Running Post Install Script to add entry in .bashrc and create a Cronjob");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Running Post Install Script to add entry in .bashrc and create a Cronjob");
		logs.append("------------------------------------------------------------------------------------------\n");

		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());

				String skcIp = getSkcHostIp();
				logger.info("SKC IP - " + skcIp);
				logs.append("SKC IP - " + skcIp + "\n");

				// Loading Docker Images
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");

				nodeChannel.setCommand(
						resDestPath + "dashboard-resources/scripts/post_install.sh -i https://" + skcIp + ":8443");

				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {

					Thread.sleep(1000);

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;
						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}
						logs.append(logStream + "\n");

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("Success - ");
							logs.append("Success - \n");
						} else {
							throw new SKCException(HttpStatus.FAILED_DEPENDENCY,
									"Post Install Script Failed - " + resDestPath);
						}
						break;
					}

				}

				logger.info("Disconnecting from - " + nodeSession.getHost());
				nodeChannel.disconnect();
				nodeSession.disconnect();
			} catch (JSchException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			// catch (Exception e) {

			// e.printStackTrace();
			// }
		}
	}

	@Override
	public boolean triggerDeleteDashboardScript(String[] nodesList, String sshUsername, String sshPassword,
			StringBuilder logs) {

		System.out.println("Inside triggerDeleteDashboardScript  \nNodes");
		for (String node : nodesList) {
			System.out.println(node);
		}

		Session session = null;
		ChannelExec channel;
		byte tmp[] = new byte[1024];
		InputStream in = null;
		boolean successFlag = false;

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Triggering Uninstall Script");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Triggering Uninstall Script\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		for (String node : nodesList) {

			try {
				session = jSch.getSession(sshUsername, node, 22);
				session.setPassword(sshPassword);
				session.setConfig("StrictHostKeyChecking", "no");
				session.connect();

				logger.info("Connecting to - " + node);
				logs.append("Connecting to - " + node);
				logger.info("\n=== Uninstalling K8s Community Dashboard ===");
				logs.append("\n=== Uninstalling K8s Community Dashboard ===\n");

				channel = (ChannelExec) session.openChannel("exec");
				// channel.setCommand("ls "+resDestPath);
				logger.info(resDestPath + "dashboard-resources/scripts/del_dbrd.sh");
				logs.append(resDestPath + "dashboard-resources/scripts/del_dbrd.sh");

				channel.setCommand(resDestPath + "dashboard-resources/scripts/del_dbrd.sh");

				ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
				channel.setOutputStream(responseStream);

				in = channel.getInputStream();

				channel.connect();

				tmp = new byte[1024];

				while (true) {
					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;
						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}
						logs.append(logStream);
					}

					if (channel.isClosed()) {
						int exitCode = channel.getExitStatus();
						logger.info("exit-status: " + channel.getExitStatus());
						logs.append("exit-status: " + channel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("----------------Success !!--------------------");
							logs.append("----------------Success !!--------------------\n");
							successFlag = true;

						} else {
							logger.info("SKC Uninstall Failed");
							logs.append("SKC Uninstall Failed\n");
						}
						channel.disconnect();
						session.disconnect();
						break;
					}

					Thread.sleep(1000);

				}
			} catch (UnknownHostException e) {

				e.printStackTrace();
			} catch (JSchException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return successFlag;
	}

	@Override
	public boolean triggerCleanUpScript(String[] nodesList, String sshUsername, String sshPassword,
			StringBuilder logs) {

		System.out.println("Inside triggerCleanUpScript  \nNodes");
		for (String node : nodesList) {
			System.out.println(node);
		}

		Session session = null;
		ChannelExec channel;
		byte tmp[] = new byte[1024];
		InputStream in = null;
		boolean successFlag = false;

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Triggering CleanUp Script");
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Triggering CleanUp Script\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		for (String node : nodesList) {

			try {
				session = jSch.getSession(sshUsername, node, 22);
				session.setPassword(sshPassword);
				session.setConfig("StrictHostKeyChecking", "no");
				session.connect();

				logger.info("Connecting to - " + node);
				logs.append("Connecting to - " + node);
				logger.info("\n=== Running Cleanup Script ===");
				logs.append("\n=== Running Cleanup Script ===\n");

				channel = (ChannelExec) session.openChannel("exec");
				// channel.setCommand("ls "+resDestPath);

				// TODO
				// Change filename and path
				// Also add exception

				logger.info(resDestPath + "dashboard-resources/scripts/skc_cleanup.sh");
				logs.append(resDestPath + "dashboard-resources/scripts/skc_cleanup.sh");

				channel.setCommand(resDestPath + "dashboard-resources/scripts/skc_cleanup.sh");

				ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
				channel.setOutputStream(responseStream);

				in = channel.getInputStream();

				channel.connect();

				tmp = new byte[1024];

				while (true) {
					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;
						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}
						logs.append(logStream);
					}

					if (channel.isClosed()) {
						int exitCode = channel.getExitStatus();
						logger.info("exit-status: " + channel.getExitStatus());
						logs.append("exit-status: " + channel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("----------------Success !!--------------------");
							logs.append("----------------Success !!--------------------\n");
							successFlag = true;

						} else {
							logger.info("CleanUp Script Failed");
							logs.append("CleanUp Script Failed\n");
						}
						channel.disconnect();
						session.disconnect();
						break;
					}

					Thread.sleep(1000);

				}
			} catch (UnknownHostException e) {

				e.printStackTrace();
			} catch (JSchException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return successFlag;

	}

	@Override
	public void dockerImagesTask(String[] nodesList, String sshUsername, String sshPassword, String task,
			StringBuilder logs) throws SKCException {

		System.out.println("Inside load images  \nNodes");
		for (String node : nodesList) {
			System.out.println(node);
		}
		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("Starting Docker Images Task - " + task.toUpperCase());
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("Starting Docker Images Task - " + task.toUpperCase() + "\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		if (!(task.equals("load") || task.equals("unload"))) {

			throw new SKCException(HttpStatus.METHOD_NOT_ALLOWED,
					"Task " + task + " is not supported for DockerImagesTasks");
		}

		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());

				// Loading Docker Images
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");
				nodeChannel.setCommand(resDestPath + "dashboard-resources/scripts/modify_docker_images.sh " + task);

				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {

					Thread.sleep(1000);

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;
						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}
						logs.append(logStream + "\n");

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("Images Loaded - " + resDestPath);
							logs.append("Images Loaded - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not Load Images - " + resDestPath);
						}
						break;
					}

				}

				logger.info("Disconnecting from - " + nodeSession.getHost());
				nodeChannel.disconnect();
				nodeSession.disconnect();
			} catch (JSchException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
		}

	}

	@Override
	public void bashrcTask(String[] nodesList, String sshUsername, String sshPassword, String task, StringBuilder logs)
			throws SKCException {

		// System.out.println("Inside bashrc \nNodes");
		// for (String node : nodesList) {
		// System.out.println(node);
		// }

		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;

		logger.info("------------------------------------------------------------------------------------------");
		logger.info(".bashrc Entry - " + task.toUpperCase());
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append(".bashrc Entry - " + task.toUpperCase() + "\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		if (!(task.equals("add") || task.equals("remove"))) {
			throw new SKCException(HttpStatus.METHOD_NOT_ALLOWED,
					"Task " + task + " is not supported for ModifyBashrcTasks");
		}

		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());

				String skcIp = getSkcHostIp();
				logger.info("SKC IP - " + skcIp);
				logs.append("SKC IP - " + skcIp + "\n");

				// Loading Docker Images
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");
				if (task.equals("add")) {
					nodeChannel.setCommand(resDestPath + "dashboard-resources/scripts/modify_bashrc.sh " + task
							+ " SKC_IP https://" + skcIp + ":8443 -b");
				} else if (task.equals("remove")) {
					nodeChannel.setCommand(
							resDestPath + "dashboard-resources/scripts/modify_bashrc.sh " + task + " SKC_IP -b");
				} else {
					throw new SKCException(HttpStatus.METHOD_NOT_ALLOWED,
							"Task " + task + " is not supported for ModifyBashrcTasks");
				}

				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {

					Thread.sleep(1000);

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;
						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}
						logs.append(logStream + "\n");

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("bashrc updated - " + resDestPath);
							logs.append("bashrc updated - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not update bashrc - " + resDestPath);
						}
						break;
					}

				}

				logger.info("Disconnecting from - " + nodeSession.getHost());
				nodeChannel.disconnect();
				nodeSession.disconnect();
			} catch (JSchException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			// } catch (Exception e) {

			// e.printStackTrace();
			// }
		}
	}

	@Override
	public void cronJobTask(String[] nodesList, String sshUsername, String sshPassword, String task, StringBuilder logs)
			throws SKCException {

		System.out.println("Inside addCronJob  \nNodes");
		for (String node : nodesList) {
			System.out.println(node);
		}

		Session nodeSession = null;
		ChannelExec nodeChannel = null;
		byte tmp[] = new byte[1024];
		InputStream in = null;

		logger.info("------------------------------------------------------------------------------------------");
		logger.info("CronJobs for Dashboard Status - " + task.toUpperCase());
		logger.info("------------------------------------------------------------------------------------------");

		logs.append("------------------------------------------------------------------------------------------\n");
		logs.append("CronJobs for Dashboard Status - " + task.toUpperCase() + "\n");
		logs.append("------------------------------------------------------------------------------------------\n");

		if (!(task.equals("add") || task.equals("remove"))) {
			throw new SKCException(HttpStatus.METHOD_NOT_ALLOWED,
					"Task " + task + " is not supported for ModifyCronJobTasks");
		}

		for (String node : nodesList) {
			nodeSession = null;
			nodeChannel = null;

			try {
				logger.info("Connecting to Node - " + node);
				logs.append("Connecting to Node - " + node + "\n");
				nodeSession = jSch.getSession(sshUsername, node, 22);
				nodeSession.setPassword(sshPassword);
				nodeSession.setConfig("StrictHostKeyChecking", "no");
				nodeSession.connect();

				logger.info("Connected to - " + nodeSession.getHost());

				// Loading Docker Images
				nodeChannel = (ChannelExec) nodeSession.openChannel("exec");

				if (task.equals("add") || task.equals("remove")) {
					nodeChannel.setCommand(resDestPath + "dashboard-resources/scripts/modify_cronjob.sh " + task);
				} else {
					throw new SKCException(HttpStatus.METHOD_NOT_ALLOWED,
							"Task " + task + " is not supported for ModifyCronJobTasks");
				}

				in = nodeChannel.getInputStream();

				nodeChannel.connect();

				tmp = new byte[1024];

				while (true) {

					Thread.sleep(1000);

					while (in.available() > 0) {
						int i = in.read(tmp, 0, 1024);
						if (i < 0)
							break;
						String logStream = new String(tmp, 0, i);

						for (String s : logStream.split("\n")) {
							logger.info(s);
						}
						logs.append(logStream + "\n");

					}

					if (nodeChannel.isClosed()) {
						int exitCode = nodeChannel.getExitStatus();
						logger.info("exit-status: " + nodeChannel.getExitStatus());
						logs.append("exit-status: " + nodeChannel.getExitStatus() + "\n");
						if (exitCode == 0) {
							logger.info("CronJob Created - " + resDestPath);
							logs.append("CronJob Created - " + resDestPath + "\n");
						} else {
							throw new SKCException(HttpStatus.INTERNAL_SERVER_ERROR,
									"Could not Create CronJob - " + resDestPath);
						}
						break;
					}

				}

				logger.info("Disconnecting from - " + nodeSession.getHost());
				nodeChannel.disconnect();
				nodeSession.disconnect();
			} catch (JSchException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			// } catch (Exception e) {

			// e.printStackTrace();
			// }
		}
	}

	@Override
	public Boolean healthCheck(String event, DashboardDetailsDTO payload) throws SKCException {
		logger.info("Inside healthCheck() method of {}", this.getClass());

		if (payload.getId() == null) {
			logger.error("Request payload doesn't contain Dashboard ID.");
			throw new SKCException(HttpStatus.NOT_ACCEPTABLE, ExceptionConstants.INPUT_ID_NULL.toString());
		}

		// if (!isEventValid(event)) {
		// throw new SKCException(ExceptionConstants.EVENT_INVALID.toString());
		// }

		if (WebhookEventConstants.DASHBOARD_MODIFIED.toString().equalsIgnoreCase(event)) {
			logger.info("Event:- Dashboard Modified");

			int dId = updateDashboard(payload.getId(), payload);

			if (!hashMap.containsKey(dId))
				insertHashMap(dId);

			else if (payload.getDashboardStatus() != null)
				updateHashMap(dId, payload.getDashboardStatus());

			logger.info("Event successfully logged");
			return true;
		}
		if (WebhookEventConstants.DASHBOARD_STATUS.toString().equalsIgnoreCase(event)) {
			logger.info("Event:- Dashboard Status");

			if (!hashMap.containsKey(payload.getId())) {
				insertHashMap(payload.getId());
			}

			if (!hashMap.get(payload.getId()).equals(payload.getDashboardStatus().toString())) {
				updateHashMap(payload.getId(), payload.getDashboardStatus());
			}

			logger.info("Event successfully registered");
			return true;
		}

		// return false;
		logger.error("Request header contains invalid event");
		throw new SKCException(HttpStatus.NOT_ACCEPTABLE, ExceptionConstants.EVENT_INVALID.toString());

	}

	private boolean isEventValid(String event) {
		for (WebhookEventConstants constants : WebhookEventConstants.values())
			if (constants.toString().equalsIgnoreCase(event)) {
				logger.info("Event {} is valid", event);
				return true;
			}
		logger.error("Event {} is invalid", event);
		return false;
	}

	@EventListener(ApplicationReadyEvent.class)
	public void cronJobUpdateTimeoutWatcher() {
		// Timer timer = new Timer();
		TimerTask updateTimeoutChecker = new TimerTask() {

			@Override
			public void run() {
				logger.debug("Checking for timed out cronJobs");

				LocalDateTime currentTime = LocalDateTime.now();

				hashMap.forEach((id, status) -> {

					if (getTimeDiffInMinutes(status.getTimestamp(), currentTime) > 3) { // cronjob didnt update for 3
																						// minutes
						logger.info("Dashboard with ID - " + id + " timed Out");
						updateHashMap(id, DashboardStatus.DASHBOARD_INACTIVE);
					}
				});

			}
		};
		logger.info("Task for cronJobUpdateTimeoutWatcher Started");
		timer.schedule(updateTimeoutChecker, new Date(), 120000); // 2 minutes
	}

	public long getTimeDiffInMinutes(LocalDateTime from, LocalDateTime to) {

		Duration duration = Duration.between(from, to);
		long seconds = duration.getSeconds();

		long minutes = (seconds % 3600) / 60;

		return minutes;
	}

	private void insertHashMap(int id) throws SKCException {
		Optional<DashboardDetails> optionalDashboardDetails = dashboardRepository.findById(id);

		if (!optionalDashboardDetails.isPresent()) {
			logger.error("Dashboard with ID {} not found", id);
			throw new SKCException(HttpStatus.NOT_FOUND, ExceptionConstants.DASHBOARD_NOT_FOUND.toString());
		}

		logger.info("Dashboard with ID {} inserted into cache with status {} ", id,
				optionalDashboardDetails.get().getDashboardStatus().toString());

		DashboardStatusDetails currentStatus = new DashboardStatusDetails(
				optionalDashboardDetails.get().getDashboardStatus().toString(), LocalDateTime.now());

		hashMap.put(id, currentStatus);
	}

	private void updateHashMap(int id, DashboardStatus status) {
		Optional<DashboardDetails> optionalDashboardDetails = dashboardRepository.findById(id);

		DashboardDetails dashboardDetails = optionalDashboardDetails.get();

		dashboardDetails.setDashboardStatus(status);

		dashboardRepository.saveAndFlush(dashboardDetails);

		logger.info("Dashboard with ID {} updated in cache with status {}", id, status.toString());

		DashboardStatusDetails currentStatus = new DashboardStatusDetails(status.toString(), LocalDateTime.now());

		hashMap.put(id, currentStatus);
	}

	private void removeHashMap(int id) {
		if (hashMap.containsKey(id))
			hashMap.remove(id);
		logger.info("Dashboard with ID {} deleted from cache", id);
	}

	public String refresh(List<String> ipList, String clusterName) {
		System.out.println("Refresh start");
		DashboardDetails dashboard = dashboardRepository.findOneByName(clusterName);
		String url = dashboard.getUrl();
		int loc1 = url.lastIndexOf('/');
		int loc2 = url.lastIndexOf(':');
		String host = url.substring(loc1 + 1, loc2);
		int port = Integer.parseInt(url.substring(1 + loc2));
		if (pingHost(host, port, 2000)) {
			System.out.println("IP not changed");
			System.out.println("Refresh End");
			return "IP not changed";
		}

		System.out.println("Current IP is not running");

		System.out.println("Iterating over all ipList start");
		for (int i = 0; i < ipList.size(); ++i) {
			System.out.println(ipList.get(i) + " : " + pingHost(ipList.get(i), port, 2000));
		}
		System.out.println("Iterating end");

		for (int i = 0; i < ipList.size(); ++i) {
			if (pingHost(ipList.get(i), port, 2000)) {
				dashboard.setUrl("https://" + ipList.get(i) + ":" + port);
				dashboardRepository.saveAndFlush(dashboard);
				System.out.println("https://" + ipList.get(i) + ":" + port);
				System.out.println("Refresh End");
				return "IP changed";
			}
		}
		System.out.println("Refresh End return null");
		return null;
	}
}
