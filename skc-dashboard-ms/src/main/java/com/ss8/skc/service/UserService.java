package com.ss8.skc.service;

import com.ss8.skc.dto.ChangePasswordDTO;
import com.ss8.skc.dto.UserDetailsDTO;
import com.ss8.skc.exception.SKCException;

public interface UserService {
    UserDetailsDTO addUser(UserDetailsDTO userDetailsDTO) throws SKCException;
    String fetchToken(UserDetailsDTO userDetailsDTO) throws SKCException;
    Boolean deleteUserByUserId(Integer id) throws SKCException;
    Boolean deleteUsersByDashboardId(Integer id) throws SKCException;
    Boolean changePassword(ChangePasswordDTO changePasswordDTO) throws SKCException;
}
