package com.ss8.skc.service;

import java.io.IOException;
import java.util.List;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.ss8.skc.dto.DashboardDetailsDTO;
import com.ss8.skc.exception.SKCException;

public interface DashboardService {

	List<DashboardDetailsDTO> listDashboards(String search) throws SKCException;

	DashboardDetailsDTO addDashboard(DashboardDetailsDTO dashboardDetailsDTO) throws SKCException;

	Integer updateDashboard(Integer dashboardId, DashboardDetailsDTO dashboardDetailsDTO) throws SKCException;

	int deleteDashboard(int id) throws SKCException;

	String getSkcHostIp() throws SKCException;

	boolean pingHost(String host, int port, int timeout);

	Integer triggerAddDashboardScript(String[] nodesList, String sshUsername, String sshPassword, String clusterName,
			StringBuilder logs) throws SKCException;

	void postInstallScript(String[] nodesList, String sshUsername, String sshPassword, StringBuilder logs)
			throws SKCException;

	boolean triggerDeleteDashboardScript(String[] nodesList, String sshUsername, String sshPassword,
			StringBuilder logs);

	boolean triggerCleanUpScript(String[] nodesList, String sshUsername, String sshPassword, StringBuilder logs);

	String[] fetchClusterNodesList(Session session, StringBuilder logs) throws SKCException, JSchException, IOException;

	Boolean controllerNodesReachable(String[] nodesList, StringBuilder logs) throws SKCException;

	Boolean isStorageAvailable(String[] nodesList, Integer expectedStorageInMB, String sshUsername, String sshPassword,
			StringBuilder logs) throws SKCException;

	void transferAndExtractResources(String[] nodesList, String sshUsername, String sshPassword, StringBuilder logs)
			throws SKCException;

	void transferAndExtractDeleteResources(String[] nodesList, String sshUsername, String sshPassword,
			StringBuilder logs) throws SKCException;

	void deleteResources(String[] nodesList, String sshUsername, String sshPassword, StringBuilder logs)
			throws SKCException;

	void dockerImagesTask(String[] nodesList, String sshUsername, String sshPassword, String task, StringBuilder logs)
			throws SKCException;

	void bashrcTask(String[] nodesList, String sshUsername, String sshPassword, String task, StringBuilder logs)
			throws SKCException;

	void cronJobTask(String[] nodesList, String sshUsername, String sshPassword, String task, StringBuilder logs)
			throws SKCException;

	Boolean healthCheck(String event, DashboardDetailsDTO payload) throws SKCException;

	String refresh(List<String> ipList, String clusterName);

}
