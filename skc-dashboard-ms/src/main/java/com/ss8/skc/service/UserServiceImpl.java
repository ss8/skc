package com.ss8.skc.service;

import java.util.List;
import java.util.Optional;

import com.ss8.skc.dao.UserCredentials;
import com.ss8.skc.dao.UserDetails;
import com.ss8.skc.dto.ChangePasswordDTO;
import com.ss8.skc.dto.UserDetailsDTO;
import com.ss8.skc.exception.SKCException;
import com.ss8.skc.repository.SKCUsersRepository;
import com.ss8.skc.repository.UserRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SKCUsersRepository userCredRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public UserDetailsDTO addUser(UserDetailsDTO userDetailsDTO) throws SKCException {
        // TODO Auto-generated method stub

        Optional<UserDetails> user = userRepository.findByUsernameAndDashboardId(userDetailsDTO.getUsername(), userDetailsDTO.getDashboardId());

        if (!user.isPresent()) {
            UserDetails newUser = UserDetailsDTO.createEntity(userDetailsDTO);
            newUser = userRepository.saveAndFlush(newUser);
            return UserDetailsDTO.valueOf(newUser);
        } else {
            throw new SKCException(HttpStatus.CONFLICT,"User for Dashboard already Exists");
        }
    }

    @Override
    public String fetchToken(UserDetailsDTO userDetailsDTO) throws SKCException {
        // TODO Auto-generated method stub

        Optional<UserDetails> user = userRepository.findByUsernameAndDashboardId(userDetailsDTO.getUsername(),
                userDetailsDTO.getDashboardId());

        if (user.isPresent()) {
            return user.get().getToken();
        } else {
            throw new SKCException(HttpStatus.NOT_FOUND,"User not Found");
        }
    }

    @Override
    public Boolean deleteUserByUserId(Integer id) throws SKCException {

        Optional<UserDetails> user = userRepository.findById(id);

        if (user.isPresent()) {
            userRepository.deleteById(id);
            return true;
        } else {
            throw new SKCException(HttpStatus.NOT_FOUND,"User not found");
        }
    }

    @Override
    public Boolean deleteUsersByDashboardId(Integer id) throws SKCException {
        // TODO Auto-generated method stub
        List<UserDetails> users = userRepository.findByDashboardId(id);

        if (!users.isEmpty()) {
            userRepository.deleteAll(users);
            return true;
        } else {
            throw new SKCException(HttpStatus.NOT_FOUND,"Users not found");
        }
    }

    @Override
    public Boolean changePassword(ChangePasswordDTO changePasswordDTO) throws SKCException {
        // TODO Auto-generated method stub

        logger.trace("Entered changePassword() of {}", this.getClass());
        Optional<UserCredentials> user = userCredRepository.findByUsername(changePasswordDTO.getUsername());

        if (user.isPresent()) {
            UserCredentials userDetails = user.get();

            if (passwordEncoder.matches(changePasswordDTO.getOldPassword(), userDetails.getPassword())) {

                userDetails.setPassword(passwordEncoder.encode(changePasswordDTO.getNewPassword()));
                userCredRepository.saveAndFlush(userDetails);

                logger.debug("Password Change Successful for User - " + userDetails.getUsername());
                logger.trace("Exiting changePassword() of {}", this.getClass());
                return true;
            } else {
                logger.debug("Old Password Does not Match for User - "+userDetails.getUsername());
                logger.trace("Exiting changePassword() of {}", this.getClass());
                throw new SKCException(HttpStatus.NOT_ACCEPTABLE,"password.incorrect");
            }
        } else {
            logger.debug("User - "+changePasswordDTO.getUsername()+" not Found");
            logger.trace("Exiting changePassword() of {}", this.getClass());
            throw new SKCException(HttpStatus.NOT_FOUND,"user.not.found");
        }
    }

}
