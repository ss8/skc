package com.ss8.skc.jwt;

import com.google.common.net.HttpHeaders;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
public class JwtConfig {

    @Value("${jwt.secretKey}")
    private String secretKey;

    @Value("${jwt.tokenPrefix}")
    private String tokenPrefix;

    @Value("${jwt.tokenExpirationAfterDays}")
    private Integer tokenExpirationAfterDays;

    

    public JwtConfig() {
        // this.secretKey="securesecuresecuresecuresecuresecuresecure";
        // this.tokenPrefix="Bearer ";
        // this.tokenExpirationAfterDays=14;
    }

    public String getAuthorizationHeader(){
        return HttpHeaders.AUTHORIZATION;
    }

    public String getSecretKey() {
        
        System.out.println("=====================================================");
        System.out.println("Getting secret key = "+secretKey);
        System.out.println("=====================================================");
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        System.out.println("=====================================================");
        System.out.println("Setting secret key to ="+secretKey);
        System.out.println("=====================================================");
        this.secretKey = secretKey;
    }

    public String getTokenPrefix() {
        return tokenPrefix.trim()+" ";
    }

    public void setTokenPrefix(String tokenPrefix) {
        this.tokenPrefix = tokenPrefix;
    }

    public Integer getTokenExpirationAfterDays() {
        return tokenExpirationAfterDays;
    }

    public void setTokenExpirationAfterDays(Integer tokenExpirationAfterDays) {
        this.tokenExpirationAfterDays = tokenExpirationAfterDays;
    }

    
}
