package com.ss8.skc.utils;

public enum WebhookEventConstants {

	DASHBOARD_MODIFIED("DASHBOARD MODIFIED"),
	DASHBOARD_STATUS("DASHBOARD STATUS"),
	DASHBOARD_CREATED("DASHBOARD CREATED"),
	DASHBOARD_DELETED("DASHBOARD DELETED");

	private final String value;

	private WebhookEventConstants(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return this.value;
	}

}
