package com.ss8.skc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.ss8.skc.dao.DashboardDetails;

@Repository
public interface DashboardRepository extends JpaRepository<DashboardDetails, Integer>, JpaSpecificationExecutor<DashboardDetails> {
	
	List<DashboardDetails> findByName(String name);
	List<DashboardDetails> findByNameContaining(String name);

	List<DashboardDetails> findByUrl(String url);
	DashboardDetails findOneByName(String name);


}
