package com.ss8.skc.repository;

import java.util.Optional;

import com.ss8.skc.auth.SKCUser;
import com.ss8.skc.dao.UserCredentials;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SKCUsersRepository extends JpaRepository<UserCredentials,Integer>{
    Optional<UserCredentials> findByUsername(String username);
    
}
