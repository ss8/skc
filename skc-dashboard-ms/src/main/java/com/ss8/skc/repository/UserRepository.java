package com.ss8.skc.repository;

import java.util.List;
import java.util.Optional;

import com.ss8.skc.dao.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDetails,Integer>{
    Optional<UserDetails> findByUsername(String username);
    Optional<UserDetails> findByUsernameAndDashboardId(String username, Integer dashboardId);
    List<UserDetails> findByDashboardId(Integer dashboardId);

}
