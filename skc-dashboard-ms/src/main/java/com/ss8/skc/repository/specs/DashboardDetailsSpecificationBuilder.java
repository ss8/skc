package com.ss8.skc.repository.specs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import com.ss8.skc.dao.DashboardDetails;
import com.ss8.skc.dto.SearchCriteria;

public class DashboardDetailsSpecificationBuilder {

    private final List<SearchCriteria> params;

    public DashboardDetailsSpecificationBuilder() {
        params = new ArrayList<SearchCriteria>();
    }

    public DashboardDetailsSpecificationBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    public Specification<DashboardDetails> build() {
        if (params.isEmpty()) {
            return null;
        }

        List<Specification<DashboardDetails>> specs = params.stream().map(DashboardDetailsSpecification::new)
                .collect(Collectors.toList());

        Specification<DashboardDetails> result = specs.get(0);

        for (int i = 1; i < params.size(); i++) {
            // result = params.get(i).isOrPredicate()
            // ? Specification.where(result).or(specs.get(i)
            // : Specification.where(result).and(specs.get(i));
        }
        return result;
    }

}
