package com.ss8.skc.auth;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ss8.skc.repository.SKCUsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("test")
public class SKCUserDaoService implements SKCUserDao {

    // private final PasswordEncoder passwordEncoder;

    @Autowired
    SKCUsersRepository usersRepository;

    // @Autowired
    // public SKCUserDaoService(PasswordEncoder passwordEncoder) {
    //     this.passwordEncoder = passwordEncoder;
    // }

    @Override
    public Optional<SKCUser> selectSKCUserByUsername(String username) {

        return getSKCUsers()
        .stream()
        .filter(skcUser -> username.equals(skcUser.getUsername()))
        .findFirst();
    }

    private List<SKCUser> getSKCUsers() {
        List<SKCUser> skcUsers = usersRepository.findAll()
        .stream()
        .map(user -> 
            new SKCUser(user.getUsername(), 
            user.getPassword(), 
            user.getUserRole().getgrantedAuthorities(), 
            user.isAccountNonExpired(), 
            user.isAccountNonLocked(), 
            user.isCredentialsNonExpired(), 
            user.isEnabled())
        ).collect(Collectors.toList());

        for(SKCUser user : skcUsers){
            System.out.println(user.getUsername()+" "+user.getPassword()+" "+user.getAuthorities().toString()+" ");
        }
        return skcUsers;
    }

}
