package com.ss8.skc.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SKCUserService implements UserDetailsService {

    private final SKCUserDao skcUserDao;

    @Autowired
    public SKCUserService(@Qualifier("test") SKCUserDao skcUserDao) {
        this.skcUserDao = skcUserDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return skcUserDao.selectSKCUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username " + username + " not found"));
    }

}
