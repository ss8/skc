package com.ss8.skc.auth;

import java.util.Optional;

public interface SKCUserDao {

    Optional<SKCUser> selectSKCUserByUsername(String username);
    
}
