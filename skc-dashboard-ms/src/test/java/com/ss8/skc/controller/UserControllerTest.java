package com.ss8.skc.controller;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ss8.skc.dto.ChangePasswordDTO;
import com.ss8.skc.dto.UserDetailsDTO;
import com.ss8.skc.exception.SKCException;
import com.ss8.skc.service.UserServiceImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureMockMvc
public class UserControllerTest {

	final static String BASE_URL = "/user";
	final static int USER_DELETE_ID = 1;
	final static int USER_DELETE_DASHBOARD_ID = 1;

	final static String REST_API_USER_ADD = BASE_URL + "/add";
	final static String REST_API_USER_TOKEN = BASE_URL + "/token";
	final static String REST_API_USER_DELETE = BASE_URL + "/delete/" + USER_DELETE_ID;
	final static String REST_API_USER_DELETE_DASHBOARD = BASE_URL + "/delete/dashboard/" + USER_DELETE_DASHBOARD_ID;
	final static String REST_API_USER_CHANGEPASSWORD = BASE_URL + "/changePassword";

	private MockMvc mockMvc;
	private ObjectMapper mapper;

	@InjectMocks
	UserController handle;

	@Mock
	UserServiceImpl mockUserServiceImpl;

	@Captor
	ArgumentCaptor<Integer> integerArgumentCaptor;

	@Captor
	ArgumentCaptor<UserDetailsDTO> userDetailsDTOArgumentCaptor;

	@Captor
	ArgumentCaptor<ChangePasswordDTO> changePasswordArgumentCaptor;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(handle).build();
		mapper = new ObjectMapper();
	}

	@Test
	public void addUser_SuccessTest() throws Exception {
		UserDetailsDTO dummyUserDetailsDTO = prepareDummyUserDetailsDTO(1, "ABC", 1, "dummyusertoken");

		when(mockUserServiceImpl.addUser(userDetailsDTOArgumentCaptor.capture())).thenReturn(dummyUserDetailsDTO);
		
		MvcResult mvcResult = this.mockMvc.perform(post(REST_API_USER_ADD)
													.content(mapper.writeValueAsString(dummyUserDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
												   )
											.andExpect(status().isOk()).andReturn();

		// Cannot verify as object id is changed		
		// verify(mockUserServiceImpl, times(1)).addUser(dummyUserDetailsDTO);

		assertTrue(new ReflectionEquals(dummyUserDetailsDTO).matches(userDetailsDTOArgumentCaptor.getValue()));
		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(mapper.writeValueAsString(dummyUserDetailsDTO), mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void addUser_NullInput_BadRequest_FailureTes() throws Exception {
		UserDetailsDTO nullUserDetailsDTO = null;

		MvcResult mvcResult = this.mockMvc.perform(post(REST_API_USER_ADD).content(mapper.writeValueAsString(nullUserDetailsDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();

		assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
	}

	@Test(expected = NestedServletException.class)
	public void addUser_Errors_FailureTest() throws Exception {
		UserDetailsDTO invalidUserDetailsDTO = prepareDummyUserDetailsDTO(1, "username length should not be greater than 20", null, "");

		this.mockMvc.perform(post(REST_API_USER_ADD).content(mapper.writeValueAsString(invalidUserDetailsDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotAcceptable()).andReturn();
	}

	@Test(expected = NestedServletException.class)
	public void addUser_UserExists_FailureTest() throws Exception{
		UserDetailsDTO dummyUserDetailsDTO = prepareDummyUserDetailsDTO(1, "ABC", 1, "dummyusertoken");

		when(mockUserServiceImpl.addUser(userDetailsDTOArgumentCaptor.capture())).thenThrow(new SKCException(HttpStatus.CONFLICT,"User for Dashboard already Exists"));

		this.mockMvc.perform(post(REST_API_USER_ADD).content(mapper.writeValueAsString(dummyUserDetailsDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isConflict()).andReturn();
	}

	@Test
	public void fetchToken_SuccessTest() throws Exception {
		UserDetailsDTO dummyUserDetailsDTO = prepareDummyUserDetailsDTO(null, "ABC", 1, "dummy");
		final String userToken = "this is a dummy token";

		when(mockUserServiceImpl.fetchToken(userDetailsDTOArgumentCaptor.capture())).thenReturn(userToken);

		MvcResult mvcResult = this.mockMvc.perform(get(REST_API_USER_TOKEN).content(mapper.writeValueAsString(dummyUserDetailsDTO)).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		// verify(mockUserServiceImpl, times(1)).fetchToken(dummyUserDetailsDTO);

		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(userToken, mvcResult.getResponse().getContentAsString());
	}

	@Test(expected = NestedServletException.class)
	public void fetchToken_Errors_FailureTest() throws Exception {
		UserDetailsDTO invalidUserDetailsDTO = prepareDummyUserDetailsDTO(1, "username length should not be greater than 20", null, "");

		this.mockMvc.perform(get(REST_API_USER_TOKEN).content(mapper.writeValueAsString(invalidUserDetailsDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotAcceptable()).andReturn();
	}

	@Test
	public void fetchToken_NullInput_BadRequest_FailureTest() throws Exception {
		UserDetailsDTO nullUserDetailsDTO = null;

		MvcResult mvcResult = this.mockMvc.perform(get(REST_API_USER_TOKEN).content(mapper.writeValueAsString(nullUserDetailsDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();

		assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
	}

	@Test(expected = NestedServletException.class)
	public void fetchToken_UserNotFound_FailureTest() throws Exception {
		UserDetailsDTO dummyUserDetailsDTO = prepareDummyUserDetailsDTO(null, "ABC", 1, "dummy");

		when(mockUserServiceImpl.fetchToken(userDetailsDTOArgumentCaptor.capture())).thenThrow(new SKCException(HttpStatus.NOT_FOUND,"User not Found"));

		this.mockMvc.perform(get(REST_API_USER_TOKEN).content(mapper.writeValueAsString(dummyUserDetailsDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotFound()).andReturn();
	}

	@Test
	public void deleteUserByUserId_SuccessTest() throws Exception {
		when(mockUserServiceImpl.deleteUserByUserId(integerArgumentCaptor.capture())).thenReturn(true);

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_USER_DELETE)).andExpect(status().isOk()).andReturn();

		verify(mockUserServiceImpl, times(1)).deleteUserByUserId(USER_DELETE_ID);

		assertEquals(USER_DELETE_ID, integerArgumentCaptor.getValue());
		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(true, Boolean.parseBoolean(mvcResult.getResponse().getContentAsString()));
	}

	@Test(expected = NestedServletException.class)
	public void deleteUserByUserId_UserNotFound_FailureTest() throws Exception {
		when(mockUserServiceImpl.deleteUserByUserId(integerArgumentCaptor.capture())).thenThrow(new SKCException(HttpStatus.NOT_FOUND,"User not found"));

		this.mockMvc.perform(delete(REST_API_USER_DELETE)).andExpect(status().isNotFound()).andReturn();
	}

	@Test
	public void deleteUsersByDashboardId_SuccessTest() throws Exception {
		when(mockUserServiceImpl.deleteUsersByDashboardId(integerArgumentCaptor.capture())).thenReturn(true);

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_USER_DELETE_DASHBOARD)).andExpect(status().isOk()).andReturn();

		verify(mockUserServiceImpl, times(1)).deleteUsersByDashboardId(USER_DELETE_DASHBOARD_ID);

		assertEquals(USER_DELETE_DASHBOARD_ID, integerArgumentCaptor.getValue());
		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(true, Boolean.parseBoolean(mvcResult.getResponse().getContentAsString()));
	}

	@Test(expected = NestedServletException.class)
	public void deleteUsersByDashboardId_UsersNotFound_FailureTest() throws Exception{
		when(mockUserServiceImpl.deleteUsersByDashboardId(integerArgumentCaptor.capture())).thenThrow(new SKCException(HttpStatus.NOT_FOUND,"Users not found"));

		this.mockMvc.perform(delete(REST_API_USER_DELETE_DASHBOARD)).andExpect(status().isNotFound()).andReturn();
	}

	@Test
	public void changePassword_SuccessTest() throws Exception {
		ChangePasswordDTO dummyChangePasswordDTO = prepareDummyChangePasswordDTO("ABC", "ABC123", "PQR456");

		when(mockUserServiceImpl.changePassword(changePasswordArgumentCaptor.capture())).thenReturn(true);

		MvcResult mvcResult = this.mockMvc.perform(post(REST_API_USER_CHANGEPASSWORD).content(mapper.writeValueAsString(dummyChangePasswordDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andReturn();

		// verify(mockUserServiceImpl, times(1)).changePassword(dummyChangePasswordDTO);

		assertTrue(new ReflectionEquals(dummyChangePasswordDTO).matches(changePasswordArgumentCaptor.getValue()));
		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(true, Boolean.parseBoolean(mvcResult.getResponse().getContentAsString()));
	}

	@Test
	public void changePassword_NullInput_BadRequest_FailureTest() throws Exception {
		ChangePasswordDTO nullChangePasswordDTO = null;

		MvcResult mvcResult = this.mockMvc.perform(post(REST_API_USER_CHANGEPASSWORD).content(mapper.writeValueAsString(nullChangePasswordDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest()).andReturn();

		assertEquals(HttpStatus.BAD_REQUEST.value(), mvcResult.getResponse().getStatus());
	}

	@Test(expected = NestedServletException.class)
	public void changePassword_Errors_FailureTest() throws Exception {
		ChangePasswordDTO errorChangePasswordDTO = prepareDummyChangePasswordDTO("This is a dummy username with length above 20", "ab", "ab");

		this.mockMvc.perform(post(REST_API_USER_CHANGEPASSWORD).content(mapper.writeValueAsString(errorChangePasswordDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotAcceptable()).andReturn();
	}

	@Test(expected = NestedServletException.class)
	public void changePassword_PasswordIncorrect_FailureTest() throws Exception {
		ChangePasswordDTO incorrectChangePasswordDTO = prepareDummyChangePasswordDTO("ABC", "ABC123", "ABC123");

		when(mockUserServiceImpl.changePassword(changePasswordArgumentCaptor.capture())).thenThrow(new SKCException(HttpStatus.NOT_ACCEPTABLE,"password.incorrect"));

		this.mockMvc.perform(post(REST_API_USER_CHANGEPASSWORD).content(mapper.writeValueAsString(incorrectChangePasswordDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotAcceptable()).andReturn();
	}

	@Test(expected = NestedServletException.class)
	public void changePassword_UserNotFound_FailureTest() throws Exception {
		ChangePasswordDTO incorrectChangePasswordDTO = prepareDummyChangePasswordDTO("ABC", "ABC123", "ABC123");

		when(mockUserServiceImpl.changePassword(changePasswordArgumentCaptor.capture())).thenThrow(new SKCException(HttpStatus.NOT_FOUND,"user.not.found"));

		this.mockMvc.perform(post(REST_API_USER_CHANGEPASSWORD).content(mapper.writeValueAsString(incorrectChangePasswordDTO)).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isNotAcceptable()).andReturn();
	}

	private UserDetailsDTO prepareDummyUserDetailsDTO(Integer id, String username, Integer dashboardId, String token) {
		UserDetailsDTO userDetailsDTO = new UserDetailsDTO();

		userDetailsDTO.setId(id);
		userDetailsDTO.setUsername(username);
		userDetailsDTO.setDashboardId(dashboardId);
		userDetailsDTO.setToken(token);

		return userDetailsDTO;
	}
	
	public ChangePasswordDTO prepareDummyChangePasswordDTO(String username, String oldPassword, String newPassword) {
		ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();

		changePasswordDTO.setUsername(username);
		changePasswordDTO.setOldPassword(oldPassword);
		changePasswordDTO.setNewPassword(newPassword);

		return changePasswordDTO;
	}

}
