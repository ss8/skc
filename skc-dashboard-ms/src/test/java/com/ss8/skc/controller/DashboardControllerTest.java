package com.ss8.skc.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.ss8.skc.controller.DashboardEvents.BASHRC_DELETE;
import static com.ss8.skc.controller.DashboardEvents.END;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.ss8.skc.dao.ClusterType;
import com.ss8.skc.dao.DashboardStatus;
import com.ss8.skc.dto.DashboardDetailsDTO;
import com.ss8.skc.dto.ResponseDTO;
import com.ss8.skc.dto.SshDetailsDTO;
import com.ss8.skc.service.DashboardServiceImpl;
import com.ss8.skc.service.UserServiceImpl;
import com.ss8.skc.utils.WebhookEventConstants;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.Errors;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ss8.skc.exception.SKCException;
import org.springframework.web.util.NestedServletException;

@RunWith(MockitoJUnitRunner.class)
@AutoConfigureMockMvc
public class DashboardControllerTest {

	final static String SEARCH_TERM = "te";
	final static int DELETE_ID = 1;
	final static String WEBHOOK_EVENT_HEADER = "X-Event";

	final static String REST_API_LIST_DASHBOARDS = "/dashboard/list";
	final static String REST_API_SEARCH_DASHBOARDS = "/dashboard/list?search=" + SEARCH_TERM;
	final static String REST_API_DELETE_DASHBOARD = "/dashboard/delete/" + DELETE_ID;
	final static String REST_API_HEALTH_DASHBOARD = "/dashboard/health";

	private MockMvc mockMvc;

	@InjectMocks
	private DashboardController handle;

	@Mock
	Errors error;

	@Mock
	private JSch jSch;

	@Mock
	Session mockSession;


	@Mock
	private DashboardServiceImpl mockDashboardServiceImpl;

	@Mock
	private UserServiceImpl mockUserServiceImpl;

	@Captor
	private ArgumentCaptor<String> stringArgumentCaptor;

	@Captor
	private ArgumentCaptor<Integer> integerArgumentCaptor;

	@Captor
	private ArgumentCaptor<DashboardDetailsDTO> dashboardDetailsDTOCaptor;

	@Mock
	private DashboardDetailsDTO mockDashboardDetailsDTO;

	@Mock
	private List<DashboardDetailsDTO> mockDashboardDetailsDTOs;

	@Captor
	private ArgumentCaptor<Integer> intArgumentCaptor;

	private ObjectMapper mapper;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.standaloneSetup(handle).build();
		mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		mockDashboardDetailsDTOs = prepareDummyData();

	}

	//--------------------Fetch Cluster List------------//

	@Test
	public void fetchClusterListSuccessTest()throws Exception{
		ResponseDTO expectedResponse = new ResponseDTO();
		List<String> clusterList = new ArrayList<String>();

		createClusterListDummy();

		clusterList.add("Cluster1:172.0.17.10");
		clusterList.add("Cluster2:189.0.57.10");
		clusterList.add("test-cluster:100.0.19.41");
		clusterList.add("test-cluster-cant-connect:102.23.19.21");

		expectedResponse.successClusterDetailsDTO(clusterList);
		expectedResponse.setLogs("Cluster1:172.0.17.10\nCluster2:189.0.57.10\ntest-cluster:100.0.19.41\ntest-cluster-cant-connect:102.23.19.21\n");

		// when(skiClusterListFilePath.toString()).thenReturn("cluster-list.txt");

		ReflectionTestUtils.setField(handle, "skiClusterListFilePath", "cluster-list-test.txt");


		MvcResult mvcResult= this.mockMvc.perform(get("/dashboard/clusterList")).andExpect(status().isOk()).andReturn();

		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(mapper.writeValueAsString(expectedResponse), mvcResult.getResponse().getContentAsString());
		deleteClusterListDummy();
	}

	public void createClusterListDummy() throws IOException{
		File clusterList = new File("cluster-list-test.txt");
		FileWriter fileWriter = new FileWriter(clusterList);

		fileWriter.write("Cluster1:172.0.17.10");
		fileWriter.write(System.getProperty( "line.separator" ));
		fileWriter.write("Cluster2:189.0.57.10");
		fileWriter.write(System.getProperty( "line.separator" ));
		fileWriter.write("test-cluster:100.0.19.41");
		fileWriter.write(System.getProperty( "line.separator" ));
		fileWriter.write("test-cluster-cant-connect:102.23.19.21");

		fileWriter.close();
	}

	public void deleteClusterListDummy(){
		File clusterList = new File("cluster-list-test.txt");
		clusterList.delete();
	}

	@Test
	public void fetchClusterListFailureTest()throws Exception{		

		ReflectionTestUtils.setField(handle, "skiClusterListFilePath", "cluster-list-test-empty.txt");


		MvcResult mvcResult= this.mockMvc.perform(get("/dashboard/clusterList")).andExpect(status().isNotFound()).andReturn();

		assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
		
	}

	@Test
	public void fetchClusterListFileNotFound()throws Exception{

		ReflectionTestUtils.setField(handle, "skiClusterListFilePath", "cluster-list-file_not_found.txt");

		MvcResult mvcResult= this.mockMvc.perform(get("/dashboard/clusterList")).andExpect(status().isNotFound()).andReturn();

		assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
		
	}

	// -------------------ADD Dashboard-----------------//
	@Test
	public void addDashboardSuccessTest() throws Exception {

		when(mockDashboardServiceImpl.addDashboard(dashboardDetailsDTOCaptor.capture()))
				.thenReturn(createMockDashboardDetailsDTO());

		ResponseEntity<ResponseDTO> response = handle.addDashboard(createMockDashboardDetailsDTO(), error);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody() instanceof ResponseDTO);
	}

	@Test(expected = SKCException.class)
	public void addDashboardNullInputTest() throws Exception {
		handle.addDashboard(null, error);
	}

	@Test(expected = NestedServletException.class)
	public void addDashboardInvalidInputTest() throws JsonProcessingException, Exception {

		DashboardDetailsDTO dashboardDetailsDTO = createMockDashboardDetailsDTO();
		dashboardDetailsDTO.setName("ab* %$@");

		String uri = "/dashboard/add";

		ObjectMapper mapper = new ObjectMapper();

		this.mockMvc.perform(post(uri).content(mapper.writeValueAsString(dashboardDetailsDTO))
				.contentType(MediaType.APPLICATION_JSON));

	}

	// -------------------Update Dashboard-----------------//
	@Test
	public void updateDashboardSuccessTest() throws Exception {

		ResponseEntity<ResponseDTO> response = handle.updateDashboard(1, createMockDashboardDetailsDTO(), error);

		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertTrue(response.getBody() instanceof ResponseDTO);
	}

	@Test(expected = SKCException.class)
	public void updateDashboardNullInputTest() throws Exception {

		handle.addDashboard(null, error);
	}

	@Test(expected = NestedServletException.class)
	public void updateDashboardInvalidInputTest() throws JsonProcessingException, Exception {

		DashboardDetailsDTO dashboardDetailsDTO = createMockDashboardDetailsDTO();
		dashboardDetailsDTO.setName("ab");

		String uri = "/dashboard/update/1";

		ObjectMapper mapper = new ObjectMapper();

		this.mockMvc.perform(put(uri).content(mapper.writeValueAsString(dashboardDetailsDTO))
				.contentType(MediaType.APPLICATION_JSON));

	}

	public DashboardDetailsDTO createMockDashboardDetailsDTO() {
		DashboardDetailsDTO mockdash = new DashboardDetailsDTO();
		mockdash.setName("Test");
		mockdash.setUrl("https://123.32.322.32:8002/");
		mockdash.setClusterType(ClusterType.GCP);
		mockdash.setDashboardStatus(DashboardStatus.DASHBOARD_ACTIVE);

		return mockdash;
	}

	@Test
	public void listDashboards_ALL_SuccessTest() throws Exception {

		ResponseDTO mockResponseDTO = prepareDummyListResponseDTO(mockDashboardDetailsDTOs);

		when(mockDashboardServiceImpl.listDashboards(stringArgumentCaptor.capture()))
				.thenReturn(mockDashboardDetailsDTOs);

		MvcResult mvcResult = this.mockMvc.perform(get(REST_API_LIST_DASHBOARDS).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		verify(mockDashboardServiceImpl, times(1)).listDashboards(Mockito.anyString());

		assertEquals("", stringArgumentCaptor.getValue());
		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());

		assertEquals(mapper.writeValueAsString(mockResponseDTO), mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void listDashboards_SEARCH_SuccessTest() throws Exception {

		List<DashboardDetailsDTO> mockResponseDashboardDetailsDTOs = prepareSearchResponseData(SEARCH_TERM);
		ResponseDTO mockResponseDTO = prepareDummyListResponseDTO(mockResponseDashboardDetailsDTOs);

		when(mockDashboardServiceImpl.listDashboards(stringArgumentCaptor.capture()))
				.thenReturn(mockResponseDashboardDetailsDTOs);

		MvcResult mvcResult = this.mockMvc.perform(get(REST_API_SEARCH_DASHBOARDS).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andReturn();

		verify(mockDashboardServiceImpl, times(1)).listDashboards(SEARCH_TERM);

		assertEquals(SEARCH_TERM, stringArgumentCaptor.getValue());
		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());

		assertEquals(mapper.writeValueAsString(mockResponseDTO), mvcResult.getResponse().getContentAsString());

	}

	@Test
	public void healthCheck_SuccessTest() throws Exception {
		ResponseDTO mockResponseDTO = prepareDummyHealthResponseDTO(true);
		DashboardDetailsDTO mockDashboardDetailsDTO = new DashboardDetailsDTO();
		mockDashboardDetailsDTO.setId(1);
		mockDashboardDetailsDTO.setDashboardStatus(DashboardStatus.DASHBOARD_ACTIVE);

		when(mockDashboardServiceImpl.healthCheck(stringArgumentCaptor.capture(), dashboardDetailsDTOCaptor.capture())).thenReturn(true);

		MvcResult mvcResult = this.mockMvc.perform(
								post(REST_API_HEALTH_DASHBOARD)
								.content(mapper.writeValueAsString(mockDashboardDetailsDTO))
								.header(WEBHOOK_EVENT_HEADER, WebhookEventConstants.DASHBOARD_STATUS.toString())
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON)
								)
								.andExpect(status().isOk()).andReturn();
		
		// Cannot verify as object id is changed
		// verify(mockDashboardServiceImpl, times(1)).healthCheck(WebhookEventConstants.DASHBOARD_STATUS.toString(), mapper.readValue(content, DashboardDetailsDTO.class));

		assertEquals(WebhookEventConstants.DASHBOARD_STATUS.toString(), stringArgumentCaptor.getValue());
		assertTrue(new ReflectionEquals(mockDashboardDetailsDTO).matches(dashboardDetailsDTOCaptor.getValue()));
		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(mapper.writeValueAsString(mockResponseDTO), mvcResult.getResponse().getContentAsString());
	}

	@Test(expected = NestedServletException.class)
	public void healthCheck_Errors_FailureTest() throws Exception {
		DashboardDetailsDTO errorDashboardDetailsDTO = new DashboardDetailsDTO();

		errorDashboardDetailsDTO.setName("This name is incorrect as it is greater than 20");

		this.mockMvc.perform(post(REST_API_HEALTH_DASHBOARD)
								.header(WEBHOOK_EVENT_HEADER, WebhookEventConstants.DASHBOARD_STATUS.toString())
								.content(mapper.writeValueAsString(errorDashboardDetailsDTO))
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
							.andExpect(status().isNotAcceptable()).andReturn();
	}

	@Test(expected = NestedServletException.class)
	public void healthCheck_EmptyEvent_FailureTest() throws Exception {
		DashboardDetailsDTO emptyDashboardDetailsDTO = new DashboardDetailsDTO();

		this.mockMvc.perform(post(REST_API_HEALTH_DASHBOARD)
									.content(mapper.writeValueAsString(emptyDashboardDetailsDTO))
									.contentType(MediaType.APPLICATION_JSON)
									.accept(MediaType.APPLICATION_JSON))
								.andExpect(status().isNotAcceptable()).andReturn();
	}


	// ------------------------ deleteDashboard ------------------------------ //

	@Test
	public void deleteDashboard_SuccessTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {"a", "b"};

		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();
		doNothing().when(mockSession).disconnect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);

		when(mockDashboardServiceImpl.controllerNodesReachable(any(), any())).thenReturn(true);

		when(mockDashboardServiceImpl.triggerDeleteDashboardScript(any(), anyString(),  anyString(), any())).thenReturn(true);

		when(mockUserServiceImpl.deleteUsersByDashboardId(anyInt())).thenReturn(true);

		when(mockDashboardServiceImpl.deleteDashboard(anyInt())).thenReturn(DELETE_ID);

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_DELETE_DASHBOARD)
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON))
											.andExpect(status().isOk()).andReturn();

		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(DELETE_ID, mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getDeleteResponseDTO().getId());
	}

	@Test
	public void deleteDashboard_ControllerNodesUnreachable_SuccessTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {"a", "b"};

		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();
		doNothing().when(mockSession).disconnect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);

		when(mockDashboardServiceImpl.controllerNodesReachable(any(), any())).thenReturn(false);

		when(mockDashboardServiceImpl.triggerDeleteDashboardScript(any(), anyString(),  anyString(), any())).thenReturn(true);

		when(mockUserServiceImpl.deleteUsersByDashboardId(anyInt())).thenReturn(true);

		when(mockDashboardServiceImpl.deleteDashboard(anyInt())).thenReturn(DELETE_ID);

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_DELETE_DASHBOARD)
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON))
											.andExpect(status().isOk()).andReturn();

		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(DELETE_ID, mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getDeleteResponseDTO().getId());
	}

	@Test
	public void deleteDashboard_CleanUpScriptTriggered_SuccessTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {"a", "b"};

		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();
		doNothing().when(mockSession).disconnect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);

		when(mockDashboardServiceImpl.controllerNodesReachable(any(), any())).thenReturn(false);

		when(mockDashboardServiceImpl.triggerDeleteDashboardScript(any(), anyString(),  anyString(), any())).thenReturn(false);

		when(mockDashboardServiceImpl.triggerCleanUpScript(any(), anyString(), anyString(), any())).thenReturn(true);

		when(mockUserServiceImpl.deleteUsersByDashboardId(anyInt())).thenReturn(true);

		when(mockDashboardServiceImpl.deleteDashboard(anyInt())).thenReturn(DELETE_ID);

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_DELETE_DASHBOARD)
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON))
											.andExpect(status().isOk()).andReturn();

		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(DELETE_ID, mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getDeleteResponseDTO().getId());
	}

	@Test
	public void deleteDashboard_SKCException_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {""};

		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_DELETE_DASHBOARD)
								.content(mapper.writeValueAsString(dummySshDetailsDTO))
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isPreconditionFailed()).andReturn();
		
		System.out.println(mvcResult.getResponse().getContentAsString());

		assertEquals(HttpStatus.PRECONDITION_FAILED.value(), mvcResult.getResponse().getStatus());
		assertEquals("No Controller Nodes found. Cannot proceed", mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getError());
	}

	@Test
	public void deleteDashboard_JSchException_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");

		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenThrow(new JSchException("Auth fail"));

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_DELETE_DASHBOARD)
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
										.andExpect(status().isUnauthorized()).andReturn();
		
		assertEquals(HttpStatus.UNAUTHORIZED.value(), mvcResult.getResponse().getStatus());
		assertEquals("Authentication failed!! Please provide Correct credentials", mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getError());
	}

	@Test
	public void deleteDashboard_Exception_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");

		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenThrow(new IOException());

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_DELETE_DASHBOARD)
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
										.andExpect(status().isInternalServerError()).andReturn();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), mvcResult.getResponse().getStatus());
		assertNotNull(mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void deleteDashboard_ClusterUnreachable_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("12.12.12.12", "xyz123", "dummyUser", "dummyPassword");

		MvcResult mvcResult = this.mockMvc.perform(delete(REST_API_DELETE_DASHBOARD)
								.content(mapper.writeValueAsString(dummySshDetailsDTO))
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isNotFound()).andReturn();
		
		// System.out.println(mvcResult.getResponse().getContentAsString());

		assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
		assertEquals("Cluster unreachable", mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getError());
	}

	// ------------------------------ Install Dashboard ---------------------------------- //

	@Test
	public void installDashboard_SuccessTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {"a"};

		when(mockDashboardServiceImpl.pingHost(anyString(), anyInt(), anyInt())).thenReturn(true);
		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();
		
		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);
		when(mockDashboardServiceImpl.controllerNodesReachable(any(), any())).thenReturn(true);
		when(mockDashboardServiceImpl.isStorageAvailable(any(),anyInt(),anyString(), anyString(), any())).thenReturn(true);

		doNothing().when(mockDashboardServiceImpl).transferAndExtractResources(any(), anyString(), anyString(), any());
		doNothing().when(mockDashboardServiceImpl).dockerImagesTask(any(), anyString(), anyString(), any(), any());
		
		when(mockDashboardServiceImpl.triggerAddDashboardScript(any(), anyString(), anyString(), anyString(), any())).thenReturn(0);

		// doNothing().when(mockDashboardServiceImpl).postInstallScript(any(), anyString(), anyString(), any());

		MvcResult mvcResult = this.mockMvc.perform(post("/dashboard/install")
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
											.andExpect(status().isOk()).andReturn();

		assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
		assertEquals(dummySshDetailsDTO.getClusterIp(), mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getAddResponseDTO().getDashboardDetailsDTO().getUrl());
	}
	
	// TODO First add validations in SshDetailsDTO
	public void installDashboard_Errors_FailureTest() throws Exception {}

	@Test
	public void installDashboard_DashboardAlreadyExists_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {"a"};

		when(mockDashboardServiceImpl.pingHost(anyString(), anyInt(), anyInt())).thenReturn(true);
		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();
		
		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);
		when(mockDashboardServiceImpl.controllerNodesReachable(any(), any())).thenReturn(true);
		when(mockDashboardServiceImpl.isStorageAvailable(any(),anyInt(), anyString(), anyString(), any())).thenReturn(true);

		doNothing().when(mockDashboardServiceImpl).transferAndExtractResources(any(), anyString(), anyString(), any());
		doNothing().when(mockDashboardServiceImpl).dockerImagesTask(any(), anyString(), anyString(), any(), any());
		
		when(mockDashboardServiceImpl.triggerAddDashboardScript(any(), anyString(), anyString(), anyString(), any())).thenReturn(2);

		MvcResult mvcResult = this.mockMvc.perform(post("/dashboard/install")
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
											.andExpect(status().isPreconditionFailed()).andReturn();

		assertEquals(HttpStatus.PRECONDITION_FAILED.value(), mvcResult.getResponse().getStatus());
		assertEquals("Dashboard Already Exists", mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getError());
	}

	@Test
	public void installDashboard_ClusterUnreachable_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");

		when(mockDashboardServiceImpl.pingHost(anyString(), anyInt(), anyInt())).thenReturn(false);

		MvcResult mvcResult = this.mockMvc.perform(post("/dashboard/install")
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
											.andExpect(status().isNotFound()).andReturn();

		assertEquals(HttpStatus.NOT_FOUND.value(), mvcResult.getResponse().getStatus());
		assertEquals("Cluster unreachable", mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getError());
	}

	@Test
	public void installDashboard_SKCException_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {""};

		when(mockDashboardServiceImpl.pingHost(anyString(), anyInt(), anyInt())).thenReturn(true);
		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);

		MvcResult mvcResult = this.mockMvc.perform(post("/dashboard/install")
								.content(mapper.writeValueAsString(dummySshDetailsDTO))
								.contentType(MediaType.APPLICATION_JSON)
								.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isPreconditionFailed()).andReturn();
		
		System.out.println(mvcResult.getResponse().getContentAsString());

		assertEquals(HttpStatus.PRECONDITION_FAILED.value(), mvcResult.getResponse().getStatus());
		assertEquals("No Controller Nodes found. Cannot proceed", mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getError());
	}

	@Test
	public void installDashboard_JSchException_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");

		when(mockDashboardServiceImpl.pingHost(anyString(), anyInt(), anyInt())).thenReturn(true);
		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenThrow(new JSchException("Auth fail"));

		MvcResult mvcResult = this.mockMvc.perform(post("/dashboard/install")
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
										.andExpect(status().isUnauthorized()).andReturn();
		
		assertEquals(HttpStatus.UNAUTHORIZED.value(), mvcResult.getResponse().getStatus());
		assertEquals("Authentication failed!! Please provide Correct credentials", mapper.readValue(mvcResult.getResponse().getContentAsString(), ResponseDTO.class).getError());
	}

	@Test
	public void installDashboard_Exception_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");

		when(mockDashboardServiceImpl.pingHost(anyString(), anyInt(), anyInt())).thenReturn(true);
		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();

		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenThrow(new IOException());

		MvcResult mvcResult = this.mockMvc.perform(post("/dashboard/install")
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
										.andExpect(status().isInternalServerError()).andReturn();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), mvcResult.getResponse().getStatus());
		assertNotNull(mvcResult.getResponse().getContentAsString());
	}

	@Test
	public void installDashboard_NullException_FailureTest() throws Exception {
		SshDetailsDTO dummySshDetailsDTO = new SshDetailsDTO("localhost", "xyz123", "dummyUser", "dummyPassword");
		String[] mockClusterNodeList = {"a"};

		when(mockDashboardServiceImpl.pingHost(anyString(), anyInt(), anyInt())).thenReturn(true);
		when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(mockSession);
		doNothing().when(mockSession).setPassword(anyString());
        doNothing().when(mockSession).setConfig(anyString(), anyString());
        doNothing().when(mockSession).connect();
		
		when(mockDashboardServiceImpl.fetchClusterNodesList(any(), any())).thenReturn(mockClusterNodeList);
		when(mockDashboardServiceImpl.controllerNodesReachable(any(), any())).thenReturn(true);
		when(mockDashboardServiceImpl.isStorageAvailable(any(),anyInt(), anyString(), anyString(), any())).thenReturn(true);

		doNothing().when(mockDashboardServiceImpl).transferAndExtractResources(any(), anyString(), anyString(), any());
		doNothing().when(mockDashboardServiceImpl).dockerImagesTask(any(), anyString(), anyString(), any(), any());
		
		when(mockDashboardServiceImpl.triggerAddDashboardScript(any(), anyString(), anyString(), anyString(), any())).thenReturn(1);

		MvcResult mvcResult = this.mockMvc.perform(post("/dashboard/install")
													.content(mapper.writeValueAsString(dummySshDetailsDTO))
													.contentType(MediaType.APPLICATION_JSON)
													.accept(MediaType.APPLICATION_JSON))
										.andExpect(status().isInternalServerError()).andReturn();
		
		assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), mvcResult.getResponse().getStatus());
		assertNotNull(mvcResult.getResponse().getContentAsString());
	}


	// -------------------------- eventsEmitter ----------------------------- //

	@Test
	public void eventsEmitter_onCompletion_SuccessTest() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(get("/dashboard/events"))
											.andExpect(request().asyncStarted())
											.andDo(MockMvcResultHandlers.log())
											.andReturn();
		
		ReflectionTestUtils.setField(handle, "dashboardEvents", BASHRC_DELETE);
		ReflectionTestUtils.setField(handle, "dashboardEvents", END);

		this.mockMvc.perform(asyncDispatch(mvcResult))
						.andDo(MockMvcResultHandlers.log())
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.TEXT_EVENT_STREAM));

		System.out.println(mvcResult.getResponse().getContentAsString().replaceAll("data:", "").replaceAll("\n\n", ""));
		
	}

	@Test
	public void eventsEmitter_noUpdate_FailureTest() throws Exception {
		MvcResult mvcResult = this.mockMvc.perform(get("/dashboard/events"))
											.andExpect(request().asyncStarted())
											.andDo(MockMvcResultHandlers.log())
											.andReturn();

		this.mockMvc.perform(asyncDispatch(mvcResult))
						.andDo(MockMvcResultHandlers.log())
						.andExpect(status().isOk())
						.andExpect(content().contentType(MediaType.TEXT_EVENT_STREAM));
	}


	private List<DashboardDetailsDTO> prepareDummyData() {
		List<DashboardDetailsDTO> dummyDashboardDetailsDTOs = new ArrayList<>();

		dummyDashboardDetailsDTOs.add(new DashboardDetailsDTO(1, "Test A", "x.x.x.x:1111", LocalDateTime.now(),
				LocalDateTime.now(), DashboardStatus.DASHBOARD_ACTIVE, ClusterType.AWS));
		dummyDashboardDetailsDTOs.add(new DashboardDetailsDTO(2, "Test B", "x.x.x.x:1111", LocalDateTime.now(),
				LocalDateTime.now(), DashboardStatus.DASHBOARD_INACTIVE, ClusterType.AZURE));
		dummyDashboardDetailsDTOs.add(new DashboardDetailsDTO(3, "Cluster ON Prem", "x.x.x.x:1111", LocalDateTime.now(),
				LocalDateTime.now(), DashboardStatus.DASHBOARD_INACTIVE, ClusterType.ON_PREMISE));
		dummyDashboardDetailsDTOs.add(new DashboardDetailsDTO(4, "Cluster On Prem B", "x.x.x.x:1111",
				LocalDateTime.now(), LocalDateTime.now(), DashboardStatus.DASHBOARD_ACTIVE, ClusterType.ON_PREMISE));

		return dummyDashboardDetailsDTOs;
	}

	private List<DashboardDetailsDTO> prepareSearchResponseData(String searchTerm) {
		List<DashboardDetailsDTO> dashboardDetailsDTOs = new ArrayList<>();

		for (DashboardDetailsDTO dashboardDetailsDTO : this.mockDashboardDetailsDTOs) {
			if (dashboardDetailsDTO.getName().contains(searchTerm))
				dashboardDetailsDTOs.add(dashboardDetailsDTO);
		}
		return dashboardDetailsDTOs;
	}

	// private <T> ResponseDTO prepareDummyResponseDTO(T data) {
	// ResponseDTO responseDTO = new ResponseDTO();
	// if(data instanceof List<?>) {
	// List<DashboardDetailsDTO> tempData = new ArrayList<>();
	// for(T e : (List<?>) data) {

	// }
	// responseDTO.successListDashboardDetailsDTOs(tempData);
	// }
	// return responseDTO;
	// }

	private ResponseDTO prepareDummyListResponseDTO(List<DashboardDetailsDTO> data) {
		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.successListDashboardDetailsDTOs(data);
		return responseDTO;
	}

	private ResponseDTO prepareDummyDeleteResponseDTO(int id) {
		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.successDeleteDashboardDetailsDTO(id);
		return responseDTO;
	}

	private ResponseDTO prepareDummyHealthResponseDTO(Boolean flag) {
		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.successHealthResponseDTO(flag);
		return responseDTO;
	}

}
