package com.ss8.skc.service;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Map;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.ss8.skc.exception.SKCException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

@RunWith(MockitoJUnitRunner.class)
public class InstallDashboardTest {

    @Spy
    @InjectMocks
    private DashboardServiceImpl handle;

    private JSch jSch = mock(JSch.class);

    private ProcessBuilder processBuilder = mock(ProcessBuilder.class);

    private Process process = mock(Process.class);

    @Mock
    Session nodeSession;

    @Mock
    ChannelExec nodeChannel;

    StringBuilder logs;

    String[] nodesList;

    @SuppressWarnings("unchecked")
    private static void setSKCIP() throws Exception {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put("SKC_IP", "dummySKCIP");
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        setSKCIP();

        logs = new StringBuilder();
        nodesList = new String[] { "10.2.44.21", "32.33.22.32" };

        when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(nodeSession);
        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.isClosed()).thenReturn(true);

        doNothing().when(nodeSession).setPassword(anyString());
        doNothing().when(nodeSession).setConfig(anyString(), anyString());
        doNothing().when(nodeSession).connect();
        doNothing().when(nodeChannel).connect();
    }

    /*
     * controllerNodesReachable
     */
    @Test
    public void controllerNodesReachable_Success() throws SKCException {
        when(handle.pingHost(anyString(), anyInt(), anyInt())).thenReturn(true);

        assertEquals(true, handle.controllerNodesReachable(nodesList, logs));
    }

    @Test(expected = SKCException.class)
    public void controllerNodesReachable_Failure() throws SKCException {
        when(handle.pingHost(anyString(), anyInt(), anyInt())).thenReturn(false);

        handle.controllerNodesReachable(nodesList, logs);
    }

    /*
     * isStorageAvailable
     */

    @Test
    public void isStorageAvailable_Success() throws SKCException, JSchException, IOException {

        InputStream[] in = { new ByteArrayInputStream("1000M".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("11230M".getBytes(Charset.forName("UTF-8"))) };

        when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(nodeSession);
        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        when(nodeChannel.isClosed()).thenReturn(true);

        doNothing().when(nodeSession).setPassword(anyString());
        doNothing().when(nodeSession).setConfig(anyString(), anyString());
        doNothing().when(nodeSession).connect();
        doNothing().when(nodeChannel).connect();

        assertEquals(true, handle.isStorageAvailable(nodesList, 50,"sshUsername", "sshPassword", logs));
    }

    @Test(expected = SKCException.class)
    public void isStorageAvailable_Failure() throws SKCException, JSchException, IOException {

        InputStream[] in = { new ByteArrayInputStream("100M".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("130M".getBytes(Charset.forName("UTF-8"))) };

        when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(nodeSession);
        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        when(nodeChannel.isClosed()).thenReturn(true);

        doNothing().when(nodeSession).setPassword(anyString());
        doNothing().when(nodeSession).setConfig(anyString(), anyString());
        doNothing().when(nodeSession).connect();
        doNothing().when(nodeChannel).connect();

        handle.isStorageAvailable(nodesList,500, "sshUsername", "sshPassword", logs);
    }

    /*
     * fetchClusterNodesList
     */

    @Test
    public void fetchClusterNodesList_Success() throws JSchException, IOException, SKCException {

        InputStream in = new ByteArrayInputStream(
                "Ready 12.233.21.23\nReady 223.33.212.34".getBytes(Charset.forName("UTF-8")));
        String[] expected = { "12.233.21.23", "223.33.212.34" };

        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.getExitStatus()).thenReturn(0);
        when(nodeChannel.getInputStream()).thenReturn(in);

        String[] response = handle.fetchClusterNodesList(nodeSession, logs);

        assertArrayEquals(expected, response);

    }

    @Test(expected = SKCException.class)
    public void fetchClusterNodesList_AllNodesNotReady() throws JSchException, IOException, SKCException {

        InputStream in = new ByteArrayInputStream(
                "NotReady 12.233.21.23\nReady 223.33.212.34".getBytes(Charset.forName("UTF-8")));

        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.getExitStatus()).thenReturn(0);
        when(nodeChannel.getInputStream()).thenReturn(in);

        String[] response = handle.fetchClusterNodesList(nodeSession, logs);

    }

    @Test(expected = SKCException.class)
    public void fetchClusterNodesList_EmptyResponseFromKubectl() throws JSchException, IOException, SKCException {

        InputStream in = new ByteArrayInputStream(" ".getBytes(Charset.forName("UTF-8")));

        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.getExitStatus()).thenReturn(0);
        when(nodeChannel.getInputStream()).thenReturn(in);

        String[] response = handle.fetchClusterNodesList(nodeSession, logs);

    }

    @Test(expected = SKCException.class)
    public void fetchClusterNodesList_Failure() throws JSchException, IOException, SKCException {

        InputStream in = new ByteArrayInputStream(
                "Ready 12.233.21.23\nReady 223.33.212.34".getBytes(Charset.forName("UTF-8")));

        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.getExitStatus()).thenReturn(1);
        when(nodeChannel.getInputStream()).thenReturn(in);

        String[] response = handle.fetchClusterNodesList(nodeSession, logs);

    }

    /*
     * transferAndExtractResources
     */

    @Test
    public void transferAndExtractResources_Success()
            throws JSchException, IOException, InterruptedException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(processBuilder.command(anyList())).thenReturn(processBuilder);
        when(processBuilder.start()).thenReturn(process);

        /*
         * Step 1 - make directory Step 2 - scp resources Step 3 - Extract res
         */
        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);
        when(process.waitFor()).thenReturn(0);

        handle.transferAndExtractResources(nodesList, "sshUsername", "sshPassword", logs);

    }

    @Test(expected = SKCException.class)
    public void transferAndExtractResources_DirectoryCreationFailed()
            throws JSchException, IOException, InterruptedException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        /*
         * Step 1 - make directory Step 2 - scp resources Step 3 - Extract res
         */
        when(nodeChannel.getExitStatus()).thenReturn(1);
        when(nodeChannel.getInputStream()).thenReturn(in[0]);

        handle.transferAndExtractResources(nodesList, "sshUsername", "sshPassword", logs);

    }

    @Test(expected = SKCException.class)
    public void transferAndExtractResources_ScpFailed()
            throws JSchException, IOException, InterruptedException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        /*
         * Step 1 - make directory Step 2 - scp resources Step 3 - Extract res
         */
        ReflectionTestUtils.setField(handle, "resSrcPath", "source/");
        ReflectionTestUtils.setField(handle, "resDestPath", "dest/");

        when(nodeChannel.getInputStream()).thenReturn(in[0]);
        when(nodeChannel.getExitStatus()).thenReturn(0);

        when(processBuilder.command(anyList())).thenReturn(null);
        when(processBuilder.start()).thenReturn(process);
        when(process.waitFor()).thenReturn(1);
        when(process.getErrorStream()).thenReturn(in[0]);

        handle.transferAndExtractResources(nodesList, "sshUsername", "sshPassword", logs);

    }

    @Test(expected = SKCException.class)
    public void transferAndExtractResources_ExtractionFailed()
            throws JSchException, IOException, InterruptedException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        /*
         * Step 1 - make directory Step 2 - scp resources Step 3 - Extract res
         */
        ReflectionTestUtils.setField(handle, "resSrcPath", "source/");
        ReflectionTestUtils.setField(handle, "resDestPath", "dest/");

        when(nodeChannel.getInputStream()).thenReturn(in[0]);
        when(nodeChannel.getExitStatus()).thenReturn(0, 1);

        when(processBuilder.command(anyList())).thenReturn(null);
        when(processBuilder.start()).thenReturn(process);

        when(process.waitFor()).thenReturn(0);

        handle.transferAndExtractResources(nodesList, "sshUsername", "sshPassword", logs);

    }

    /*
     * dockerImagesTask
     */

    @Test
    public void dockerImagesTask_LoadSuccess() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.dockerImagesTask(nodesList, "sshUsername", "sshPassword", "load", logs);
    }

    @Test(expected = SKCException.class)
    public void dockerImagesTask_LoadFailure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(1, 1);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.dockerImagesTask(nodesList, "sshUsername", "sshPassword", "load", logs);

    }

    @Test(expected = SKCException.class)
    public void dockerImagesTask_InvalidOperation() throws IOException, SKCException {

        handle.dockerImagesTask(nodesList, "sshUsername", "sshPassword", "wrong_input", logs);
    }

    /*
     * triggerAddDashboardScript
     */

    @Test
    public void triggerAddDashboardScript_Success() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0);
        when(nodeChannel.getInputStream()).thenReturn(in[0]);

        Integer response = handle.triggerAddDashboardScript(nodesList, "sshUsername", "sshPassword", "clusterName",
                logs);

        assertEquals(0, response);

    }

    @Test
    public void triggerAddDashboardScript_Failure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(1);
        when(nodeChannel.getInputStream()).thenReturn(in[0]);
        doNothing().when(handle).deleteResources(any(String[].class), anyString(), anyString(), any());

        Integer response = handle.triggerAddDashboardScript(nodesList, "sshUsername", "sshPassword", "clusterName",
                logs);

        assertEquals(1, response);

    }

    @Test
    public void triggerAddDashboardScript_DashboardAlreadyExists() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(2);
        when(nodeChannel.getInputStream()).thenReturn(in[0]);

        Integer response = handle.triggerAddDashboardScript(nodesList, "sshUsername", "sshPassword", "clusterName",
                logs);

        assertEquals(2, response);

    }

    /*
     * postInstallScript
     */

    @Test
    public void postInstallScript_Success() throws SKCException, IOException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.postInstallScript(nodesList, "sshUsername", "sshPassword", logs);
    }

    @Test(expected = SKCException.class)
    public void postInstallScript_Failure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(1, 1);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.postInstallScript(nodesList, "sshUsername", "sshPassword", logs);

    }

}
