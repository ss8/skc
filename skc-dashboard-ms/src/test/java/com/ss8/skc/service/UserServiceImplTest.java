package com.ss8.skc.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.ss8.skc.dao.UserCredentials;
import com.ss8.skc.dao.UserDetails;
import com.ss8.skc.dto.ChangePasswordDTO;
import com.ss8.skc.dto.UserDetailsDTO;
import com.ss8.skc.exception.SKCException;
import com.ss8.skc.repository.SKCUsersRepository;
import com.ss8.skc.repository.UserRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    

    @Spy
	@InjectMocks
	private UserServiceImpl handle;

    @Mock
    private UserRepository mockUserRepository;

    @Mock
    private SKCUsersRepository mockUserCredRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    private UserDetailsDTO dummyInput;

    private UserDetails dummyData;


    @Before
    public void setup(){
        dummyInput = new UserDetailsDTO();
        dummyInput.setDashboardId(1);
        dummyInput.setUsername("admin");
        // dummyInput.setToken("dummy_Token");

        dummyData = new UserDetails();
        dummyData.setDashboardId(1);
        dummyData.setUsername("admin");
        dummyData.setToken("dummy_Token");
    }

    /*
    *   addUser
    */

    @Test
    public void addUser_Success() throws SKCException{
       

        when(mockUserRepository.findByUsernameAndDashboardId("admin", 1)).thenReturn(Optional.empty());
        when(mockUserRepository.saveAndFlush(any())).thenReturn(dummyData);

        UserDetailsDTO response = handle.addUser(dummyInput);

    
        assertEquals(dummyInput.getUsername(), response.getUsername());
        assertEquals(dummyInput.getDashboardId(), response.getDashboardId());
    }


    @Test(expected = SKCException.class)
    public void addUser_Failure() throws SKCException{
        
        when(mockUserRepository.findByUsernameAndDashboardId("admin", 1)).thenReturn(Optional.of(dummyData));
           
        UserDetailsDTO response = handle.addUser(dummyInput);
    }

    /*
    *   fetchToken
    */

    @Test
    public void fetchToken_Success() throws SKCException{

        when(mockUserRepository.findByUsernameAndDashboardId("admin", 1)).thenReturn(Optional.of(dummyData));


        String tokenResponse = handle.fetchToken(dummyInput);

        assertEquals("dummy_Token", tokenResponse);
    }

    @Test(expected = SKCException.class)
    public void fetchToken_Failure() throws SKCException{
        UserDetailsDTO dummyInput = new UserDetailsDTO();
        dummyInput.setDashboardId(1);
        dummyInput.setUsername("admin");


        when(mockUserRepository.findByUsernameAndDashboardId("admin", 1)).thenReturn(Optional.empty());

        String tokenResponse = handle.fetchToken(dummyInput);
    }

    /*
    *   deleteUserByUserId
    */
    @Test
    public void deleteUserByUserId_Success() throws SKCException{

        when(mockUserRepository.findById(1)).thenReturn(Optional.of(dummyData));

        doNothing().when(mockUserRepository).deleteById(anyInt());

        Boolean response = handle.deleteUserByUserId(1);
        
        assertEquals(Boolean.TRUE, response);
    }

    @Test(expected = SKCException.class)
    public void deleteUserByUserId_Failure() throws SKCException{

        when(mockUserRepository.findById(1)).thenReturn(Optional.empty());

        Boolean response = handle.deleteUserByUserId(1);
    }



    /*
    *   deleteUsersByDashboardId
    */
    @Test
    public void deleteUsersByDashboardId_Success() throws SKCException{

        List<UserDetails> userList = new ArrayList();
        userList.add(dummyData);


        when(mockUserRepository.findByDashboardId(1)).thenReturn(userList);

        doNothing().when(mockUserRepository).deleteAll(anyList());

        Boolean response = handle.deleteUsersByDashboardId(1);
        
        assertEquals(Boolean.TRUE, response);

    }

    @Test(expected = SKCException.class)
    public void deleteUsersByDashboardId_Failure() throws SKCException{
        List<UserDetails> userList = new ArrayList();
        
        when(mockUserRepository.findByDashboardId(1)).thenReturn(userList);

        Boolean response = handle.deleteUsersByDashboardId(1);
        
    }

    /*
    *   changePassword
    */
    @Test
    public void changePassword_Success() throws SKCException{
        ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();
        changePasswordDTO.setUsername("admin");
        changePasswordDTO.setOldPassword("dummyPassword");
        changePasswordDTO.setNewPassword("test1");

        UserCredentials dummyUserCredentials = new UserCredentials();
        dummyUserCredentials.setUsername("admin");
        dummyUserCredentials.setPassword("dummyPassword");


        when(mockUserCredRepository.findByUsername("admin")).thenReturn(Optional.of(dummyUserCredentials));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(true);
        when(passwordEncoder.encode(anyString())).thenReturn("dummyPassword");
        when(mockUserCredRepository.saveAndFlush(any())).thenReturn(dummyUserCredentials);

        Boolean response = handle.changePassword(changePasswordDTO);

        assertEquals(Boolean.TRUE, response);
    }

    @Test(expected = SKCException.class)
    public void changePassword_OldPasswordDoesNotMatch() throws SKCException{

        ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();
        changePasswordDTO.setUsername("admin");
        changePasswordDTO.setOldPassword("wrongdummyPassword");
        changePasswordDTO.setNewPassword("test1");

        UserCredentials dummyUserCredentials = new UserCredentials();
        dummyUserCredentials.setUsername("admin");
        dummyUserCredentials.setPassword("dummyPassword");


        when(mockUserCredRepository.findByUsername("admin")).thenReturn(Optional.of(dummyUserCredentials));
        when(passwordEncoder.matches(anyString(), anyString())).thenReturn(false);

        Boolean response = handle.changePassword(changePasswordDTO);
        
    }


    @Test(expected =  SKCException.class)
    public void changePassword_UserNotFound() throws SKCException{
        ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();
        changePasswordDTO.setUsername("admin");
        changePasswordDTO.setOldPassword("dummyPassword");
        changePasswordDTO.setNewPassword("test1");

        UserCredentials dummyUserCredentials = new UserCredentials();
        dummyUserCredentials.setUsername("admin");
        dummyUserCredentials.setPassword("dummyPassword");


        when(mockUserCredRepository.findByUsername("admin")).thenReturn(Optional.empty());

        Boolean response = handle.changePassword(changePasswordDTO);
    }
}
