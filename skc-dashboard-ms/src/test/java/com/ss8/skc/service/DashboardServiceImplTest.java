package com.ss8.skc.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Timer;
import java.util.concurrent.atomic.AtomicReference;

import com.ss8.skc.SkcApplication;
import com.ss8.skc.dao.ClusterType;
import com.ss8.skc.dao.DashboardDetails;
import com.ss8.skc.dao.DashboardStatus;
import com.ss8.skc.dto.DashboardDetailsDTO;
import com.ss8.skc.exception.SKCException;
import com.ss8.skc.repository.DashboardRepository;
import com.ss8.skc.utils.WebhookEventConstants;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

@RunWith(MockitoJUnitRunner.class)
public class DashboardServiceImplTest {

	final static String SEARCH_TERM = "te";
	final static int DELETE_SUCCESS_ID = 1;
	final static int DELETE_FAILURE_ID = 6;
	final static int HEALTH_SUCCESS_ID = 1;
	final static int HEALTH_FAILURE_ID = 5;

	@Mock
	private DashboardRepository dummyDashboardRepository;

	@Spy
	@InjectMocks
	private DashboardServiceImpl handle;

	@Autowired
	private ApplicationEventPublisher publisher;


	@Mock
	ApplicationReadyEvent applicationReadyEvent;

	private List<DashboardDetails> dummyDashboardDetails;

	@BeforeClass
	public static void init() {
		System.setProperty("SKC_IP", "12.223.12.2");
	}

	@Before
	public void setUp() {
		dummyDashboardDetails = prepareDummyData();
	}

	@Test
	public void updateDashboard_Sucess() throws SKCException {

		DashboardDetails dummyResponse = new DashboardDetails();
		dummyResponse.setName("dummy_name");
		dummyResponse.setid(1);
		dummyResponse.setDashboardStatus(DashboardStatus.DASHBOARD_ACTIVE);

		when(dummyDashboardRepository.findById(1)).thenReturn(Optional.of(dummyResponse));

		DashboardDetailsDTO changes = new DashboardDetailsDTO();
		changes.setName("changed_name");
		changes.setDashboardStatus(DashboardStatus.DASHBOARD_INACTIVE);

		when(dummyDashboardRepository.saveAndFlush(dummyResponse)).thenReturn(dummyResponse);

		int response = handle.updateDashboard(1, changes);

		assertEquals(1, response);
	}

	@Test(expected = SKCException.class)
	public void updateDashboard_Failure() throws SKCException {

		DashboardDetails dummyResponse = new DashboardDetails();
		dummyResponse.setName("dummy_name");
		dummyResponse.setid(1);
		dummyResponse.setDashboardStatus(DashboardStatus.DASHBOARD_ACTIVE);

		DashboardDetailsDTO changes = new DashboardDetailsDTO();
		changes.setName("changed_name");
		changes.setDashboardStatus(DashboardStatus.DASHBOARD_INACTIVE);

		when(dummyDashboardRepository.findById(1)).thenReturn(Optional.empty());

		int response = handle.updateDashboard(1, changes);
	}

	@Test
	public void listDashboards_ALL_SuccessTest() throws SKCException {

		when(dummyDashboardRepository.findAll()).thenReturn(dummyDashboardDetails);

		List<DashboardDetailsDTO> actualDashboardDetailsDTOs = handle.listDashboards("");

		assertEquals(dummyDashboardDetails.size(), actualDashboardDetailsDTOs.size());
		// assertThat(handle.listDashboards("")).isNotEmpty().hasSize(dummyDashboardDetails.size()).containsAll(dummyDashboardDetails.iterator());
		// assertEquals(expected, actual);

	}

	@Test
	public void listDashboards_SEARCH_SuccessTest() throws SKCException {
		List<DashboardDetails> dummySearchDashboardDetails = prepareDummySearchData(SEARCH_TERM);

		when(dummyDashboardRepository.findByNameContaining(SEARCH_TERM)).thenReturn(dummySearchDashboardDetails);

		List<DashboardDetailsDTO> actualDashboardDetailsDTOs = handle.listDashboards(SEARCH_TERM);

		assertEquals(dummySearchDashboardDetails.size(), actualDashboardDetailsDTOs.size());
	}

	// @Test
	// public void deleteDashboard_SuccessTest() throws SKCException {

	// Optional<DashboardDetails> dummyOptional = Optional.empty();

	// if (hasID(DELETE_SUCCESS_ID)) {
	// dummyOptional = Optional.of(new DashboardDetails());
	// }

	// when(dummyDashboardRepository.findById(DELETE_SUCCESS_ID)).thenReturn(dummyOptional);
	// doNothing().when(dummyDashboardRepository).deleteById(DELETE_SUCCESS_ID);

	// int actualId = handle.deleteDashboard(DELETE_SUCCESS_ID);

	// verify(dummyDashboardRepository, times(1)).deleteById(DELETE_SUCCESS_ID);

	// assertEquals(DELETE_SUCCESS_ID, actualId);
	// }

	@Test(expected = SKCException.class)
	public void deleteDashboard_FailureTest() throws SKCException {
		Optional<DashboardDetails> dummyOptional = Optional.empty();

		if (hasID(DELETE_FAILURE_ID)) {
			dummyOptional = Optional.of(new DashboardDetails());
		}

		when(dummyDashboardRepository.findById(DELETE_FAILURE_ID)).thenReturn(dummyOptional);

		handle.deleteDashboard(DELETE_FAILURE_ID);

	}

	@Test
	public void healthCheck_DASHBOARD_MODIFIED_SuccessTest() throws SKCException {
		DashboardDetailsDTO dummyPayload = new DashboardDetailsDTO();
		dummyPayload.setId(HEALTH_SUCCESS_ID);
		dummyPayload.setDashboardStatus(DashboardStatus.DASHBOARD_INACTIVE);
		dummyPayload.setUrl("x.y.z.a:111");
		Optional<DashboardDetails> dummyOptional = Optional.of(DashboardDetailsDTO.createEntity(dummyPayload));

		when(dummyDashboardRepository.findById(HEALTH_SUCCESS_ID)).thenReturn(dummyOptional);
		doReturn(HEALTH_SUCCESS_ID).when(handle).updateDashboard(HEALTH_SUCCESS_ID, dummyPayload);

		Boolean returnValue = handle.healthCheck(WebhookEventConstants.DASHBOARD_MODIFIED.toString(), dummyPayload);

		assertEquals(true, returnValue);

		// If cache already has id

		returnValue = handle.healthCheck(WebhookEventConstants.DASHBOARD_MODIFIED.toString(), dummyPayload);

		verify(handle, times(2)).updateDashboard(HEALTH_SUCCESS_ID, dummyPayload);

		assertEquals(true, returnValue);

	}

	@Test
	public void healthCheck_DASHBOARD_STATUS_SuccessTest() throws SKCException {
		DashboardDetailsDTO dummyPayload = new DashboardDetailsDTO();
		dummyPayload.setId(HEALTH_SUCCESS_ID);
		dummyPayload.setDashboardStatus(DashboardStatus.DASHBOARD_INACTIVE);
		Optional<DashboardDetails> dummyOptional = Optional.of(DashboardDetailsDTO.createEntity(dummyPayload));

		when(dummyDashboardRepository.findById(HEALTH_SUCCESS_ID)).thenReturn(dummyOptional);
		// doNothing().when(dummyDashboardRepository).saveAndFlush(DashboardDetailsDTO.createEntity(dummyPayload));
		// when(dummyDashboardRepository.saveAndFlush(DashboardDetailsDTO.createEntity(dummyPayload)))
		// .thenReturn(DashboardDetailsDTO.createEntity(dummyPayload));

		Boolean returnValue = handle.healthCheck(WebhookEventConstants.DASHBOARD_STATUS.toString(), dummyPayload);

		assertEquals(true, returnValue);

		dummyPayload.setDashboardStatus(DashboardStatus.DASHBOARD_ACTIVE);

		returnValue = handle.healthCheck(WebhookEventConstants.DASHBOARD_STATUS.toString(), dummyPayload);

		assertEquals(true, returnValue);
	}

	@Test(expected = SKCException.class)
	public void healthCheck_EVENT_INVALID_FailureTest() throws SKCException {
		DashboardDetailsDTO dummyPayload = new DashboardDetailsDTO();
		dummyPayload.setId(HEALTH_SUCCESS_ID);

		handle.healthCheck("Some Rnadom Event", dummyPayload);
	}

	@Test(expected = SKCException.class)
	public void healthCheck_DASHBOARD_NOT_FOUND_FailureTest() throws SKCException {
		DashboardDetailsDTO dummyPayload = new DashboardDetailsDTO();
		dummyPayload.setId(HEALTH_FAILURE_ID);

		when(dummyDashboardRepository.findById(HEALTH_FAILURE_ID)).thenReturn(Optional.empty());

		handle.healthCheck(WebhookEventConstants.DASHBOARD_STATUS.toString(), dummyPayload);
	}

	@Test(expected = SKCException.class)
	public void healthCheck_INPUT_ID_NULL_FailureTest() throws SKCException {
		DashboardDetailsDTO dummyPayload = new DashboardDetailsDTO();

		handle.healthCheck(WebhookEventConstants.DASHBOARD_STATUS.toString(), dummyPayload);
	}

	private List<DashboardDetails> prepareDummyData() {
		List<DashboardDetails> dummyDashboardDetails = new ArrayList<DashboardDetails>();

		dummyDashboardDetails.add(new DashboardDetails(1, "Test A", "x.x.x.x:1111", LocalDateTime.now(),
				LocalDateTime.now(), DashboardStatus.DASHBOARD_ACTIVE, ClusterType.AWS));
		dummyDashboardDetails.add(new DashboardDetails(2, "Test B", "x.x.x.x:1111", LocalDateTime.now(),
				LocalDateTime.now(), DashboardStatus.DASHBOARD_INACTIVE, ClusterType.AZURE));
		dummyDashboardDetails.add(new DashboardDetails(3, "Cluster ON Prem", "x.x.x.x:1111", LocalDateTime.now(),
				LocalDateTime.now(), DashboardStatus.DASHBOARD_INACTIVE, ClusterType.ON_PREMISE));
		dummyDashboardDetails.add(new DashboardDetails(4, "Cluster On Prem B", "x.x.x.x:1111", LocalDateTime.now(),
				LocalDateTime.now(), DashboardStatus.DASHBOARD_ACTIVE, ClusterType.ON_PREMISE));

		return dummyDashboardDetails;
	}

	private List<DashboardDetails> prepareDummySearchData(String searchTerm) {
		List<DashboardDetails> dummyData = new ArrayList<>();

		for (DashboardDetails d : dummyDashboardDetails) {
			if (d.getName().contains(searchTerm)) {
				dummyData.add(d);
			}
		}
		return dummyData;
	}

	private Boolean hasID(int id) {
		for (DashboardDetails d : dummyDashboardDetails) {
			if (d.getid() == id)
				return true;
		}
		return false;
	}

	@Test
	public void cronJobUpdateTimeoutWatcher_verifyInvoked() {
		handle.cronJobUpdateTimeoutWatcher();

		verify(handle, times(1)).cronJobUpdateTimeoutWatcher();
	}

	@Mock
	Timer timer;

	@Test
	public void cronJobUpdateTimeoutWatcher_verifyTimerScheduled() {

		handle.cronJobUpdateTimeoutWatcher();

		verify(timer).schedule(any(), any(), anyLong());
	}

	@Test
	public void getTimeDiffInMinutes_Test() {
		long response = handle.getTimeDiffInMinutes(LocalDateTime.now(), LocalDateTime.now().plusMinutes(5));

		assertEquals(5, response);
	}
}
