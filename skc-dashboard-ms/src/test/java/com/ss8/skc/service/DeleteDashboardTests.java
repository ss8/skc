package com.ss8.skc.service;

import static org.mockito.ArgumentMatchers.anyString;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.Map;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.ss8.skc.exception.SKCException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DeleteDashboardTests {
    @Spy
    @InjectMocks
    private DashboardServiceImpl handle;

    private JSch jSch = mock(JSch.class);

    // private ProcessBuilder processBuilder = mock(ProcessBuilder.class);

    @Mock
    Session nodeSession;

    @Mock
    ChannelExec nodeChannel;

    StringBuilder logs;

    String[] nodesList;

    @SuppressWarnings("unchecked")
    private static void setSKCIP() throws Exception {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put("SKC_IP", "dummySKCIP");
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        setSKCIP();

        logs = new StringBuilder();
        nodesList = new String[] { "10.2.44.21", "32.33.22.32" };

        when(jSch.getSession(anyString(), anyString(), anyInt())).thenReturn(nodeSession);
        when(nodeSession.openChannel("exec")).thenReturn(nodeChannel);

        when(nodeChannel.isClosed()).thenReturn(true);

        doNothing().when(nodeSession).setPassword(anyString());
        doNothing().when(nodeSession).setConfig(anyString(), anyString());
        doNothing().when(nodeSession).connect();
        doNothing().when(nodeChannel).connect();
    }

    /*
     * triggerDeleteDashboardScript
     */

    @Test
    public void triggerDeleteDashboardScript_Success() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0);
        when(nodeChannel.getInputStream()).thenReturn(in[0]);

        boolean response = handle.triggerDeleteDashboardScript(nodesList, "sshUsername", "sshPassword", logs);

        assertEquals(true, response);

    }

    @Test
    public void triggerDeleteDashboardScript_Failure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(1);
        when(nodeChannel.getInputStream()).thenReturn(in[0]);

        boolean response = handle.triggerDeleteDashboardScript(nodesList, "sshUsername", "sshPassword", logs);

        assertEquals(false, response);

    }

    /*
     * dockerImagesTask
     */

    @Test(expected = SKCException.class)
    public void dockerImagesTask_InvalidOperation() throws IOException, SKCException {

        handle.dockerImagesTask(nodesList, "sshUsername", "sshPassword", "wrong_input", logs);
    }

    @Test
    public void dockerImagesTask_UnloadSuccess() throws SKCException, IOException {
        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.dockerImagesTask(nodesList, "sshUsername", "sshPassword", "unload", logs);

    }

    @Test
    public void dockerImagesTask_UnloadFailure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.dockerImagesTask(nodesList, "sshUsername", "sshPassword", "unload", logs);

    }

    /*
     * bashrcTask
     */

    @Test
    public void bashrcTask_AddSuccess() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.bashrcTask(nodesList, "sshUsername", "sshPassword", "add", logs);
    }

    @Test(expected = SKCException.class)
    public void bashrcTask_AddFailure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(1, 1);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.bashrcTask(nodesList, "sshUsername", "sshPassword", "add", logs);

    }

    @Test(expected = SKCException.class)
    public void bashrcTask_InvalidOperation() throws IOException, SKCException {

        handle.bashrcTask(nodesList, "sshUsername", "sshPassword", "wrong_input", logs);
    }

    @Test
    public void bashrcTask_RemoveSuccess() throws SKCException, IOException {
        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.bashrcTask(nodesList, "sshUsername", "sshPassword", "remove", logs);

    }

    @Test
    public void bashrcTask_RemoveFailure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.bashrcTask(nodesList, "sshUsername", "sshPassword", "remove", logs);

    }

    /*
     * cronJobTask
     */

    @Test
    public void cronJobTask_AddSuccess() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.cronJobTask(nodesList, "sshUsername", "sshPassword", "add", logs);
    }

    @Test(expected = SKCException.class)
    public void cronJobTask_AddFailure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(1, 1);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.cronJobTask(nodesList, "sshUsername", "sshPassword", "add", logs);

    }

    @Test(expected = SKCException.class)
    public void cronJobTask_InvalidOperation() throws IOException, SKCException {

        handle.cronJobTask(nodesList, "sshUsername", "sshPassword", "wrong_input", logs);
    }

    @Test
    public void cronJobTask_RemoveSuccess() throws SKCException, IOException {
        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.cronJobTask(nodesList, "sshUsername", "sshPassword", "remove", logs);

    }

    @Test
    public void cronJobTask_RemoveFailure() throws IOException, SKCException {

        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.cronJobTask(nodesList, "sshUsername", "sshPassword", "remove", logs);

    }

    /*
     * deleteResources
     */

    @Test
    public void deleteResources_Success() throws IOException, SKCException {
        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(0, 0);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.deleteResources(nodesList, "sshUsername", "sshPassword", logs);

    }

    @Test(expected = SKCException.class)
    public void deleteResources_Failure() throws IOException, SKCException {
        InputStream[] in = { new ByteArrayInputStream("Sample input node 1".getBytes(Charset.forName("UTF-8"))),
                new ByteArrayInputStream("Sample input node 2".getBytes(Charset.forName("UTF-8"))) };

        when(nodeChannel.getExitStatus()).thenReturn(1, 1);
        when(nodeChannel.getInputStream()).thenReturn(in[0], in[1]);

        handle.deleteResources(nodesList, "sshUsername", "sshPassword", logs);

    }

}
