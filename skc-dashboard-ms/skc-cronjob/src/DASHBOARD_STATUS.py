from json.decoder import JSONDecodeError
import os
import json
from pathlib import Path
from typing import Optional
import yaml
import requests
from inspect import getsourcefile
from urllib.parse import urljoin
from dotenv import load_dotenv
from K8sClient import K8sClient

# Load any variable mentioned in .env in environment
load_dotenv()

# Getting running script dir path
cwd = os.path.dirname(os.path.realpath(getsourcefile(lambda: 0)))


class DashboardStatus:
    # TODO use standardised exception mechanism instead of returning None, Error etc or raising Exception
    def __init__(self):
        # Initialize a K8sClient object
        self.__k8sClient = K8sClient()

        # If SKC_IP is not present in environment variable raise error
        if os.getenv('SKC_IP') is None:
            raise ValueError('SKC_IP is not set in environment')

        # Declare files to be read and used
        resources_file = os.path.join(cwd, 'config', 'resources.yaml')
        values_file = os.path.join(cwd, 'config', 'values.yaml')

        # Read values.yaml or values.yml or values.json to get SKC related data
        self.__skc = self.__read_json_or_yaml_file(values_file)
        # Read resources.yaml or resources.yml or resources.json to get the resources to be checked
        self.__resources_file_data = self.__read_json_or_yaml_file(
            resources_file)

        if self.__skc is None or self.__resources_file_data is None:
            raise Exception()

        # Generate/Build SKC endpoints
        self.__build_endpoints()

    def __build_endpoints(self) -> None:
        '''
            Build the URLs of different SKC endpoints & assign them as class variables

            Returns:
                None
        '''

        # Read & construct the SKC URLs
        skc_ip = os.getenv('SKC_IP')
        self.__skc_health_endpoint = urljoin(
            skc_ip, self.__skc['skc']['uris']['health']['path'])
        self.__skc_health_endpoint_method = self.__skc['skc']['uris']['health']['method']
        self.__skc_login_endpoint = urljoin(
            skc_ip, self.__skc['skc']['uris']['login']['path'])
        self.__skc_login_endpoint_method = self.__skc['skc']['uris']['login']['method']

    def __read_json_or_yaml_file(self, file_path: str) -> Optional[dict]:
        '''
            Read a given JSON or YAML file and return the resultant dictionary

            Args:
                file_path (str):    Path of JSON or YAML file
                                    Required

            Returns:
                dict:   If file is read successfully
                None:   If there was some error
        '''

        if not isinstance(file_path, str):
            print('Please provide file path')
            return None

        if not os.path.isfile(file_path) or not os.access(path=file_path, mode=os.R_OK):
            print(
                "Either given file doesn't exist or is not a regular file or doesn't have read permission")
            return None

        with open(file_path, 'r') as file:
            if '.yaml' in file_path or '.yml' in file_path:
                try:
                    return yaml.safe_load(file)
                except yaml.YAMLError as err:
                    print('Error while reading resources yaml file: %s\n' % err)
                    raise Exception()
            else:
                try:
                    return json.load(file)
                except JSONDecodeError as err:
                    print('Error while reading resources json file: %s\n' & err)
                    raise Exception()

    def __write_json_or_yaml_file(self, file_path: str, data: dict) -> None:
        '''
            Writes dashboard json data into dashboard_details.json

            Args:
                file_path (str):    Path of JSON or YAML file
                                    Required
                data (dict):        Dashboard data
                                    Required

            Returns:
                None
        '''

        if not isinstance(file_path, str) or not isinstance(data, dict):
            print('Please provide values for file_path & data')
            return None

        if not isinstance(data, dict) and not isinstance(data, str):
            print('data should be string or dictionary')
            return None

        with open(file_path, 'w') as json_file:
            json.dump(data, json_file, indent=2)

    def __get_configmap_data(self, configmap: str, namespace: str) -> Optional[dict]:
        '''
            Returns the data of given configmap from provided namespace

            Args:
                configmap (str):    Name of configmap
                                    Required
                namespace (str):    Namespace of configmap
                                    Required

            Returns:
                dict:               Data of configmap in dictionary datatype
                None:               In case any error occured while retrieving data
        '''

        if not isinstance(configmap, str) or not isinstance(namespace, str):
            print('Please provide a valid configmap, namespace & data')
            return None

        if not self.__k8sClient.check_configmap_exists(configmap=configmap, namespace=namespace):
            print("Given {0} configmap doesn't exist in {1} namespace".format(
                configmap, namespace))
            return None

        return self.__k8sClient.get_configmap(configmap=configmap, namespace=namespace).data

    def __modify_configmap(self, configmap: str, namespace: str, data: dict) -> None:
        '''
            Modifies the data of given configmap from provided namespace

            Args:
                configmap (str):    Name of configmap
                                    Required
                namespace (str):    Namespace of configmap
                                    Required
                data (dict):        New data of the configmap
                                    Required

            Returns:
                None
        '''

        if not isinstance(configmap, str) or not isinstance(namespace, str) or not isinstance(data, dict):
            print('Please provide a valid configmap, namespace & data')
            return None

        if not self.__k8sClient.check_configmap_exists(configmap=configmap, namespace=namespace):
            print("Given {0} configmap doesn't exist in {1} namespace".format(
                configmap, namespace))
            return None

        configmap_details = self.__k8sClient.get_configmap(
            configmap=configmap, namespace=namespace)

        configmap_details.data = data

        self.__k8sClient.modify_configmap(
            configmap=configmap, namespace=namespace, data=configmap_details)

    def __skc_login(self) -> Optional[str]:
        ''' 
            Log into SKC and get the Bearer token for Authorization. 

            Returns: 
                token (str):    Bearer token provided by SKC upon successful login. 
        '''

        try:
            login_payload = {
                'username': self.__skc['skc']['username'],
                'password': self.__skc['skc']['password']
            }

            print('\nLogging into SKC...')

            res = requests.request(method=self.__skc_login_endpoint_method,
                                   url=self.__skc_login_endpoint, json=login_payload, verify=False)
            if res.status_code == 200:
                print('{} successfully logged in'.format(
                    self.__skc['skc']['username']))
                return res.headers['Authorization'].replace('Bearer ', '')
            else:
                return None
        except requests.exceptions.RequestException as err:
            print(err)
            return None

    def __check_all_deployment_healthy(self) -> bool:
        ''' 
            Check if all deployments mentioned in resources file are healthy 
            It checks whether a deployment's replica and readyReplica value are equal 

            Returns:  
                True (bool): If all deployments are healthy 
                False (bool): If any of the deployment is unhealthy 
        '''

        for deployment in self.__resources_file_data['deployments']:
            print('\nChecking {0} deployment health status in {1} namespace...'.format(
                deployment['name'], deployment['namespace']))
            if not self.__k8sClient.check_deployment_healthy(deployment=deployment['name'], namespace=deployment['namespace']):
                return False

        return True

    def __check_all_service_exists(self) -> bool:
        ''' 
            Check if all services mentioned in resources file exist. 

            Returns: 
                True (bool): If all service exist 
                False (bool): If any of the service is non-existent  
        '''

        for service in self.__resources_file_data['services']:
            print('\nChecking existence of {0} service in {1} namespace...'.format(
                service['name'], service['namespace']))
            if not self.__k8sClient.check_service_exists(service=service['name'], namespace=service['namespace']):
                return False

        return True

    # TODO: In future, this can return a dict with key: ingress_name & value: Ingress IP
    def __check_ingress_ip(self) -> Optional[str]:
        ''' 
            Check the IPs of the ingresses mentioned in resources file

            Returns:
                str:    IP of the ingress
                None:   In case any exception is encountered 
        '''

        for ingress in self.__resources_file_data['ingresses']:
            print('\nChecking IP of {0} ingress in {1} namespace...'.format(
                ingress['name'], ingress['namespace']))
            ingress_ip = self.__k8sClient.check_ingress_ip(
                namespace=ingress['namespace'], ingress=ingress['name'])
            if ingress_ip is not None:
                return ingress_ip
            else:
                return None

        return None

    def __get_controller_nodeport(self, service: str, namespace: str, portName: str) -> Optional[str]:
        '''
            Get the nodeport of ingress controller service

            Args:
                service (str):      Name of nodeport service
                                    Required
                namespace (str):    Namespace
                                    Required
                portName (str):     Name of port
                                    Required

            Returns:
                str:                Nodeport of service
                None:               If any exception is encountered
        '''

        if not isinstance(service, str) or not isinstance(namespace, str) or not isinstance(portName, str):
            print('Please provide namespace, service and port name')
            return None

        print('\nGetting node port of {0} service from {1} namespace..'.format(
            service, namespace))
        return self.__k8sClient.get_service_nodeport(service=service, namespace=namespace, portName=portName)

    '''
        TODO
        Add post processing to data (if any in future) beforing returning it
    '''

    def __report_to_skc(self, dashboardEvent: str, dashboardData: dict) -> Optional[dict]:
        ''' 
            Report the findings/status of dashboard to SKC. 

            Args: 
                dashboardEvent (str):   The event to report to SKC. 
                                        Default: None 
                                        Required 
                dashboardData (dict):   The new data of the dashboard. 
                                        Default: None 
                                        Required

            Returns:
                dict:                   Dashboard Data received from SKC(if any)
                None:                   In case any exception is encountered
        '''

        if not isinstance(dashboardEvent, str) or not isinstance(dashboardData, dict):
            print('Please provide a valid dashboard event and data')
            return None

        login_token = self.__skc_login()

        if login_token is None:
            return None

        request_header = {
            'Authorization': 'Bearer: ' + login_token,
            self.__skc['skc']['event']['header']: dashboardEvent
        }

        try:
            print('\nReporting {0} status to SKC with {1} event...'.format(
                dashboardData['dashboardStatus'], dashboardEvent))
            res = requests.request(method=self.__skc_health_endpoint_method, url=self.__skc_health_endpoint,
                                   json=dashboardData, headers=request_header, verify=False)

            res.raise_for_status()

            if res.status_code == 200:
                print('Successfully logged {0} status!'.format(
                    dashboardData['dashboardStatus']))
                # Complete the implementation
                return dashboardData
            else:
                print(
                    'There was an error reporting status to SKC: {}'.format(res.text))
                return None
        except requests.exceptions.HTTPError as err:
            print('HTTP error  code: ', err)
            return None
        except requests.exceptions.ConnectionError as err:
            print('Connection error: ', err)
            return None
        except requests.exceptions.Timeout as err:
            print('Timeout error: ', err)
            return None
        except requests.exceptions.RequestException as err:
            print('Something went wrong! ', err)
            return None

    # main function to check all resource status and call report to skc
    def run(self) -> None:
        ''' 
            The main function to call to check dashboard status which evaluates the conditions 
            and reports to SKC accordingly.

            Returns:
                None
        '''

        # Read dashboard details according to the values provided in values.yaml/values.json
        if self.__skc['dashboard-details']['type'] == 'volume':
            dashboardDetails_file = os.path.join(
                self.__skc['dashboard-details']['path'], self.__skc['dashboard-details']['name'])
            dashboardDetails = self.__read_json_or_yaml_file(
                dashboardDetails_file)
        elif self.__skc['dashboard-details']['type'] == 'configmap':
            configmap_data = self.__get_configmap_data(
                configmap=self.__skc['dashboard-details']['name'], namespace=self.__skc['dashboard-details']['namespace'])
            dashboardDetails = json.loads(
                configmap_data[self.__skc['dashboard-details']['key']])

        if dashboardDetails is None:
            raise Exception('dashboard_details.json file could not be read')

        print("CURRENT DASHBOARD DETAILS:", dashboardDetails)
        # Assume event will be 'status'
        dashboardEvent = self.__skc['skc']['event']['types']['status']
        # Assume dashboard is 'active'
        dashboardDetails['dashboardStatus'] = self.__skc['skc']['status']['active']

        if not self.__check_all_deployment_healthy() or not self.__check_all_service_exists():
            dashboardDetails['dashboardStatus'] = self.__skc['skc']['status']['inactive']
            print("Dashboard Status set to INACTIVE as either all deployments are not healthy or all services are not present")

        dashboardIP = self.__check_ingress_ip()
        print("CURRENT DASHBOARD IP: ", dashboardIP)
        # TODO get ingress service details from resources file
        dashboardPort = self.__get_controller_nodeport(
            service='ingress-nginx-controller', namespace='ingress-nginx', portName='https')
        print("CURRENT INGRESS HTTPS PORT/DASHBOARD PORT: ", dashboardPort)

        if dashboardIP is None or dashboardPort is None:
            dashboardDetails['dashboardStatus'] = self.__skc['skc']['status']['inactive']
            print(
                "Dashboard Status set to INACTIVE as either Dashboard IP or PORT is not valid")
        else:
            dashboardURL = 'https://' + \
                str(dashboardIP) + ':' + str(dashboardPort)
            print("CURRENT DASHBOARD URL: ", dashboardURL)
            if dashboardURL != dashboardDetails['url']:
                dashboardEvent = self.__skc['skc']['event']['types']['modify']
                dashboardDetails['url'] = dashboardURL
                print(
                    "Dashboard Event set to MODIFY as current dashboard URL is different than previously reported")

        newDashboardDetails = self.__report_to_skc(
            dashboardEvent=dashboardEvent, dashboardData=dashboardDetails)

        if newDashboardDetails is not None:
            if self.__skc['dashboard-details']['type'] == 'configmap':
                configmap_data[self.__skc['dashboard-details']
                               ['key']] = json.dumps(newDashboardDetails)
                self.__modify_configmap(configmap=self.__skc['dashboard-details']['name'],
                                        namespace=self.__skc['dashboard-details']['namespace'], data=configmap_data)
            elif self.__skc['dashboard-details']['type'] == 'volume':
                self.__write_json_or_yaml_file(
                    data=newDashboardDetails, file_path=dashboardDetails_file)
        else:
            print('Something went wrong!')


if __name__ == '__main__':
    dashboardStatus = DashboardStatus()
    dashboardStatus.run()
