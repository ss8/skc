from typing import Optional
from kubernetes import client, config
from kubernetes.client.models.extensions_v1beta1_ingress import ExtensionsV1beta1Ingress
from kubernetes.client.models.extensions_v1beta1_ingress_list import ExtensionsV1beta1IngressList
from kubernetes.client.models.v1_config_map import V1ConfigMap
from kubernetes.client.models.v1_deployment import V1Deployment
from kubernetes.client.models.v1_deployment_list import V1DeploymentList
from kubernetes.client.models.v1_service import V1Service
from kubernetes.client.models.v1_service_list import V1ServiceList
from kubernetes.client.rest import ApiException


# Provides method on top of K8sClient provided methods
class K8sClient:
    def __init__(self) -> None:
        '''
            Initialize the K8s Configuration
        '''
        config.load_config()

    def get_deployments(self, namespace: str = None) -> Optional[V1DeploymentList]:
        '''
            Returns the list of deployments along with their details.

            Args:
                namespace (str):    Namespace
                                    Optional
                                    If provided, returns the list of deployments of given namespace only

            Returns:
                V1DeploymentList:   List of all deployments with detailed information
                None:               In case there is some exception while fetching details
        '''

        if namespace is not None and not isinstance(namespace, str):
            print('Please provide a valid namespace value')
            return None

        v1 = client.AppsV1Api()
        if namespace is None:
            ret = v1.list_deployment_for_all_namespaces()
        else:
            ret = v1.list_namespaced_deployment(
                namespace=namespace)
        return ret

    def get_ingresses(self, namespace: str = None) -> Optional[ExtensionsV1beta1IngressList]:
        '''
            Returns the list of ingresses along with their details

            Args:
                namespace (str):    Namespace
                                    Optional
                                    If provided, returns the list of ingresses of given namespace only

            Returns:
                ExtensionsV1beta1IngressList:   List of all ingresses with their detailed information
                None:                           In case there is some exception while fetching details
        '''

        if namespace is not None and not isinstance(namespace, str):
            print('Please provide a valid namespace value')
            return None

        extensionv1 = client.ExtensionsV1beta1Api()

        if namespace is None:
            ret = extensionv1.list_ingress_for_all_namespaces()
        else:
            ret = extensionv1.list_namespaced_ingress(namespace)

        return ret

    def get_services(self, namespace: str = None) -> Optional[V1ServiceList]:
        '''
            Returns the list of services along with their details

            Args:
                namespace (str):    Namespace
                                    Optional
                                    If provided, returns the list of ingresses of given namespace only

            Returns:
            V1ServiceList:          List of all services with their detailed information
            None:                   In case there is some exception while fetching details
        '''

        if namespace is not None and not isinstance(namespace, str):
            print('Please provide a valid namespace value')
            return None

        v1 = client.CoreV1Api()

        if namespace is None:
            ret = v1.list_service_for_all_namespaces()
        else:
            ret = v1.list_namespaced_service(namespace)

        return ret

    def get_deployment(self, deployment: str, namespace: str) -> Optional[V1Deployment]:
        '''
            Returns the detailed information of a given deployment in a given namespace

            Args:
                deployment (str):   Name of deployment
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                V1Deployment:       Detailed information of given deployment
                None:               In case there is some exception while fetching details
        '''

        if not isinstance(deployment, str) or not isinstance(namespace, str):
            print("Please provide a value for deployment & namespace")
            return None

        appsV1 = client.AppsV1Api()

        try:
            return appsV1.read_namespaced_deployment(name=deployment, namespace=namespace)
        except ApiException as e:
            print("Error in getting details of {0} deployment from {1} namespace".format(
                deployment, namespace))
            print("Exception: {0}".format(e.reason))
            return None

    def get_ingress(self, ingress: str, namespace: str) -> Optional[ExtensionsV1beta1Ingress]:
        '''
            Returns the detailed information of a given ingress in a given namespace

            Args:
                ingress (str):      Name of ingress
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                ExtensionsV1beta1Ingress:       Detailed information of given ingress
                None:                           In case there is some exception while fetching details
        '''

        if not isinstance(ingress, str) or not isinstance(namespace, str):
            print('Please provide a value for service & namespace')
            return None

        extensionsV1Beta1 = client.ExtensionsV1beta1Api()

        try:
            return extensionsV1Beta1.read_namespaced_ingress(name=ingress, namespace=namespace)
        except ApiException as e:
            print("Error in getting details of {0} ingress from {1} namespace".format(
                ingress, namespace))
            print("Exception: {0}".format(e.reason))
            return None

    def get_service(self, service: str, namespace: str) -> Optional[V1Service]:
        '''
            Returns the detailed information of a given service in a given namespace

            Args:
                service (str):      Name of service
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                V1Service:          Detailed information of given service
                None:               In case there is some exception while fetching details
        '''

        if not isinstance(service, str) or not isinstance(namespace, str):
            print('Please provide a value for service & namespace')
            return None

        v1 = client.CoreV1Api()

        try:
            return v1.read_namespaced_service(name=service, namespace=namespace)
        except ApiException as e:
            print('Error in getting details of {0} service from {1} namespace'.format(
                service, namespace))
            print('Exception: {0}'.format(e.reason))
            return None

    def check_deployment_healthy(self, deployment: str, namespace: str) -> bool:
        '''
            Returns the whether the given deployment is healthy or not.
            This checks whether the number of required replicas is equal to number of ready replicas

            Args:
                deployment (str):   Name of deployment
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                True:               If deployment is healthy.
                False:              If deployment is unhelathy or if there was an exception
        '''

        if not isinstance(deployment, str) or not isinstance(namespace, str):
            print('Please provide a value for namespace & deployment')
            return False

        depl = self.get_deployment(deployment=deployment, namespace=namespace)

        if depl is None:
            return False

        if depl.spec.replicas == depl.status.ready_replicas:
            print("Deployment {0} is healthy".format(deployment))
            return True

        print('Deployment {0} is not healthy'.format(deployment))
        return False

    def check_ingress_ip(self, ingress: str, namespace: str) -> Optional[str]:
        '''
            Returns the IP given to the ingress(if any)
            If multiple IPs are present, this returns the first IP from the list.

            Args:
                ingress (str):      Name of ingress
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                str:                IP of the ingress
                None:               In case there is some exception while fetching details or IP is not present
        '''

        if not isinstance(ingress, str) or not isinstance(namespace, str):
            print('Please provide a namespace and an ingress')
            return None

        ing = self.get_ingress(ingress=ingress, namespace=namespace)

        if ing is None:
            return None

        if ing.status.load_balancer.ingress is None:
            print('No IP is assigned to {0} ingress in {1} yet'.format(
                ingress, namespace))
            return None

        return ing.status.load_balancer.ingress[0].ip

    def check_service_exists(self, service: str, namespace: str) -> bool:
        '''
            Checks whether a given service exists in the provided namespace.

            Args:
                service (str):      Name of service
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                True:               If service exists
                None:               If service doesn't exist or if there was some exception
        '''

        if not isinstance(service, str) or not isinstance(namespace, str):
            print('Please provide a value for namespace & service')
            return False

        serv = self.get_service(service=service, namespace=namespace)

        if serv is None:
            return False

        print('{0} service exists in {1} namespace'.format(service, namespace))
        return True

    def get_service_nodeport(self, service: str, namespace: str, portName: str) -> Optional[str]:
        '''
            Returns the nodeport(if exists) of a given service.

            Args:
                service (str):      Name of service
                                    Required
                namespace (str):    Namespace
                                    Required
                portName (str):     Name of the port
                                    Required

            Returns:
                str:                NodePort of the given service's port
                None:               In case there is some exception while fetching details
        '''

        if not isinstance(service, str) or not isinstance(namespace, str) or not isinstance(portName, str) or not self.check_service_exists(namespace=namespace, service=service):
            print("Please provide a valid namesapce, service & name of port")
            return None

        res = self.get_service(service=service, namespace=namespace)

        if res is None:
            return None

        if res.spec.type != "NodePort":
            print('Provided service is not of type NodePort')
            print('Please provide a valid NodePort service')
            return None

        for port in res.spec.ports:
            if port.name == portName:
                return port.node_port

        print('Port {0} is not present for {1} service in {2} namespace'.format(
            portName, service, namespace))
        return None

    def get_configmap(self, configmap: str, namespace: str) -> Optional[V1ConfigMap]:
        '''
            Returns the detailed information of a given configmap in a given namespace

            Args:
                configmap (str):    Name of configmap
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                V1ConfigMap:        Detailed information of given configmap
                None:               In case there is some exception while fetching details
        '''

        if not isinstance(configmap, str) or not isinstance(namespace, str):
            print('Please provide a valid configmap and namespace')
            return None

        v1 = client.CoreV1Api()

        try:
            return v1.read_namespaced_config_map(name=configmap, namespace=namespace)
        except ApiException as e:
            print('Error in getting details of {0} configmap from {1} namespace'.format(
                configmap, namespace))
            print('Exception: {0}'.format(e.reason))
            return None

    def check_configmap_exists(self, configmap: str, namespace: str) -> bool:
        '''
            Checks whether a given configmap exists in the provided namespace.

            Args:
                configmap (str):      Name of configmap
                                    Required
                namespace (str):    Namespace
                                    Required

            Returns:
                True:               If configmap exists
                None:               If configmap doesn't exist or if there was some exception
        '''
        if not isinstance(configmap, str) or not isinstance(namespace, str):
            print('Please provide a valid configmap and namespace')
            return False

        config = self.get_configmap(configmap=configmap, namespace=namespace)

        if config is None:
            return False

        print('{0} configmap exists in {1} namespace'.format(
            configmap, namespace))
        return True

    def modify_configmap(self, configmap: str, namespace: str, data: V1ConfigMap) -> None:
        '''
            Patches/Modifies an existing configmap in given namespace

            Args:
                configmap (str):        Name of configmap
                                        Required
                namespace (str):        Namespace
                                        Required
                data (V1ConfigMap):     Body of configmap
                                        Required

            Returns:
                None
        '''

        if not isinstance(configmap, str) or not isinstance(namespace, str) or not isinstance(data, V1ConfigMap):
            print('Please provide a valid configmap, namespace and data')
            return None

        if not self.check_configmap_exists(configmap=configmap, namespace=namespace):
            print('Error in modifying details of {0} configmap from {1} namespace'.format(
                configmap, namespace))
            return None

        v1 = client.CoreV1Api()

        try:
            v1.patch_namespaced_config_map(
                name=configmap, namespace=namespace, body=data)
            print('{0} configmap in {1} namespace patched successfully'.format(
                configmap, namespace))
        except ApiException as e:
            print('Error in modifying details of {0} configmap from {1} namespace'.format(
                configmap, namespace))
            print('Exception: {0}'.format(e.reason))
            return None
