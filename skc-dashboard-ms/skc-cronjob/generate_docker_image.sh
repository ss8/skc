#!/bin/bash

set -o pipefail -e

WORKING_DIR=$(cd $(dirname $0); pwd)

IMAGE_NAME="ss8/skc-cronjob"
IMAGE_TAG="latest"

helpfunction() {
  echo ""
	echo "Usage: $0 [-t tag]"
	echo "Get Help: $0 -h|--help"
	echo -e "\t -t|--tag Image TAG"
	echo -e "\t          (Default: latest)"
	echo -e "\t -h|--help Print this help or usage message"
}

ARGS=$(getopt -a --options t:h --long "tag:,help" -- "$@")
 
eval set -- "$ARGS"
 
while true; do
  case "$1" in
	-t|--tag)
	  IMAGE_TAG="$2"
	  echo "Tag: $IMAGE_TAG"
	  shift 2;;
  -h|--help)
    helpfunction
    exit
    break;;
  --)
    # exit
    break;;
  esac
done

main() {
  docker build -t ${IMAGE_NAME}:${IMAGE_TAG} -f ${WORKING_DIR}/Dockerfile ${WORKING_DIR} > /dev/null
  echo ${IMAGE_NAME}:${IMAGE_TAG}
}

main

set +o pipefail +e