#!/bin/bash

# This script should be run on all controller/master nodes after successful dashboard installation

set -e

SCRIPTS_DIR=$(cd $(dirname $0); pwd)
source ${SCRIPTS_DIR}/lib_dbrd.sh

helpfunction() {
	echo ""
	echo "Usage: $0 -i skc_ip"
	echo "Get Help: $0 -h|--help"
	echo -e "\t -i|--ip \t SKC backend IP"
	echo -e "\t -h|--help \t Help or usage"
	# exit 1
}

main() {
	# _info "Task: Create Virtual Environment - CVE"
	# sh ${SCRIPTS_DIR}/create_venv.sh
	# _info "Task: Add .bashrc Entry - ABE"
	# sh ${SCRIPTS_DIR}/modify_bashrc.sh add SKC_IP "$1" -b
	_info "Task: Add Cronjob - AC"
	sh ${SCRIPTS_DIR}/modify_cronjob.sh add
}

ARGS=$(getopt -a --options i:h --long "ip:,help" -- "$@")
 
eval set -- "$ARGS"
 
while true; do
  case "$1" in
	-i|--ip)
      skc_ip="$2"
      echo "SKC IP: $skc_ip"
      shift 2;;
    -h|--help)
      helpfunction
	  exit
      break;;
    --)
	#   exit
      break;;
  esac
done

if [ -z "$skc_ip" ]; then
	helpfunction >&2
	exit 1
fi

_info "Starting post-installation script..."
main $skc_ip
_info "Post-installation script successful!"

set +e