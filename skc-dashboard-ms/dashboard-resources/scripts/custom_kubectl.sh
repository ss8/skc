#!/bin/bash

helpfunction() {
	echo ""
	echo "Usage: $0 <apply|delete> <yaml_path>"
	echo ""
	echo "Get Help: $0 -h|--help"
	echo -e "\t -h|--help \t Help or usage"
}

ARGS=$(getopt -a --options h --long "help" -- "$@")

eval set -- "$ARGS"
 
while true; do
  case "$1" in
    -h|--help)
      helpfunction
	  exit
      break;;
    --)
	#   exit
      break;;
  esac
done

if [[ "$#" -ne 3 ]]; then
	echo "Some or all of the required inputs are not provided" >&2
	helpfunction
	exit 1
fi


if [[ "$2" != "apply" ]] && [[ "$2" != "delete" ]]; then
	echo "Only apply and delete methods are available" >&2
	exit 1
fi

if ! [[ -e "$3" ]]; then
	echo "Provided YAML file doesn't exist" >&2
	exit 1
fi

if [[ "${3: -4}" != ".yml" ]] && [[ "${3: -5}" != ".yaml" ]]; then
	echo "Please provide a YAML file (.yaml or .yaml)" >&2
	exit 1
fi

eval "cat <<EOF
$(<$3)
EOF
" | kubectl $2 -f -