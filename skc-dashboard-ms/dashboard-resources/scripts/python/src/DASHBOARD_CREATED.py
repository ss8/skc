from inspect import getsourcefile
from pathlib import Path
import os
import json
import requests
from dotenv import load_dotenv

import utils

load_dotenv()

# Getting running script dir path
cwd = os.path.dirname(os.path.realpath(getsourcefile(lambda: 0)))

# Creating data directory inside python directory if it does not exist
Path(os.path.join(cwd, 'data')).mkdir(parents=True, exist_ok=True)


# Function to prepare payload to be sent as a request
def prepare_request_payload(ingress_url, ingress_port, dashboard_status, dashboard_id=0, dashboard_name="", cluster_type=""):
    request_payload = {
        "name": dashboard_name,
        "url": "https://" + ingress_url + ":" + str(ingress_port),
        "clusterType": cluster_type,
        "dashboardStatus": dashboard_status
    }

    return request_payload


# Function to prepare the json to be stored for future use
def prepare_save_data(dashbaord_id, dashboard_url, dashboard_status, error=""):
    response_data = {
        "id": dashbaord_id,
        "url": dashboard_url,
        "dashboardStatus": dashboard_status,
        "error": error
    }

    return response_data


# Defining skc endpoint
SKC_LOGIN_URL = os.getenv("SKC_LOGIN_URL")
SKC_ADD_URL = os.getenv("SKC_ADD_URL")
EVENT_HEADER = os.getenv("EVENT_HEADER")
EVENT_DASHBOARD_CREATED = os.getenv("EVENT_DASHBOARD_CREATED")

# with open(cwd + '/data/ingressport.txt') as ingress_port_file:
#     ingress_port = ingress_port_file.read()

# with open(cwd + '/data/ingressurl.txt') as ingress_url_file:
#     ingress_url = ingress_url_file.read()

login_token = utils.skc_login(url=SKC_LOGIN_URL, username=os.getenv(
    "SKC_USERNAME"), password=os.getenv("SKC_PASSWORD"))

if login_token == -1:
    print("Username or password is incorrect")
    exit(1)
elif login_token == -2:
    print("An error occured while logging into SKC")
    exit(1)

ingress_url = utils.check_ingress_status(
    namespace='kubernetes-dashboard', ingress_name='dashboard-ingress')
ingress_port = utils.get_ingress_controller_port(
    namespace='ingress-nginx', svc='ingress-nginx-controller', protocol='https')


request_payload = prepare_request_payload(ingress_url=ingress_url, ingress_port=ingress_port,
                                          dashboard_status="DASHBOARD_ACTIVE", dashboard_name=os.getenv("CLUSTER_NAME"), cluster_type=os.getenv("CLUSTER_TYPE"))

request_header = {
    "Authorization": "Bearer " + login_token,
    EVENT_HEADER: EVENT_DASHBOARD_CREATED
}

try:
    r = requests.post(url=SKC_ADD_URL, headers=request_header,
                      json=request_payload, verify=False)
except Exception as e:
    exit(1)


if(r.status_code == 200):
    data = r.json()

    dashboardId = data['add_data']['dashboardDetails']['id']
    dashboardUrl = data['add_data']['dashboardDetails']['url']
    dashboardStatus = data['add_data']['dashboardDetails']['dashboardStatus']
    error = ""

    save_data_json = prepare_save_data(
        dashbaord_id=dashboardId, dashboard_url=dashboardUrl, dashboard_status=dashboardStatus)

    with open(os.path.join(cwd, 'data', 'dashboard_details.json'), 'w') as json_file:
        json.dump(save_data_json, json_file)

    SKC_USER_ADD_URL = os.getenv("SKC_USER_ADD_URL")

    sa = os.getenv("SKC_SA")
    sa_token = utils.get_sa_token(namespace='kubernetes-dashboard', sa=sa)

    if sa_token == False:
        print("There was some error while getting sa token for {}".format(sa))
        exit(1)

    user_payload = {
        "username": sa,
        "token": sa_token,
        "dashboardId": dashboardId
    }

    user_headers = {
        "Authorization": "Bearer " + login_token
    }

    try:
        r = requests.post(
            url=SKC_USER_ADD_URL, headers=user_headers, json=user_payload, verify=False)
        if r.status_code == 200:
            print('SA token for service account {} successfully added to SKC'.format(sa))
        else:
            print("There was some error while adding SA token to SKC")
            exit(1)
    except Exception as e:
        print("There was some error while adding SA token to SKC")
        exit(1)
else:
    print("Something went wrong!")
    print("Response Code: {}".format(r.status_code))
    print("Data: {}".format(r.json()))
    exit(1)
