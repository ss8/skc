from inspect import getsourcefile
import os
import sys
import json
import requests
from dotenv import load_dotenv

import utils

load_dotenv()

# Getting running script dir path
cwd = os.path.dirname(os.path.realpath(getsourcefile(lambda: 0)))

# Defining skc endpoint
SKC_LOGIN_URL = os.getenv("SKC_LOGIN_URL")
SKC_URL = os.getenv("SKC_HEALTH_URL")
EVENT_HEADER = os.getenv("EVENT_HEADER")

ingress_port = ""
ingress_url = ""
dashboard_details = {}


def get_dashboard_status():
    is_Controller_Up = utils.check_deployment_status(
        namespace='ingress-nginx', deployment_name='ingress-nginx-controller')
    is_Dashboard_Up = utils.check_deployment_status(
        namespace='kubernetes-dashboard', deployment_name='kubernetes-dashboard')
    is_Metrics_Scraper_Up = utils.check_deployment_status(
        namespace='kubernetes-dashboard', deployment_name='dashboard-metrics-scraper')
    is_Metrics_Server_Up = utils.check_deployment_status(
        namespace='kube-system', deployment_name='metrics-server')

    if is_Controller_Up and is_Dashboard_Up and is_Metrics_Scraper_Up and is_Metrics_Server_Up:
        return "DASHBOARD_ACTIVE"
    else:
        return "DASHBOARD_INACTIVE"


login_token = utils.skc_login(url=SKC_LOGIN_URL, username=os.getenv(
    "SKC_USERNAME"), password=os.getenv("SKC_PASSWORD"))

if login_token == -1:
    print("Username or password is incorrect")
    exit(1)
elif login_token == -2:
    print("An error occured while logging into SKC")
    exit(1)

# with open(cwd + '/data/ingressport.txt') as ingress_port_file:
#     ingress_port = ingress_port_file.read()

# with open(cwd + '/data/ingressurl.txt') as ingress_url_file:
#     ingress_url = ingress_url_file.read()

with open(os.path.join(cwd, 'data', 'dashboard_details.json')) as json_file:
    dashboard_details = json.load(json_file)

ingress_url = utils.check_ingress_status(
    namespace='kubernetes-dashboard', ingress_name='dashboard-ingress')
ingress_port = utils.get_ingress_controller_port(
    namespace='ingress-nginx', svc='ingress-nginx-controller', protocol='https')


if type(ingress_url) == bool or type(ingress_port) == bool:
    EVENT_DASHBOARD = EVENT_DASHBOARD = os.getenv("EVENT_DASHBOARD_STATUS")
else:
    if dashboard_details['url'] != "https://" + ingress_url + ":" + str(ingress_port):
        EVENT_DASHBOARD = os.getenv("EVENT_DASHBOARD_MODIFIED")
        dashboard_details['url'] = "https://" + \
            ingress_url + ":" + str(ingress_port)
    else:
        EVENT_DASHBOARD = os.getenv("EVENT_DASHBOARD_STATUS")

dashboard_details['dashboardStatus'] = get_dashboard_status()

headers = {
    "Authorization": "Bearer " + login_token,
    EVENT_HEADER: EVENT_DASHBOARD
}

try:
    r = requests.post(url=SKC_URL, headers=headers,
                      json=dashboard_details, verify=False)
except Exception as e:
    exit(1)

if r.status_code == 200:
    # data = r.json()

    with open(os.path.join(cwd, 'data', 'dashboard_details.json'), 'w') as json_file:
        json.dump(dashboard_details, json_file)
