# Custom python script to get K8s resource details using kubectl

import subprocess
import json
import requests


kubectl = '/usr/local/bin/kubectl'


# Function to check if a namespace exists
def check_namespace_exists(namespace):
    proc = subprocess.Popen(args=[kubectl, 'get', 'ns', '-o', 'json'],
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    ns_list_json = json.loads(stdout.decode('utf8').replace("'", '"'))

    for ns in ns_list_json['items']:
        if ns['metadata']['name'] == namespace:
            return True

    return False


# Function to check if a deployment exists in a given namespace
def check_deployment_exists(namespace, deployment_name):
    if not check_namespace_exists(namespace=namespace):
        return False

    proc = subprocess.Popen(args=[kubectl, 'get', 'deployments', '-n',
                            namespace, '-o', 'json'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    deployment_list_json = json.loads(stdout.decode('utf-8').replace("'", '"'))

    for deployment in deployment_list_json['items']:
        if deployment['metadata']['name'] == deployment_name:
            return True

    return False


# Function to check if deployment has desired number of replicas available
def check_deployment_status(namespace, deployment_name):
    if not check_deployment_exists(namespace=namespace, deployment_name=deployment_name):
        return False

    proc = subprocess.Popen(args=[kubectl, 'get', 'deployment', '-n', namespace, deployment_name, '-o', 'json'],
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    deployment_json = json.loads(stdout.decode('utf-8').replace("'", '"'))

    try:
        if deployment_json['spec']['replicas'] == deployment_json['status']['availableReplicas']:
            return True
    except Exception as e:
        return False

    return False


# Check if an inggress exists in a given namespace
def check_ingress_exists(namespace, ingress_name):
    if not check_namespace_exists(namespace=namespace):
        return False

    proc = subprocess.Popen(args=[kubectl, 'get', 'ingress', '-n',
                                  namespace, '-o', 'json'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    ingress_list_json = json.loads(stdout.decode('utf-8').replace("'", '"'))

    for ingress in ingress_list_json['items']:
        if ingress['metadata']['name'] == ingress_name:
            return True

    return False


# TODO Complete checking ingress status function
# Check if an ingress is active
def check_ingress_status(namespace, ingress_name):
    if not check_ingress_exists(namespace=namespace, ingress_name=ingress_name):
        return False

    proc = subprocess.Popen(args=[kubectl, 'get', 'ingress', '-n', namespace, ingress_name, '-o', 'json'],
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    ingress_json = json.loads(stdout.decode('utf-8').replace("'", '"'))

    if ingress_json['status']['loadBalancer']['ingress'][0]['ip']:
        return ingress_json['status']['loadBalancer']['ingress'][0]['ip']

    return False


def check_sa_exists(namespace, sa):
    if not check_namespace_exists(namespace=namespace):
        return False

    proc = subprocess.Popen(args=[kubectl, 'get', 'sa', '-n', namespace,
                            '-o', 'json'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    sa_list_json = json.loads(stdout.decode('utf-8').replace("'", '"'))

    for tempsa in sa_list_json['items']:
        if tempsa['metadata']['name'] == sa:
            return True

    return False


# Returns token of a service account
def get_sa_token(namespace, sa):
    if not check_sa_exists(namespace=namespace, sa=sa):
        return False

    proc = subprocess.Popen(args=[kubectl, '-n', namespace, 'get', 'sa/' + sa, '-o',
                            'jsonpath="{.secrets[0].name}"'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    sa_token_name = stdout.decode('utf-8').replace('"', '')

    proc = subprocess.Popen(args=[kubectl, 'get', 'secret', '-n', namespace, sa_token_name, '-o',
                            'go-template="{{.data.token | base64decode}}"'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    return stdout.decode('utf-8').replace('"', '')


def check_service_exists(namespace, svc):
    if not check_namespace_exists(namespace=namespace):
        return False

    proc = subprocess.Popen(args=[kubectl, '-n', namespace, 'get', 'svc',
                            '-o', 'json'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    svc_json_list = json.loads(stdout.decode('utf-8').replace("'", '"'))

    for svc_json in svc_json_list['items']:
        if svc_json['metadata']['name'] == svc:
            return True

    return False


def get_ingress_controller_port(namespace, svc, protocol):
    if not check_service_exists(namespace=namespace, svc=svc):
        return False

    proc = subprocess.Popen(args=[kubectl, 'get', 'svc', '-n', namespace, svc, '-o', 'json'],
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    stdout, stderr = proc.communicate()

    ingress_svc_json = json.loads(stdout.decode('utf-8').replace("'", '"'))

    for port in ingress_svc_json['spec']['ports']:
        if port['name'] == protocol:
            return port['nodePort']

    return -1


# Function to login to skc and return JWT after successfull login
# Takes 3 parameters:
# url: skc login endpoint
# username: skc username
# password: skc password
# Return Type:
# If successfull login, return token as str
# If username or password is incorrect, returns -1
# If an exception occurs, returns -2
# type: (str, str, str) -> (str | Literal[-1, -2])
def skc_login(url, username, password):
    login_payload = {
        'username': username,
        'password': password
    }

    try:
        r = requests.post(url=url, json=login_payload, verify=False)
        if r.status_code == 200:
            print('{} successfully logged in'.format(username))
            return r.headers['Authorization'].replace('Bearer ', '')
        else:
            return -1
    except Exception as e:
        return -2
