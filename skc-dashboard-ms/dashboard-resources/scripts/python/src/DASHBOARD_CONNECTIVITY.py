import os
import requests
from dotenv import load_dotenv

load_dotenv()

SKC_URL = os.getenv('SKC_ADD_URL')

try:
    res = requests.get(SKC_URL, verify=False)
    # if(res.status_code != 403):
    print("Request to SKC successfully made")
    # exit(res.status_code)
except Exception as e:
    print("Unable to connect to SKC")
    exit(1)
