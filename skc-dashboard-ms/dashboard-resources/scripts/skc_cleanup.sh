#!/bin/sh

WORKING_DIR=$(cd $(dirname ${0}); pwd)
kubectl="/usr/local/bin/kubectl"

dbrd_status_script="dbrd_status_script.sh"

controllernode_label="on-control-plane"

bashrc_variables="SKC_IP"
bashrc_banner_text="# Do not remove below line. It is used by SKC and will be removed with SKC."

skc_docker_images_file="${WORKING_DIR}/../images_list.txt"


get_nodes() {
	${kubectl} get nodes -o wide | egrep "${1}" | awk '{print $1}'
}

get_terminating_namespaces() {
	${kubectl} get namespaces -o wide | egrep "Terminating" | awk '{print $1}'
}

remove_terminating_namespaces() {
	# TODO test & handle if namespace stuck in terminating state using skc- prefixed namespace

	local terminating_namespaces=$(get_terminating_namespaces)

	echo -e "\nRemoving namespaces created by SKC stuck in Terminating state..."

	if [ -z "${terminating_namespaces}" ]; then
		echo -e "No such namespaces found.."
	fi

	for namespace in $terminating_namespaces; do
		if [[ $(kubectl get namespace ${namespace} -o jsonpath="{.metadata.annotations.created-by}") == "SKC" ]]; then
			echo "Deleting ${namespace} forcefully..."
			${kubectl} get namespace "${namespace}" -o json \
  				| tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/" \
  				| kubectl replace --raw /api/v1/namespaces/${namespace}/finalize -f -
		fi
	done
}

remove_k8s_resources() {
	echo -e "\n\tRemoving K8s resources..."

	remove_terminating_namespaces

	sleep 5

	local yamldir=${WORKING_DIR}/../yamls

	for yaml in ${yamldir}/*.yaml; do
		if [[ ! "${yaml}" == *metrics-server* ]]; then
			echo -e "\nRemoving ${yaml}"
			${kubectl} delete -f ${yaml} --ignore-not-found --timeout="150s"
		fi
	done

	local met_op=$(kubectl get deployment -n kube-system metrics-server -o jsonpath="{.metadata.annotations.created-by}" 2>/dev/null)

	if [[ "${met_op}" == "SKC" ]]; then
		for yaml in ${yamldir}/*.yaml; do
			if [[ "${yaml}" == *metrics-server* ]]; then
				echo -e "\nRemoving ${yaml}"
				${kubectl} delete -f "${yaml}" --ignore-not-found --timeout="150s"
			fi
		done
	fi

	sleep 5

	remove_terminating_namespaces

	sleep 5
	
	echo -e "\n\t\t Done!"
}

remove_controllernode_labels() {
	echo -e "\n\tRemoving controller node labels..."
	local nodes=$(get_nodes "controller|master")

	for node in $nodes; do
		${kubectl} label nodes "${node}" "${controllernode_label}"-
	done
	echo -e "\n\t\t Done!"
}

unload_docker_images() {
	echo -e "\n\tUnloading/Removing docker images..."
	for line in $(cat ${skc_docker_images_file}); do
		echo "${line}"
		docker rmi "${line}"
	done
	echo -e "\n\t\t Done!"
}

remove_cronjob() {
	echo -e "\n\tRemove cronjob..."
	local cronl="/tmp/cronl"
	crontab -l > ${cronl}
	sed -i "/${dbrd_status_script}/d" "${cronl}"
	crontab ${cronl}
	rm -f ${cronl}
	echo -e "\n\t\t Done!"
}

remove_bashrc_entry() {
	echo -e "\n\tRemove Bashrc entry..."
	for variable in $bashrc_variables; do
		local bashrc_content=$(grep -w "${variable}" ~/.bashrc)
		if [[ ${#bashrc_content} -ne 0 ]]; then
			sed -i "/\b${variable}\b/d" ~/.bashrc
		fi
	done

	sed -i "/${bashrc_banner_text}/d" ~/.bashrc
	echo -e "\n\t\t Done!"
}

main() {
	remove_k8s_resources
	remove_controllernode_labels
	unload_docker_images
	remove_cronjob
	remove_bashrc_entry
}

main