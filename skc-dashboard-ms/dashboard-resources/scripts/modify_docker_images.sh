#!/bin/bash

SCRIPTS_DIR=$(cd $(dirname $0); pwd)
# load lib file
source ${SCRIPTS_DIR}/lib_dbrd.sh

# This function loads all docker images from tar files in a folder.
# It takes 1 argument
# $1: Path to docker images folder
load_docker_images()
{
    local imagedir=${1}

    echo "Loading docker images related to k8s dashboard"
    tar -xzvf "${imagedir}.tgz" --directory ${WORKING_DIR}
    
    for file in ${imagedir}/*.tar; do
        docker image load < ${file}
    done

    rm -rf ${imagedir}

    _info "Done: ${FUNCNAME[0]}"
}

# This function removes docker images listed in given folder.
unload_docker_images()
{
    # local imagedir=${1}

    # tar -xzvf "${imagedir}.tgz" --directory ${WORKING_DIR}

    # for image in ${imagedir}/*.tar; do 
    #     echo ${image} | sed -r "s#(.*)_#\1:#g" |sed "s#_#\/#g" \
    # 	    | sed "s#.tar##"| sed "s#${imagedir}##"| xargs docker rmi;
    # done

    # rm -rf ${imagedir}

    local skc_docker_images_file="${SCRIPTS_DIR}/../images_list.txt"

    echo -e "\n\tUnloading/Removing docker images..."
	for line in $(cat ${skc_docker_images_file}); do
		echo "${line}"
		docker rmi "${line}"
	done
	echo -e "\n\t\t Done!"

    _info "Done: ${FUNCNAME[0]}"
}


if [[ $# -ne 1 ]]; then
	echo "Plaese provide load/unload as a parameter"
	exit 1
fi

if [[ "$1" == "load" ]]; then
	load_docker_images ${DIMGDIR}
fi

if [[ "$1" == "unload" ]]; then
	unload_docker_images ${DIMGDIR}
fi
