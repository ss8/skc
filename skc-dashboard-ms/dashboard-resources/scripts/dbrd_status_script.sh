# !/bin/bash

echo "Dashbord Status Script Start at: $(date)" >> /tmp/skc

source ~/.bashrc

SCRIPTS_DIR=$(cd $(dirname $0); pwd)

source ${SCRIPTS_DIR}/python/env/bin/activate
python3 ${SCRIPTS_DIR}/python/src/DASHBOARD_STATUS.py >> /tmp/skc
deactivate

echo "Dashboard Status Script End at: $(date)" >> /tmp/skc