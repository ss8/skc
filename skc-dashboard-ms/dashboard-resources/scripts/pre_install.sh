#!/bin/bash

# This script should be run on all controller/master nodes before dashboard installation starts

set -e

SCRIPTS_DIR=$(cd $(dirname $0); pwd)
source ${SCRIPTS_DIR}/lib_dbrd.sh

main() {
	_info "Task: Docker Images Load - DIL"
	sh ${SCRIPTS_DIR}/modify_docker_images.sh load
}

_info "Starting pre-installation script..."
main
_info "Pre-installation script successful!"

set -e