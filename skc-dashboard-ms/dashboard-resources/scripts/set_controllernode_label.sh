#!/bin/bash

# A script to set/unset a custom label on each controller/master node of a K8s Cluster.

set -e

SCRIPTS_DIR=$(cd $(dirname $0); pwd)
# load lib file
source ${SCRIPTS_DIR}/lib_dbrd.sh

NODE_LABEL_KEY="on-control-plane"
NODE_LABEL_VALUE="true"

help() {
	echo "Help Message"
}

get_node_names() {
	# Using kubectl get nodes to get controller/master node details & then extracting only node names
	# ${kubectl} get nodes -o wide | egrep "control-plane|master" | awk '{print $1}'
	${kubectl} get nodes -o wide | egrep "$1" | awk '{print $1}'
}

set_node_label() {
	# Take node, label key & value as input & apply it using kubectl label ...
	${kubectl} label nodes $1 $2=$3 --overwrite
}

unset_node_label() {
	${kubectl} label nodes $1 $2-
}

add_label() {
	_info "Adding Label:- ${NODE_LABEL_KEY}:${NODE_LABEL_VALUE} on all controller/master nodes..."

	local nodes=$(get_node_names "controller|master")
	echo "Nodes found:- $nodes"

	for node in $nodes; do
		echo "Adding label on Node:- $node ..."
		set_node_label $node $NODE_LABEL_KEY $NODE_LABEL_VALUE
		# echo "Applied!"
	done

	_info "Label added successfully on all controller/master nodes!"
}

remove_label() {
	_info "Removing Label:- ${NODE_LABEL_KEY}:${NODE_LABEL_VALUE} from all controller/master nodes..."

	local nodes=$(get_node_names "controller|master")
	echo "Nodes found:- $nodes"

	for node in $nodes; do
		echo "Removing label from Node:- $node ..."
		unset_node_label $node $NODE_LABEL_KEY
		# echo "Applied!"
	done

	_info "Label removed successfully from all controller/master nodes!"
}

main() {
	if [[ $1 == "add" ]]; then
		add_label
	fi
	if [[ $1 == "remove" ]]; then
		remove_label
	fi
}

main $1

set +e