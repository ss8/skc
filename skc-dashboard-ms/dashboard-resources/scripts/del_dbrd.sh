#!/bin/bash

set -o pipefail -e

SCRIPTS_DIR=$(cd $(dirname $0); pwd)
# load lib file
source ${SCRIPTS_DIR}/lib_dbrd.sh


# Sanity check for existence of k8s dashboard
stop_if_dbrd_not_exist()
{
    set +e 
    is_dbrd_exist 
    [[ $? -eq ${notexist} ]] && 
           echo "kubernetes-dashboard does not exists" &&
           exit 0
    set -e
    _info "Done: ${FUNCNAME[0]}"
}

delete_sa() {
    export admin_sa=${2}
    sh ${SCRIPTS_DIR}/custom_kubectl.sh delete ${1}
}

delete_ingress() {
    sh ${SCRIPTS_DIR}/custom_kubectl.sh delete ${1}
}

unconfigure_sa_dbrd()
{
    local yamldir=${1}
    local kdelete="${kubectl} delete -f "
    echo "Removing dashboard deployment"

    # 4.1 Remove metrics-server deployment if it was created by SKC
	if [[ $(kubectl get deployment -n kube-system metrics-server -o jsonpath="{.metadata.annotations.created-by}") == "SKC" ]]; then
		_info "Removing Metrics Server"
		${kdelete} ${yamldir}/04-metrics-server.yaml
	fi

    # 4.2 Deploy Dashboard Ingress
    _info "Removing ingress for dashboard"
    delete_ingress ${yamldir}/03-dashboard-ingress.yaml admin

    # 4.3 Deploy skc-admin service account
    _info "Removing skc-admin Service Account"
    delete_sa ${yamldir}/02-skc-admin-sa.yaml admin

    # 4.4 Deploy Dashboard
    _info "Removing Dashboard"
    ${kdelete} ${yamldir}/01-dashboard.yaml --timeout="180s"

    # 4.5 Deploy Ingress Controller
    _info "Removing NGINX Ingress Controller"
    ${kdelete} ${yamldir}/00-controller-bare-metal.yaml --timeout="180s"

    echo "Done: ${FUNCNAME[0]}"
}


main()
{
    local yamldir="${WORKING_DIR}/yamls"
    # Step 1: Check whether k8s dashboard is exist
    stop_if_dbrd_not_exist

    # Step 2: Deploy YAML files
    unconfigure_sa_dbrd ${yamldir}

    # Step 3: Remove docker images
    sh ${SCRIPTS_DIR}/modify_docker_images.sh unload

    # Step 4: Remove cronjob 
    sh ${SCRIPTS_DIR}/modify_cronjob.sh remove
    
    # Step 5: Remove SKC entries from .bashrc
    # sh ${SCRIPTS_DIR}/modify_bashrc.sh remove SKC_IP -b

    # Step 6: Removing custom label added set on all controller/master nodes during installation
    sh ${SCRIPTS_DIR}/set_controllernode_label.sh remove
}
 
main

set +o pipefail +e
