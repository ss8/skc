#!/bin/bash

set -e

SCRIPTS_DIR=$(cd $(dirname $0); pwd)
# load lib file
source ${SCRIPTS_DIR}/lib_dbrd.sh

PYTHON_DIR_NAME="python"
PYTHON_DIR_PATH="${SCRIPTS_DIR}/${PYTHON_DIR_NAME}"
WHEELS_DIR_NAME="wheels"
WHEELS_DIR_PATH="${PYTHON_DIR_PATH}/${WHEELS_DIR_NAME}"
ENV_DIR_NAME="env"
ENV_DIR_PATH="${PYTHON_DIR_PATH}/${ENV_DIR_NAME}"
REQUIREMENTS_FILE_NAME="requirements.txt"
REQUIREMENTS_FILE_PATH="${PYTHON_DIR_PATH}/${REQUIREMENTS_FILE_NAME}"

create_venv()
{
    local pydir="${PYTHON_DIR_PATH}"
    local envdir="${ENV_DIR_PATH}"
    local wheelsdir="${WHEELS_DIR_PATH}"
	local reqfile="${REQUIREMENTS_FILE_PATH}"
    pip3 install $(grep -i "virtualenv" ${reqfile}) \
    	--no-index --find-links ${pydir}/wheels --user

    [[ -d ${envdir} ]] && rm -rf ${envdir}
    python3 -m venv ${envdir}

    source ${envdir}/bin/activate
    pip3 install -r ${reqfile} --no-index --find-links ${wheelsdir}
}

main() {
    create_venv
}

_info "Creating a python virtual env named:- ${ENV_DIR_NAME}..."
main
_info "Successfully created virtual env ${ENV_DIR_NAME}!"

set +e