#!/bin/bash

set -o pipefail -e

helpfunction() {
	echo ""
	echo "Usage: $0 add <variable_name> <variable_value> [-b|--banner]"
	echo "Usage: $0 remove <variable_name> [-b|--banner]"
	echo ""
	echo "Get Help: $0 -h|--help"
	echo -e "\t -b|--banner \t Banner text"
	echo -e "\t -h|--help \t Help or usage"
}

ARGS=$(getopt -a --options bh --long "banner,help" -- "$@")

eval set -- "$ARGS"

banner_text="# Do not remove below line. It is used by SKC and will be removed with SKC."
is_banner=0
 
while true; do
  case "$1" in
	-b|--banner)
		is_banner=1
		echo "Banner selected"
		shift;;
    -h|--help)
      helpfunction
	  exit
      break;;
    --)
	#   exit
      break;;
  esac
done

SCRIPTS_DIR=$(cd $(dirname $0); pwd)

source ${SCRIPTS_DIR}/lib_dbrd.sh

add_bashrc_entry() {
	local bashrc_content=$(grep -w ${1} ~/.bashrc) 
	
	if [[ ${#bashrc_content} -ne 0 ]]; then
		echo "Following line/s were present in bashrc and are now removed"
		echo ${bashrc_content}
		sed -i "/\b${1}\b/d" ~/.bashrc
	fi
	
	if [[ ${is_banner} -eq 1 ]]; then
		echo ${banner_text} >> ~/.bashrc
	fi
	
	echo "export ${1}=${2}" >> ~/.bashrc
	
	 _info "Done: ${FUNCNAME[0]}"
}

# Function te remove lines from .bashrc containing provided pattern
remove_bashrc_entry() {
	echo "Removing lines from .bashrc containing ${1}"
	
	local bashrc_content=$(grep -w ${1} ~/.bashrc) 
	
	# sed -i "/\b${1}\b/d" ~/.bashrc

	if [[ ${#bashrc_content} -ne 0 ]]; then
		echo "Following line/s were present in bashrc and are now removed"
		echo ${bashrc_content}
		sed -i "/\b${1}\b/d" ~/.bashrc
	fi
	
	if [[ ${is_banner} -eq 1 ]]; then
		echo "Removing banner text"
		sed -i "/${banner_text}/d" ~/.bashrc
	fi
	
	 _info "Done: ${FUNCNAME[0]}"
}

if [[ $# < 2 ]]; then
	echo "Please provide add/remove method"
	helpfunction
	exit 1
fi

if [[ "$2" != "add" ]] && [[ "$2" != "remove" ]]; then
	echo "Only add and remove methods are available" >&2
	helpfunction
	exit 1
fi

if [[ "$2" == "add" ]]; then
	if [[ -z "$3" ]] || [[ -z "$4" ]]; then
		echo "Please provide variable name and it's value as arguments after add" >&2
		helpfunction
		exit 1
	fi
	add_bashrc_entry "$3" "$4"
fi

if [[ "$2" == "remove" ]]; then
	if [[ -z "$3" ]]; then
		echo "Please provide variable name to be removed as an argument after remove" >&2
		helpfunction
		exit 1
	fi
	remove_bashrc_entry "$3"
fi

set +o pipefail +e