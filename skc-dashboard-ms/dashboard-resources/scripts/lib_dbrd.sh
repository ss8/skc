#!/bin/bash
## Dashboard library: common functions for k8s dashboard scripts

# $$8 $$8 $$8 Variable Section 8$$ 8$$ 8$$
WORKING_DIR="${SCRIPTS_DIR}/.."
DIMGDIR="${WORKING_DIR}/docker-images"
kubectl="/usr/local/bin/kubectl"
status_script="dbrd_status_script.sh"
DASHBOARD_NAMESPACE="kubernetes-dashboard"
DASHBOARD_DEPLOYMENT_NAME="kubernetes-dashboard"
DEPLOYMENT_STATUS_TIMEOUT="30s"

INGRESS_CONTROLELR_NAMESPACE="ingress-nginx"
INGRESS_CONTROLLER_DEPLOYMENT_NAME="ingress-nginx-controller"
KRS="${kubectl} rollout status"
INGRESS_CONTROLLER_ROLLOUT_STATUS_CMD="${KRS} deployment/${INGRESS_CONTROLLER_DEPLOYMENT_NAME}
                -n ${INGRESS_CONTROLELR_NAMESPACE} --timeout=${DEPLOYMENT_STATUS_TIMEOUT}"

DASHBOARD_METRICS_SCRAPER_DEPLOYMENT_NAME="dashboard-metrics-scraper"
DASHBOARD_ROLLOUT_STATUS_CMD="${KRS} deployment/${DASHBOARD_DEPLOYMENT_NAME}
                -n ${DASHBOARD_NAMESPACE} --timeout=${DEPLOYMENT_STATUS_TIMEOUT}"
DASHBOARD_METRICS_SCRAPER_ROLLOUT_STATUS_CMD="${KRS} deployment/${DASHBOARD_METRICS_SCRAPER_DEPLOYMENT_NAME}
                -n ${DASHBOARD_NAMESPACE} --timeout=${DEPLOYMENT_STATUS_TIMEOUT}"

METRICS_SERVER_NAMESPACE="kube-system"
METRICS_SERVER_DEPLOYMENT_NAME="metrics-server"
METRICS_SERVER_ROLLOUT_STATUS_CMD="${KRS} deployment/${METRICS_SERVER_DEPLOYMENT_NAME}
                -n ${METRICS_SERVER_NAMESPACE} --timeout=${DEPLOYMENT_STATUS_TIMEOUT}"

MAX_ATTEMPTS=5
SLEEP_TIMER=10

exist=0 notexist=1

## Decide existence of bashboard based on kubernetes deployment
# Return value: 
# 1: if dashboard not exist
# 0: if dashboard exist
is_dbrd_exist()
{
    # if kubectl not present, can't check dashboard existence. Return not exist(1)
        [[ ! -x ${kubectl} ]] && return ${notexist}

    ${kubectl} get ns | grep ${DASHBOARD_NAMESPACE}
    # no namespace found means no dashboard present
    [[ $? -eq 1 ]] && return ${notexist}

    local kgd_db="${kubectl} get deployment -n ${DASHBOARD_NAMESPACE} ${DASHBOARD_DEPLOYMENT_NAME}"
    local dbrd_avrc=$(${kgd_db} -o jsonpath='{.status.availableReplicas}')
    local dbrd_sprc=$(${kgd_db} -o jsonpath='{.spec.replicas}')

     # Dashbords is healthy if availableReplicas equals specified replicas    
    [[ ${dbrd_avrc} -eq ${dbrd_sprc} ]] && return ${exist}

    return ${notexist};    ## dashboard not exist
}

_info(){
    local _done="--------------"
    echo -e "\n${_done} $1 ${_done}\n"
    echo -e "${_done} On: `date` ${_done}\n"
}

## End lib_dbrd.sh 
