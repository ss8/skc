#!/bin/bash
### The script is configuring k8s dashboard k8s resources
set -o pipefail -e

# Declaring variables
SCRIPTS_DIR=$(cd $(dirname $0); pwd)
source ${SCRIPTS_DIR}/lib_dbrd.sh

helpfunction() {
	echo ""
	echo "Usage: $0 -n cluster_name -t cluster_type -i skc_ip"
	echo "Get Help: $0 -h|--help"
	echo -e "\t -n|--name \t Name to be given to cluster"
	echo -e "\t -t|--type \t Type of cluster (AWS, GCP, AZU, ON_PREM)"
	echo -e "\t -i|--ip \t SKC backend IP"
	echo -e "\t -h|--help \t Help or usage"
	# exit 1
}

ARGS=$(getopt -a --options n:t:i:h --long "name:,type:,ip:,help" -- "$@")
 
eval set -- "$ARGS"
 
while true; do
  case "$1" in
    -n|--name)
      cluster_name="$2"
      echo "Name: $cluster_name"
      shift 2;;
    -i|--ip)
      skc_ip="$2"
      echo "SKC IP: $skc_ip"
      shift 2;;
	-t|--type)
	  cluster_type="$2"
	  echo "Type: $cluster_type"
	  shift 2;;
    -h|--help)
      helpfunction
	  exit
      break;;
    --)
	#   exit
      break;;
  esac
done

if [ -z "$cluster_type" ] || [ -z "$cluster_name" ] || [ -z "$skc_ip" ]; then
	helpfunction
	exit 1
fi

export cluster_name=$cluster_name
export cluster_type=$cluster_type
export SKC_IP=$skc_ip


# Step 1: Sanity check if dashboard already exists
stop_if_dbrd_exist()
{
    local pydir=${1}
    local envdir=${2}

    set +e 
    is_dbrd_exist 
    [[ $? -eq ${exist} ]] && 
           _info "kubernetes-dashboard is already exists" &&
           exit 2
    set -e

    # Stop if SKC communication failed
    sh ${SCRIPTS_DIR}/create_venv.sh
    connect_to_skc ${envdir} "DASHBOARD_CONNECTIVITY"

}


# Function to call custom_kubectl.sh to apply dynamic sa.yaml
# $1: Path to create sa yaml
# $2: Name of sa
apply_sa() {
    echo "Exporting service account name: ${2}"
    export admin_sa=${2}
    echo "Creating ${2} service account"
    sh ${SCRIPTS_DIR}/custom_kubectl.sh apply ${1}
}

# Function to call custom_kubectl.sh to apply dynamic ingress.yaml
# $1: Path to create ingress yaml
# $2: Name of sa whose token needs to be used in Authorization header
apply_ingress() {
    echo "Getting the ${2} service account token"
    export sa_token=$(sh ${SCRIPTS_DIR}/extract_token.sh ${2})
    echo "Creating ingress with ${2} service account token"
    sh ${SCRIPTS_DIR}/custom_kubectl.sh apply ${1}
}


# Step 4- Deploy YAML files
configure_sa_dbrd()
{
    local yamldir="${1}"
    local kapply="${kubectl} apply -f"
    echo "Starting dashboard deployment"
    # 4.1 Deploy Ingress Controller
    echo "Deploying NGINX Ingress Controller"
    ${kapply} ${yamldir}/00-controller-bare-metal.yaml
    
    # Watching Ingress Controller deployment
    ATTEMPTS=0
    until $INGRESS_CONTROLLER_ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq $MAX_ATTEMPTS ]; do
        $INGRESS_CONTROLLER_ROLLOUT_STATUS_CMD
        ATTEMPTS=$((ATTEMPTS + 1))
        sleep $SLEEP_TIMER
    done
    
    _info "NGINX Ingress Controller deployed"
    
    # 4.2 Deploy Dashboard
    echo "Deploying kubernetes dashboard"
    ${kapply} ${yamldir}/01-dashboard.yaml
    
    # Watching dashboard deployment
    ATTEMPTS=0
    until $DASHBOARD_ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq $MAX_ATTEMPTS ]; do
        $DASHBOARD_ROLLOUT_STATUS_CMD
        ATTEMPTS=$((ATTEMPTS + 1))
        sleep $SLEEP_TIMER
    done
    
    ATTEMPTS=0
    until $DASHBOARD_METRICS_SCRAPER_ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq $MAX_ATTEMPTS ]; do
        $DASHBOARD_METRICS_SCRAPER_ROLLOUT_STATUS_CMD
        ATTEMPTS=$((ATTEMPTS + 1))
        sleep $SLEEP_TIMER
    done
    _info "Kubernetes dashboard deployed"
    
    # 4.3 Deploy skc-admin service account
    echo "Creating skc-admin Service Account"
    apply_sa ${yamldir}/02-skc-admin-sa.yaml admin
    _info "skc-admin Service Account created"

    # 4.4 Deploy Dashboard Ingress
    echo "Configuring an ingress for dashboard"
    apply_ingress ${yamldir}/03-dashboard-ingress.yaml admin
    _info "An ingress for dashboard configured"
    
    # 4.5 Deploy metrics-server deployment
	# Only if metrics-server is not already present
	
	set +e
	kubectl get deployments -n ${METRICS_SERVER_NAMESPACE} ${METRICS_SERVER_DEPLOYMENT_NAME}
	ret=$?
	if [[ ${ret} -ne 1 ]]; then
		_info "Metrics Server already exists"
	else
		echo "Deploying Metrics Server"
		${kapply} ${yamldir}/04-metrics-server.yaml
		
		# Watching metrics-server deployment
		ATTEMPTS=0
		until $METRICS_SERVER_ROLLOUT_STATUS_CMD || [ $ATTEMPTS -eq $MAX_ATTEMPTS ]; do
			$METRICS_SERVER_ROLLOUT_STATUS_CMD
			ATTEMPTS=$(( ATTEMPTS + 1 ))
			sleep $SLEEP_TIMER
		done
		
		_info "Metrics Server deployed by SKC"
	fi
	
	
	set -e
	
	# Wait for dashboard ingress to get an IP
    # sleep 15
    until [[ ! -z $(kubectl get ingress -n kubernetes-dashboard dashboard-ingress -o jsonpath="{.status.loadBalancer.ingress[].ip}") ]]; do
    	echo "Waiting for Ingress to assign an IP for K8s Dashboard... "
    	sleep $SLEEP_TIMER
    done
    _info "Done: ${FUNCNAME[0]}"
    
}

## Report the dashboard status to SKC UI
connect_to_skc()
{
    local envdir=${1}
    local pydir="${envdir}/.."
    local event=${2}

    source ${envdir}/bin/activate
    set +e
    python3 "${pydir}/src/${event}.py"
    ret=$?
    set -e
    deactivate

    if [ ${ret} -ne 0 ] ; then
       echo "Resolve SKC issues for event ${event} and retry"
       exit 1
    fi
    _info "Done: ${FUNCNAME[0]}"
}


main()
{
    local pydir="${SCRIPTS_DIR}/python"
    local envdir="${pydir}/env"
    local yamldir="${WORKING_DIR}/yamls"

    # Step 1: Do not create if dashboard resources already present
    stop_if_dbrd_exist ${pydir} ${envdir}
	
	# Step 2: Add SKC_IP entry in bashrc
    # sh ${SCRIPTS_DIR}/modify_bashrc.sh add SKC_IP ${skc_ip} -b
    
    # Step 3: Load required docker images
    sh ${SCRIPTS_DIR}/modify_docker_images.sh load

    # Step 4: Add a custom label on all controller/master nodes
    sh ${SCRIPTS_DIR}/set_controllernode_label.sh add
    
    # Step 5: Deploy ingress controller and k8s dashboard and metrics server etc
    configure_sa_dbrd ${yamldir}
    
    # Step 6: Call SKC backend /add endpoint to add dashboard entry to database
    connect_to_skc ${envdir} "DASHBOARD_CREATED"
    
    # Step 7: Set status script to run every 1 min
    sh ${SCRIPTS_DIR}/modify_cronjob.sh add
}

## main function
main
set +o pipefail +e
### End add script
