#!/bin/bash

SCRIPTS_DIR=$(cd $(dirname $0); pwd)

source ${SCRIPTS_DIR}/lib_dbrd.sh

custom_kubectl=${SCRIPTS_DIR}/custom_kubectl.sh

cronjob_namespace=kubernetes-dashboard
cronjob_yaml="${WORKING_DIR}/yamls/skc-cronjob.yaml"
dashboard_details_configmap=dashboard-details-config
dashboard_details_configmap_annotations="created-by=SKC"
dashboard_details_configmap_labels="app=skc-cronjob"
dashboard_details_file_path="${SCRIPTS_DIR}/python/src/data/dashboard_details.json"

# This function takes a cronjob and adds it to user cronfile
# It takes 1 argument
# $1: cronjob to be added
add_cronjob()
{
    # echo "Assigning correct permissions to ${status_script}"
	# chmod 755 ${SCRIPTS_DIR}/${status_script}

    # echo "Setting cronjob"
    # local cronl="/tmp/cronl"
    # set +e
    # #cronentry=$1
    # #echo $cronentry
    # # backup exisiting entries
    # crontab -l > ${cronl}
    # # append new entry
    # echo "*/1 * * * * ${SCRIPTS_DIR}/${status_script}" >> ${cronl}
    # # Reload entries
    # crontab ${cronl}  
    # rm -f ${cronl}
    # set -e

    # New Cronjob
    # Create configmap for dashboard-details
    ${kubectl} create configmap -n ${cronjob_namespace} ${dashboard_details_configmap} --from-file=${dashboard_details_file_path}
    # Assign annoataions and labels to the configmap
    ${kubectl} annotate configmap -n ${cronjob_namespace} ${dashboard_details_configmap} ${dashboard_details_configmap_annotations}
    ${kubectl} label configmap -n ${cronjob_namespace} ${dashboard_details_configmap} ${dashboard_details_configmap_labels}
    # Export SKC_IP to the environment (alrady exported to env by add_dbrd.sh)
    # Apply skc-cronjob.yaml to create cronjob
    sh ${custom_kubectl} apply ${cronjob_yaml}
    _info "Done: ${FUNCNAME[0]}"
}

# This function takes removes a cronjob for current user
remove_cronjob() {
    # echo "Removing cronjob"
    # local cronl="/tmp/cronl"
    # crontab -l > ${cronl}
    # sed -i "/${status_script}/d" ${cronl}
    # crontab ${cronl}
    # rm -f ${cronl}

    # New Cronjob
    # Remove cronjob from k8s
    ${kubectl} delete -f ${cronjob_yaml}
    ${kubectl} delete configmap -n ${cronjob_namespace} ${dashboard_details_configmap}
    _info "Done: ${FUNCNAME[0]}"
}

if [[ $# -ne 1 ]]; then
	echo "Plaese provide add/remove as a parameter"
	exit 1
fi

if [[ "$1" == "add" ]]; then
	add_cronjob
fi

if [[ "$1" == "remove" ]]; then
	remove_cronjob
fi