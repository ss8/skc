#!/bin/bash

# This script pulls docker images from internet, saves ut in .tar files & combines them in a compressed .tgz

set -o pipefail -e

SCRIPT_DIR=$(cd $(dirname $0); pwd)

source ${SCRIPT_DIR}/lib_var.sh

DIRECTORY_NAME="docker-images"
TAR_NAME="${DIRECTORY_NAME}.tgz"
SAVE_LOCATION=${SCRIPT_DIR}
# DOCKER_IMAGES_LIST_FILE="${SCRIPT_DIR}/images_list.txt"
DOCKER_IMAGES_LIST_FILE="${IMAGES_LIST_FILE}"

helpfunction() {
	echo ""
	echo "Usage: $0 [-s save_location] [-d temp_dir_name] [-t tar_name] [-i path_to_images_list_file]"
	echo "Get Help: $0 -h|--help"
	echo -e "\t -s|--save Path of directory where tgz should be created"
	echo -e "\t           (Default path of directory containing this script)"
	echo -e "\t -d|--dir Name of folder to be created during tar generation"
	echo -e "\t          (Default docker-images)"
	echo -e "\t -t|--tar Name of TAR to be generated. It should contain .tar.gz or .tgz"
	echo -e "\t          (Default docker-images.tgz)"
	echo -e "\t -i|--images Path to text file containing list of required docker images"
	echo -e "\t             (Default images_list.txt in same directory as this script)"
	echo -e "\t -h|--help Print this help or usage message"
}

ARGS=$(getopt -a --options s:d:t:i:h --long "save:,dir:,tar:,images:,help" -- "$@")
 
eval set -- "$ARGS"
 
while true; do
  case "$1" in
    -s|--save)
      SAVE_LOCATION="$2"
      echo "Save Location: $SAVE_LOCATION"
      shift 2;;
    -d|--dir)
      DIRECTORY_NAME="$2"
	  	TAR_NAME="${DIRECTORY_NAME}.tgz"
      echo "Directory Name: $DIRECTORY_NAME"
      shift 2;;
		-t|--tar)
			TAR_NAME="$2"
			echo "Tar Name: $TAR_NAME"
			shift 2;;
		-i|--images)
			DOCKER_IMAGES_LIST_FILE="$2"
			echo "Images list location: $DOCKER_IMAGES_LIST_FILE"
			shift 2;;
		-h|--help)
			helpfunction
	  	exit
      break;;
    --)
			# exit
      break;;
  esac
done

if ! [[ -e "$DOCKER_IMAGES_LIST_FILE" ]]; then
	echo "Please provide correct path to images list text file " >&2
	exit 1
fi

if [[ "${DOCKER_IMAGES_LIST_FILE: -4}" != ".txt" ]]; then
	echo "Please provide a valid .txt file for images list" >&2
	exit 1
fi

if [[ "${TAR_NAME: -4}" != ".tgz" ]] && [[ "${TAR_NAME: -7}" != ".tar.gz" ]]; then
	echo "Please provide a valid file extension (.tar.gz or .tgz) in tar name" >&2
	exit 1
fi

create_dir() {
	mkdir -p $1
}

gen_filename() {
	echo ${1} | sed "s#/#_#g" | sed "s#:#_#g" | sed 's/$/.tar/'
}

main() {

	# Step 1: Create a temporary directory to save all .tar into
	echo ""
	echo "Creating a temporary directory ${DIRECTORY_NAME} in ${SAVE_LOCATION}"
	create_dir "${SAVE_LOCATION}/${DIRECTORY_NAME}"

	# Step 2: Pull all images using text file from DOCKER_IMAGES_LIST_FILE variable
	echo ""
	echo "Pulling all images from ${DOCKER_IMAGES_LIST_FILE} file"
	for image_name in $(cat ${DOCKER_IMAGES_LIST_FILE}); do
		echo ""
		
		set +e
		docker images --filter=reference=${image_name} | awk '{print $1":"$2}' | egrep ${image_name}
		local ret=$?
		if [ ${ret} -ne 0 ] ; then
			echo "Pulling Image: ${image_name}"
			docker pull ${image_name}
		else
			echo "Found ${image_name} locally"
		fi
		set -e
		
	done
	
	# Step 3: Save all required docker images in .tar files inside directory from DIRECTORY_NAME variable
	echo ""
	echo "Saving all required docker images into ${SAVE_LOCATION}/${DIRECTORY_NAME} in .tar files"
	for image_name in $(cat ${DOCKER_IMAGES_LIST_FILE}); do
		filename=$(gen_filename ${image_name})
		docker save ${image_name} > ${SAVE_LOCATION}/${DIRECTORY_NAME}/${filename}
		echo ""
		echo "Saved ${image_name} in ${filename}"
	done

	# Step 4: Compress temporary directory into a tgz file
	echo ""
	echo "Compressing ${DIRECTORY_NAME} into ${TAR_NAME}"
	tar -C ${SAVE_LOCATION} -cvzf ${SAVE_LOCATION}/${TAR_NAME} ${DIRECTORY_NAME}
	
	# Step 5: Delete generated temporary directory to free up space
	echo ""
	echo "Removing temporary directory: ${DIRECTORY_NAME}"
	rm -rf ${SAVE_LOCATION}/${DIRECTORY_NAME}
}

main

set +o pipefail +e

