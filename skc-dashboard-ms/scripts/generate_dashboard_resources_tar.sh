#!/bin/bash

# This script generates a dashboard-resources.tgz file using dashboard-resources directory.

set -o pipefail -e

SCRIPT_DIR=$(cd $(dirname $0); pwd)

source ${SCRIPT_DIR}/lib_var.sh

DASHBOARD_DIRECTORY_NAME="dashboard-resources"
DASHBOARD_DIRECTORY_LOCATION="${SCRIPT_DIR}/.."
ADD_DASHBOARD_TAR_NAME="dashboard-resources.tgz"
DELETE_DASHBOARD_TAR_NAME="delete-dashboard-resources.tgz"
DASHBOARD_SAVE_LOCATION="${SCRIPT_DIR}/../skc-resources"

IS_DOCKER_DIRECTORY_PRESENT=0
DOCKER_IMAGES_TAR="docker-images.tgz"
DOCKER_IMAGES_TAR_LOCATION="${SCRIPT_DIR}"
GENERATE_DOCKER_IMAGES_TAR="generate_docker_images_tar.sh"
GENERATE_DOCKER_IMAGES_TAR_LOCATION="${SCRIPT_DIR}"

create_save_directory() {
	mkdir -p ${DASHBOARD_SAVE_LOCATION}
}

generate_cronjob_image() {
	sh ${SCRIPT_DIR}/add_cronjob_to_image_list.sh
}

copy_skc_resources() {
	cp ${SCRIPT_DIR}/../skc-cronjob/infra/k8s/skc-cronjob.yaml ${DASHBOARD_RESOURCES_DIRECTORY}/yamls
}

create_docker_images_tar() {
	#Check if docker-images.tgz is present
	echo ""
	echo "Checking if ${DOCKER_IMAGES_TAR} is present.."
	if [[ -e ${DOCKER_IMAGES_TAR_LOCATION}/${DOCKER_IMAGES_TAR} ]]; then
		IS_DOCKER_DIRECTORY_PRESENT=1
	fi

	# If no, generate docker images tar using generate_docker_images_tar.sh
	if [[ ${IS_DOCKER_DIRECTORY_PRESENT} == 0 ]]; then
		echo ""
		echo "${DOCKER_IMAGES_TAR} not found at ${DOCKER_IMAGES_TAR_LOCATION}"
		echo "Generating ${DOCKER_IMAGES_TAR} at ${DASHBOARD_DIRECTORY_LOCATION}/${DASHBOARD_DIRECTORY_NAME}"
		sh ${GENERATE_DOCKER_IMAGES_TAR_LOCATION}/${GENERATE_DOCKER_IMAGES_TAR} -s ${DASHBOARD_DIRECTORY_LOCATION}/${DASHBOARD_DIRECTORY_NAME}
	else
	# If yes, copy docker-images.tgz to dashboard-resources directory
		echo ""
		echo "Found ${DOCKER_IMAGES_TAR} at ${DOCKER_IMAGES_TAR_LOCATION}"
		echo "Copying ${DOCKER_IMAGES_TAR} from ${DOCKER_IMAGES_TAR_LOCATION} to ${DASHBOARD_DIRECTORY_LOCATION}/${DASHBOARD_DIRECTORY_NAME}"
		cp ${DOCKER_IMAGES_TAR_LOCATION}/${DOCKER_IMAGES_TAR} ${DASHBOARD_DIRECTORY_LOCATION}/${DASHBOARD_DIRECTORY_NAME}
	fi
}

create_add_dashboard_resources_tar() {
	create_docker_images_tar

	# Compress dashboard-resources directory into add-dashboard-resources.tgz
	echo ""
	echo "Generating ${ADD_DASHBOARD_TAR_NAME} at ${DASHBOARD_SAVE_LOCATION}"
	tar -C ${DASHBOARD_DIRECTORY_LOCATION} -cvzf ${DASHBOARD_SAVE_LOCATION}/${ADD_DASHBOARD_TAR_NAME} ${DASHBOARD_DIRECTORY_NAME}
}

create_delete_dashboard_resources_tar() {
	# Compress dashboard-resources directory into delete-dashboard-resources.tgz

	# Remove docker-images.tgz if exists
	# if [ ! -z ${DASHBOARD_DIRECTORY_LOCATION}/${DASHBOARD_DIRECTORY_NAME}/ ]

	echo ""
	echo "Generating ${DELETE_DASHBOARD_TAR_NAME} at ${DASHBOARD_SAVE_LOCATION}"
	tar -C ${DASHBOARD_DIRECTORY_LOCATION} -cvzf ${DASHBOARD_SAVE_LOCATION}/${DELETE_DASHBOARD_TAR_NAME} ${DASHBOARD_DIRECTORY_NAME}
}

main() {
	# Create & Add cronjob image to images_list.txt
	generate_cronjob_image
	copy_skc_resources
	cp ${SCRIPT_DIR}/../images_list.txt ${DASHBOARD_DIRECTORY_LOCATION}/${DASHBOARD_DIRECTORY_NAME}
	create_save_directory
	create_delete_dashboard_resources_tar
	create_add_dashboard_resources_tar
}

main

set +o pipefail +e

