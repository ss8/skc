SCRIPT_DIR=$(cd $(dirname $0); pwd)

IMAGES_LIST_FILE="${SCRIPT_DIR}/../images_list.txt"

DASHBOARD_RESOURCES_DIRECTORY="${SCRIPT_DIR}/../dashboard-resources"
DASHBOARD_RESOURCES_SAVE_DIRECTORY="${SCRIPT_DIR}/../skc-resources"

DOCKER_IMAGES_TAR="${DASHBOARD_RESOURCES_DIRECTORY}/docker-images.tgz"
ADD_DASHBOARD_TAR="${DASHBOARD_RESOURCES_SAVE_DIRECTORY}/add-dashboard-resources.tgz"
DELETE_DASHBOARD_TAR="${DASHBOARD_RESOURCES_SAVE_DIRECTORY}/delete-dashboard-resources.tgz"

GENERATE_DOCKER_IMAGES_TAR="${SCRIPT_DIR}/generate_docker_images.sh"

SKC_CRONJOB_DOCKER_IMAGE="${SCRIPT_DIR}/../skc-cronjob/generate_docker_image.sh"

