#!/usr/bin/expect -f

set FILENAME1 [lindex $argv 0]
set USERNAME [lindex $argv 1]
set IP [lindex $argv 2]
set DESTPATH [lindex $argv 3]
set PASSWORD [lindex $argv 4]

# spawn scp $1 $2 $3@$4:$5
spawn scp -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null $FILENAME1 $USERNAME@$IP:$DESTPATH
expect_after eof {exit 0}
expect {
	"assword:" {
		# send -- "$6\r"
		# send -- "ss8inc\r"
		send -- "$PASSWORD\r"
	}
}
expect eof

