#!/bin/bash

set -o pipefail -e

SCRIPT_DIR=$(cd $(dirname $0); pwd)

source ${SCRIPT_DIR}/lib_var.sh

# Create skc-cronjob image and get the name of image
skc_image=$(sh ${SKC_CRONJOB_DOCKER_IMAGE})

# Check if skc-cronjob already exists in images_list.
# If yes, remove the previous image name
sed -i "/\bskc-cronjob\b/d" ${IMAGES_LIST_FILE}

echo -e "\n${skc_image}" >> ${IMAGES_LIST_FILE}

set +o pipefail +e